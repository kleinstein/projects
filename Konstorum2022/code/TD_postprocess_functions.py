# Functions for tensor decomposition post-processing of gene x sample x time data
# Anna Konstorum (anna.konstorum@yale.edu)

import os
import matplotlib.pyplot as plt
from matplotlib import cm
import seaborn as sns
import pandas as pd
import numpy as np


# Returns top-scoring component genes
############################################
def top_genes_out(comp, genes_ranked, ensg_symbol, save=False):
    results_dir = '/Users/akonsto/Box Sync/shaw_platelets/results/tensor/'
    os.chdir(results_dir)

    comp_num = int(comp[1]) - 1
    genes_q = genes_ranked[comp_num].quantile([0.05, 0.1, 0.9, 0.95])

    ## Top 10%
    genes_high = genes_ranked[comp_num][genes_ranked[comp_num]['c'] >= genes_q.iloc[2]['c']]
    # Get Gene Symbol
    genes_high['SYM'] = ensg_symbol.loc[list(genes_high.index)]['SYMBOL']
    # Save to file
    if save:
        filename = 'genes_plateletTD_high_' + comp + '.csv'
        genes_high.to_csv(filename, sep=',', header=True)

    return genes_high

# Plots component scores for all modes
############################################
def plot_comp(comp, samples_ranked, subjects_ranked, genes_ranked, study_M, hue_samples):
    if hue_samples == "Cohort":
        hue_samples_order = ["YHP", "BH", "Hark", "YCCI"]
        palette_samples = 'bright'
    elif hue_samples == "Group":
        hue_samples_order = ["Young", "Older (Comm)", "Older (SNF)"]
        palette_samples = 'Set2'
    elif hue_samples == "Biological sex":
        hue_samples_order = ["F", "M"]
        palette_samples = 'Dark2'
    elif hue_samples == "Response":
        hue_samples_order = ["Low", "High"]
        palette_samples = 'Set1'

    comp_num = int(comp[1:])
    merge = pd.concat([samples_ranked[comp_num - 1], subjects_ranked[comp_num - 1]], axis=1)

    merge_f = merge.copy()
    merge_f['Group'] = 'Young'
    merge_f['Group'][merge_f['Cohort'] == 'BH'] = 'Older (SNF)'
    merge_f['Group'][merge_f['Cohort'] == 'YHP'] = 'Older (Comm)'
    merge_f['Group'].value_counts()

    genes_q = genes_ranked[comp_num - 1].quantile([0.05, 0.1, 0.9, 0.95, 0.99])
    genes_q_99 = genes_q.iloc[4]['c']
    genes_q_95 = genes_q.iloc[3]['c']

    genes_top = genes_ranked[comp_num - 1][genes_ranked[comp_num - 1]['c'] >= genes_q_99]
    genes_high = genes_ranked[comp_num - 1][genes_ranked[comp_num - 1]['c'] < genes_q_99]
    genes_high = genes_high[genes_high['c'] >= genes_q_95]

    gr = genes_ranked[comp_num - 1]
    gr['Score'] = 'Medium'
    gr.loc[genes_top.index, 'Score'] = 'Top'
    gr.loc[genes_high.index, 'Score'] = "High"

    colors_score = ["Red", "Orange", "Green"]
    # Set your custom color palette
    customPalette = sns.set_palette(sns.color_palette(colors_score))

    fig = plt.figure(figsize=(10, 5))
    # fig = plt.figure()
    title_full = "Component " + comp[1:] + " outcomes"
    plt.suptitle(title_full, fontsize=20)

    ax1 = plt.subplot(121)
    ax2 = plt.subplot(122)

    ax1_set = sns.scatterplot(x=list(range(0, merge_f.shape[0])), y='c',
                              hue=hue_samples, hue_order=hue_samples_order,
                              data=merge_f, ax=ax1, palette=palette_samples, s=50)
    ax1_set.set_ylabel('Component score', fontsize=20)
    ax1_set.set_title('Subjects (sorted)', fontsize=20)
    ax1_set.set_yticklabels(np.round(ax1.get_yticks(), 2), size=20)
    ax1_set.set_xticklabels([])
    ax1_set.legend(fontsize=16)

    ax2_set = study_M.plot.bar(y=comp, ax=ax2, legend=False, rot=0, color='lightseagreen')
    ax2_set.set_title('Day', fontsize=25)
    ax2_set.set_xticklabels(['0', '2', '7', '28'], size=20)
    ax2_set.set_yticklabels(np.round(ax2.get_yticks(), 2), size=20)
    ax2_set.set_ylim([0, 1])

    plt.tight_layout()

# Visualizes gene expression of top scoring genes and subjects for a component
# Returns expression for subjects with high, medium, and low scores
#################################################
def vis_top(comp, genes_ranked, samples_ranked, ensg_symbol,subject_data_f,
            slice_day_0,slice_day_2,slice_day_7, slice_day_28):
    hue_samples_order = ["Young", "Older (Comm)", "Older (SNF)"]
    palette_samples = cm.get_cmap('Set2', 8)
    ps = [palette_samples(0), palette_samples(1), palette_samples(2)]
    age_color = dict(list(zip(hue_samples_order, ps)))

    comp_num = int(comp[1:])
    genes_high = top_genes_out(comp, genes_ranked, ensg_symbol)

    top_genes = list(genes_high.iloc[0:5].index)
    top_genes_S = genes_high.iloc[0:5]['SYM'].values

    top_samples = list(samples_ranked[comp_num - 1].iloc[0:5].index.values)
    top_samples2 = list(samples_ranked[comp_num - 1].iloc[19:23].index.values)
    bot_samples = list(samples_ranked[comp_num - 1].iloc[-1:-6:-1].index.values)

    all_samples = top_samples + top_samples2 + bot_samples

    sample_df = pd.DataFrame(index=all_samples, columns=['Group', 'color'])
    for sample in all_samples:
        sample_df.loc[sample, 'Group'] = subject_data_f.loc[sample, 'Group']
    for sample in all_samples:
        group = sample_df.loc[sample, 'Group']
        sample_df.loc[sample, 'color'] = age_color[group]

    df_all_top = []
    for gene in top_genes:
        df = pd.DataFrame(index=top_samples, columns=['0', '2', '7', '28'])
        for sample in top_samples:
            df.loc[sample, '0'] = slice_day_0.loc[gene, sample]
            df.loc[sample, '2'] = slice_day_2.loc[gene, sample]
            df.loc[sample, '7'] = slice_day_7.loc[gene, sample]
            df.loc[sample, '28'] = slice_day_28.loc[gene, sample]
        df.columns = pd.to_numeric(df.columns)
        df_all_top.append(df)

    df_all_top2 = []
    for gene in top_genes:
        df = pd.DataFrame(index=top_samples2, columns=['0', '2', '7', '28'])
        for sample in top_samples2:
            df.loc[sample, '0'] = slice_day_0.loc[gene, sample]
            df.loc[sample, '2'] = slice_day_2.loc[gene, sample]
            df.loc[sample, '7'] = slice_day_7.loc[gene, sample]
            df.loc[sample, '28'] = slice_day_28.loc[gene, sample]
        df.columns = pd.to_numeric(df.columns)
        df_all_top2.append(df)

    df_all_bot = []
    for gene in top_genes:
        df = pd.DataFrame(index=bot_samples, columns=['0', '2', '7', '28'])
        for sample in bot_samples:
            df.loc[sample, '0'] = slice_day_0.loc[gene, sample]
            df.loc[sample, '2'] = slice_day_2.loc[gene, sample]
            df.loc[sample, '7'] = slice_day_7.loc[gene, sample]
            df.loc[sample, '28'] = slice_day_28.loc[gene, sample]
        df.columns = pd.to_numeric(df.columns)
        df_all_bot.append(df)

    fig = plt.figure(figsize=(60, 20))

    ax1 = plt.subplot(351)
    ax2 = plt.subplot(352, sharey=ax1)
    ax3 = plt.subplot(353, sharey=ax1)
    ax4 = plt.subplot(354, sharey=ax1)
    ax5 = plt.subplot(355, sharey=ax1)

    ax6 = plt.subplot(356, sharey=ax1)
    ax7 = plt.subplot(357, sharey=ax1)
    ax8 = plt.subplot(358, sharey=ax1)
    ax9 = plt.subplot(359, sharey=ax1)
    ax10 = plt.subplot(3, 5, 10, sharey=ax1)

    ax11 = plt.subplot(3, 5, 11, sharey=ax1)
    ax12 = plt.subplot(3, 5, 12, sharey=ax1)
    ax13 = plt.subplot(3, 5, 13, sharey=ax1)
    ax14 = plt.subplot(3, 5, 14, sharey=ax1)
    ax15 = plt.subplot(3, 5, 15, sharey=ax1)

    ax1_set = df_all_top[0].transpose().plot(ax=ax1, legend=False, color=sample_df.loc[top_samples, 'color'])
    ax1_set.set_ylabel('log2(c+1)', fontsize=30)
    ax1_set.set_title(top_genes_S[0], fontsize=40)
    ax1_set.set_xticklabels([])
    ax1_set.tick_params(axis='y', labelsize=30)

    ax2_set = df_all_top[1].transpose().plot(ax=ax2, legend=False, color=sample_df.loc[top_samples, 'color'])
    ax2_set.set_title(top_genes_S[1], fontsize=40)
    ax2_set.set_xticklabels([])
    ax2_set.tick_params(axis='y', labelsize=30)

    ax3_set = df_all_top[2].transpose().plot(ax=ax3, legend=False, color=sample_df.loc[top_samples, 'color'])
    ax3_set.set_title(top_genes_S[2], fontsize=40)
    ax3_set.set_xticklabels([])
    ax3_set.tick_params(axis='y', labelsize=30)

    ax4_set = df_all_top[3].transpose().plot(ax=ax4, legend=False, color=sample_df.loc[top_samples, 'color'])
    ax4_set.set_title(top_genes_S[3], fontsize=40)
    ax4_set.set_xticklabels([])
    ax4_set.tick_params(axis='y', labelsize=30)

    ax5_set = df_all_top[4].transpose().plot(ax=ax5, legend=False, color=sample_df.loc[top_samples, 'color'])
    ax5_set.set_title(top_genes_S[4], fontsize=40)
    ax5_set.set_xticklabels([])
    ax5_set.tick_params(axis='y', labelsize=30)

    ax6_set = df_all_top2[0].transpose().plot(ax=ax6, legend=False, sharey=ax1, sharex=ax1,
                                              color=sample_df.loc[top_samples2, 'color'])
    ax6_set.set_ylabel('log_2(c+1)', fontsize=30)
    ax6_set.tick_params(axis='x', labelsize=30)
    ax6_set.tick_params(axis='y', labelsize=30)

    ax7_set = df_all_top2[1].transpose().plot(ax=ax7, legend=False, sharey=ax2, sharex=ax2,
                                              color=sample_df.loc[top_samples2, 'color'])
    ax7_set.tick_params(axis='x', labelsize=30)
    ax7_set.tick_params(axis='y', labelsize=30)

    ax8_set = df_all_top2[2].transpose().plot(ax=ax8, legend=False, sharey=ax3,
                                              color=sample_df.loc[top_samples2, 'color'])
    ax8_set.tick_params(axis='x', labelsize=30)
    ax8_set.tick_params(axis='y', labelsize=30)

    ax9_set = df_all_top2[3].transpose().plot(ax=ax9, legend=False, sharey=ax4,
                                              color=sample_df.loc[top_samples2, 'color'])
    ax9_set.tick_params(axis='x', labelsize=30)
    ax9_set.tick_params(axis='y', labelsize=30)

    ax10_set = df_all_top2[4].transpose().plot(ax=ax10, sharey=ax5, legend=False,
                                               color=sample_df.loc[top_samples2, 'color'])
    ax10_set.tick_params(axis='x', labelsize=30)
    ax10_set.tick_params(axis='y', labelsize=30)

    ax11_set = df_all_bot[0].transpose().plot(ax=ax11, legend=False, sharey=ax1, sharex=ax1,
                                              color=sample_df.loc[bot_samples, 'color'])
    ax11_set.set_ylabel('log2(c+1)', fontsize=30)
    ax11_set.set_xlabel('Day', fontsize=30)
    ax11_set.tick_params(axis='x', labelsize=30)
    ax11_set.tick_params(axis='y', labelsize=30)

    ax12_set = df_all_bot[1].transpose().plot(ax=ax12, legend=False, sharey=ax2, sharex=ax2,
                                              color=sample_df.loc[bot_samples, 'color'])
    ax12_set.tick_params(axis='x', labelsize=30)
    ax12_set.tick_params(axis='y', labelsize=30)
    ax12_set.set_xlabel('Day', fontsize=30)

    ax13_set = df_all_bot[2].transpose().plot(ax=ax13, legend=False, sharey=ax3,
                                              color=sample_df.loc[bot_samples, 'color'])
    ax13.tick_params(axis='x', labelsize=30)
    ax13.tick_params(axis='y', labelsize=30)
    ax13_set.set_xlabel('Day', fontsize=30)

    ax14_set = df_all_bot[3].transpose().plot(ax=ax14, legend=False, sharey=ax4,
                                              color=sample_df.loc[bot_samples, 'color'])
    ax14_set.tick_params(axis='x', labelsize=30)
    ax14_set.tick_params(axis='y', labelsize=30)
    ax14_set.set_xlabel('Day', fontsize=30)

    ax15_set = df_all_bot[4].transpose().plot(ax=ax15, sharey=ax5, legend=False,
                                              color=sample_df.loc[bot_samples, 'color'])
    ax15_set.tick_params(axis='x', labelsize=30)
    ax15_set.tick_params(axis='y', labelsize=30)
    ax15_set.set_xlabel('Day', fontsize=30)


plt.tight_layout()

# Visualizes gene expression of top scoring genes and subjects for a component 
###############################
def vis_top_1row(comp, genes_ranked, samples_ranked, ensg_symbol,subject_data_f,
                 slice_day_0,slice_day_2,slice_day_7, slice_day_28):
    hue_samples_order = ["Young", "Older (Comm)", "Older (SNF)"]
    palette_samples = cm.get_cmap('Set2', 8)
    ps = [palette_samples(0), palette_samples(1), palette_samples(2)]
    age_color = dict(list(zip(hue_samples_order, ps)))

    comp_num = int(comp[1:])
    genes_high = top_genes_out(comp, genes_ranked, ensg_symbol)

    top_genes = list(genes_high.iloc[0:5].index)
    top_genes_S = genes_high.iloc[0:5]['SYM'].values

    top_samples = list(samples_ranked[comp_num - 1].iloc[0:5].index.values)
    top_samples2 = list(samples_ranked[comp_num - 1].iloc[19:23].index.values)
    bot_samples = list(samples_ranked[comp_num - 1].iloc[-1:-6:-1].index.values)

    all_samples = top_samples + top_samples2 + bot_samples

    sample_df = pd.DataFrame(index=all_samples, columns=['Group', 'color'])
    for sample in all_samples:
        sample_df.loc[sample, 'Age_group'] = subject_data_f.loc[sample, 'Group']
    for sample in all_samples:
        group = sample_df.loc[sample, 'Age_group']
        sample_df.loc[sample, 'color'] = age_color[group]

    df_all_top = []
    for gene in top_genes:
        df = pd.DataFrame(index=top_samples, columns=['0', '2', '7', '28'])
        for sample in top_samples:
            df.loc[sample, '0'] = slice_day_0.loc[gene, sample]
            df.loc[sample, '2'] = slice_day_2.loc[gene, sample]
            df.loc[sample, '7'] = slice_day_7.loc[gene, sample]
            df.loc[sample, '28'] = slice_day_28.loc[gene, sample]
        df.columns = pd.to_numeric(df.columns)
        df_all_top.append(df)

    df_all_top2 = []
    for gene in top_genes:
        df = pd.DataFrame(index=top_samples2, columns=['0', '2', '7', '28'])
        for sample in top_samples2:
            df.loc[sample, '0'] = slice_day_0.loc[gene, sample]
            df.loc[sample, '2'] = slice_day_2.loc[gene, sample]
            df.loc[sample, '7'] = slice_day_7.loc[gene, sample]
            df.loc[sample, '28'] = slice_day_28.loc[gene, sample]
        df.columns = pd.to_numeric(df.columns)
        df_all_top2.append(df)

    df_all_bot = []
    for gene in top_genes:
        df = pd.DataFrame(index=bot_samples, columns=['0', '2', '7', '28'])
        for sample in bot_samples:
            df.loc[sample, '0'] = slice_day_0.loc[gene, sample]
            df.loc[sample, '2'] = slice_day_2.loc[gene, sample]
            df.loc[sample, '7'] = slice_day_7.loc[gene, sample]
            df.loc[sample, '28'] = slice_day_28.loc[gene, sample]
        df.columns = pd.to_numeric(df.columns)
        df_all_bot.append(df)

    fig = plt.figure(figsize=(30, 10))

    ax1 = plt.subplot(351)
    ax2 = plt.subplot(352, sharey=ax1)
    ax3 = plt.subplot(353, sharey=ax1)
    ax4 = plt.subplot(354, sharey=ax1)
    ax5 = plt.subplot(355, sharey=ax1)

    ax1_set = df_all_top[0].transpose().plot(ax=ax1, legend=False, color=sample_df.loc[top_samples, 'color'])
    ax1_set.set_ylabel('log2(c+1)', fontsize=20)
    ax1_set.set_title(top_genes_S[0], fontsize=20)
    ax1_set.tick_params(axis='y', labelsize=20)
    ax1_set.set_xlabel('Day', fontsize=20)
    plt.xticks([0,2,7,28])

    ax2_set = df_all_top[1].transpose().plot(ax=ax2, legend=False, color=sample_df.loc[top_samples, 'color'])
    ax2_set.set_title(top_genes_S[1], fontsize=20)
    ax2_set.tick_params(axis='y', labelsize=20)
    ax2_set.set_xlabel('Day', fontsize=20)

    ax3_set = df_all_top[2].transpose().plot(ax=ax3, legend=False, color=sample_df.loc[top_samples, 'color'])
    ax3_set.set_title(top_genes_S[2], fontsize=20)
    ax3_set.tick_params(axis='y', labelsize=20)
    ax3_set.set_xlabel('Day', fontsize=20)

    ax4_set = df_all_top[3].transpose().plot(ax=ax4, legend=False, color=sample_df.loc[top_samples, 'color'])
    ax4_set.set_title(top_genes_S[3], fontsize=20)
    ax4_set.tick_params(axis='y', labelsize=20)
    ax4_set.set_xlabel('Day', fontsize=20)

    ax5_set = df_all_top[4].transpose().plot(ax=ax5, legend=False, color=sample_df.loc[top_samples, 'color'])
    ax5_set.set_title(top_genes_S[4], fontsize=20)
    ax5_set.tick_params(axis='y', labelsize=20)
    ax5_set.set_xlabel('Day', fontsize=20)

plt.tight_layout()


# Plot genes for each component sorted by score
###############################
def plot_genes_sorted(genes_ranked):
    colors_score = ["Red", "Orange", "Green"]
    customPalette = sns.set_palette(sns.color_palette(colors_score))
    gr_all = list()
    for i in range(0, 5):
        genes_q = genes_ranked[i].quantile([0.05, 0.1, 0.9, 0.95, 0.99])
        # genes_q_99 = genes_q.iloc[4]['c']
        genes_q_95 = genes_q.iloc[3]['c']
        genes_q_90 = genes_q.iloc[2]['c']

        ## Top 10%
        genes_top = genes_ranked[i][genes_ranked[i]['c'] >= genes_q_95]
        genes_high = genes_ranked[i][genes_ranked[i]['c'] < genes_q_95]
        genes_high = genes_high[genes_high['c'] >= genes_q_90]

        gr = genes_ranked[i]
        gr['Score'] = 'Medium'
        gr.loc[genes_top.index, 'Score'] = 'Top'
        gr.loc[genes_high.index, 'Score'] = "High"
        gr_all.append(gr)
    fig = plt.figure(figsize=(16, 5))

    ax1 = plt.subplot(231)
    ax2 = plt.subplot(232)
    ax3 = plt.subplot(233)
    ax4 = plt.subplot(234)
    ax5 = plt.subplot(235)

    ax1_set = sns.scatterplot(x=list(range(0, gr_all[0].shape[0])), y='c',
                              hue='Score', hue_order=['Top', 'High', "Medium"],
                              palette=customPalette, s=8, data=gr_all[0], ax=ax1, legend=False)
    ax1_set.set_ylabel('Component score', fontsize=16)
    ax1_set.set_xlabel('Genes (sorted)', fontsize=16)
    ax1_set.set_title('Component 1', fontsize=16)

    ax2_set = sns.scatterplot(x=list(range(0, gr_all[1].shape[0])), y='c',
                              hue='Score', hue_order=['Top', 'High', "Medium"],
                              palette=customPalette, s=8, data=gr_all[1], ax=ax2, legend=False)
    ax2_set.set_ylabel('Component score', fontsize=16)
    ax2_set.set_xlabel('Genes (sorted)', fontsize=16)
    ax2_set.set_title('Component 2', fontsize=16)

    ax3_set = sns.scatterplot(x=list(range(0, gr_all[1].shape[0])), y='c',
                              hue='Score', hue_order=['Top', 'High', "Medium"],
                              palette=customPalette, s=8, data=gr_all[2], ax=ax3, legend=False)
    ax3_set.set_ylabel('Component score', fontsize=16)
    ax3_set.set_xlabel('Genes (sorted)', fontsize=16)
    ax3_set.set_title('Component 3', fontsize=16)

    ax4_set = sns.scatterplot(x=list(range(0, gr_all[1].shape[0])), y='c',
                              hue='Score', hue_order=['Top', 'High', "Medium"],
                              palette=customPalette, s=8, data=gr_all[3], ax=ax4, legend=False)
    ax4_set.set_ylabel('Component score', fontsize=16)
    ax4_set.set_xlabel('Genes (sorted)', fontsize=16)
    ax4_set.set_title('Component 4', fontsize=16)

    ax5_set = sns.scatterplot(x=list(range(0, gr_all[1].shape[0])), y='c',
                              hue='Score', hue_order=['Top', 'High', "Medium"],
                              palette=customPalette, s=8, data=gr_all[4], ax=ax5, legend=False)
    ax5_set.set_ylabel('Component score', fontsize=16)
    ax5_set.set_xlabel('Genes (sorted)', fontsize=16)
    ax5_set.set_title('Component 5', fontsize=16)

    plt.tight_layout()
    return (gr_all)