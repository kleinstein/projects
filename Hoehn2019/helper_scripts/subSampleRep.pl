#!/usr/bin/perl


use strict;
use warnings;
use hotSpotUtils;

open(IN,$ARGV[0]) or die($ARGV[0]);
my $outdir = $ARGV[1];
my $depth = $ARGV[2];
my $outrep = $ARGV[3];

my @files;
my $h=<IN>;

my %aseqs;
my %gseqs;
my %seq2f;
my %partfiles;
while(<IN>){
	my $line=$_;
	chomp($line);
	my @in = split("\t",$line);
	my $seqs = getfasta($in[0]);
	foreach my $k (keys %$seqs){
		if(exists($aseqs{$k})){die($k)}
		if($k =~ /GERM/){
			$gseqs{$k}=$seqs->{$k};
			next;
		}
		$aseqs{$k}=$seqs->{$k};
		if($in[0] =~ /\/(\d+).fa/){
			$seq2f{$k}=$1;
		}else{
			die($in[0])
		}
		$partfiles{$seq2f{$k}}=$in[3];
	}
}

my %iseq;
my %seqf;
my %files;
my $nseq=0;
my $lseq=0;
my $dump = 0;
my @keys = keys %aseqs;
print "Sequences: ".scalar(@keys)."\n";
if(scalar(@keys) < $depth){
	print "Not enough sequences!\n";
	$dump = 1;
	%iseq=%aseqs;

}else{
	while($nseq < $depth){
		my @keys = keys %aseqs;
		if(scalar(@keys)==0){print "Not enough sequences!\n"; last;}
		my $s1 = $keys[int(rand(scalar(@keys)))];
		my $file = $seq2f{$s1};
		$iseq{$s1} = $aseqs{$s1};
		delete($aseqs{$s1});
		if(exists($files{$file})){
			$files{$file} = $files{$file}+1;
		}else{
			$files{$file} = 1;
		}
		$lseq=$nseq;
		$nseq=0;
		foreach my $k (keys %iseq){ #count up all sequences in clones with at least 2 seqs
			if($files{$seq2f{$k}} >= 2){
				$nseq++;
			}
		}
		my @sk = keys %iseq;
		if($nseq != $lseq && $nseq % 100 == 0){
			print scalar(@sk)."\t$nseq\n";
		}
	}
	print "nseqs: $nseq!\n";
}

my %opened;
my %repfile;
my %counts;
my $counter=0;
foreach my $k (keys %iseq){
	my $file=$seq2f{$k};
	if($dump || $files{$file} >= 2){
		if(!exists($opened{$file})){
			open(OUT,">$outdir/$file\.fa") or die();
			print OUT ">$file\_GERM\n".$gseqs{"$file\_GERM"}."\n";
			print OUT ">$k\n$iseq{$k}\n";
			system("cp $partfiles{$file} $outdir/$file.part.txt");
			$opened{$seq2f{$k}}=1;
			$repfile{$file}="$outdir/$file.fa\tN\t$file\_GERM\t$outdir/$file.part.txt\n";
			$counts{$file}=1;
		}else{
			open(OUT,">>$outdir/$seq2f{$k}.fa") or die();
			print OUT ">$k\n$iseq{$k}\n";
			$counts{$file} = $counts{$file}+1;
		}
	}
}
@keys = keys %repfile;
open(OUT,">$outrep") or die($outrep);
print OUT scalar(@keys)."\n";
foreach my $name (sort { $counts{$b} <=> $counts{$a} } keys %counts) {
	#print $counts{$name}."\t$name\n";
    print OUT $repfile{$name};
}




