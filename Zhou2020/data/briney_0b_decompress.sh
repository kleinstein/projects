#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 10-
#SBATCH --constraint E5-2660_v3
#SBATCH -c 1
#SBATCH --mem-per-cpu=2G

# 326651 + 326713 (JSON)
# Job Wall-clock time: 03:14:46
# Memory Utilized: 4.20 MB

# CSV subjects
# Job Wall-clock time: 00:27:17
# Memory Utilized: 4.20 MB

PATH_SAVE="/home/qz93/scratch60/briney2019/consensus_csv/"
PATH_LOG="${PATH_SAVE}decompress_$(date '+%m%d%Y_%H%M%S').log"
cd "${PATH_SAVE}"

SUBJ_ARRAY=(326651 326713 316188 326650 326737 326780 326797 326907 327059 D103)
#SUBJ_ARRAY=(316188 326650 326737 326780 326797 326907 327059 D103) # CSV

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"

for SUBJ in ${SUBJ_ARRAY[@]}; do

	if [[ ${SUBJ} == "326651" || ${SUBJ} == "326713" ]]; then
		SUBJ_FILENAME="${SUBJ}_jsons.tar.gz"
		UNZIPPED_DIRNAME="consensus-cdr3nt-90_json"
	else
		SUBJ_FILENAME="${SUBJ}_csv.tar.gz"
		UNZIPPED_DIRNAME="consensus-cdr3nt-90_minimal"
	fi

	if [[ (-s "${SUBJ_FILENAME}" ) ]]; then
		echo "unzipping ${SUBJ_FILENAME}..." &>> "${PATH_LOG}"
		# unzip
		tar -xzf "${SUBJ_FILENAME}"
		# rename folder
		mv "${UNZIPPED_DIRNAME}" "${SUBJ}"
	else
		echo "${SUBJ_FILENAME} does not exist" &>> "${PATH_LOG}"
	fi

done

