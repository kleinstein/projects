#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --job-name=repsim
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=18
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=24:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com


array=("001" "003" "004" "008" "011" "013" "014" "015" "016" "018" "021" "023" "025" "026" "033" "049" "054" "063" "064" "065" "073" "074" "083" "093" "095" "097" "034")

sims=20
count=0

rm simmasked.log
for i in "${array[@]}"
do
	echo $i
	echo $count
	igphyml --repfile $i/mrna/V1/seqs_lineages.GY3.tsv -m HLP19 -o n --run_id H0tree --outrep $i/mrna/V1/seqs_lineages.HLP.tsv --threads 18
	mkdir $i/mrna/V1/simsf
	rm $i/mrna/V1/simsrg
	if [ $(($count%2)) == 0 ]
	then
		echo "TRUE" 
		#sbatch --partition=pi_kleinstein --cpus-per-task=8 --job-name=$i runSim.sh $i/simulated_$rep 8
		#perl simulateRepertoire.pl $sims 18 $i/mrna/V1/seqs_lineages.HLP.tsv 0.7 $i/mrna/V1 HD1310.tsv simulatedf pi_kleinstein >> simmasked.log
		perl simulateRepertoire.pl $sims 18 $i/mrna/V1/seqs_lineages.HLP.tsv 0.7 $i/mrna/V1 N simulatedrg pi_kleinstein 0 >> simmasked.log
	else
		echo "FALSE"
		#sbatch --partition=general --cpus-per-task=8 --job-name=$i runSim.sh $i/simulated_$rep 8
		#perl simulateRepertoire.pl $sims 18 $i/mrna/V1/seqs_lineages.HLP.tsv 0.7 $i/mrna/V1 HD1310.tsv simulatedf general >> simmasked.log
		perl simulateRepertoire.pl $sims 18 $i/mrna/V1/seqs_lineages.HLP.tsv 0.7 $i/mrna/V1 N simulatedrg general 0 >> simmasked.log
	fi
	count=$((count + 1))
	#break
done
