# Characterizing expanding B cells in flu vaccine response between young and old patients


Mamie Wang (mamie.wang@yale.edu)

--------------------

## Description

Code for analyzing a single-cell RNAseq dataset with paired BCR sequencing for B cell enriched samples from 6 patients receiving influenza vaccine (3 young and 3 old) at pre-vaccination and 7 days post-vaccination timepoints.

--------------------

## Dependencies

* R v4.1.0 
   * Seurat v4.0.3
* Python v3.9.6
   * [MELD v1.0](https://github.com/KrishnaswamyLab/MELD): `pip install meld`
* [Immcantation development docker image](https://immcantation.readthedocs.io/en/stable/docker/intro.html): `apptainer pull docker://immcantation/suite:devel`
* [Azimuth docker image v0.4.5](https://hub.docker.com/r/satijalab/azimuth): `apptainer pull docker://satijalab/azimuth:0.4.5`

--------------------

## Data

Single-cell RNA sequencing and V(D)J data have been deposited in NCBI’s Gene Expression Omnibus and are available at the GEO Series accession number GSE175524

--------------------

## Scripts

* Preprocessing scripts (`scripts/`)
	* 1_igblast.sh: assign genes to BCR sequences using IgBlast 
	* 2_makedb.sh: convert IgBlast output to table format
	* 3_filter_BCR.R: quality control BCR sequences
	* 4_clonotyping.sh: clonotype BCR sequences
	* 5_bcr_annotation.R/.sh: annotate BCR sequence propoerties
	* 6_clustering.R/.sh/.ipynb: cluster gene expression dataset
	* 7_azimuth.R/.sh: cell type annotation of the gene expression data using azimuth
	* 8_annotation.R/.sh: quality control B cell type annotation
	* 9_MELD_Age_d0.py/.sh: differential abundance analysis on pre-vaccination B cells between age groups
	* 10_MELD_day.py/.sh: differential abundance analysis on B cells between pre- and post-vaccination timepoint
* Figure production scripts (`figures/`)
	* Figure 1: study design
	* Figure 2: pre-vaccination age group comparison in gene expression
	* Figure 3: clonal size comparison between timepoints
	* Figure 4: gene expression comparison between timepoints
	* Figure 5: differential abundance analysis between timepoints
	* Figure 6: analysis on an unusually large and persistent clone in an older adult
	* Supplementary Figure 1 - 12