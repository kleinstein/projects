#!/bin/bash

#SBATCH --nodes=1
#SBATCH --cpus-per-task=32
#SBATCH --mem=150g
#SBATCH --partition week
#SBATCH --time=7-0:00:00

module load R/4.1.0

build=$1
nprocs=$2

Rscript full_results_all_builds.R $build $nprocs