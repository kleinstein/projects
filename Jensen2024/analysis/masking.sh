#!/bin/bash

#SBATCH --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --mem=50g

module load R/4.1.0

Rscript mask_hlclones.R