# Kenneth B. Hoehn
# 8/26/2022
# Run cloning scripts, create clonal germlines
# then perform phylogenetic analyses

id=$1
threads=$2
permutations=$3

module load R/4.1.0-foss-2020b

Rscript groupClones.R $id $threads
Rscript SHM_freq.R $id $threads
Rscript switches_dowser_igphyml.R $id 10 $threads $permutations 100
