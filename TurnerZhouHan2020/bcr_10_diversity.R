# reviewer 3; Extended Data Figure 3f
# diversity

library(alakazam) # 0.3.0
?estimateAbundance

# db
load("~/Dropbox (yale)/projects/ellebedy_10x_P05/bcr/01092020/01232020/moreData/muFreq_DTN_P05_GEXmeta.RData")

# all GC
idx_GC_all = which(db[["SEQ_TYPE"]]=="10x" & db[["anno_10x_B2"]]=="GC")
cat("all GC:", length(idx_GC_all), "\n")

# all early blood PB
idx_PB_all = which( (db[["SEQ_TYPE"]]=="10x" & db[["TP_ANNO_B2"]]=="d5_PB" & grepl(pattern="_PBMC|_BCell", x=db[["SAMPLE"]]) ) | 
                    (db[["SEQ_TYPE"]]=="bulk" & db[["CELL_TYPE"]]=="PB") )
cat("all early blood PB:", length(idx_PB_all), "\n")

# QIV-specific clones
cloneIDs_mAb = cloneInfo_mAb[["cloneID"]]
stopifnot( all( cloneIDs_mAb %in% unique(db[["CLONE"]]) ) )

db[["QIV"]] = ifelse(db[["CLONE"]] %in% cloneIDs_mAb, "QIV-specific", "Not QIV-specific")

table(db[["QIV"]][idx_GC_all], useNA="ifany")
table(db[["QIV"]][idx_PB_all], useNA="ifany")

# should be no intersection
stopifnot( length( intersect(idx_GC_all, idx_PB_all) ) == 0 )

# divide GC by timepoint
db_GC = db[idx_GC_all, ]
table(db_GC[["TIMEPOINT"]], useNA="ifany")

db_GC[["abundance_group"]] = paste(db_GC[["QIV"]], db_GC[["TIMEPOINT"]])
table(db_GC[["abundance_group"]], useNA="ifany")

# PB only early blood
db_PB = db[idx_PB_all, ]
db_PB[["abundance_group"]] = paste(db_PB[["QIV"]], db_PB[["TIMEPOINT"]])
table(db_PB[["abundance_group"]], useNA="ifany")


### diversity

set.seed(2974572)
abund_GC = estimateAbundance(data=db_GC, group="abundance_group")

p1 = plotAbundanceCurve(abund_GC) + theme(legend.text = element_text(size=rel(1.5)),
                                       axis.text.y = element_text(size=rel(1.5)),
                                       axis.text.x = element_text(size=rel(1.5)),
                                       axis.title.y = element_text(size=rel(1.5)),
                                       axis.title.x = element_text(size=rel(1.5))) + ggtitle("")
ggsave(filename="abundance_GC.pdf", plot=p1, device="pdf", width=7, height=5, units="in")


set.seed(29738745)
abund_PB = estimateAbundance(data=db_PB, group="abundance_group")

p2 = plotAbundanceCurve(abund_PB) + theme(legend.text = element_text(size=rel(1.5)),
                                          axis.text.y = element_text(size=rel(1.5)),
                                          axis.text.x = element_text(size=rel(1.5)),
                                          axis.title.y = element_text(size=rel(1.5)),
                                          axis.title.x = element_text(size=rel(1.5))) + ggtitle("")
ggsave(filename="abundance_PB.pdf", plot=p2, device="pdf", width=7, height=5, units="in")

