# Kenneth B. Hoehn
# 8/26/2022
# Compare SHM estimates on each sequence when using BALB/c
# or IMGT V gene germline databases

library(alakazam)
library(dowser)
library(shazam)
library(dplyr)
library(tidyr)
library(ggpubr)

nproc=7
files = list.files("processed/")
files = files[grepl("\\d-\\d",files)]

ids = unique(unlist(lapply(strsplit(files,split="_"),function(x)x[1])))

b = tibble()
m = tibble()
for(id in ids){
	print(id)
	btemp = readChangeoDb(paste0("processed/",id,"_balbc_db-pass.tsv"))
	mtemp = readChangeoDb(paste0("processed/",id,"_mouse_db-pass.tsv"))
	btemp$sample = id
	mtemp$sample = id
	b = bind_rows(b, btemp)
	m = bind_rows(m, mtemp)
}

balbc_ref = readIMGT("share/germlines/imgt/balbc/vdj")
mouse_ref = readIMGT("share/germlines/imgt/mouse/vdj")

b$clone_id = 1:nrow(b)
b = createGermlines(b, references=balbc_ref, organism="balbc",nproc=nproc)

m$clone_id = 1:nrow(m)
m = createGermlines(m, references=mouse_ref, organism="mouse",nproc=nproc)

# calculate SHM from different germlines
b = observedMutations(b, regionDefinition=IMGT_V, frequency=TRUE, combine=TRUE, nproc=nproc)
m = observedMutations(m, regionDefinition=IMGT_V, frequency=TRUE, combine=TRUE, nproc=nproc)

# compare SHM with old and new references
index = match(b$sequence_id, m$sequence_id)
mean(b$sequence_id == m$sequence_id[index], na.rm=TRUE)

b$omu_freq = m$mu_freq[index]
b$ov_call = m$v_call[index]
b$ojunction_length = m$junction_length[index]
maxshm = max(c(b$omu_freq, b$mu_freq))

b = filter(b, productive)
bspread = b %>%
	select(sequence_id, mu_freq, omu_freq) %>%
	gather("Database","freq",-sequence_id)

bspread$Database[bspread$Database == "omu_freq"] = "IMGT"
bspread$Database[bspread$Database == "mu_freq"] = "BALB/c"

pointsize=0.1
alpha=0.5
g1 = ggplot(filter(b, productive),aes(x=omu_freq, y=mu_freq)) + 
	geom_point(size=pointsize,alpha=alpha) + geom_abline() + xlab("IMGT SHM estimate") + 
	ylab("BALB/c SHM estimate") +
	theme_bw() + xlim(0,maxshm) + ylim(0,maxshm)

g2 = ggplot(filter(b, productive),
	aes(x=ojunction_length, y=junction_length)) + geom_point(size=pointsize,alpha=alpha) + geom_abline() +
	xlab("IMGT junction length") + ylab("BALBc junction length") +
	theme_bw()

g3 = ggplot(bspread, aes(x=Database,y=freq)) + 
	geom_boxplot(outlier.shape=NA) +
	geom_jitter(width=0.1,size=pointsize,alpha=alpha) +
	ylab("SHM estimate")+
	theme_bw()

pdf("results/old_vs_new.pdf",width=7,height=2.5)
gridExtra::grid.arrange(grobs=list(g3,g1,g2), ncol=3)
dev.off()

png("results/old_vs_new.png",width=7,height=2.5,units='in',res=300)
gridExtra::grid.arrange(grobs=list(g3,g1,g2), ncol=3)
dev.off()

b$diff = b$mu_freq - b$omu_freq

print(paste("More BALBc SHM:",round(mean(b$diff > 0, na.rm=TRUE),digits=3)))
print(paste("More IMGT SHM:",round(mean(b$diff < 0, na.rm=TRUE),digits=3)))
print(paste("Same SHM:",round(mean(b$diff == 0, na.rm=TRUE),digits=3)))

# 4 sequences weren't found in IMGT run, but have very low mutation frequencies
# should be fine
filter(b, is.na(diff))$mu_freq

difftable = b %>%
	filter(diff > 0) %>%
	group_by(ov_call) %>%
	summarize(n=n(), mean_diff = mean(diff), max_diff = max(diff), nsample = n_distinct(sample)) %>%
	arrange(desc(n))

write.csv(difftable,"analysis/difftable.csv",row.names=FALSE)

writeChangeoDb(b, file="processed/alldata.tsv")
b = readChangeoDb("processed/alldata.tsv")

fb = filter(b, diff <= 0)
print(paste("Removed",mean(b$diff > 0),"sequences"))

writeChangeoDb(fb, file="processed/alldata_filtered.tsv")
fb = readChangeoDb("processed/alldata_filtered.tsv")

print((1-nrow(fb)/nrow(b)) * 100)

# print out filtered data for each sample
for(s in unique(fb$sample)){
	print(s)
	temp = filter(fb, sample == s)
	writeChangeoDb(temp, file=paste0("processed/",s,"_collapse-unique_atleast-3_igblast_db-pass.tsv"))
}


