nextflow pull nf-core/airrflow -r 4.0
nextflow run nf-core/airrflow -r 4.0 -profile docker \
--input metadata_BCR_umi.tsv \
--library_generation_method dt_5p_race_umi \
--cprimer_position R2 \
--index_file false \
--umi_length 12 \
--umi_position R1 \
--race_linker ./linker.fasta \
--cprimers ./Cprimers.fasta \
--cluster_sets true \
--remove_chimeric false \
--cloneby 'subject_id' \
--crossby 'rep_id' \
--lineage_trees \
--lineage_tree_builder "pratchet" \
--lineage_tree_exec "" \
-c custom.config \
--outdir "./results_pratchet" \
-w "./work" \
--max_cpus 20 \
--max_memory 60.GB \
--validate_params false \
-resume
