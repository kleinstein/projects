
def get_lev_dist2nearest_old(l):
    clone_indices=np.where(table_full.CLONE==clone_list[l])[0]

    dist2nearest_clone_min=1000
    dist2nearest_clone_min_jun=1000
    sum_non_unique_v=0
    sum_non_unique_jun=0
    gene_groups=np.unique(table_full.VJ_GROUP,return_counts=True)
    if gene_groups[1][0]>gene_groups[1][1]:
        min_group=np.where(table_full.VJ_GROUP[clone_indices]==gene_groups[0][1])[0]
        maj_group=np.where(table_full.VJ_GROUP[clone_indices]==gene_groups[0][0])[0]
    if gene_groups[1][0]<gene_groups[1][1]:
        min_group=np.where(table_full.VJ_GROUP[clone_indices]==gene_groups[0][0])[0]
        maj_group=np.where(table_full.VJ_GROUP[clone_indices]==gene_groups[0][1])[0]
  
    for i in range(0,min_group.shape[0]):
        for j in range(0,maj_group.shape[0]):
            d_ij=sift4(table_full.SEQUENCE_INPUT[clone_indices[min_group[i]]],table_full.SEQUENCE_INPUT[clone_indices[maj_group[j]]],max_offset=30)
            d_ij=2*(d_ij-np.abs(table_full.SEQUENCE_INPUT[clone_indices[min_group[i]]].__len__()-table_full.SEQUENCE_INPUT[clone_indices[maj_group[j]]].__len__() ))/(table_full.SEQUENCE_INPUT[clone_indices[min_group[i]]].__len__()+table_full.SEQUENCE_INPUT[clone_indices[maj_group[j]]].__len__() )
            if d_ij<dist2nearest_clone_min:
                dist2nearest_clone_min=d_ij

            d_jun_ij=sift4(table_full.JUNCTION[clone_indices[min_group[i]]],table_full.JUNCTION[clone_indices[maj_group[j]]],max_offset=30)
            #d_jun_ij=2*d_jun_ij/(d_jun_ij+table_full.JUNCTION[clone_indices[i]].__len__()+table_full.JUNCTION[clone_indices[j]].__len__() )
            d_jun_ij=2*(d_jun_ij-np.abs(table_full.JUNCTION[clone_indices[min_group[i]]].__len__()-table_full.JUNCTION[clone_indices[maj_group[j]]].__len__() ))/(table_full.JUNCTION[clone_indices[min_group[i]]].__len__()+table_full.JUNCTION[clone_indices[maj_group[j]]].__len__() )
            if d_jun_ij<dist2nearest_clone_min_jun:
                dist2nearest_clone_min_jun=d_jun_ij

            if table_full.JUNCTION[clone_indices[min_group[i]]].__len__()!=table_full.JUNCTION[clone_indices[maj_group[j]]].__len__():
                sum_non_unique_jun=sum_non_unique_jun+1

        
    return dist2nearest_clone_min,dist2nearest_clone_min_jun,sum_non_unique_jun

def get_lev_dist2nearest(l):
    clone_indices=np.where(table_full.CLONE==clone_list[l])[0]

    dist2nearest_clone_min=1000
    dist2nearest_clone_min_jun=1000
    sum_non_unique_v=0
    sum_non_unique_jun=0
    gene_groups=np.unique(table_full.VJ_GROUP,return_counts=True)
    if gene_groups[1][0]>gene_groups[1][1]:
        min_group=np.where(table_full.VJ_GROUP[clone_indices]==gene_groups[0][1])[0]
        maj_group=np.where(table_full.VJ_GROUP[clone_indices]==gene_groups[0][0])[0]
    if gene_groups[1][0]<gene_groups[1][1]:
        min_group=np.where(table_full.VJ_GROUP[clone_indices]==gene_groups[0][0])[0]
        maj_group=np.where(table_full.VJ_GROUP[clone_indices]==gene_groups[0][1])[0]
  
    for i in range(0,min_group.shape[0]):
        for j in range(0,maj_group.shape[0]):
            d_ij=sift4(table_full.SEQUENCE_INPUT[clone_indices[min_group[i]]],table_full.SEQUENCE_INPUT[clone_indices[maj_group[j]]],max_offset=30)
            d_ij=2*(d_ij-np.abs(table_full.SEQUENCE_INPUT[clone_indices[min_group[i]]].__len__()-table_full.SEQUENCE_INPUT[clone_indices[maj_group[j]]].__len__() ))/(table_full.SEQUENCE_INPUT[clone_indices[min_group[i]]].__len__()+table_full.SEQUENCE_INPUT[clone_indices[maj_group[j]]].__len__() )
            if d_ij<dist2nearest_clone_min:
                dist2nearest_clone_min=d_ij

            d_jun_ij=sift4(table_full.JUNCTION[clone_indices[min_group[i]]],table_full.JUNCTION[clone_indices[maj_group[j]]],max_offset=30)
            #d_jun_ij=2*d_jun_ij/(d_jun_ij+table_full.JUNCTION[clone_indices[i]].__len__()+table_full.JUNCTION[clone_indices[j]].__len__() )
            d_jun_ij=2*(d_jun_ij-np.abs(table_full.JUNCTION[clone_indices[min_group[i]]].__len__()-table_full.JUNCTION[clone_indices[maj_group[j]]].__len__() ))/(table_full.JUNCTION[clone_indices[min_group[i]]].__len__()+table_full.JUNCTION[clone_indices[maj_group[j]]].__len__() )
            if d_jun_ij<dist2nearest_clone_min_jun:
                dist2nearest_clone_min_jun=d_jun_ij

            if table_full.JUNCTION[clone_indices[min_group[i]]].__len__()!=table_full.JUNCTION[clone_indices[maj_group[j]]].__len__():
                sum_non_unique_jun=sum_non_unique_jun+1

        
    return dist2nearest_clone_min,dist2nearest_clone_min_jun,sum_non_unique_jun