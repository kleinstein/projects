


use strict;
use warnings;
use hotSpotUtils;

my @folders=glob("*/*/$ARGV[0]"); #seqs_lineages.tsv or seqs_3000_lineages.tsv
#my @visits=("V1","V2","V3");
my $minseq=$ARGV[1];
my $min = 9**9**9;
my $max = 0;
my $mean = 0;
my $nfile = 0;
my $y=0;
foreach my $f (@folders){
	my $c=0;
	my $cr=0;
	open(IN,"$f") or do{print "$f not found!";next;};
	my $seqcount=0;
	my $h=<IN>;
	while(<IN>){
		my $line=$_;
		my @in=split("\t",$line);
		my $seqs = getfasta($in[0]);
		my @k=keys %$seqs;
		if(scalar(@k) >= $minseq){
			$c++;
			$cr += (scalar(@k)-1);
		}
	}
	print "$f\t$c\t$cr\n";
	if($cr < $min){$min = $cr;}
	if($cr > $max){$max = $cr;}
	$mean += $cr;
	$nfile++;
}
$mean /= $nfile;
print "$min\t$mean\t$max\n";
