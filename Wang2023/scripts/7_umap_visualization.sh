#!/bin/bash
#SBATCH --partition=bigmem
#SBATCH --job-name=umap
#SBATCH --mem-per-cpu=400G
#SBATCH --time=2-00:00:00
#SBATCH --error=log/tSNE_embed.%A_%a.err
#SBATCH --output=log/tSNE_embed.%A_%a.out
#SBATCH --array=1-4

module load miniconda
conda activate r_4.2

script="/gpfs/ysm/project/kleinstein/mw957/repos/bcr_embeddings/scripts/5_tSNE_visualization.py"

path="/gpfs/gibbs/pi/kleinstein/mw957/BCR_embed/data"

files=("combined_cdr3_heavy_ProtT5.pkl" "combined_cdr3_light_ProtT5.pkl" "combined_distinct_heavy_test_ProtT5.pkl" "combined_distinct_light_test_ProtT5.pkl")

taskID=${SLURM_ARRAY_TASK_ID}

echo "[$0 $(date +%Y%m%d-%H%M%S)] [start] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"

file=${files[$taskID-1]}
outfile="${file%.*}_umap.pkl"

echo python $script $path/$file $path/$outfile
python $script $path/$file $path/$outfile

echo "[$0 $(date +%Y%m%d-%H%M%S)] [end] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"
