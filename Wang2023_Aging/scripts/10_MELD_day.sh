#!/bin/bash
#SBATCH --partition=general
#SBATCH --job-name=MELD_d7
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=60G
#SBATCH --time=12:00:00
#SBATCH --error=log/MELD_d7.%A_%a.err
#SBATCH --output=log/MELD_d7.%A_%a.out
#SBATCH --array=1

module load miniconda
conda activate r_4.0

taskID=${SLURM_ARRAY_TASK_ID}

cell_types=("full" "Naive B" "ABC" "RMB" "PB" "PB proliferating")
cell_type=${cell_types[$taskID-1]}

echo "[$0 $(date +%Y%m%d-%H%M%S)] [start] $SLURM_JOBID"

python 10_MELD_day.py "$cell_type"

echo "[$0 $(date +%Y%m%d-%H%M%S)] [end] $SLURM_JOBID"
