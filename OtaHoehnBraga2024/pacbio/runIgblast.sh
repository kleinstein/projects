#!/usr/bin/bash
# Kenneth B. Hoehn
# 3/18/22
# kenneth.hoehn@yale.edu
# Run IgBlast on consensus sequences

module load Python/3.7.0-foss-2018b

threads=1
bases=("MO-06232021-scBCR.ccs_quality-pass_primers-pass_combined_primers-pass_reheader_reheader_length-pass_cluster-pass_reheader_align-pass_consensus-pass" "Miyo-scBCR-07142021.ccs_quality-pass_primers-pass_combined_primers-pass_reheader_reheader_length-pass_cluster-pass_reheader_align-pass_consensus-pass")

for base in ${bases[@]}
do
    file=$base.fastq

    ofile=presto/$base.fasta
    echo $ofile

    # run IgBlast to align to V and J genes
    AssignGenes.py igblast -s $ofile \
    	-b ~/share/igblast --organism human --loci ig --format blast \
    	--nproc $threads

    MakeDb.py igblast -i presto/$base\_igblast.fmt7 \
    	-s $ofile \
        -r ~/share/germlines/imgt/human/vdj/imgt_human_IGHV.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGHD.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGHJ.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGKV.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGKJ.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGLV.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGLJ.fasta \
         --extended
done