Scripts by Nima Nouri for covid19, 2020, data from Avraham Unterman project
------------------------------------------------------------------------

Data is available in ig.med.yale.edu:/mnt/data2/projects/covid19_avraham_unterman
The analysis was conducted in following scripts:

1. `constants.R` contains constants required to run scripts.
2. `qc.R` performs quality control checks.
3. `docker_jobs.sh` processes cellranger outputs.
4. `process_cite.R` processes CITE data: qc and summary.
5. `pre_defineclone.R` packs two time points before clonal inference (CITE and VDJ).
6. `clone_germs_jobs.sh` performs cloning and creates germlines.
7. `after_defineclone.R` packs cloned H and L chains data together in separate folders/files.
8. `pack_all.R` packs all processed data in one single file.
9. `process_fetures.R` analyzes different features of the BCR library for underlying analysis.
10. `conv_ab.R` provides a summary of convergent antibodies.
11. `plot_BCR_panel.R` produces BCR panel figure.