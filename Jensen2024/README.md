# Scripts for combined heavy and light chain project
Cole G. Jensen
cole.jensen@yale.edu

# Dependencies

R 3.6.1, (4.2.2 where noted):
dowser v1.2.1
alakazam v1.2.1
shazam v1.1.2
ggtree v3.2.1
ape v5.7-1
dplyr v1.1.2
seqinr v4.2-30
stringr v1.5.0
igraph v1.5.0
phangorn v2.11.1

Python (3.7.0)
changeo v1.0.0
presto v0.6.1

Python (2.7.12)
bcr-phylo (https://github.com/matsengrp/bcr-phylo-benchmark)


# Creating the naive data fasta file for bcr-phylo

To process the naive B cell data, go to simulations
 and run
```
# make the fasta file for bcr-phylogenetic
Rscript simulations/fasta_creation.R

```

# Running bcr-phylo

The naive dataset is now ready for bcr-phylo. First [download bcr-phylo](https://github.com/matsengrp/bcr-phylo-benchmark). Commit **b2902d7** was used for this analysis. Then make the following changes to the simulator.py.
- line 603 change the node.name to be 'node.name = args.naive_seq_name'
- line 613 change the leaf.name to be 'leaf.name = args.naive_seq_name'
- line 873 add parser.add_argument('--naive_seq_name', default='', help='sequence identifer')
- line 904 add args.naive_seq_name = str(records[0].name)

Then you can run the bash script, gcSim.sh. This runs the python script 'simulateGC.py' which utilizes bcr-phylo. There are five arguments for this script. They are:
- The number of generations to simulate
- A 0 or a 1 to indicate if selection is accounted for while simulating. A 1 indicates the use of selection.
- The carrying capacity of the simulation with selection.
- How many times this will run, i.e. how many clones will be produced.
- The number of processors used.

The command used for this analysis was:
```
conda activate bpb
bash simulations/gcSim.sh 125 1 1000 50 5
conda deactivate
```
It should be noted, that the clone size for each clone produced here will be 50 sequences. To change that amount, replace 50 on line 49 of simulateGC.py with another value.

# Processing bcr-phylo output

The lineage trees created by bcr-phylo will be used as the true trees in the analysis and the starting point for the rest of the simulation process. However, they are created as pickled files; so they all need to be unpickled. The following script assumes that the lineage trees have been moved from their starting folder to a new folder with the other lineage trees. After running the 'unpickle.py' script there will be 50 new files called 'starting_tree_x.nw'. One newick file for each unpickled tree.

```
python simulations/unpickle.py
```

# Create the starting tree object and complete the simulation process

Now that the lineage trees have been unpickled, the original tree files can be compiled together to create one object that contains all of the 'true' trees for the simulations. USed the 'shmulate_simulations.R' script to do this as well as gather the starting trees into a list and simulates sequences. The starting trees are later used as the true trees when calculating the RF distance and as the starting point of the simulations. The shmulate simulation process uses a slightly altered version of shazam::shmulate_tree and creates the two clones objects(hclones for heavy only and hlclones for the heavy/light clones) that will be used to compare against each other.  

```
Rscript simulations/shmulate_simulations.R
```

To make the different iterations of the simulated data, the script 'simulation_iterations.R' was used. This essentially is a loop that makes different simulation outputs (hlclones and hclones). To use this script you'll need to call 'simulation_iters.sh', specify the number of processors to use, how informative the log file should be (0 being no additional information than baseline and 5+ being all additional information), and how many different simulation sets are desired. An example of running this scripts:

```
bash simulations/simulation_iters.sh 1 5 10
```

# Run the Analysis

To test if incorporating light chains was beneficial we did a number of tests on the data. We first made different copies of the simulated data with various amounts of light chains masked. To do this run we wrote mask_hlclones.R which will mask a given amount of light chains within a clone, while assuring that at least one light chain remains. An example of how to run this script is:

```
bash analysis/masking.sh
```

Now you can get the RF distance for varying levels of masking. To test the accuracy of tree reconstruction on simulated data run the 'simulations_rf.R' script:

```
bash analysis/run_sim_rf.sh
```

This script runs 'get_sim_rf.sh' which calls the 'simulations_rf.R' script.

To test bootstrapping, we ran two different scripts. One for simulated data and one for empirical data. To test the empirical data run:

```
bash analysis/run_emp_bootstraps.sh
```

This script calls 'get_emp_bootstraps.sh' which calls empirical_bootstrapping.R.

To test the simulated data run:

```
bash analysis/run_sim_bootstraps.sh
```

This script calls 'get_sim_bootstraps.sh' which, in turn, calls sim_bootstraps.R.

To test the mean branch length error, you need to have first tested the RF distance and created your simulations. Once those are both done, you can run the 'get_tree_error.R' script (assuming there are 20 different simulated data sets):

```
bash analysis/tree_error.sh 20
```

To run simulations based on triplet trees, enter the `simulations` directory. From there, edit the hardcoded parameter settings and file paths in the `hl_sim_triplet.R` scripts to their position in your setup. Then run:

```
Rscript analysis/hl_sim_triplet.R
```
