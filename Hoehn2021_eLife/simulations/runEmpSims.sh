#!/usr/bin/bash

generation_times=(12 24)
sels=(0 1)
carry_caps=(1000)
reps=1
nproc=20

for time in ${generation_times[@]}
do
    for sel in ${sels[@]}
    do
        for carry in ${carry_caps[@]}
        do
            echo $time $sel $carry $reps
            sbatch --cpus-per-task=$nproc --mem-per-cpu=5gb empSim.sh \
                $time $sel $carry $reps $nproc
        done
    done
done

