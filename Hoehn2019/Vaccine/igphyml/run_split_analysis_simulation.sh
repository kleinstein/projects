#/usr/bin/bash


module load R

bases=("IB/+7d/simulatedrg_ncdr_lineages")
END=1

for base in "${bases[@]}"
do
	echo $base
	for i in $(seq 1 $END)
	do
		echo $i
		perl  splitRepertoire.pl $base\_$i.tsv $base\_$i\_linSize.txt
		Rscript splitD7.R $base\_$i\_linSize.txt $base\_$i
		qs=(1 2 3 4)
		for q in "${qs[@]}"
		do
			echo $base\_$i\_q$q
			sbatch --cpus-per-task=1 --partition=general runq.sh $base\_$i\_q$q 1
		done
	done
done
