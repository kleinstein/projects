Analysis and publication scripts from Gupta et al, 2017. J Immunol
------------------------------------------------------------------------

This directory contains scripts to evaluate performance of clonal
grouping algorithms on experimental and simulation datasets as 
described in Gupta et al, 2017. J Immunol. The analysis was conducted
in the following steps:

1. `Performance_experimental.R` executed in the directory with clonal 
grouping of the human experimnetal datasets. Filenames are assumed to be 
formatted as follows: 
`[experiment]_[model]_[norm]_[threshold]_[linkage]_clone-pass.tab`
2. `Condense_performance_experimental.R` executed in the same 
directory as the previous step.
3. `Performance_simulation.R` executed in the directory with clonal 
grouping of the simulation datasets. Filenames are assumed to be 
formatted as follows: 
`[repertoire]_[simulation]_[model]_[norm]_[threshold]_[linkage]_clone-pass.tab`
4. `Condense_performance_simulation.R` executed in the same 
directory as the previous step.
5. `GermlinesForEntropy.sh` executed on the simulation datasets.
6. `Entropy.R` executed on the output of the previous step.
7. `Publication_figures.R` executed on the output of steps 2, 4, and 6.