


use strict;
use warnings;

my $minseq=3;
my $hlpdiff=3;
my $threads=16;
my $depth=3000;

my @folders=glob("*/*/seqs_$depth\_lineages.tsv");

my $c=0;
foreach my $f (@folders){
	if($f=~/(\S+)\/(\S+)\/seqs_$depth\_lineages.tsv/){
	my $sub=$1;
	my $visit=$2;
	my $file="run_$sub\_$visit.sh";
	open(OUT,">$file") or die($f);

	my $part = "general";
	if($c < 8){
		$part = "pi_kleinstein";
	}
	$c++;

		print OUT "#!/usr/bin/bash
#SBATCH --partition=$part
#SBATCH --job-name=$sub:$visit
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=$threads
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=48:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=kenneth.hoehn\@gmail.com
#SBATCH --exclude=c24n04

module load CBLAS/20110120-foss-2016b
module load ATLAS/3.10.2-GCC-5.4.0-2.26-LAPACK-3.6.1\n";
		print OUT "maskCDR3.pl $sub/$visit/seqs\_$depth\_lineages.tsv\n";
		print OUT "igphyml --repfile $sub/$visit/seqs\_$depth\_ncdr_lineages.tsv -m GY --threads $threads --run_id GY --minseq $minseq --outrep $sub/$visit/seqs_$depth\_ncdr_GY\_lineages.tsv\n";
		print OUT "igphyml --repfile $sub/$visit/seqs_$depth\_ncdr_GY\_lineages.tsv -m HLP19 --motifs FCH --omegaOpt e,ce --threads $threads --run_id HLP19ci -o lr --minseq $hlpdiff --cbmat\n";
		system("sbatch run_$sub\_$visit.sh");
	}
}

