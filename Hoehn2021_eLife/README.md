# Scripts for B cell measurable evolution project
Kenneth B. Hoehn
kenneth.hoehn@yale.edu
12/27/2020

# Dependencies

R 3.6.1, (4.0.3 where noted):
dowser v0.0.3, (v0.1.0 where noted)
alakazam v1.0.2.999
shazam v1.0.2.999
ggtree v2.0.4
ape v5.4-1
dplyr v1.0.2

Python (3.7.0)
changeo v1.0.0
presto v0.6.1

Python (2.7.12)
bcr-phylo (https://github.com/matsengrp/bcr-phylo-benchmark)


# Processing Nielsen et al. 2019 raw data

To process data from the STORK study cohort, move to data/boyd_stork
 and run
```
# download raw sequence data files
Rscript makeDownloadScript.R

# Move to appropriate place
mkdir raw
mv *fastq raw

# run presto, igblast and MakeDb
# on each file individually
bash runPrestoSubjects.sh

# combine datasets into one file
Rscript combineDatasets.R

# move file to processing spot
mkdir ../data/processed/boyd_stork/
cp changeo/db-pass/boyd_stork_Processed_igblast_db-pass.tsv \
    ../data/processed/boyd_stork/
```

# Processing Observed Antibody Space (OAS) data

The GeneralStudyCondenser.py script provided comes from commit e0a9d73 of:
https://bitbucket.org/kbhoehn/longitudinal_analysis_scripts

Download and unpack all unpaired amino acid JSON and nucleotide data from 
OAS: http://opig.stats.ox.ac.uk/webapps/oas/downloads

Place the nucleotide dataset folder (named oas) in the top level directory.
Place the JSON datasets (named oas_json) in top level as well.

Once these files are in their appropriate place, run:
```
cd analysis
bash runStudyCondensor.sh <study> <cores> 0
```
To submit all jobs to do processing for OAS datasets that need them. For each
dataset this will run the GeneralStudyCondenser script, as well as IgBlast and
MakeDb.py to create an AIRR tsv for each file.

# Processing monoclonal antibodies

Some datasets have accompanying monoclonal antibodies. These are placed in 
data/mabs. These sequences can be downloaded from their respective 
studies or supplied on request.

To add these sequences to their respective datasets, use:
```
# Run processmAbs.py for each dataset after downloading sequences.
bash processmAbs.sh
```

You will need to re-run igblast and makedb on each of these studies,
which can be accomplied by re-running runStudyCondenser.sh but commenting out
the lines that run GeneralStudyCondenser.py. Then run:

```
bash runStudyCondensor.sh <study> <cores> 1
```

To re-form the dataset but with monoclonal antibodies added.

# Other datasets

Data from wang_2014, montgomery_wnv, oconnor_musk, oconnor_thymus, 
turner_2020_all, turner_2020_blood are available from the authors of their 
respective studies. Those datasets are placed in the data/processed subfolder
with their respective names. These datasets were already clonally clustered.

# Chimera filtering, genotyping, and clonal clustering

Datasets from OAS and Nielsen 2019 need to be genotyped and clonally
clustered before phylogenetic analysis. Study names are shown in data/processed.
For each from OAS and Nielsen 2019 (name: boyd_stork), run:
```
# Infer genotype and clonal clusters for each subject
Rscript cloneAndGenotype.R <study_name> <cores> "genotyped"
```
This will run clonesAndGenotype.R, which will:

1. read in processed data from OAS
2. filter sequences for productivity
3. remove pre-vaccine timepoints from Gupta_2017
4. create initial germline sequneces using CreateGermlines.py
5. filter sequences as PCR chimeras if they contain more than 5 mutations 
    in any 10bp window. Excluding simulations and mAb/curated sequences.
6. infer genotypes using Tigger
7. attempt to find a Hamming distance threshold for clonal clustering 
    (use 0.1 if unsuccessful)
8. assign clonal clusters using DefineClones.py
9. create clonal germlines using CreateGermlines.py
10. Output processed data and hamming distance plots

The outputs of this script will be in the ``../data/processed/<study>`` folder. 
Requires dowser v0.0.3 and no other to run as is. Sorry. 
Can easily edit the CreateGermlines calls to be compatible with later 
versions of dowser though. Can install the appropriate version using 
devtools::install_bitbucket("kleinstein/dowser@9589a27").

# Testing for measurable evolution

To perform the date randomization test, run the following code for all studies:
```
# Build trees and perform correlation test on each study
Rscript rootToTipTrees_cluster.R <study_name> <cores> 10 "all" 500 "genotyped"
```
To perform analyses on HIV datasets sampled within the first 60 weeks, run:
```
# Build trees and perform correlation test on each study
Rscript rootToTipTrees_cluster.R <study_name> <cores> 10 "60" 500 "genotyped"
```

This script will:
1. It will read in the clonally-clustered file from the study specified. It will 
also read in the file ``../info/metadata.csv`` which contains the relevant timepoint 
and condition information for all studies. Processing varies by study, but most 
of the processing is done to ensure only productive sequences are included, that 
only the samples desired are included from each study, and that subject/timepoint 
information is in properly named columns.
2. Timepoint information is inconsistently coded across datasets, and numerical 
values with units are encoded in metadata.csv. Within each subject, the script 
will match each timepoint to its numerical value and units from metadata.csv. If 
only certain timepoints are desired (e.g. first 60 weeks) these will be filtered 
out as well.
3. Format clonal clusters for tree building. This varies somewhat by study. In 
all cases, identical sequences from the same timepoint are collapsed. For OAS 
studies, mAb-containing clones are marked and mAb sequences are removed.
4. Clones are filtered for at least 10 sequences, and downsampled to at most 
500 sequences. They are then filtered out if they contain only 1 timepoint.
5. Trees are build using the phangorn functions.
6. Correlation test is performed using clustered permutations and resolved 
polytomies.
7. If being run on simulated data, also perform correlation test on the same 
trees with randomly assigned times.
8. Order trees by p value, output results

# Amino acid distance correlation test (Fig 3A)

```
Rscript rootToTipTrees_cluster_aadist.R turner_2020_all all genotyped

```

This will re-perform the correlation test using the amino acid Hamming distance 
from the germline instead of the patristic distance along the tree as divergence. 
It is only run on turner_2020_all (Turner et al 2020) in the paper, but could 
be run for other studies if desired.

# Simulations

These are done in the simulations directory.

## Setting up bcr-phylo
The affinity maturation simulation program bcr-phylo can be obtained from
https://github.com/matsengrp/bcr-phylo-benchmark (commit 26102fe)

A few lines need to be changed to print out the sequence ID and sample time
to work with other programs:

line 657
```
# from:
fh.write('>%s\n%s\n' % (node.name, get_pair_seq(node.nuc_seq, args.pair_bounds, iseq)))
# to:
fh.write('>%s|study=gc_simulations|timepoint=%s|subject=%s\n%s\n' % (node.name, node.time, args.id, get_pair_seq(node.nuc_seq, args.pair_bounds, iseq)))
```
line 663
```
# from:
fh.write('>%s\n%s\n' % (node.name, node.nuc_seq))  # [:len(node.nuc_seq) - args.n_pads_added]))
# to:
fh.write('>%s|study=gc_simulations|timepoint=%s|subject=%s\n%s\n' % (node.name, node.time, args.id, node.nuc_seq))  # [:len(node.nuc_seq) - args.n_pads_added]))
```
line 758
```
# add:
parser.add_argument('--id', default='', help='sequence identifer')
```

From here follow the installation instructions so the conda environment can be
run with 
```
source activate bpb
```

## Running GC simulations
These need to be in a directory above the top-level directory called
"programs/bcr-phylo-benchmark". This location can be toggled in simulateGC.py

To run GC simulations, run:
```
bash runGCSims.sh

# once these are finished, run:
python3 collateSims.py "gc"
```
This will then create a study called gc_simulations which can be run through the
standard pipeline using cloneAndGenotype.R and rootToTipTrees_cluster.R

## Running empirical simulations
To run empirical simulations, run:
```
# Get sequence counts for each clone at each timepoint for subject Fv
Rscript prepSamples.R

bash runEmpSims.sh

# once these are finished, run:
python3 collateSims.py "emp"
```
This will then create a study called emp_simulations which can be run through the
standard pipeline using cloneAndGenotype.R and rootToTipTrees_cluster.R

# Comparison among uniform/clustered/resolved permutation tests

To compare the clustered to the uniform permutation test for each dataset, run
```
Rscript uniformTest.R <cores>
Rscript uniformResults.R
```

These scripts will repeat the correlation test analysis on each dataset, but 
will perform permutations either 1) uniformly across all tips, 2) among 
single-timepoint monophyletic clades, and 3) among single-timepoint monophyletic 
clades with polytomies resolved to the fewest number of clusters possible. 
It will then generate accompanying figures to go along with these analyses.


# Consolidate correlation test results

Once all studies and simulations (see below) have been run:

```
Rscript summarizeResults.R

```

This will consolidate all correlation test results across all studies into one 
big table. It will also calculate the total study range in weeks based on the 
sample timepoints and units available. It will also identify adjusted measurably 
evolving lineages.


# Selection analysis

```
Rscript selectionAnalysis.R <study> <nproc> 100 all
```

This will run IgPhyML via dowser (0.1.0 or greater, sorry) and R 4.0.3 (sorry) 
on all adjusted measurably evolving lineages.


# Summarizing results and making figures

Once all of the above are successfully run (congratulations!), make figures:

```
Rscript figures.R
```

Figure drafts will be in the analysis/figures/drafts folder. These are touched
up with Adobe Illustrator to create the final figure panels.

