#--------------------------------------------
# Generate antiBERTy embedding
# 
# AntiBERTy is available on PyPI:
# https://pypi.org/project/antiberty/
#
# Example usage: 
#
# python antiBERTy_embed.py S_FULL.parquet HL S_FULL_antiBERTy.pt
#--------------------------------------------
from antiberty import AntiBERTyRunner
import argparse
import torch
import numpy as np
import pandas as pd
import time
from Bio import SeqIO
import pandas as pd
import math

#--------------------------------------------
parser = argparse.ArgumentParser(description="Generate antiBERTy embedding")
parser.add_argument("parquet_file", type=str, help="Path to the parquet file")
parser.add_argument("sequence", type=str, help="Column name of sequence to embed (H, L, HL)")
parser.add_argument("output_file", type=str, help="Output file path")
args = parser.parse_args()

#--------------------------------------------
def batch_loader(data, batch_size):
    num_samples = len(data)
    for i in range(0, num_samples, batch_size):
        end_idx = min(i + batch_size, num_samples)
        yield i, end_idx, data[i:end_idx]

def insert_space_every_other_except_cls(input_string):
    parts = input_string.split('[CLS]')
    modified_parts = [''.join([char + ' ' for char in part]).strip() for part in parts]
    result = ' [CLS] '.join(modified_parts)
    return result


dat = pd.read_parquet(args.parquet_file)

#--------------------------------------------
# Format the sequence
max_length = 512-2
X = dat.loc[:,args.sequence]
X = X.apply(lambda a: a[:max_length])
X = X.str.replace('<cls><cls>', '[CLS][CLS]')
X = X.apply(insert_space_every_other_except_cls)
sequences = X.str.replace('  ', ' ')

#--------------------------------------------
# Load the model
antiberty = AntiBERTyRunner()

##--------------------------------------------
## Embedding the sequences
start_time = time.time()
batch_size = 500
n_seqs = len(sequences)
dim = 512

n_batches = math.ceil(n_seqs / batch_size)
embeddings = torch.empty((n_seqs, dim))

i = 1
for start, end, batch in batch_loader(sequences, batch_size):
    print(f'Batch {i}/{n_batches}\n')
    x = antiberty.embed(batch)
    x = [a.mean(axis = 0) for a in x]
    embeddings[start:end] = torch.stack(x)
    i += 1

end_time = time.time()
print(end_time - start_time)

torch.save(embeddings, args.output_file)