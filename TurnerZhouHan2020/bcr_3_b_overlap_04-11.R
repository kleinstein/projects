NPROC = 20 #* match this with -c in .sh
cat("nproc:", NPROC, "\n")

#*
PLOT_OVERLAP = T

pathRoot = "/home/qz93/project/ellebedy_pairedBulk/"
pathData = paste0(pathRoot, "changeo/distToNearest/")
pathWork = paste0(pathRoot, "changeo/distToNearest/")

library(alakazam) # v0.2.11
#library(shazam)   # v0.1.10/11
library(doMC)
library(foreach)
registerDoMC(NPROC)

# see script for help doc on plotOverlap()
source("/home/qz93/projects/scripts/turnerZhouHan_2020/susanna_overlapPlot.R")
library(dplyr)
library(ggplot2)

#### meta setup ####

COL_SUBJECT="SUBJECT"
COL_SAMPLE="SAMPLE"


#### between-subject overlap ####

if (PLOT_OVERLAP) {
    # ideally only makes sense to do this without subsampling
    
    # db
    filename = "distToNearest_btwSubj_subsample-NULL.RData"
    
    setwd(pathData)
    stopifnot(file.exists(filename))
    load(filename)
    
    # every subject's mAb has "mAb" in $SAMPLE
    # differentiate the subjects
    # also add subject prefix to S[7-12] so that they get ordered by subject
    
    # db[[COL_SAMPLE]] = sapply(1:nrow(db), function(i){
    #     if (db[[COL_SAMPLE]][i]=="mAb") {
    #         return(paste0(db[[COL_SUBJECT]][i], "_mAb"))
    #     } else {
    #         return(db[[COL_SAMPLE]][i])
    #     }
    # })
     
    db[[COL_SAMPLE]] = paste(db[[COL_SUBJECT]], db[[COL_SAMPLE]], sep="_")
    
    stopifnot(!any(is.na(db[[COL_SUBJECT]])))
    stopifnot(!any(is.na(db[[COL_SAMPLE]])))
    
    # convert character class to factor
    levels_SUBJECT = sort(unique(db[[COL_SUBJECT]]))
    levels_SAMPLE = sort(unique(db[[COL_SAMPLE]]))
    
    db[[COL_SUBJECT]] = factor(db[[COL_SUBJECT]], levels=levels_SUBJECT)
    db[[COL_SAMPLE]] = factor(db[[COL_SAMPLE]], levels=levels_SAMPLE)
    
    # AA from nucleotide 
    
    setwd(pathWork)
    saveName = "distToNearest_btwSubj_subsample-NULL_SEQUENCE_IMGT_AA.RData"
    
    if (!file.exists(saveName)) {
        # db$SEQUENCE_IMGT_AA = alakazam::translateDNA(db$SEQUENCE_IMGT, trim=F)
        
        # parallelly distribute (takes >2 hrs when doing it sequentially)
        STEP_SIZE = 5000 #*
        NUM_STEPS = ceiling(nrow(db)/STEP_SIZE)
        idx_lst = vector(mode="list", length=NUM_STEPS)
        
        idx_start = 1
        for (i in 1:NUM_STEPS) {
            idx_end = min(idx_start+STEP_SIZE-1, nrow(db))
            idx_lst[[i]] = idx_start:idx_end
            idx_start=idx_end+1
        }
        stopifnot( sum(unlist(lapply(idx_lst, length))) == nrow(db) )
        
        SEQUENCE_IMGT_AA_lst = foreach(i=1:NUM_STEPS) %dopar% {
            curIdx = idx_lst[[i]]
            curAA = translateDNA(db$SEQUENCE_IMGT[curIdx], trim=F)
            return(curAA)
        }
        SEQUENCE_IMGT_AA = unlist(SEQUENCE_IMGT_AA_lst)
        
        save(SEQUENCE_IMGT_AA, file=saveName)
    } else {
        load(saveName) # SEQUENCE_IMGT_AA
    }
    
    db$SEQUENCE_IMGT_AA = SEQUENCE_IMGT_AA
    
    # between-subject
    cat("\nplotting btw-subject seq overlap...\n")
    plotName = paste0("seqOverlap_btwSubj.pdf")
    pdf(plotName, width=8, height=8)
    plotDbOverlap(db, group=COL_SUBJECT, 
                  features=c("SEQUENCE_IMGT", "SEQUENCE_IMGT_AA"),
                  similarity=c("min", "min"), exact=c(T,T), na.rm=F,
                  print_zero=T, long_x_angle=60, silent=F, 
                  heatmap_colors=c("white","hotpink", "grey50"), 
                  xlab="Sequence (nucleotide)", ylab="Sequence (AA)", title="Between-subject",
                  geom_text_size=5, axis_text_size=20, title_text_size=15, axis_title_size=15)
    dev.off()
    
    # between-sample
    cat("\nplotting btw-sample seq overlap...\n")
    plotName = paste0("seqOverlap_btwSamp.pdf")
    pdf(plotName, width=12, height=12)
    plotDbOverlap(db, group=COL_SAMPLE, 
                  features=c("SEQUENCE_IMGT", "SEQUENCE_IMGT_AA"),
                  similarity=c("min", "min"), exact=c(T,T), na.rm=F,
                  print_zero=T, long_x_angle=60, silent=F, 
                  heatmap_colors=c("white","hotpink", "grey50"), 
                  xlab="Sequence (nucleotide)", ylab="Sequence (AA)", title="Between-sample",
                  geom_text_size=5, axis_text_size=20, title_text_size=15, axis_title_size=15)
    dev.off()
    
    rm(db, SEQUENCE_IMGT_AA)
}

