#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 14-

# -c 16
# --mem-per-cpu=6G
# --constraint E5-2660_v3

##### slurm stats

# 30c x 6G each; 6240
# Job Wall-clock time: 07:18:18
# Memory Utilized: 111.40 GB
# Memory Efficiency: 61.89% of 180.00 GB

##### system requirement 
# https://support.10xgenomics.com/single-cell-gene-expression/software/overview/system-requirements

# 8-core Intel or AMD processor per node (16 recommended)
# 6GB RAM per core
# Shared filesystem (e.g. NFS)

# Many Linux systems have default user limits (ulimits) for 
# maximum open files and maximum user processes as low as 1024 or 4096. 
# Because Cell Ranger spawns multiple processes per core, 
# jobs that use a large number of cores can exceed these limits. 
# We recommend higher limits:
# user open files	16k
# system max files	10k per GB RAM available to Cell Ranger
# user processes	64 per core available to Cell Ranger

# https://www.osc.edu/resources/getting_started/howto/howto_use_ulimit_command_to_set_soft_limits
# to see current limits
# ulimit -a

# on ruddle, looks like these are all satisfied:
# open files              (-n) 51200
# max memory siz  (kbytes, -m) 5242880 [not sure if this is the one; 10000*16*6=960000]
# max user processes      (-u) 512057


##### 10x software
PATH_10X="/ysm-gpfs/pi/kleinstein/cellrager_3.1.0/"
PATH_CR_EXE="${PATH_10X}cellranger-3.1.0/cellranger"

# different from the reference for GEX
# https://support.10xgenomics.com/single-cell-vdj/software/pipelines/latest/installation
# https://support.10xgenomics.com/single-cell-vdj/software/downloads/latest
#PATH_CR_REF="${PATH_10X}refdata-cellranger-GRCh38-3.0.0/" # human
PATH_CR_REF="${PATH_10X}refdata-cellranger-mm10-3.0.0/" # mouse

#export PATH=${PATH_CR_MAIN}:$PATH


##### meta

#PATH_ROOT="/home/qz93/scratch60/"
PATH_ROOT="/gpfs/loomis/scratch60/kleinstein/qz93/ellebedy_murineCovid/" # loomis scratch60
          
PATH_OUTPUT="${PATH_ROOT}cr_gex/"

PATH_FASTQ="${PATH_ROOT}raw/fastq/"

PATH_SAMPLE_LIST="${PATH_ROOT}cr_gex/cellRange_gex_run_sampleList_1.txt" #*

##### log

PATH_LOG="${PATH_OUTPUT}gex_1_$(date '+%m%d%Y_%H%M%S').log"

dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
#parallel --version | head -n 1 &>> "${PATH_LOG}"
"${PATH_CR_EXE}" vdj --version &>> "${PATH_LOG}"

echo "sample/library list: ${PATH_SAMPLE_LIST}" &>> "${PATH_LOG}"

N_LINES=$(wc -l < "${PATH_SAMPLE_LIST}")
echo "N_LINES="$N_LINES &>> "${PATH_LOG}"

cd "${PATH_OUTPUT}"

for ((IDX=1;IDX<=${N_LINES};IDX++)); do
	
	echo "IDX=${IDX}" &>> "${PATH_LOG}"

	# read Library Name from file
	CUR_LIB=$(sed "${IDX}q;d" "${PATH_SAMPLE_LIST}")
	echo "CUR_LIB=${CUR_LIB}" &>> "${PATH_LOG}"

	# A unique run id, used to name output folder [a-zA-Z0-9_-]+.
	# The pipeline will create a new folder at current directory
	# named with the ID you specified for its output. 
	# If this folder already exists, cellranger will assume it is an existing 
	# pipestance and attempt to resume running it.
	
	# 5 b/c "WEAA-"
	#* only works if lib names unique 

	#ID="${CUR_LIB:5}"
	ID="${CUR_LIB}"
	echo "ID=${ID}" &>> "${PATH_LOG}"

	# lib-specific log
	PATH_LOG_LIB="${PATH_OUTPUT}gex_${ID}_$(date '+%m%d%Y_%H%M%S').log"

	# depending on library prep and sequencing setup, may or may not need to specify --sample
	# https://support.10xgenomics.com/single-cell-vdj/software/pipelines/latest/using/fastq-input#mkfastq

	# https://support.10xgenomics.com/single-cell-vdj/software/pipelines/latest/using/vdj

	# output files: ${PATH_OUTPUT}/${ID}/outs/*

	"${PATH_CR_EXE}" count \
		--id="${ID}" \
		--fastqs="${PATH_FASTQ}" \
		--transcriptome="${PATH_CR_REF}" \
		--sample="${CUR_LIB}" \
		&>> "${PATH_LOG_LIB}"

done

echo "DONE" &>> "${PATH_LOG}"
