#!/usr/bin/bash

nproc_in=5
nproc_out=4

sbatch presto.sh 1 300 $nproc_in $nproc_out
sbatch presto.sh 301 600 $nproc_in $nproc_out
sbatch presto.sh 601 900 $nproc_in $nproc_out
sbatch presto.sh 901 1200 $nproc_in $nproc_out