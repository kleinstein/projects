#Utilities for doing analysis with BCR phylogenetics, especially with regards to 
#hotspot selection models
#20/Feb/2016
#Kenneth Hoehn

use strict;
use warnings;

#We play with fire in this script
no warnings 'recursion';


sub hasPTC{
  my $seq = uc $_[0];
  for(my $i=0;$i<length($seq);$i+=3){
    if(substr($seq,$i,3) =~ /TAA/){return 1}
    if(substr($seq,$i,3) =~ /TAG/){return 1}
    if(substr($seq,$i,3) =~ /TGA/){return 1}
  }
  return 0;
}

sub assignTipStates{
  my $node = $_[0];
  my $states = $_[1];
  my $place = $_[2];
  my $skiproot = $_[3];
  if(!defined($skiproot)){$skiproot=0}
  if(exists($node->{"left"})){
    $place = assignTipStates($node->{"left"},$states,$place,$skiproot);
    $place = assignTipStates($node->{"right"},$states,$place,$skiproot);
  }else{
    if($skiproot){
      if($node->{"id"} !~ /\_GERM/){
        $node->{"id"}=$states->[$place++];
      }
    }else{
      $node->{"id"}=$states->[$place++];
    }
  }
  return $place;
}

sub getIDs{
  my$node=$_[0];
  my@tips=@{$_[1]};
  if(exists($node->{"left"})){
    @tips = @{getIDs($node->{"left"},\@tips)};
    @tips = @{getIDs($node->{"right"},\@tips)};
  }else{
    push(@tips,$node->{'id'});
  }
  return \@tips;
}

sub numberNodes{
  my $node=$_[0];
  my $number=$_[1];
  $number++;
  $node->{"num"}=$number;
  if(exists($node->{"left"})){
    $number=numberNodes($node->{"left"},$number);
    $number=numberNodes($node->{"right"},$number);
  }
  return $number;
}

sub translate{
  my $s = $_[0];
  my $ct3 = codonTable();
  my $ct1 = codonTableSingle();
  my $aa = "";
  #if(length($s)%3==0){
    for(my$i=0;$i<length($s);$i+=3){
      my $ss = substr($s,$i,3);
      if(exists($ct3->{lc $ss})){
        if(!exists($ct1->{uc $ct3->{lc $ss}})){
          $aa .= '*';
        }else{
          my $val = $ct1->{uc $ct3->{lc $ss}};
          if($ss =~ /[a-z]/){
            $val = lc $val;
          }
          $aa .= $val;
        }
      }elsif($ss =~ /---/){
        $aa .="-";
      }elsif($ss =~ /[A-Z]--/){
        my $c = "-";my $counter=$i+2;#move forward and fix the codon
        my $gaps = 1;
        while($c eq "-"){
          $counter++;$gaps++;
          $c=substr($s,$counter,1);
        }
        if($counter == length($s)){last;}
        $s = substr($s,0,$i+1).substr($s,$counter,2).substr($s,$i+1,$gaps).substr($s,$counter+2);
        $i-=3;
      }elsif($ss =~ /[A-Z][A-Z]\-/){
        my $c = "-";my $counter=$i+2;#move forward and fix the codon
        my $gaps = 0;
        while($c eq "-"){
          $counter++;$gaps++;
          $c=substr($s,$counter,1);
        }
        if($counter == length($s)){last;}
        #print "$i\t$counter\t".length($s)."\n";
        $s = substr($s,0,$i+2).substr($s,$counter,1).substr($s,$i+2,$gaps).substr($s,$counter+1);
        $i-=3;
      }elsif($ss =~ /[A-Z]-[A-Z]/){#fix split codon
        $s = substr($s,0,$i+1).substr($s,$i+2,2)."-".substr($s,$i+4);
        $i-=3;
      }
      elsif($ss =~ /-[A-Z][A-Z]/){
        $i-=2;
        $aa .= "-";
      }elsif($ss =~ /--[A-Z]/){
        $i-=1;
        $aa .= "-";
      }
      else{
        $aa .= "X";
      }
    }
  return $aa;
}


#Print ML amino acid sequence at all nodes
sub aaEachSite{
  my $node = $_[0];
  my $string = $_[1];
  my $codons = $_[2];
  my $aatable1 = $_[3];
  my $aatable3 = $_[4];
  if(exists($node->{"left"})){
    aaEachSite($node->{"left"}, $string,$codons,$aatable1,$aatable3);
    aaEachSite($node->{"right"},$string,$codons,$aatable1,$aatable3);
  }

  if($node->{"level"}!=0){
  my @sequence;
    for(my $i=0; $i < scalar(@{$node->{"Codon_lhoods"}});$i++){
      my %aas;
      for(my $j=0; $j < 61; $j++){ #tally up relative likelihoods of amino acids
        my $n = $node->{"Codon_lhoods"}->[$i][$j];
        my $aa =  $aatable3->{$codons->[$j]}; #triple letter amino acid
        if(exists($aas{$aa})){
          $aas{$aa} = $aas{$aa} + log(1+exp($n - $aas{$aa}))
        }else{
          $aas{$aa} = $n
        }
      }
      my $max = -inf;
      my $maxchar = -1;
      foreach my $key (keys %aas){
        my $n = $aas{$key};
        if($n > $max){
          $max = $n; 
          $maxchar=$key;
        }
      }
      $node->{"AA_$i"} = $maxchar;
    }
    }
}

#print out for figtree
sub printtreestring_AAeach_figtree{
  my $node = $_[0];
  my $seqsf = $_[1];
  my $file = $_[2];
  my $slength = $_[3];
  my $alrt = $_[4];
  if(!defined($alrt)){$alrt=0;}

  my $seqs = getfasta($seqsf);
  my @keys = keys %$seqs;
  open(FIG,">$file") or die($file);
  print FIG "#NEXUS\nBegin taxa;\n\tDimensions ntax=".scalar(@keys).";\n\tTaxlabels\n";
  foreach my $k (@keys){
    print FIG "\t\t$k\n";
  }
  print FIG "\t\t;\nEnd;\nBegin trees;Tree TREE1 = [\&R] ";
  my $t="";
  my $tree = printtreestring_AAeach($node,$t,$slength,$alrt);
  print FIG "$tree;\nEnd;";
}

#Print out the tree in Newick format to a string
sub printtreestring_AAeach{
  my $node = $_[0];
  my $string = $_[1];
  my $slength = $_[2];
  my $alrt = $_[3];
  if(exists($node->{"left"})){
    $string = $string."(";
    $string=printtreestring_AAeach($node->{"left"},$string,$slength,$alrt);
    $string = $string.",";
    $string=printtreestring_AAeach($node->{"right"},$string,$slength,$alrt);
    $string=$string.")";
  }
  if($node->{"level"} != 0){
    my $a = "";
    if($alrt == 1){
      if($node->{"alrt"} ne ""){
        $string = $string.$node->{"id"}."[\&aLRT=".$node->{"alrt"}.",AA_0\=".$node->{"AA_0"};
      }else{
        $string = $string.$node->{"id"}."[\&AA_0\=".$node->{"AA_0"};
      }
    }else{
      $string = $string.$node->{"id"}."[\&AA_0\=".$node->{"AA_0"};
    }
    for(my $j = 1; $j < $slength; $j++){
      $string = $string.",AA_$j\=".$node->{"AA_$j"};
    }
    $string = $string."]:".$node->{"dist"};
  }
  return($string);
}

#Read in a fasta file
sub getfasta{
my $in = $_[0];
  chomp($in);
  open(FASTAFILEIN, $in) or die("Couldn't open $in.\n");  # Create a new file
  my %seqs;
  my $id;
  my $s = '';
  while(<FASTAFILEIN>){
    my $line=$_;
    chomp($line);
    if($line =~/^\s*$/) {next;}     
    elsif($line =~/^\s*#/) {next;}  
    elsif($line =~ /^\>\s*(\S+)\s*$/) {
      my $temp = $1;
      $temp =~ s/\s//sg;
      if(length($s) > 0) {
        $s =~ s/\s//sg;
        $seqs{$id} = $s;
        $s = '';
      }
      $id = $temp;
    }else{
     $s .= $line;
   }
  }
  $s =~ s/\s//sg;
  $seqs{$id} = $s;
  close(FASTAFILEIN);
  return \%seqs;
}

#raw distance between sequences of the same length, not counting gaps on either
sub distngap{ 
  my ($one,$two) = @_;
  my $dist = 0;
  $one = uc $one;
  $two = uc $two;
  if(length($one) != length($two)){die("Seqs not of same length! 1st:$one\n2nd:$two\n")}
  for(my $i = 0; $i < length($one); $i++ ){
    if(substr($one,$i,1) ne substr($two,$i,1) && substr($one,$i,1) ne '-' && substr($two,$i,1) ne '-' && substr($one,$i,1) ne 'N' && substr($two,$i,1) ne 'N'){
      $dist++;
    }
  }
  return($dist);
}

sub distngap_av{ 
  my ($one,$two) = @_;
  $one = uc $one;
  $two = uc $two;
  my $dist = 0;
  my $sites=0;
  if(length($one) != length($two)){die("Seqs not of same length! 1st:$one\n2nd:$two\n")}
  for(my $i = 0; $i < length($one); $i++ ){
    if(substr($one,$i,1) ne '-' && substr($two,$i,1) ne '-' && substr($one,$i,1) ne 'N' && substr($two,$i,1) ne 'N'){
      if(substr($one,$i,1) ne substr($two,$i,1)){
        $dist++;
      }
      $sites++;
    }
  }
  my @ret=($dist,$sites);
  return \@ret;
}

#raw distance between sequences of the same length, not counting gaps on either
sub distngap_aa{ 
  my ($one,$two) = @_;
  my $dist = 0;
  if(length($one) != length($two)){die("Seqs not of same length! 1st:$one\n2nd:$two\n")}
  for(my $i = 0; $i < length($one); $i++ ){
    if(substr($one,$i,1) ne substr($two,$i,1) && substr($one,$i,1) ne '-' && substr($two,$i,1) ne '-'){
      $dist++;
    }
  }
  return($dist);
}

#Position of differences between two strings
sub diffPos{
  my $s1 = $_[0];
  my $s2 = $_[1];
  if(length($s1) != length($s2)){die("$s1 $s2 no same length")}
  my @diffpos;
  for(my $i = 0; $i < length($s1); $i++){
    if(substr($s1,$i,1) ne substr($s2,$i,1)){
      push(@diffpos,$i);
    }
  }
  return(\@diffpos);
}

#raw distance between sequences of the same length, not counting gaps
sub ardistngap{ 
  my ($first,$second) = @_;
  my @one = @{$first};
  my @two = @{$second};
  my $dist = 0;
  if(scalar(@one) != scalar(@two)){die("Not of equal length:\n@one\n\n@two")}
  for(my $i = 0; $i < scalar(@one); $i++ ){
     if($one[$i] ne "-" && $two[$i] ne "-" && $one[$i] ne "N" && $two[$i] ne "N"){
      if($one[$i] ne $two[$i]){
        $dist++;
      }
    }
  }
  return($dist);
}


#read in an unrooted newick tree
#and convert to tree rooted at leftmost taxa
sub readInUnrootedNewick{
  my $in = $_[0];
  my $rootname = $_[1];
  my $printtree = $_[2];
  if($in =~ /\($rootname\:(\d+\.*\d*[Ee]*-*\d*),/){
    #extract leftmost taxa as the root
    my $rdist = $1;
    my $next = "(".$';#remove root taxon
    my $oin = $next;

    my %root; 
    my %rright;
    my %rleft;
  
    #read in the rest of the tree as rooted
    $rright{"dist"}=0;
    rootedNewick($next,\%rright,0);
    getdists(\%rright); #parse distance

    my $t = printtreestring(\%rright,"").";";
    if($t ne $oin){print "Tree read in incorrectly!\nOriginal Tree:\n$oin\nRead tree:\n$t\n";die();}
    else{print "Tree read in correctly\n";}
  
    #set up root node
    $root{"dist"} = 0;
    $root{"id"}="";
    $root{"level"} = -1;
    $rright{"dist"} = $rdist;
    $root{"right"} = \%rright;

    $rleft{"dist"}= 0.0000000000000000000001;
    $rleft{"level"}= 0;
    $rleft{"id"}=$rootname;
    $root{"left"} = \%rleft;

    relevel(\%root,1); #increase levels by 1

    getdivergence(\%root,0); #get divergences

    #optionally print out trees to make sure everything is good
    if($printtree){
      $t = printtreestring(\%root,"").";";
      print("$in\n$oin\n$t\n");
    }
    return(\%root);
  }else{die("Input tree not formatted correctly - root is not leftmost taxa.\n");}
}

sub relevel{
  my $node = $_[0];
  my $increase = $_[1];
  if(exists($node->{"right"})){
    relevel($node->{"right"},$increase);
    relevel($node->{"left"},$increase);
  }
  $node->{"level"}=$node->{"level"}+$increase;
}

#Read in a rooted Newick tree
sub rootedNewick {
  my $in = $_[0];
  my $node = $_[1];
  my $level = $_[2];
  my $first; my $id;
  if($in =~ /(,|\(|\)|;)/){
    $first = $&;
    $node->{"id"} = $`;
    $node->{"level"} = $level;
    $in = $';
    if($first eq ","){$in = $first.$in}
  }else{
    die($in);
  }
  if($first eq "("){#left
    my %n;
    $node->{"left"} = \%n;
    $in = rootedNewick($in,\%n,$level+1);
  }
  elsif($first  eq ","){#up
    return($in);
  }
  elsif($first  eq ")"){#up
    return($in);
  }
  elsif($first  eq ";"){#up
    return($in);
  }
  my $second;
  my $pre;
  if($in =~ /(,|\(|\)|;)/){
    $second = $&;
    $in = $';
    $pre = $`;
  }else{
    die($in);
  }
  if($second eq ","){#right
    my %n;
    $node->{"right"} = \%n;
    $in = rootedNewick($in,\%n,$level+1);
  }elsif($second  eq ")"){#up
  }
  if($in =~ /(,|\(|\)|;)/){
    $node->{"id"} = $`;
    $in = $';
    if($& eq ","){$in = $&.$'}
  }else{
    die($in);
  }
  return($in);
}


#read in a rooted newick tree
sub readInRootedNewick{
  my $in = $_[0];
  my $printtree = $_[1];
  my $alrt = $_[2];
  if(!defined($alrt)){$alrt=0}

   my %root; #set up root node
   my $oin = $in;
   $root{"dist"}=0;

   my $nin = "";
   if($alrt == 2){
    while($in =~ /(\[?\&?AA\_\d+\=[A-Z][a-z]+)([,|\]])/){
      $nin.=$`.$1;
      if($2 eq "]"){$nin.="]"}
      else{$nin.="%";}
      $in=$';
    }
    $nin.=$in;
    $in=$nin;
    $nin="";
    while($in =~ /(\[?\&?aLRT\=-?\d+\.\d+)([,|\]])/){
      $nin.=$`.$1;
      if($2 eq "]"){$nin.="]"}
      else{$nin.="%";}
      $in=$';
    }
    $nin.=$in;
    $in=$nin;
    $nin="";
    while($in =~ /(\[?\&?CO\_\d+\=[A-Z]+)([,|\]])/){
      $nin.=$`.$1;
      if($2 eq "]"){$nin.="]"}
      else{$nin.="%";}
      $in=$';
    }
    $nin.=$in;
    $in=$nin;
   }
   if($alrt == 3){
    while($in =~ /(\[\&\!name\=\"1\"\])/){
      $nin.=$`.$1;
      $in=$';
    }
    $nin.=$in;
    $in=$nin;
    $nin="";
   }
   if($alrt == 4){
    while($in =~ /(#\d+)/){
      $nin.=$`.$1;
      $in=$';
    }
    $nin.=$in;
    $in=$nin;
    $nin="";
   }
   if($alrt == 5){
    #print "trying\n";
    while($in =~ /(\[\&pZero\=\d?.?\d+),cZero\=\d+,cOne\=\d+,oLab=\d+,num=\d+]/){
      #print "$1\n";
      $nin.=$`.$1."]";
      $in=$';
    }
    $nin.=$in;
    $in=$nin;
    $nin="";
   }
    if($alrt == 6){
    #print "trying\n";
    while($in =~ /\[\&CDRdiff=-?\d*.?\d+,LR=-?\d*.?\d+,Num=-?\d+,altid=\"\S{1,11}\"\]/){
      #print "$1\n";
      my $m=$&;
      my $post=$';
      my$pre=$`;
      $m=~s/,/|/g;
      $nin.=$pre.$m;
      $in=$post;
    }
    $nin.=$in;
    $in=$nin;
    $nin="";
   }
   if($alrt == 7){
    #print "trying\n";
    while($in =~ /\[\&Num\=(\d+\_\d+)\]/){
      #print "$1\n";
      my $m=$&;
      my $post=$';
      my$pre=$`;
      $m=~s/,/|/g;
      $nin.=$pre.$m;
      $in=$post;
    }
    $nin.=$in;
    $in=$nin;
    $nin="";
   }
   if($alrt == 8){
    #print "trying\n";
    while($in =~ /\[\&state=(\S+?),Num\=(\d+?)\]/){
      #print "$1\n";
      my $m=$&;
      my $post=$';
      my$pre=$`;
      $m=~s/,/|/g;
      $nin.=$pre.$m;
      $in=$post;
    }
    $nin.=$in;
    $in=$nin;
    $nin="";
   }
   rootedNewick($in,\%root,0);
   getdists(\%root,$alrt); #parse distance
   getdivergence(\%root,0); #get divergences

   my $t = printtreestring(\%root,"",$alrt).";";
   #print "$t\n";
   #if($alrt eq 0){
   # if($t ne $oin){print "Tree read in incorrectly!\nOriginal Tree:\n$oin\nRead tree:\n$t\n";die();}
   # else{print "Tree read in correctly\n";}
   #}
  
   if($printtree){
    print "processed tree:\n";
     $t = printtreestring(\%root,"").";";
     print("$oin\n$t\n");
   }
   return(\%root);
}

#Once tree is read in, need to get the branch lengths
sub getdists{
  my $node = $_[0];
  my $alrt = $_[1];
  if(!defined($alrt)){$alrt=0;}
  if(exists($node->{"left"})){
    getdists($node->{"left"},$alrt);
    getdists($node->{"right"},$alrt);
  }
  if(!exists($node->{"id"})){
    die("Node ID doens't exist!");
  }else{
    if($node->{"id"}=~/\:/){
      $node->{"dist"} = $';
      my $pre = $`;
      if($alrt==1){
        if($pre =~ /^(-?\d+\.?\d+)#(\d+)$/){
          $node->{"alrt"}=$1;
          $node->{"label"}=$2;
          #print "$1\t$2\n";
          $pre="";
        }elsif($pre =~ /^(\S*)#(\d+)$/){
          $pre=$1;
          $node->{"label"}=$2;
          $node->{"alrt"}="";
        }elsif($pre =~ /^(-?\d+\.?\d+)$/){
          $node->{"alrt"}=$1;
          $pre="";
        }else{
          $node->{"alrt"}="";
        }
      }elsif($alrt==2){
        if($pre =~ /(\S*)(\[.*\])/){
          $node->{"aas"}=$2;
          $pre="$1";
        }else{
          $node->{"aas"}="";
        }
      }
      elsif($alrt==3){
        if($pre =~ /(\S*)\[\&\!name\=\"(\d+)\"\]/){
          $node->{"olab"}=$2;
          $pre="$1";
        }else{
          $node->{"olab"}="";
        }
      }
      elsif($alrt==4){
        if($pre =~ /(\S*)#(\d+)/){
          $node->{"olab"}=$2;
          $pre="$1";
        }else{
          $node->{"olab"}="";
        }
      }
      elsif($alrt==5){
        #print "Pre: $pre\n";
        #die();
        if($pre =~ /(\S*)\[\&pZero\=(-?\d?.?\d+)]/){
          $node->{"olab"}=$2;
          $pre="$1";
        }else{
          $node->{"olab"}="";
        }
      }
       elsif($alrt==6){
        #print "Pre: $pre\n";
        #die();
        if($pre =~ /(\S*)(\[\&CDRdiff=-?\d*.?\d+\|LR=-?\d*.?\d+\|Num=-?\d+\|altid=\"\S{1,11}\"\])/){
          $node->{"olab"}=$2;
          $pre="$1";
        }else{
          $node->{"olab"}="";
        }
      }
      elsif($alrt==7){
        if($pre =~ /(\S*)\[\&Num=(\d+\_\d+)\]/){
          $node->{"enum"}=$2;
          $pre="$1";
        }else{
          $node->{"enum"}="";
        }
      }
      elsif($alrt==8){
        if($pre =~ /(\S*)\[\&state=(\S+?)\|Num\=(\d+?)\]/){
          #print "$2\n";
          $node->{"pstate"}=$2;
          $pre="$1";
        }else{
          die("\n$pre\n");
         # $node->{"pstate"}="";
        }
      }
      $node->{"id"} = $pre;
      #print $node->{"label"}."\n";
      #if(!exists($node->{'left'})){
      #print "$pre\n";  
      #}
      
      #print exists($node->{'left'})."\n";
      #print exists($node->{'right'})."\n";
      if($node->{"dist"} == 0){
        $node->{"dist"} = 0.0000000000000000000001;#print "zero length branch length\n";
      }
    }else{
      if($node->{"level"} != 0){
        die($node->{"id"}." level ".$node->{"level"}." is formatted incorrectly!");
      }
    }
  }
}

sub setUnlabled{
  my $node=$_[0];
  if($node->{"level"} != 0){
    if(!exists($node->{"olab"})){
      $node->{"olab"}=0;
    }elsif($node->{"olab"} eq ""){
        $node->{"olab"}=0;
      }
  }
  if(exists($node->{"left"})){
    setUnlabled($node->{"left"});
    setUnlabled($node->{"right"});
  }
}

#re-root the tree at a given node
sub reRoot{
  my $oldroot= $_[0]; #current root
  my $new = $_[1]; #id of new root

  my $old = $oldroot->{"left"};
  #get reference to new root node
  my $root = getnode($oldroot,$new);

  #add blank zero length node to new root
  my %blank;
  $blank{"right"}=$root->{"up"}; #tree is right of new
  $blank{"left"}=$root;  #root is left of new
  $root->{"up"}=\%blank;  #blank is up of root
  #re-assign blank to either left or right of tree, depending on where root was
  if(exists($blank{"right"}->{"left"})){
    print $blank{"right"}->{"left"}."\t$root\n";
    if($blank{"right"}->{"left"} eq $root){ 
      print "new root on left\n";
      $blank{"right"}->{"left"} = \%blank;
    }
  }
  if(exists($blank{"right"}->{"right"})){
    if($blank{"right"}->{"right"} eq $root){
      print "new root on right\n";
      $blank{"right"}->{"right"} = \%blank;
    }
  }
  my $dist = $root->{"dist"};
  $blank{"dist"}=$dist;
  $root->{"dist"}=0.0000000000000000000001;

  $root->{"level"}=1;
  $blank{"level"}=0;
  $blank{"id"}="blank";
  $blank{"subtaxa"}="NONE";
  
  #remove zero-length node from old root
  $old->{"up"}->{"right"}->{"up"}=$old;
  $old->{"dist"}=$oldroot->{"right"}->{"dist"};

  #change, left, right, and anc branches for each node
  reRoot_recurse($blank{"right"},\%blank,0,$dist);

  #re-calculate divergence and subtaxa
  getdivergence(\%blank,0); #get divergences

  return(\%blank);
}

sub reRoot_recurse{
  my $node = $_[0];
  my $anc = $_[1];
  my $level = $_[2];
  my $dist = $_[3];

  print $node->{"subtaxa"}."\t".$anc->{"subtaxa"}."\t".$node->{"dist"}."\n";

  my $oanc = $node->{"up"};
  $node->{"level"}=$level;
  $level++;
  my $ndistl; my $ndistr;
  if(exists($node->{"left"})){#if internal node
    my $oanc = $node->{"up"};
    my $ol = $node->{"left"};
    my $or = $node->{"right"};
    if($node->{"left"} eq $anc){
      $node->{"right"}=$oanc;
      $node->{"left"}=$or;
      $ndistr=$node->{"dist"};
      $ndistl=$node->{"left"}->{"dist"};
    }elsif($node->{"right"} eq $anc){
      $node->{"left"}=$oanc;
      $node->{"right"}=$ol;
      $ndistl=$node->{"dist"};
      $ndistr=$node->{"right"}->{"dist"};
    }elsif($node->{"up"} eq $anc){
      $node->{"left"}=$ol;
      $node->{"right"}=$or;
      $ndistl=$node->{"left"}->{"dist"};
      $ndistr=$node->{"right"}->{"dist"};
    }else{die("something weird happened")}
    reRoot_recurse($node->{"left"},$node,$level,$ndistl);
    reRoot_recurse($node->{"right"},$node,$level,$ndistr);
  }
  $node->{"dist"}=$dist;
  $node->{"up"}=$anc;
}


#re-root the tree at a given node
sub reRoot_internal{
  my $oldroot= $_[0]; #current root
  my $root = $_[1]; #hash reference of new root

  my $old = $oldroot->{"left"};

  #add blank zero length node to new root
  my %blank;
  $blank{"right"}=$root->{"up"}; #tree is right of new
  $blank{"left"}=$root;  #root is left of new
  $root->{"up"}=\%blank;  #blank is up of root
  #re-assign blank to either left or right of tree, depending on where root was
  if(exists($blank{"right"}->{"left"})){
    print $blank{"right"}->{"left"}."\t$root\n";
    if($blank{"right"}->{"left"} eq $root){ 
      print "new root on left\n";
      $blank{"right"}->{"left"} = \%blank;
    }
  }
  if(exists($blank{"right"}->{"right"})){
    if($blank{"right"}->{"right"} eq $root){
      print "new root on right\n";
      $blank{"right"}->{"right"} = \%blank;
    }
  }
  my $dist = $root->{"dist"};
  $blank{"dist"}=$dist;
  $root->{"dist"}=0.0000000000000000000001;

  $root->{"level"}=1;
  $blank{"level"}=0;
  $blank{"id"}="blank";

  #remove zero-length node from old root
  $old->{"up"}->{"right"}->{"up"}=$old;
  $old->{"dist"}=$oldroot->{"right"}->{"dist"};
  $blank{"subtaxa"}="NONE";

  #change, left, right, and anc branches for each node
  reRoot_recurse($blank{"right"},\%blank,0,$dist);
  reRoot_recurse($blank{"left"},\%blank,0,0.0000000000000000000001);

  #re-calculate divergence and subtaxa
  return(\%blank);
}



#Get the divergences for each node in the tree
sub getdivergence{
  my $node = $_[0];
  my $div = $_[1];
  $node->{"divergence"} = $div + $node->{"dist"};
  if(exists($node->{"left"})){
    getdivergence($node->{"left"},$node->{"divergence"});
    getdivergence($node->{"right"},$node->{"divergence"});
  }
}

#Print out the tree in Newick format to a string
sub printtreestring{
  my $node = $_[0];
  my $string = $_[1];
  my $alrt = $_[2];
  if(!defined($alrt)){$alrt=0}
  if(exists($node->{"left"})){
    $string = $string."(";
    $string=printtreestring($node->{"left"},$string,$alrt);
    $string = $string.",";
    $string=printtreestring($node->{"right"},$string,$alrt);
    $string=$string.")";
  }
  if($node->{"level"} != 0){
    if($alrt == 0){
      $string = $string.$node->{"id"}.":".$node->{"dist"};
    }elsif($alrt == 1){
      $string = $string.$node->{"id"}.$node->{"alrt"}.":".$node->{"dist"};
    }else{
        $string = $string.$node->{"id"}.":".$node->{"dist"};
    }
  }
  return($string);
}

sub printtreestring_label{
  my $node = $_[0];
  my $string = $_[1];
  my $alrt = $_[2];
  if(!defined($alrt)){$alrt=0}
  if(exists($node->{"left"})){
    $string = $string."(";
    $string=printtreestring_label($node->{"left"},$string,$alrt);
    $string = $string.",";
    $string=printtreestring_label($node->{"right"},$string,$alrt);
    $string=$string.")";
  }
  if($node->{"id"} ne ""){$node->{"id"} .= "_".$node->{"sequence"}->[0];}
  if($node->{"level"} != 0){
    if($alrt == 0){
      $string = $string.$node->{"id"}.":".$node->{"dist"};
    }elsif($alrt == 1){
      #$string = $string.$node->{"id"}."_".$node->{"sequence"}->[0].$node->{"alrt"}.":".$node->{"dist"};
      die();
    }else{
        $string = $string.$node->{"id"}.":".$node->{"dist"};
    }
  }
  return($string);
}

#Print out the tree in Newick format to a string
#sub printtreestring_freq{
#  my $node = $_[0];
#  my $string = $_[1];
#  if(exists($node->{"left"})){
#    $string = $string."(";
#    $string=printtreestring_freq($node->{"left"},$string);
#    $string = $string.",";
#    $string=printtreestring_freq($node->{"right"},$string);
#    $string=$string.")";
#  }
#  if($node->{"level"} != 0){
#    $string = $string.$node->{"id"}."[&WRC=".$node->{"WRC_c"}.",WRC_count=".$node->{"WRC_count"}.",GYW=".$node->{"GYW_c"}.",GYW_count=".$node->{"GYW_count"}.",WA=".$node->{"WA_c"}.",WA_count=".$node->{"WA_count"}.",TW=".$node->{"TW_c"}.",TW_count=".$node->{"TW_count"}.",SYC=".$node->{"SYC_c"}.",SYC_count=".$node->{"SYC_count"}.",GRS=".$node->{"GRS_c"}.",GRS_count=".$node->{"GRS_count"}.",WRCGYW=".($node->{"WRC_c"}+$node->{"GYW_c"}).",WATW=".($node->{"WA_c"}+$node->{"TW_c"}).",SYCGRS=".($node->{"SYC_c"}+$node->{"GRS_c"})."]:".$node->{"dist"};
#  }
#  return($string);
#}


#print out for figtree
sub printtreestring_figtree{
  my $node = $_[0];
  my $seqsf = $_[1];
  my $file = $_[2];

  my $seqs = getfasta($seqsf);
  my @keys = keys %$seqs;
  open(FIG,">$file") or die($file);
  print FIG "#NEXUS\nBegin taxa;\n\tDimensions ntax=".scalar(@keys).";\n\tTaxlabels\n";
  foreach my $k (@keys){
    print FIG "\t\t$k\n";
  }
  print FIG "\t\t;\nEnd;\nBegin trees;Tree TREE1 = [\&R] ";
  my $t="";
  my $tree = printtreestring_freq($node,$t);
  print FIG "$tree;\nEnd;";

}

#print out for figtree
sub printtreestring_joint_figtree{
  my $node = $_[0];
  my $seqs = $_[1];
  my $file = $_[2];
  my $codons = $_[3];
  #my $seqs = getfasta($seqsf);
  my @keys = keys %$seqs;
  open(FIG,">$file") or die($file);
  print FIG "#NEXUS\nBegin taxa;\n\tDimensions ntax=".scalar(@keys).";\n\tTaxlabels\n";
  foreach my $k (@keys){
    print FIG "\t\t$k\n";
  }
  print FIG "\t\t;\nEnd;\nBegin trees;Tree TREE1 = [\&R] ";
  my $t="";
  my $tree = printtreestring_joint($node,$t,length($seqs->{$keys[0]})/3,$codons);
  print FIG "$tree;\nEnd;";

}

sub printtreestring_joint{
  my $node = $_[0];
  my $string = $_[1];
  my $len=$_[2];
  my $codons=$_[3];
  if(exists($node->{"left"})){
    $string = $string."(";
    $string=printtreestring_joint($node->{"left"},$string,$len,$codons);
    $string = $string.",";
    $string=printtreestring_joint($node->{"right"},$string,$len,$codons);
    $string=$string.")";
  }
  if($node->{"level"} != 0){
    $string .= $node->{"id"}."[&C1=".$codons->[$node->{"joint"}->[0]];
    for(my$i=1;$i<$len;$i++){
      $string .= ",C$i=".$codons->[$node->{"joint"}->[$i]]
    }
    $string .= "]:".$node->{"dist"};
  }
  return($string);
}

#print out for figtree
sub printtreestring_freq_figtree{
  my $node = $_[0];
  my $seqs = $_[1];
  my $file = $_[2];
  my $codons = $_[3];
  #my $seqs = getfasta($seqsf);
  my @keys = keys %$seqs;
  open(FIG,">$file") or die($file);
  print FIG "#NEXUS\nBegin taxa;\n\tDimensions ntax=".scalar(@keys).";\n\tTaxlabels\n";
  foreach my $k (@keys){
    print FIG "\t\t$k\n";
  }
  print FIG "\t\t;\nEnd;\nBegin trees;Tree TREE1 = [\&R] ";
  my $t="";
  my $tree = printtreestring_freq($node,$t,length($seqs->{$keys[0]})/3,$codons);
  print FIG "$tree;\nEnd;";

}

sub printtreestring_freq{
  my $node = $_[0];
  my $string = $_[1];
  my $len=$_[2];
  my $codons=$_[3];
  if(exists($node->{"left"})){
    $string = $string."(";
    $string=printtreestring_freq($node->{"left"},$string,$len,$codons);
    $string = $string.",";
    $string=printtreestring_freq($node->{"right"},$string,$len,$codons);
    $string=$string.")";
  }
  if($node->{"level"} != 0){
    #print "$codons->[0]\t".$node->{"freqs"}->{$codons->[0]}."\n";
    $string .= $node->{"id"}."[&TTT=".$node->{"freqs"}->{$codons->[0]};
    for(my$i=1;$i<61;$i++){
      $string .= ",".(uc $codons->[$i])."=".$node->{"freqs"}->{$codons->[$i]};
    }
    $string .= "]:".$node->{"dist"};
  }
  return($string);
}

#Calculate hotspot frequency at each node
sub getHotSpotFreq{
  my $node = $_[0];
  my $codons = $_[1];
  my $motif = $_[2]; #Motif in IUPAC format
  
  #http://www.bioinformatics.org/sms/iupac.html
  my %iupac = (
    "A" => "A",
    "C" => "C",
    "G" => "G",
    "T" => "T",
    "R" => "[A|G]",
    "Y" => "[C|T]",
    "S" => "[G|C]",
    "W" => "[A|T]",
    "K" => "[G|T]",
    "M" => "[A|C]",
    "B" => "[C|G|T]",
    "D" => "[A|G|T]",
    "H" => "[A|C|T]",
    "V" => "[A|C|G]",
    "N" => "[A|C|G|T]"
  );
  
  my $regex = $iupac{substr($motif,0,1)};
  for(my $i = 1; $i < length($motif); $i++){
    $regex = $regex.$iupac{substr($motif,$i,1)}
  }
  print "$regex\n";

  getHotSpotFreq_recur($node,$codons,$regex,$motif);

}
sub getHotSpotFreq_recur{
  my $node = $_[0];
  my $codons = $_[1];
  my $regex = $_[2];
  my $motif = $_[3];#
  my $length = length($motif);

  if(exists($node->{"left"})){
    getHotSpotFreq_recur($node->{"left"},$codons,$regex,$motif,$length);
    getHotSpotFreq_recur($node->{"right"},$codons,$regex,$motif,$length);
  }

  if($node->{"level"}!=0){
  if(!exists($node->{"sequence"})){die($node->{"subtaxa"})}

  my @c = @$codons;
  my @seqtemp = @{untransarrayCodon($node->{"sequence"},$codons)};
  my $sequence = "";
  foreach my $s (@seqtemp){$sequence = $sequence.$s;}
  my @seq = split("",uc $sequence);
  my $count = 0;
  for(my $i=0;$i<(scalar(@seq)-$length+1);$i++){
    my $mer = "";
    for(my $j = 0; $j < $length; $j++){
      $mer .= $seq[$i+$j];
    }
    if($mer =~ /$regex/){
      $count++;
    }
  }
 # print "@seq\t$count\n";
  $node->{"$motif\_c"}=$count/length($sequence);
  $node->{"$motif\_count"}=$count;
  }
}


#translate codons to indexes in Q matrix
sub transarrayCodon{
  my @in = @{$_[0]};
  my %codoni = %{$_[1]};
  my @trans;
  for(my $i = 0; $i < scalar(@in); $i+=3){
    if(!exists($codoni{lc $in[$i].$in[$i+1].$in[$i+2]})){
      push(@trans,"NA");
    }else{
      push(@trans,$codoni{lc $in[$i].$in[$i+1].$in[$i+2]});
    }
  }
  return(\@trans);
}

#re-translate indexes to codons
sub untransarrayCodon{
  my @in = @{$_[0]};
  my @codons = @{$_[1]};
  my @trans;
  for(my $i = 0; $i < scalar(@in); $i++){
    if($in[$i] ne "NA"){
      push(@trans,$codons[$in[$i]]);
    }else{
      print "NA found $i\n";
      push(@trans,"NNN");
    }
  }
  return(\@trans);
}

#translate codons to indexes in Q matrix
sub transarrayChar{
  my @in = @{$_[0]};
  my %codoni = %{$_[1]};
  my @trans;
  for(my $i = 0; $i < scalar(@in); $i++){
    if(!exists($codoni{lc $in[$i]})){
      push(@trans,"NA");
    }else{
      push(@trans,$codoni{lc $in[$i]});
    }
  }
  return(\@trans);
}

#re-translate indexes to codons
sub untransarrayChar{
  my @in = @{$_[0]};
  my @codons = @{$_[1]};
  my @trans;
  for(my $i = 0; $i < scalar(@in); $i++){
    if($in[$i] ne "NA"){
      push(@trans,$codons[$in[$i]]);
    }else{
      print "NA found $i\n";
      push(@trans,"NNN");
    }
  }
  return(\@trans);
}

#translate sequence back to nucleotide
sub printseqsCodon{
  my $node = $_[0];
  my $string = $_[1];
  my $codons = $_[2];
  if(exists($node->{"left"})){
    $string = printseqsCodon($node->{"left"},$string,$codons);
    $string = printseqsCodon($node->{"right"},$string,$codons);
  }
  elsif($node->{"id"} ne ""){
    if(!exists($node->{"sequence"})){die($node->{"id"})}
    my @seq = @{untransarrayCodon($node->{"sequence"},$codons)};
    my $sequence = "";
    foreach my $s (@seq){$sequence = $sequence.$s;}
    $string = $string.">".$node->{"id"}."\n$sequence\n";
    #print "$string\n";
  }
  return($string);
}

#translate sequence back to nucleotide
sub printseqsGen{
  my $node = $_[0];
  my $string = $_[1];
  my $codons = $_[2];
  if(exists($node->{"left"})){
    $string = printseqsCodon($node->{"left"},$string,$codons);
    $string = printseqsCodon($node->{"right"},$string,$codons);
  }
  elsif($node->{"id"} ne ""){
    if(!exists($node->{"sequence"})){die($node->{"id"})}
    my @seq = @{untransarrayCodon($node->{"sequence"},$codons)};
    my $sequence = "";
    foreach my $s (@seq){$sequence = $sequence.$s;}
    $string = $string.">".$node->{"id"}."\n$sequence\n";
  }
  return($string);
}

sub seqHamming{
  my $node=$_[0];
  if(exists($node->{"up"})){
    my $l = length($node->{"ntsequence"})/3;
    my $h = distngap($node->{"ntsequence"}, $node->{'up'}->{'ntsequence'});
    print $node->{'up'}->{"subtaxa"}."\t".$node->{"subtaxa"}."\t".($h/$l)."\n";
  }
  if(exists($node->{"right"})){
    seqHamming($node->{"right"});
    seqHamming($node->{"left"});
  }
}

#Print sequences at all nodes
sub printAncSeqsCodon{
  my $node = $_[0];
  my $string = $_[1];
  my $codons = $_[2];
  if(exists($node->{"left"})){
    $string = printAncSeqsCodon($node->{"left"},$string,$codons);
    $string = printAncSeqsCodon($node->{"right"},$string,$codons);
  }

  if(!exists($node->{"sequence"})){die($node->{"subtaxa"})}
  my @seq = @{untransarrayCodon($node->{"sequence"},$codons)};
  my $sequence = "";
  foreach my $s (@seq){$sequence = $sequence.$s;}
  $string = $string.">".$node->{"level"}.";".$node->{"subtaxa"}.";".$node->{"divergence"}."\n$sequence\n";
  $node->{"ntsequence"}=$sequence;
  return($string);
}


#Print ML codon sequence at all nodes
sub printML_codon{
  my $node = $_[0];
  my $string = $_[1];
  my $codons = $_[2];
  my $rootonly = $_[3];
  if($rootonly == 1 && $node->{"level"} > 1){return $string;}

  if(exists($node->{"left"})){
    $string = printML_codon($node->{"left"},$string,$codons,$rootonly);
    $string = printML_codon($node->{"right"},$string,$codons,$rootonly);
  }

  if($node->{"level"}!=0){
  #print ">".$node->{"enum"}."\n";
  my @sequence;
  for(my $i=0; $i < scalar(@{$node->{"Codon_lhoods"}});$i++){
    my $sum;
    my $max = -inf;
    my $maxchar = -1;
    for(my $j=0; $j < 61; $j++){
      my $n = $node->{"Codon_lhoods"}->[$i][$j];
      if($n > $max){
        $max = $n; 
        $maxchar=$j;
      }
      if($j==0){$sum=$n}
      else{$sum+=log(1+exp($n-$sum));}
    }
    push(@sequence,$maxchar);
    my @test=($maxchar);
    my @seq = @{untransarrayCodon(\@test,$codons)};
    #print "$i\t".($seq[0])."\t".(exp($max-$sum))."\t$max\t$sum\n";
  }
  $node->{"sequence"} = \@sequence;
  
  my @seq = @{untransarrayCodon($node->{"sequence"},$codons)};
  my $sequence = "";
  foreach my $s (@seq){$sequence = $sequence.$s;}
  $string = $string.">".$node->{"level"}.";".$node->{"subtaxa"}.";".$node->{"divergence"}."\n$sequence\n";
  }

  return($string);
}

#Print ML codon sequence at all nodes
sub printMAR_codon{
  my $node = $_[0];
  my $string = $_[1];
  my $codons = $_[2];
  my $rootonly = $_[3];
  if($rootonly == 1 && $node->{"level"} > 1){return $string;}

  if(exists($node->{"left"})){
    $string = printMAR_codon($node->{"left"},$string,$codons,$rootonly);
    $string = printMAR_codon($node->{"right"},$string,$codons,$rootonly);
  }

  if($node->{"level"}!=0){
  my @sequence;
  for(my $i=0; $i < scalar(@{$node->{"Codon_lhoods"}});$i++){
    for(my $j=0; $j < 61; $j++){
      my $n = $node->{"Codon_lhoods"}->[$i][$j];
      $string .= $node->{"id"}."\t$i\t$j\t$n\n";
    }
  }
  }
  return($string);
}


#Print ML amino acid sequence at all nodes
sub printML_aa{
  my $node = $_[0];
  my $string = $_[1];
  my $codons = $_[2];
  my $aatable1 = $_[3];
  my $aatable3 = $_[4];
  my $rootonly = $_[5];

  if($rootonly == 1 && $node->{"level"} > 1){return $string;}
  if(exists($node->{"left"})){
    $string = printML_aa($node->{"left"}, $string,$codons,$aatable1,$aatable3,$rootonly);
    $string = printML_aa($node->{"right"},$string,$codons,$aatable1,$aatable3,$rootonly);
  }

  if($node->{"level"}!=0){
  my @sequence;
    for(my $i=0; $i < scalar(@{$node->{"Codon_lhoods"}});$i++){
      my %aas;
      for(my $j=0; $j < 61; $j++){ #tally up relative likelihoods of amino acids
        my $n = $node->{"Codon_lhoods"}->[$i][$j];
        my $aa = $aatable1->{uc $aatable3->{$codons->[$j]}}; #single letter amino acid
        if(exists($aas{$aa})){
          $aas{$aa} = $aas{$aa} + log(1+exp($n - $aas{$aa}))
        }else{
          $aas{$aa} = $n
        }
      }

      my $max = -inf;
      my $maxchar = -1;
      foreach my $key (keys %aas){
        my $n = $aas{$key};
        if($n > $max){
          $max = $n; 
          $maxchar=$key;
        }
      }
      push(@sequence,$maxchar);
    }
    #$node->{"sequence"} = \@sequence;
    my $sequence = "";
    foreach my $s (@sequence){$sequence = $sequence.$s;}
    $string = $string.">".$node->{"level"}.";".$node->{"subtaxa"}.";".$node->{"divergence"}."\n$sequence\n";
    }
  return($string);
}


sub getTotalLength {
  my $node = $_[0];
  my $dist = $_[1];
# print $node->{"level"}."\t".$node->{"dist"}."\t$dist\n";
  if(exists($node->{"left"})){
    $dist = getTotalLength($node->{"left"},$dist);
    $dist = getTotalLength($node->{"right"},$dist);
  }
  $dist += $node->{"dist"};
  return($dist);  
}

sub getInternalLength {
  my $node = $_[0];
  my $dist = $_[1];
# print $node->{"level"}."\t".$node->{"dist"}."\t$dist\n";
  if(exists($node->{"left"})){
    $dist += $node->{"dist"};
    $dist = getInternalLength($node->{"left"},$dist);
    $dist = getInternalLength($node->{"right"},$dist);
  }
  return($dist);  
}


#make subtaxa labels
sub getSubTaxa{
  my $node = $_[0];
  #if a tip
  if(!exists($node->{"right"})){
    $node->{"subtaxa"}=$node->{"id"};
  }else{
    #if internal node
    getSubTaxa($node->{"right"});
    getSubTaxa($node->{"left"});
    my @l = split(",",$node->{"left"}->{"subtaxa"});
    my @r = split(",",$node->{"right"}->{"subtaxa"});
    my @total = (@l,@r);
    @total = sort @total;
    $node->{"subtaxa"}=join(",",@total);
  }
}


#make subtaxa labels
sub getTaxa{
  my $node = $_[0];
  my $taxa = $_[1];
  #if a tip
  if(!exists($node->{"right"})){
    $taxa->{$node->{"id"}}=1;
   # print $node->{"id"}."\n";
  }else{
    #if internal node
    getTaxa($node->{"right"},$taxa);
    getTaxa($node->{"left"},$taxa);
  }
}

#get most likely ancestral sequence at each node
sub getMLAnc{
  my $node = $_[0];
  if(exists($node->{"right"})){
    getMLAnc($node->{"right"});
    getMLAnc($node->{"left"});
  }
  my @sequence;
  if($node->{"level"}!=0){
  for(my $i=0; $i < scalar(@{$node->{"mat"}});$i++){
    my $max = -inf;
    my $maxchar = -1;
    for(my $j=0; $j < 61; $j++){
      my $n = ($node->{"Codon_lhoods"}->[$i][$j]);
      if($n > $max){
        $max = $n; 
        $maxchar=$j;
      }
    }
    push(@sequence,$maxchar);
  }
  $node->{"sequence"} = \@sequence;
  }
}

sub translateSeqs{
  my $seqs = $_[0];
  my %tr = %{codonTable()};
  my %new;
  foreach my $k (keys %$seqs){
    my @s = split("",$seqs->{$k});
    my @n;
    for(my $i=0;$i < scalar(@s);$i+=3){
      push(@n,$tr{lc $s[$i].$s[$i+1].$s[$i+2]});
    }
    $new{$k}=join(",",@n);
  } 
  return \%new;
}

sub translateSeqsSingle{
  my $seqs = $_[0];
  my %tr = %{codonTable()};
  my %tr1 = %{codonTableSingle()};
  my %new;
  foreach my $k (keys %$seqs){
    my @s = split("",$seqs->{$k});
    my @n;
    for(my $i=0;$i < scalar(@s);$i+=3){
      push(@n,$tr1{uc $tr{lc $s[$i].$s[$i+1].$s[$i+2]}});
    }
    $new{$k}=join("",@n);
  } 
  return \%new;
}

#change a specified edge length
sub changeedge{
  my $node = $_[0];
  my $id = $_[1];
  my $length = $_[2];
  if(exists($node->{"left"})){
    changeedge($node->{"left"},$id,$length);
    changeedge($node->{"right"},$id,$length);
  }
  if(!exists($node->{"id"})){
    die("Node ID doens't exist!");
  }elsif($node->{"subtaxa"} eq $id){
    $node->{"dist"}=$length;
  }
}

#assign parents to each node
sub assignparents{
  my $node = $_[0];
  my $parent = $_[1];
  if($node->{"level"} != 0){
    $node->{"up"}=$parent;
  }
  if(exists($node->{"left"})){
    assignparents($node->{"left"},$node);
    assignparents($node->{"right"},$node);
  }#else{
   # print $node->{"id"}." 2\n";
  #}
}

#get a reference to a particular node
sub getnode{
  my $node = $_[0];
  my $id=$_[1];
  my $nid = "";
  if(exists($node->{"left"})){
    my $nid1 = getnode($node->{"left"},$id);
    my $nid2 = getnode($node->{"right"},$id);
    if($nid1 ne ""){$nid=$nid1}
    if($nid2 ne ""){$nid=$nid2}
  }
 if($node->{"id"} eq $id){
  return $node;
 }else{
  return $nid;
 }
}

#get a reference to a particular node
sub getnode_subtaxa{
  my $node = $_[0];
  my $id=$_[1];
  my $nid = "";
  if(exists($node->{"left"})){
    my $nid1 = getnode_subtaxa($node->{"left"},$id);
    my $nid2 = getnode_subtaxa($node->{"right"},$id);
    if($nid1 ne ""){$nid=$nid1}
    if($nid2 ne ""){$nid=$nid2}
  }
 # print $node->{"subtaxa"}."\n";
 if($node->{"subtaxa"} eq $id){
  return $node;
 }else{
  return $nid;
 }
}


#return codon translation table
sub codonTable{
  my %codons = (
"ttt" =>  "Phe",
"ttc" =>  "Phe",
"tta" =>  "Leu",
"ttg" =>  "Leu",
"ctt" =>  "Leu",
"ctc" =>  "Leu",
"cta" =>  "Leu",
"ctg" =>  "Leu",
"att" =>  "Ile",
"atc" =>  "Ile",
"ata" =>  "Ile",
"atg" =>  "Met",
"gtt" =>  "Val",
"gtc" =>  "Val",
"gta" =>  "Val",
"gtg" =>  "Val",
"tct" =>  "Ser",
"tcc" =>  "Ser",
"tca" =>  "Ser",
"tcg" =>  "Ser",
"cct" =>  "Pro",
"ccc" =>  "Pro",
"cca" =>  "Pro",
"ccg" =>  "Pro",
"act" =>  "Thr",
"acc" =>  "Thr",
"aca" =>  "Thr",
"acg" =>  "Thr",
"gct" =>  "Ala",
"gcc" =>  "Ala",
"gca" =>  "Ala",
"gcg" =>  "Ala",
"tat" =>  "Tyr",
"tac" =>  "Tyr",
"taa" =>  "STOP",
"tag" =>  "STOP",
"cat" =>  "His",
"cac" =>  "His",
"caa" =>  "Gln",
"cag" =>  "Gln",
"aat" =>  "Asn",
"aac" =>  "Asn",
"aaa" =>  "Lys",
"aag" =>  "Lys",
"gat" =>  "Asp",
"gac" =>  "Asp",
"gaa" =>  "Glu",
"gag" =>  "Glu",
"tgt" =>  "Cys",
"tgc" =>  "Cys",
"tga" =>  "STOP",
"tgg" =>  "Trp",
"cgt" =>  "Arg",
"cgc" =>  "Arg",
"cga" =>  "Arg",
"cgg" =>  "Arg",
"agt" =>  "Ser",
"agc" =>  "Ser",
"aga" =>  "Arg",
"agg" =>  "Arg",
"ggt" =>  "Gly",
"ggc" =>  "Gly",
"gga" =>  "Gly",
"ggg" =>  "Gly"
);
  return \%codons;
}

#return codon translation table
sub codonTableSingle{
  my %codons = (
"ALA" => "A",
"CYS" => "C",
"ASP" => "D",
"GLU" => "E",
"PHE" => "F",
"GLY" => "G",
"HIS" => "H",
"ILE" => "I",
"LYS" => "K",
"LEU" => "L",
"MET" => "M",
"ASN" => "N",
"PRO" => "P",
"GLN" => "Q",
"ARG" => "R",
"SER" => "S",
"THR" => "T",
"VAL" => "V",
"TRP" => "W",
"TYR" => "Y");
  return \%codons;
}



sub getS5F{
my %s5f = (
"TCGGG" =>  0.0250144523,
"GCCGG" =>  0.0362888192,
"GCCGC" =>  0.0582586814,
"CCCGC" =>  0.0623151321,
"CCGGG" =>  0.0627625532,
"GGGGC" =>  0.0634527827,
"AGGTC" =>  0.06345293,
"GCCGA" =>  0.0696285233,
"CTGAC" =>  0.0766843505,
"CAGGG" =>  0.0801581022,
"CAGAC" =>  0.0805308305,
"ATGGG" =>  0.0828673085,
"CCCCC" =>  0.0830714932,
"CCCGT" =>  0.083642048,
"CCCGG" =>  0.0882416581,
"GAGGG" =>  0.0891733497,
"GCCAA" =>  0.0901190198,
"CCCTA" =>  0.0911872337,
"GCCAT" =>  0.0937040517,
"CAGGC" =>  0.0945887871,
"GCCCT" =>  0.097294671,
"GCCCC" =>  0.0983114131,
"GTCCG" =>  0.098770713,
"TTGGG" =>  0.1033600867,
"GTCGA" =>  0.1051589036,
"GACGA" =>  0.1058588511,
"GCCGT" =>  0.1060624596,
"AAGGG" =>  0.1069855652,
"CCCTT" =>  0.1113928355,
"AAGGC" =>  0.1119998202,
"AGGAG" =>  0.1139108775,
"TCGGC" =>  0.114128521,
"CCGGC" =>  0.1206417656,
"GCGGG" =>  0.1222955703,
"AGGGG" =>  0.1252408748,
"GCCTC" =>  0.1264667087,
"GCCCG" =>  0.1280092753,
"GACTG" =>  0.1298460842,
"GTCAG" =>  0.1316025311,
"GGCAA" =>  0.1327246072,
"ACGGC" =>  0.1343814779,
"GAGGC" =>  0.1343814779,
"CGGGG" =>  0.1368420801,
"TTGGC" =>  0.1378270143,
"CCCAC" =>  0.1386658371,
"GCCCA" =>  0.1394836779,
"TGGGG" =>  0.1416350974,
"GTCAA" =>  0.1432194379,
"GGCCC" =>  0.1459616572,
"GCCAC" =>  0.1463445683,
"ATGGC" =>  0.1475845912,
"CTGTC" =>  0.1476266419,
"TGCGA" =>  0.1480104563,
"GACAG" =>  0.1482307055,
"CTGGC" =>  0.1486365711,
"GTCCT" =>  0.1489821838,
"CACGG" =>  0.1497013102,
"GGCCT" =>  0.1540302193,
"CTCAC" =>  0.1562221386,
"GTCAC" =>  0.1569090606,
"AAGTC" =>  0.1581742667,
"GCGAC" =>  0.1582053893,
"ATGAC" =>  0.1603850982,
"AAGCC" =>  0.1613039473,
"CCGCC" =>  0.1613039473,
"CTGCC" =>  0.1613039473,
"GACGG" =>  0.1618044266,
"GACAA" =>  0.162327615,
"TGGGC" =>  0.1624309966,
"GGCCA" =>  0.1634674236,
"GGGGG" =>  0.1635341261,
"CTTGT" =>  0.164961404,
"TAGGC" =>  0.1672547896,
"AGACT" =>  0.1675601784,
"TGGAC" =>  0.1691495944,
"CCCAA" =>  0.1693117159,
"GGGTC" =>  0.1699303832,
"TAGGG" =>  0.1711414864,
"ACCGT" =>  0.1729096911,
"TCGAG" =>  0.1737829015,
"TCCGG" =>  0.1740349714,
"ACGGG" =>  0.1754903737,
"GGCAT" =>  0.1757672164,
"ACGAC" =>  0.1770369832,
"CCCAG" =>  0.1775485101,
"CCCCA" =>  0.1785774399,
"GAGAC" =>  0.1788267229,
"CGGGC" =>  0.179391549,
"TTGAC" =>  0.1796135818,
"CTGGG" =>  0.1797430436,
"TGCAG" =>  0.1798569387,
"TCGCA" =>  0.1806923472,
"GGCGT" =>  0.1810607377,
"GGCGC" =>  0.1817733369,
"GCCTT" =>  0.1846237837,
"CCCGA" =>  0.1846700572,
"GTCTT" =>  0.1851644158,
"GGCTG" =>  0.1858013547,
"CCCAT" =>  0.1861497174,
"GTGGC" =>  0.1864726939,
"GGGCC" =>  0.1874502694,
"TGGCC" =>  0.1874502694,
"AGGGC" =>  0.1885912283,
"CCCCG" =>  0.1886673789,
"TCGAC" =>  0.1887608591,
"TCGGA" =>  0.1890501491,
"CCAGA" =>  0.1909151799,
"CTCCA" =>  0.1910049952,
"GCTGA" =>  0.1919286715,
"TTGAT" =>  0.1924272173,
"ACCGG" =>  0.194801402,
"CCCCT" =>  0.1954154414,
"GTCCA" =>  0.1954502257,
"AAGAC" =>  0.1954847114,
"CACCC" =>  0.2042971921,
"TAGAC" =>  0.2049378277,
"ACTGA" =>  0.2057792787,
"GCCTG" =>  0.2064277731,
"TTGGA" =>  0.2088401525,
"TGCGC" =>  0.2117034211,
"AGGCC" =>  0.2135965915,
"CAGCC" =>  0.2135965915,
"CGGAC" =>  0.2144205964,
"CGGTC" =>  0.2153538537,
"GTCTG" =>  0.2160840116,
"GGGAC" =>  0.2239774501,
"CCCTG" =>  0.224556144,
"GTCGT" =>  0.2256043535,
"GGCGG" =>  0.2325555759,
"CCGAC" =>  0.2328221666,
"ACCGA" =>  0.2332019965,
"TTGAA" =>  0.236964524,
"GTCTC" =>  0.2385321938,
"AGGAC" =>  0.2406563865,
"GGCCG" =>  0.2425496128,
"TCGGT" =>  0.2512265132,
"CACGT" =>  0.2527473387,
"CCTGC" =>  0.2538484082,
"GGCAG" =>  0.2551630761,
"TAGTC" =>  0.2574416036,
"GCAGC" =>  0.2622576367,
"GCGAG" =>  0.2649345785,
"AGTTA" =>  0.2659361722,
"GGTTG" =>  0.2659361722,
"CTTGA" =>  0.265976412,
"ACTCC" =>  0.2662381154,
"GCTCT" =>  0.2662381154,
"CTCGG" =>  0.2691040826,
"GGCTC" =>  0.2717914971,
"ACCAT" =>  0.2723347598,
"CTCTC" =>  0.2725906566,
"GTGAA" =>  0.2729554001,
"CCGTC" =>  0.2745928752,
"CTCTA" =>  0.2751321886,
"CTCTT" =>  0.2757731635,
"AGATC" =>  0.2767120864,
"GCCTA" =>  0.2768756814,
"GTGGG" =>  0.2785262228,
"AGGCA" =>  0.281719254,
"GCGGC" =>  0.2847122038,
"TCCGC" =>  0.2867730782,
"GCCAG" =>  0.2868216327,
"CTCCG" =>  0.2886288544,
"GGCTT" =>  0.2889441122,
"CTCGT" =>  0.2896158374,
"TCCAG" =>  0.2897032653,
"AGGCG" =>  0.2903367874,
"GTGCG" =>  0.2903367874,
"TCGTC" =>  0.2906579158,
"CTCGA" =>  0.2919997873,
"AAGAG" =>  0.2941414948,
"GCTCA" =>  0.294281828,
"ACTGT" =>  0.297539218,
"AGAAT" =>  0.3012677452,
"CTGGT" =>  0.303855176,
"AGAGC" =>  0.3071357547,
"AAGAA" =>  0.3076902305,
"GCTGG" =>  0.3078740395,
"CTCCT" =>  0.3093380059,
"GGAGG" =>  0.3096911461,
"GCGGA" =>  0.3127178613,
"ACCCT" =>  0.3134669085,
"TTTGT" =>  0.3152396511,
"ATCGC" =>  0.3159832058,
"CTCAT" =>  0.3195057647,
"TCTCG" =>  0.3202968959,
"GTTCA" =>  0.3203944154,
"TCCCT" =>  0.3209339765,
"CCTCG" =>  0.3209731108,
"TCCCC" =>  0.3223082122,
"TCTCT" =>  0.3223255406,
"GACTT" =>  0.3224905465,
"ACGGT" =>  0.3227970727,
"GACCT" =>  0.3239781042,
"CAGAA" =>  0.3241557573,
"AGAAC" =>  0.325037678,
"GTGAC" =>  0.325165562,
"CTCGC" =>  0.3256775988,
"TCCAT" =>  0.3287517003,
"GACGT" =>  0.3295671273,
"GAAGG" =>  0.3324592656,
"ACCGC" =>  0.334204039,
"GTTGG" =>  0.3366253535,
"GTCGC" =>  0.336677458,
"GTGTC" =>  0.3371784561,
"ATTGA" =>  0.343292514,
"TTGTC" =>  0.3436340205,
"CGCCA" =>  0.3456541914,
"CGCTT" =>  0.3456541914,
"AAGGA" =>  0.3479381527,
"ACTCG" =>  0.3483406084,
"ACTGG" =>  0.3488047567,
"CTCAG" =>  0.3496686626,
"ATCAT" =>  0.3502575991,
"AGTCT" =>  0.3531041134,
"GATCT" =>  0.3547072684,
"TATCT" =>  0.3567231783,
"ATCGG" =>  0.3576582118,
"CACTT" =>  0.3585551896,
"CTCCC" =>  0.3593457497,
"CACAA" =>  0.3611051579,
"CTCTG" =>  0.362011714,
"ACACA" =>  0.3652002084,
"AGTGC" =>  0.367483482,
"TTGGT" =>  0.3683600232,
"GCTGT" =>  0.3684755435,
"GTCCC" =>  0.3687113998,
"TCCGA" =>  0.369010738,
"CACAT" =>  0.3699331602,
"ACAGG" =>  0.3703328412,
"AGAAA" =>  0.3731236487,
"CACTC" =>  0.3735122189,
"TTTCC" =>  0.3739265864,
"TTTCG" =>  0.3739265864,
"TTTCT" =>  0.3739265864,
"TGTGT" =>  0.3743046934,
"GCTCC" =>  0.3743556763,
"GCGCC" =>  0.3757949276,
"GTGCC" =>  0.3757949276,
"TCGCC" =>  0.3757949276,
"TTGCC" =>  0.3757949276,
"CCTGA" =>  0.3769444378,
"GGAAC" =>  0.3770343045,
"GTCGG" =>  0.3774862375,
"GAGAG" =>  0.3777479869,
"ATCCA" =>  0.3819093593,
"CTGGA" =>  0.382890262,
"TCCTG" =>  0.3864454264,
"CGGAG" =>  0.3881162175,
"AGATT" =>  0.3898729736,
"AATCC" =>  0.39109782,
"ACCAC" =>  0.3962558353,
"ATGTC" =>  0.3972904367,
"TGGTC" =>  0.39879938,
"TGCAT" =>  0.3989368391,
"CTTCC" =>  0.3995308597,
"CTTCG" =>  0.3995308597,
"CTTCT" =>  0.3995308597,
"GACGC" =>  0.4007318235,
"GGTCC" =>  0.40094054,
"GTCAT" =>  0.4046903654,
"CCTCA" =>  0.4055071473,
"GACCC" =>  0.4055097989,
"TGGAG" =>  0.405534944,
"CTCAA" =>  0.4061968577,
"ATTCA" =>  0.408840159,
"ATTCC" =>  0.408840159,
"ATTCG" =>  0.408840159,
"ATTCT" =>  0.408840159,
"TATGA" =>  0.409852695,
"GCGCT" =>  0.4105360334,
"GATCA" =>  0.4116249281,
"CACCG" =>  0.4159119879,
"GACCG" =>  0.4165174158,
"GGCGA" =>  0.4173361755,
"AGAGT" =>  0.4215512549,
"GTGCA" =>  0.4217846967,
"TCTGA" =>  0.4226484365,
"ACGAG" =>  0.4230384946,
"GTTGC" =>  0.4252496731,
"CTTCA" =>  0.4274587574,
"CGATG" =>  0.4291480554,
"CAGAT" =>  0.4294621306,
"GGTCG" =>  0.4303237319,
"CCTCT" =>  0.4332343495,
"TCTCA" =>  0.4332343495,
"GACAT" =>  0.4341111658,
"GGTCA" =>  0.4364552336,
"TATGT" =>  0.4380505915,
"CAGAG" =>  0.4407400297,
"GCGTC" =>  0.4437981727,
"GACTC" =>  0.4438606133,
"CATCA" =>  0.4515979968,
"ACTCA" =>  0.4519301579,
"GGACA" =>  0.4523837471,
"GTTCC" =>  0.4530630308,
"GTTCG" =>  0.4530630308,
"GTTCT" =>  0.4530630308,
"AATCG" =>  0.4536510002,
"TCTGT" =>  0.4548577514,
"GAGTC" =>  0.4559068923,
"AGATA" =>  0.4569748613,
"ATGAG" =>  0.4584770002,
"CGTCT" =>  0.4603734469,
"ATCAA" =>  0.4610501242,
"GCGAA" =>  0.4616696505,
"TATCA" =>  0.4618630135,
"CATCC" =>  0.463707027,
"TAGAG" =>  0.4640262706,
"ATCGT" =>  0.4658336457,
"CACAC" =>  0.4662947261,
"CACCA" =>  0.4663447332,
"TATCC" =>  0.466526678,
"ATTTT" =>  0.4668103025,
"ACCAA" =>  0.4686136362,
"CCCTC" =>  0.4740438201,
"GCAGG" =>  0.4749234504,
"GGGGT" =>  0.475138619,
"TCTGG" =>  0.4754199989,
"TGTCT" =>  0.4760967481,
"TCACA" =>  0.4768783347,
"TTTCA" =>  0.4786673041,
"AATGA" =>  0.4826379196,
"ACGCC" =>  0.4830404177,
"ATGCC" =>  0.4830404177,
"ACACT" =>  0.4840674585,
"CGACG" =>  0.4844021938,
"TAGAA" =>  0.4854909198,
"TGGAA" =>  0.4859511538,
"CCGAA" =>  0.4862456298,
"ACGTC" =>  0.487336368,
"AGTGA" =>  0.4888710365,
"CCTCC" =>  0.4907173987,
"TGAGA" =>  0.4912339635,
"ACCCG" =>  0.4914414121,
"TAGGT" =>  0.4914938968,
"CATCG" =>  0.4916316081,
"GATCG" =>  0.4916618794,
"GCAAA" =>  0.4928010487,
"TCCCA" =>  0.4936755527,
"ACGAA" =>  0.4987775925,
"GTTGT" =>  0.5026883477,
"CCGAG" =>  0.5035131457,
"GGTGT" =>  0.5050593961,
"ACCCA" =>  0.505420966,
"CGTCA" =>  0.5075433504,
"GGATT" =>  0.5078190595,
"ACAAA" =>  0.5080819861,
"AGGAA" =>  0.5123746552,
"GGATA" =>  0.5143612963,
"TGATG" =>  0.5145452758,
"CGCTC" =>  0.5158613036,
"ATTGG" =>  0.5167315405,
"ACTCT" =>  0.5167324666,
"TGTTG" =>  0.5184642441,
"TTCAT" =>  0.5187978648,
"AATCT" =>  0.5189415181,
"AGTCG" =>  0.5198063537,
"GGCTA" =>  0.5200897716,
"GTGAG" =>  0.5201761636,
"TCCGT" =>  0.5209956879,
"TTTGC" =>  0.5238409554,
"CTGAG" =>  0.5253839887,
"CGGAA" =>  0.5264420253,
"TGTCG" =>  0.5282928099,
"TGTCA" =>  0.5313585607,
"AGAAG" =>  0.5315412679,
"CACGA" =>  0.5333930524,
"GCTGC" =>  0.5343808752,
"CGTCG" =>  0.5352092963,
"ACCCC" =>  0.5354721919,
"ATTGC" =>  0.537426337,
"TTCAC" =>  0.5381343434,
"TCCAA" =>  0.5384454604,
"TTAGA" =>  0.5397919147,
"CGGGA" =>  0.5411868093,
"AGTAA" =>  0.541226343,
"GGAGC" =>  0.5426250823,
"CCAGG" =>  0.5463411611,
"ATCAC" =>  0.5474107765,
"CGAAG" =>  0.5517166709,
"CGAGG" =>  0.5534664722,
"TTCGA" =>  0.5535587803,
"CGTGA" =>  0.5548209656,
"TATCG" =>  0.5548922597,
"TGTCC" =>  0.5580092633,
"CATCT" =>  0.5595898005,
"ACCAG" =>  0.5598370069,
"GCTTG" =>  0.5598433465,
"TGAAG" =>  0.5638656699,
"AGTGT" =>  0.5640806748,
"TGAAT" =>  0.5649704212,
"CGCCT" =>  0.5660377699,
"AATTA" =>  0.570327752,
"TTTGA" =>  0.5707392102,
"CTGAA" =>  0.5711009044,
"CCAAC" =>  0.5745362687,
"GCTTA" =>  0.5746744646,
"CGTCC" =>  0.5748312342,
"TGACG" =>  0.5760956812,
"TGCTA" =>  0.5767232535,
"GGACT" =>  0.5774627392,
"GCAGA" =>  0.578422142,
"ATTTA" =>  0.5798390948,
"CGAAA" =>  0.5810432685,
"TGTGG" =>  0.5824439406,
"CGGGT" =>  0.583053475,
"CACCT" =>  0.5835246581,
"TGAAA" =>  0.5890027535,
"GACAC" =>  0.5918617979,
"TGACA" =>  0.5925122243,
"CGACC" =>  0.5940245061,
"TGAGG" =>  0.5945947587,
"GCGGT" =>  0.5954922209,
"ATCCG" =>  0.595685417,
"GGATC" =>  0.5961264042,
"TTAGT" =>  0.5967427709,
"CTTGC" =>  0.5970221036,
"TGTGC" =>  0.5970849377,
"GTGGA" =>  0.5986255868,
"CCAGT" =>  0.6020667414,
"AGTTG" =>  0.6045790795,
"TTTGG" =>  0.6065692017,
"ACTTT" =>  0.6083417845,
"GGGAA" =>  0.6093539887,
"GGTGC" =>  0.6103231742,
"ACAAC" =>  0.6121117097,
"CGAGC" =>  0.6121275486,
"TTTTA" =>  0.612280488,
"TCACG" =>  0.6134850526,
"CGTGC" =>  0.6151865324,
"GGAGA" =>  0.6162396627,
"GTTTG" =>  0.616399682,
"GGACC" =>  0.6168773066,
"CCTGT" =>  0.616954227,
"CTTGG" =>  0.6184874516,
"ATCCC" =>  0.6187061491,
"GCACA" =>  0.6199789679,
"CCTTC" =>  0.6221800794,
"TCCTC" =>  0.6221996801,
"AACGC" =>  0.624482589,
"CACTA" =>  0.6250554649,
"GCAAT" =>  0.6252827508,
"AGTCA" =>  0.6262618878,
"AGGAT" =>  0.6277011133,
"GATCC" =>  0.6284348627,
"ATTGT" =>  0.6308902492,
"CGCAT" =>  0.6358919495,
"TGTGA" =>  0.6364050602,
"CGAAT" =>  0.6367596641,
"ACAGA" =>  0.6383358911,
"CACGC" =>  0.6385826206,
"GTGGT" =>  0.638670362,
"CAGGA" =>  0.6394477025,
"ATGGT" =>  0.6412813968,
"CCTTG" =>  0.6414893293,
"GTGCT" =>  0.6435224375,
"TGGGT" =>  0.6437602255,
"TATGC" =>  0.6452452106,
"TGGGA" =>  0.6452614198,
"CGTTT" =>  0.6461962952,
"TGAGT" =>  0.6469435875,
"CTTTC" =>  0.64815393,
"TCTAA" =>  0.6500528098,
"ACATC" =>  0.6507755864,
"AGGGA" =>  0.6509968605,
"TGACC" =>  0.6521653602,
"TTGAG" =>  0.652623672,
"AGATG" =>  0.6545705778,
"ATGAA" =>  0.6549103487,
"AGACG" =>  0.657572039,
"AAACT" =>  0.6589239961,
"CAACC" =>  0.6589239961,
"GCTCG" =>  0.6591092568,
"TCTCC" =>  0.6591092568,
"ACGCG" =>  0.6598640175,
"CAGCG" =>  0.6598640175,
"TAGCG" =>  0.6598640175,
"AGTAC" =>  0.6608639404,
"GCTAG" =>  0.6611595908,
"TCAGA" =>  0.6613731346,
"ACAAG" =>  0.6619624398,
"TCAAG" =>  0.6632232863,
"TGCCA" =>  0.6635143823,
"AGAGA" =>  0.6668385255,
"TGAAC" =>  0.668120396,
"GATTG" =>  0.6684170777,
"CGTGT" =>  0.6709127044,
"CGAAC" =>  0.6733471847,
"TCAGG" =>  0.6737382305,
"TTTTG" =>  0.6755531159,
"CTTTA" =>  0.6757035416,
"TCAAT" =>  0.6770171908,
"TGTTT" =>  0.678129308,
"AGTCC" =>  0.679563293,
"CAGGT" =>  0.6799718638,
"AATCA" =>  0.681534648,
"GACCA" =>  0.6818406302,
"TACGA" =>  0.6874228417,
"GTTTT" =>  0.6911943718,
"AGTGG" =>  0.6922043474,
"CCAAT" =>  0.6949485209,
"TAGGA" =>  0.6955844913,
"AGGGT" =>  0.7006529764,
"CTTTG" =>  0.7009524224,
"TACAC" =>  0.70145275,
"CGATC" =>  0.7018388984,
"CGATA" =>  0.7036426052,
"GAGCA" =>  0.7043619442,
"CAAGA" =>  0.7078901619,
"ACCTC" =>  0.7125656244,
"TCAAC" =>  0.7204291862,
"CACAG" =>  0.7216670665,
"TGATT" =>  0.7220536363,
"CCAGC" =>  0.7220667716,
"CCGGT" =>  0.7249900481,
"GCAAG" =>  0.7275405449,
"AAAGG" =>  0.7287406848,
"TCAAA" =>  0.7317808143,
"ATCAG" =>  0.7329064764,
"CGCGG" =>  0.735342504,
"AGACA" =>  0.7402021105,
"CGACT" =>  0.7416998709,
"TGATC" =>  0.7417876449,
"TGAGC" =>  0.7418876859,
"AAGTG" =>  0.7438885847,
"TCTTT" =>  0.7439551826,
"TTTTT" =>  0.7451340536,
"TGATA" =>  0.7512203597,
"AGACC" =>  0.7513074681,
"CGGCC" =>  0.752484244,
"GAGCC" =>  0.752484244,
"TAGCC" =>  0.752484244,
"CGGCG" =>  0.7544030513,
"TGGCG" =>  0.7544030513,
"GTTTC" =>  0.7577506735,
"CTTTT" =>  0.7594879524,
"GCTTT" =>  0.7634226168,
"AGCGC" =>  0.7673420794,
"CGCAG" =>  0.7684926888,
"ACAGC" =>  0.769352958,
"GCACC" =>  0.7745333998,
"GGTTA" =>  0.7783884183,
"CGTTC" =>  0.782310726,
"AAGAT" =>  0.7827213085,
"GGTGG" =>  0.7835292282,
"TCCCG" =>  0.7854390829,
"CCAAG" =>  0.786436968,
"AGTTT" =>  0.7868220748,
"CCACT" =>  0.788150468,
"TGACT" =>  0.7919923109,
"TTCAG" =>  0.7923529587,
"TTTTC" =>  0.8025496488,
"GCGTG" =>  0.8084996437,
"GGGTG" =>  0.8084996437,
"GCGAT" =>  0.8180468949,
"ATTTG" =>  0.819370905,
"ACCTT" =>  0.8212162275,
"CCGGA" =>  0.8215069319,
"GCTAA" =>  0.8233847264,
"CAGTC" =>  0.8241604429,
"GAGCG" =>  0.8253766983,
"ACGGA" =>  0.8254074238,
"ACAGT" =>  0.8287406857,
"TCAGT" =>  0.834377723,
"ATCTC" =>  0.8360079987,
"TGCTG" =>  0.8370966399,
"CACTG" =>  0.8376747905,
"CCAAA" =>  0.839780705,
"CCACG" =>  0.8419649609,
"CCGCG" =>  0.8460657834,
"TCGCG" =>  0.8460657834,
"GCATC" =>  0.8476990014,
"GACTA" =>  0.8480347401,
"TGTTC" =>  0.8482723465,
"CGAGT" =>  0.8483476062,
"TCTAG" =>  0.8506416897,
"GGTCT" =>  0.8515832356,
"ATGGA" =>  0.8527713104,
"ACTGC" =>  0.8538316438,
"GTGTG" =>  0.8605063205,
"TAGTG" =>  0.8605063205,
"TCACC" =>  0.8609327262,
"CTGTT" =>  0.8664192089,
"CGTGG" =>  0.8715655889,
"CAGTG" =>  0.8731107028,
"GGAAA" =>  0.882771831,
"GATGA" =>  0.8828017311,
"GATAA" =>  0.8859890556,
"ACTAT" =>  0.886139523,
"TCTGC" =>  0.8882871728,
"GGAGT" =>  0.8914142524,
"CTAGA" =>  0.8955090241,
"ACATA" =>  0.8963472849,
"ATTTC" =>  0.8978123927,
"GTTTA" =>  0.8978123927,
"CGCGC" =>  0.8981998357,
"GGTGA" =>  0.8982563502,
"TCCTT" =>  0.8984285111,
"ACACG" =>  0.9002043001,
"GGAAG" =>  0.9003137974,
"TCGAA" =>  0.9012373666,
"AGAGG" =>  0.9095689389,
"TGTAA" =>  0.9156350792,
"CGCGA" =>  0.9171862939,
"CCTAA" =>  0.9204130963,
"AAGGT" =>  0.9246210654,
"CCACC" =>  0.9281827983,
"AGCCT" =>  0.9287958202,
"TCTTG" =>  0.928990011,
"TATTC" =>  0.9354149124,
"TCCAC" =>  0.9357476532,
"GGGAG" =>  0.9367135205,
"CGATT" =>  0.9415313776,
"TGCAC" =>  0.941743086,
"CGACA" =>  0.9428696308,
"GTAGC" =>  0.9475606546,
"TCATG" =>  0.9489243589,
"TCATT" =>  0.9489243589,
"TATAA" =>  0.9572078929,
"ACTAC" =>  0.9589225099,
"CGTTG" =>  0.9630309671,
"CGCAC" =>  0.9637768072,
"GCTAT" =>  0.9692265917,
"ACTTC" =>  0.9763202176,
"GAGAA" =>  0.9781349802,
"GATGG" =>  0.9815805616,
"CGTTA" =>  0.9827106504,
"TCGTG" =>  0.9874690476,
"TGGTG" =>  0.9874690476,
"CATTC" =>  0.9904427875,
"GAGGT" =>  0.9926458135,
"GAAAG" =>  0.9990611034,
"AATGT" =>  0.999322774,
"TATTA" =>  1.0028562524,
"GAACA" =>  1.003348642,
"TAACA" =>  1.003348642,
"CCATA" =>  1.0038504651,
"CCATG" =>  1.0064899926,
"CCATT" =>  1.0064899926,
"TATTG" =>  1.00718427,
"CATTT" =>  1.0110449194,
"ACCTA" =>  1.0116708989,
"CATGT" =>  1.012203078,
"AGTTC" =>  1.0136794594,
"CGCCG" =>  1.0148390273,
"AGTAT" =>  1.016609517,
"TCACT" =>  1.0198035979,
"CCACA" =>  1.0201022176,
"CCGAT" =>  1.0203414063,
"CCGTG" =>  1.0255884958,
"CGGTG" =>  1.0255884958,
"ACTAG" =>  1.0285427182,
"ATGCG" =>  1.0293912476,
"GGGCG" =>  1.0293912476,
"TTGTG" =>  1.0297899566,
"CCTTA" =>  1.0311702116,
"TGCCT" =>  1.0314541066,
"GGACG" =>  1.0325137553,
"CCTGG" =>  1.0384577653,
"TTCTC" =>  1.0424723493,
"GCATA" =>  1.043819489,
"CATTA" =>  1.0450649518,
"ATCGA" =>  1.0466915516,
"TCAGC" =>  1.0491707812,
"TCTAT" =>  1.0499652655,
"GGAAT" =>  1.0510648178,
"AATTC" =>  1.0520877866,
"CATGG" =>  1.0521790215,
"CGCAA" =>  1.0578334662,
"CAAGC" =>  1.0579973115,
"ATAAG" =>  1.0600544378,
"GGTTC" =>  1.063613562,
"ACGTG" =>  1.0686625352,
"AGGTG" =>  1.0686625352,
"ACTTA" =>  1.070701587,
"CAAGT" =>  1.0730964208,
"GCTAC" =>  1.073614524,
"TGCCG" =>  1.0800434732,
"TGCTT" =>  1.0800434732,
"AACAT" =>  1.0810687092,
"CGGCA" =>  1.0838822824,
"ACCTG" =>  1.0846641084,
"TCATA" =>  1.0872217865,
"GCATG" =>  1.0906803796,
"GCATT" =>  1.0906803796,
"CGCGT" =>  1.0963564863,
"CTGTG" =>  1.1081295835,
"GAAGT" =>  1.1089157612,
"ATAGA" =>  1.1162538285,
"CTGCG" =>  1.1239302814,
"TTGCG" =>  1.1239302814,
"GTAGT" =>  1.127018298,
"TGTTA" =>  1.1374594525,
"AATAA" =>  1.1419058046,
"CGAGA" =>  1.1458020773,
"AAACA" =>  1.1488678258,
"CAACG" =>  1.1488678258,
"ACATG" =>  1.1595595034,
"ACATT" =>  1.1595595034,
"GCAAC" =>  1.1620863377,
"AATGC" =>  1.1636599483,
"GATTC" =>  1.1692448503,
"AAAAC" =>  1.1723553632,
"ATGTG" =>  1.1727406425,
"AAAGA" =>  1.174863899,
"TACCT" =>  1.1869770998,
"CGCCC" =>  1.1931024057,
"AACAG" =>  1.1943066655,
"TAAGG" =>  1.1965142314,
"ACTAA" =>  1.1965657658,
"AAACC" =>  1.202254104,
"CAACT" =>  1.202254104,
"GTTAT" =>  1.2061180053,
"CATGC" =>  1.2071827882,
"ACAAT" =>  1.2117599116,
"ACTTG" =>  1.2147160028,
"ATAAT" =>  1.2155472759,
"AATAC" =>  1.216324026,
"AAGCG" =>  1.2184693152,
"GCGCG" =>  1.2184693152,
"TAAGC" =>  1.2184904923,
"CTAGG" =>  1.2286235553,
"TACAT" =>  1.2289901793,
"TATAG" =>  1.2315671525,
"TTCCG" =>  1.2318521094,
"GAGTT" =>  1.2346866277,
"GATGC" =>  1.2383474527,
"TAAGT" =>  1.2491794081,
"GCACG" =>  1.2535682945,
"GATTA" =>  1.2558305592,
"CCTAT" =>  1.2623319099,
"TAAGA" =>  1.2675164965,
"CTGCA" =>  1.2711538933,
"TCGAT" =>  1.2752646486,
"ATTAA" =>  1.2826320867,
"GATAG" =>  1.2910118551,
"CATAC" =>  1.2977818435,
"TGGTT" =>  1.3028114173,
"CATAG" =>  1.3058156136,
"CCATC" =>  1.3059804327,
"AACAC" =>  1.3063706998,
"TACAA" =>  1.3079208516,
"CCTAC" =>  1.3084725386,
"ATAGC" =>  1.3145733445,
"AAAGT" =>  1.3149820825,
"TTAAA" =>  1.3183264808,
"GCTTC" =>  1.3215886934,
"GAACC" =>  1.3292763618,
"TAACC" =>  1.3292763618,
"TACAG" =>  1.3311559705,
"TGGAT" =>  1.3335416884,
"TGCCC" =>  1.3346080086,
"GTGAT" =>  1.3372497767,
"CTTAG" =>  1.3401561598,
"GATTT" =>  1.3432469215,
"GAACG" =>  1.3470717879,
"TAACG" =>  1.3470717879,
"AATTT" =>  1.3479663694,
"CTAAC" =>  1.3604369738,
"TATTT" =>  1.3643950719,
"GCGCA" =>  1.3660527339,
"CATGA" =>  1.3661778214,
"GAAGC" =>  1.3683901082,
"CGTAT" =>  1.3724556599,
"GCAGT" =>  1.3776632091,
"ATAGT" =>  1.383613004,
"CGCTA" =>  1.3937861051,
"CTAGC" =>  1.3987769344,
"GAATT" =>  1.4012627269,
"ACACC" =>  1.4134090183,
"GAGAT" =>  1.4166231654,
"TATAT" =>  1.418191126,
"GTTGA" =>  1.420812012,
"CTTAT" =>  1.4310344065,
"ATTAC" =>  1.4327358464,
"AATTG" =>  1.4380408136,
"AAAAG" =>  1.465044467,
"AATGG" =>  1.4675931903,
"TGGCA" =>  1.4716117074,
"GAGTG" =>  1.4723705823,
"CTAAT" =>  1.476394587,
"CTACA" =>  1.4768399822,
"GTACC" =>  1.4768399822,
"GTACT" =>  1.4768399822,
"AATAT" =>  1.4796336559,
"TTTAG" =>  1.4847172751,
"CGTAG" =>  1.4848313765,
"AGTAG" =>  1.490269831,
"TAAAT" =>  1.4965211147,
"GATAT" =>  1.4991733157,
"AAGTT" =>  1.5032470787,
"GTTAC" =>  1.5035628409,
"GAACT" =>  1.5103863978,
"TAACT" =>  1.5103863978,
"TCATC" =>  1.5130768075,
"AAAGC" =>  1.5149202685,
"CTAAG" =>  1.5152677966,
"TGTAG" =>  1.523087083,
"CATAT" =>  1.5234758119,
"ATCTA" =>  1.5274519926,
"GGGGA" =>  1.5326918858,
"CCTAG" =>  1.5341696643,
"CAATC" =>  1.5394289145,
"GAAAC" =>  1.5400349779,
"ATAAA" =>  1.5418511605,
"CTAGT" =>  1.5423430491,
"GAAAT" =>  1.5461951461,
"AGCTC" =>  1.5477324256,
"TTCTT" =>  1.5481514329,
"CGCTG" =>  1.5496291624,
"AACCG" =>  1.5501980935,
"TTCTG" =>  1.5567002788,
"TTAGG" =>  1.5578228533,
"TACCG" =>  1.5653354892,
"GTCTA" =>  1.5741461448,
"GCGTT" =>  1.5814138115,
"GCACT" =>  1.5818487018,
"TGCTC" =>  1.5833636929,
"ATACG" =>  1.5855406948,
"TTACG" =>  1.5855406948,
"ATCTT" =>  1.5861937266,
"CTGTA" =>  1.5875037607,
"GAGGA" =>  1.5899489533,
"TGCGT" =>  1.5991855518,
"TGTAT" =>  1.6000412139,
"CTTAA" =>  1.6005757081,
"TTCCA" =>  1.6060606106,
"CAGTT" =>  1.6068927536,
"GAAAA" =>  1.6076355719,
"GTAAC" =>  1.6115530162,
"TTAAC" =>  1.6141014172,
"GAAGA" =>  1.6144979862,
"CAAAT" =>  1.6203196547,
"GTAAA" =>  1.6242950211,
"TTCCT" =>  1.6311868857,
"TCTTA" =>  1.6347591216,
"GTAGG" =>  1.6357648861,
"GGATG" =>  1.6401663013,
"TATAC" =>  1.646742597,
"CAAAA" =>  1.6529544635,
"AACTT" =>  1.6544802581,
"ATGAT" =>  1.6553002738,
"TTTAA" =>  1.6623329682,
"CTGAT" =>  1.6693450085,
"TAAAA" =>  1.670605964,
"ATAAC" =>  1.6793777287,
"CCTTT" =>  1.6837324271,
"TTAGC" =>  1.6882147526,
"CTACG" =>  1.6942414075,
"GTACA" =>  1.6942414075,
"TCGTT" =>  1.6968491409,
"CGTAA" =>  1.6985420029,
"ACGTT" =>  1.7011527093,
"TACGC" =>  1.7038983409,
"AACCT" =>  1.7058646873,
"TTAAT" =>  1.7068675077,
"TTTAT" =>  1.7136350753,
"TACCC" =>  1.7148297192,
"GGGCT" =>  1.7152609339,
"TTCGT" =>  1.7244964648,
"GTAAT" =>  1.7275106294,
"GTTAG" =>  1.7292204253,
"AACTA" =>  1.7298186903,
"ATACC" =>  1.7351913706,
"ATACT" =>  1.7351913706,
"TTACC" =>  1.7351913706,
"TTACT" =>  1.7351913706,
"TTAAG" =>  1.7379660753,
"TACCA" =>  1.7427168231,
"GGTTT" =>  1.744185716,
"TTCCC" =>  1.7450624972,
"CGTAC" =>  1.7551504326,
"GTTAA" =>  1.755430783,
"ATAGG" =>  1.7581989089,
"GTGTT" =>  1.7601624622,
"AACGA" =>  1.7603354866,
"TTCGG" =>  1.7610113417,
"TAAAC" =>  1.7656009983,
"ATTAT" =>  1.7656272125,
"CATAA" =>  1.7660987287,
"GTAAG" =>  1.7663838389,
"CAAAC" =>  1.7796145093,
"TAAAG" =>  1.7923273544,
"AGCCA" =>  1.7947647963,
"TACGG" =>  1.8153403957,
"ATCTG" =>  1.8207327637,
"CAAAG" =>  1.8268012758,
"TTCGC" =>  1.8349444043,
"TTGTT" =>  1.8352541296,
"CCGTT" =>  1.8357638421,
"TATGG" =>  1.8381089928,
"TACGT" =>  1.8520813482,
"ATATA" =>  1.8576081042,
"AAATG" =>  1.8625366235,
"CTACC" =>  1.8643670648,
"CTACT" =>  1.8643670648,
"AAAAA" =>  1.8647150437,
"AGGTT" =>  1.868933361,
"TTTAC" =>  1.8766482749,
"TAGCT" =>  1.8830945568,
"TAGTT" =>  1.8832793816,
"AACGG" =>  1.8920935241,
"CAGCA" =>  1.8935033046,
"TCTTC" =>  1.9026546168,
"TGTAC" =>  1.9059253616,
"ATGTT" =>  1.9098360844,
"AAATC" =>  1.9109883398,
"TCTAC" =>  1.912909617,
"TTATC" =>  1.926758265,
"GGTAG" =>  1.9312587992,
"GAATC" =>  1.9477371191,
"TAATA" =>  1.9486591936,
"AATAG" =>  1.950438557,
"TACTT" =>  1.9525954598,
"GGTAA" =>  1.9535241995,
"TGCGG" =>  1.9802300077,
"AACGT" =>  1.9855648648,
"TAGAT" =>  1.9881453575,
"GCGTA" =>  2.0034808075,
"AAGCT" =>  2.0095957192,
"GGTAT" =>  2.0168716199,
"TAATT" =>  2.0221997332,
"ATACA" =>  2.0344927222,
"GTACG" =>  2.0344927222,
"TTACA" =>  2.0344927222,
"GGCAC" =>  2.0360168333,
"CAATA" =>  2.0509667634,
"CGGAT" =>  2.0579227048,
"CGGTT" =>  2.0615766554,
"TAATG" =>  2.0651628944,
"ATTAG" =>  2.0996720062,
"ATGCT" =>  2.1066182311,
"CGGCT" =>  2.1196752981,
"AACCC" =>  2.1279655767,
"TGGCT" =>  2.1354046145,
"GAATG" =>  2.1363556703,
"CAATT" =>  2.1428924379,
"AAATA" =>  2.1511265591,
"CAATG" =>  2.1527852244,
"GTATG" =>  2.1778496717,
"AAACG" =>  2.1800372636,
"CAACA" =>  2.1800372636,
"CAGCT" =>  2.2140511962,
"AACAA" =>  2.2337728978,
"TAATC" =>  2.2644964719,
"GTATA" =>  2.2737526349,
"TTCTA" =>  2.2936530874,
"AAGCA" =>  2.3000049351,
"TCGCT" =>  2.3111851479,
"AACTC" =>  2.3256749751,
"CCGCT" =>  2.3306119383,
"ATCCT" =>  2.3323230576,
"TACTC" =>  2.3767464621,
"AACCA" =>  2.4399831198,
"TTGCT" =>  2.4403174319,
"CTATG" =>  2.4592328561,
"GTAGA" =>  2.4633861913,
"TAGCA" =>  2.4673418356,
"CTGCT" =>  2.4780284711,
"GGGAT" =>  2.478515986,
"AACTG" =>  2.5266262935,
"AGCGG" =>  2.531993527,
"GAATA" =>  2.5617177335,
"ATATT" =>  2.5631245101,
"TACTA" =>  2.5636570285,
"TCCTA" =>  2.5685992371,
"TTATG" =>  2.5923515039,
"GTATT" =>  2.6155081473,
"CTTAC" =>  2.623051512,
"CTAAA" =>  2.6287591904,
"CTATC" =>  2.686614771,
"CTATT" =>  2.7041079757,
"GGGTT" =>  2.736188301,
"ATATC" =>  2.749182646,
"ACGAT" =>  2.788441626,
"ACGCA" =>  2.7896986457,
"TACTG" =>  2.8315550267,
"GATAC" =>  2.8504796238,
"TTATT" =>  2.8595779179,
"CAAGG" =>  2.8620656873,
"GGTAC" =>  2.8679949514,
"AAATT" =>  2.9438788679,
"TTGCA" =>  2.9674557655,
"AGGTA" =>  2.9685930103,
"TGCAA" =>  3.0187169295,
"CTATA" =>  3.0233217788,
"CCGCA" =>  3.0704194843,
"AGCTG" =>  3.0889474943,
"AGCCG" =>  3.0990818158,
"ATGCA" =>  3.1090617464,
"GATGT" =>  3.1568366233,
"AGCTA" =>  3.1735798231,
"AAAAT" =>  3.1950357235,
"AGGCT" =>  3.1980558398,
"AGCGT" =>  3.260910867,
"AGCGA" =>  3.2882742815,
"TTATA" =>  3.38884295,
"CATTG" =>  3.3968166637,
"AAGTA" =>  3.4039508668,
"TGGTA" =>  3.4064351347,
"ACGCT" =>  3.4480975395,
"GTATC" =>  3.4888626303,
"ATGTA" =>  3.5744315503,
"GAGTA" =>  3.6268305124,
"AGCTT" =>  3.7096182982,
"TCGTA" =>  3.7484879624,
"TAGTA" =>  3.7542245663,
"TTGTA" =>  3.7708096396,
"ATATG" =>  3.9284835738,
"CGGTA" =>  3.9522616044,
"AGCAC" =>  3.9608230785,
"AGCAA" =>  3.9928195946,
"GAGCT" =>  4.086624761,
"CAGTA" =>  4.1908857421,
"AGCAG" =>  4.2326198043,
"AGCCC" =>  4.2959999197,
"CCGTA" =>  4.4762565887,
"GTGTA" =>  4.5125778334,
"AGCAT" =>  4.5416574112,
"TTCAA" =>  4.5669497949,
"GGGTA" =>  5.145382293,
"ACGTA" =>  5.5221781371,
"GGGCA" =>  9.2781690515);
return \%s5f;
}


1