#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=120:00:00

module load Python/3.7.0-foss-2018b

STUDY=$1
NPROC=$2
MAB=$3

wdir=../data/processed/${STUDY} 
mkdir $wdir

echo "Running condenser"

python3 GeneralStudyCondenser.py --study ${STUDY}  --outdir ../processed/${STUDY} \
	--outfile ${STUDY}_Processed.fasta > ../processed/${STUDY}/log.txt

BASE=${STUDY}

echo "11/17/2020 run" >> ${wdir}/log.txt

if [ $MAB == 1 ]
then
    BASE=${BASE}_mab
    echo ${wdir}/${STUDY}_Processed.fasta
    cat ${wdir}/${STUDY}_Processed.fasta > ${wdir}/${BASE}_Processed.fasta
    cat ../../data/mabs/${STUDY}_processed.fasta >> ${wdir}/${BASE}_Processed.fasta
fi

echo $BASE

echo "assigning genes"
AssignGenes.py igblast --nproc ${NPROC} -s ${wdir}/${BASE}_Processed.fasta \
	-b ~/share/igblast --organism human --loci ig --format blast --outdir \
	${wdir} >> ${wdir}/log.txt

MakeDb.py igblast -i ${wdir}/${BASE}_Processed_igblast.fmt7 \
	-s ${wdir}/${BASE}_Processed.fasta \
	-r ~/share/germlines/imgt/human/vdj/imgt_human_IGHV.fasta \
	~/share/germlines/imgt/human/vdj/imgt_human_IGHD.fasta \
	~/share/germlines/imgt/human/vdj/imgt_human_IGHJ.fasta \
	--extended --outdir ${wdir} --outname ${STUDY}_Processed_igblast \
    >> ${wdir}/log.txt

#bash runClones.sh $STUDY $NPROC "genotyped"