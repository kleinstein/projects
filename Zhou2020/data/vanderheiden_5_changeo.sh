#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 20:00
#SBATCH --constraint E5-2660_v3
#SBATCH -c 12
#SBATCH --mem-per-cpu=10G

NPROC=12 #*

# note: adjust NPROC to match -c

# 12c x 10G
# Job Wall-clock time: 00:02:01
# Memory Utilized: 329.19 MB

# nx360h (E5-2660_v3) has a total max memory of 121GB per node

# IN: 
# - slide/[subj]_IGHV_FUNCTIONAL-T_genotyped_slideFiltered.tsv
# - changeo/distToNearest/thresh_all_density_full_NaiveMemoryOnly.txt
# - tag_201918-4_9May2019/human/vdj/imgt_human_IGH[DJ].fasta
# - tag_201918-4_9May2019/human/vdj/imgt_human_IGHV.fasta
# OUT: 
# - changeo/[subj]_[sort]_IGHV_FUNCTIONAL-T_genotyped_slideFiltered_parse-select.tab
# - changeo/[subj]_[sort]_clone-pass.tab, [subj]_[sort]_clone-fail.tab
# - changeo/[subj]_[sort]_germ-pass.tab, [subj]_[sort]_germ-fail.tab


# This script is designed to work when defining clones on a by-subject basis using 
# either a different threshold for each subject
# or a fixed threshold for all subjects

# the change-o commans in this file are intended to work with 0.4.5

# references
# https://stackoverflow.com/questions/9458752/variable-for-number-of-lines-in-a-file
# https://stackoverflow.com/questions/6022384/bash-tool-to-get-nth-line-from-a-file
# https://www.cyberciti.biz/faq/bash-loop-over-file/
# https://unix.stackexchange.com/questions/88216/bash-variables-in-for-loop-range

PATH_ROOT="/ysm-gpfs/home/qz93/scratch60/spatial_data/oconnor_mg/"
PATH_GENOTYPE="${PATH_ROOT}slide/"
PATH_DISTTN="${PATH_ROOT}changeo/distToNearest/"
PATH_CHANGEO="${PATH_ROOT}changeo/"

NAME_GENOTYPED="_IGHV_FUNCTIONAL-T_genotyped_slideFiltered"

LOG=${PATH_CHANGEO}changeo_$(date '+%m%d%Y_%H%M%S').log
COUNT_PATH="${PATH_CHANGEO}changeo_bySubj_count_$(date '+%m%d%Y_%H%M%S').txt"

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${LOG}"
echo "bash" $BASH_VERSION &>> "${LOG}"
ParseDb.py --version &>> "${LOG}"
DefineClones.py --version &>> "${LOG}"
CreateGermlines.py --version &>> "${LOG}"
python3 -V &>> "${LOG}"

# IMPORTANT: CHECK THIS BEFORE RUNNING
# flexible threshold from file or fixed threshold?
FLEX_THRESH=false #*
FIX_THRESH=0.1

# must set if FLEX_THRESH=true
#FN_THRESH="${PATH_DISTTN}distToNearest_thresh.txt" #* gamma-gamma
FN_THRESH="${PATH_DISTTN}thresh_all_density_full_NaiveMemoryOnly.txt" #* only NaiveMemory; no Unsorted
N_LINES=$(wc -l < "${FN_THRESH}")
echo "N_LINES="$N_LINES &>> "${LOG}"
# number of subjects = number of lines/3 (1 line each for subject, sort, and threshold)
N_SUBJ=$(expr ${N_LINES} "/" 3)
echo "N_SUBJ="$N_SUBJ &>> "${LOG}"

FN_IMGT_DJ="/ysm-gpfs/home/qz93/germlines/tag_201918-4_9May2019/human/vdj/imgt_human_IGH[DJ].fasta"
#FN_IMGT_V_NOVEL="/ysm-gpfs/home/qz93/germlines/tag_2018_03_05/imgt_human_IGHV_novel_mg_20180517.fasta" #*
FN_IMGT_V_NOVEL="/ysm-gpfs/home/qz93/germlines/tag_201918-4_9May2019/human/vdj/imgt_human_IGHV.fasta" #*

for ((SUBJ_IDX=1;SUBJ_IDX<=${N_SUBJ};SUBJ_IDX++)); do
# next line won't work; brace-expansion occurs before parameter expansion
#for SUBJ_IDX in {1..${N_SUBJ}}; do 
	
	echo "SUBJ_IDX="$SUBJ_IDX # print to standard output

	# calculate line number containing subject: (i-1)*3+1
	CUR_SUBJ_LINE=$(expr '(' ${SUBJ_IDX} - 1 ')' '*' 3 + 1)
	CUR_SORT_LINE=$(expr '(' ${SUBJ_IDX} - 1 ')' '*' 3 + 2)
	echo "CUR_SUBJ_LINE="${CUR_SUBJ_LINE} # print to standard output
	echo "CUR_SORT_LINE="${CUR_SORT_LINE} # print to standard output

	# read subject from file
	CUR_SUBJ=$(sed "${CUR_SUBJ_LINE}q;d" "${FN_THRESH}")
	echo "CUR_SUBJ="${CUR_SUBJ} # print to standard output

	# read sort from file
	CUR_SORT=$(sed "${CUR_SORT_LINE}q;d" "${FN_THRESH}")
	echo "CUR_SORT="${CUR_SORT} # print to standard output

	# get threshold
	if ${FLEX_THRESH}; then
		# calculate line number containing threshold: (i-1)*3+3
		CUR_THRESH_LINE=$(expr ${CUR_SUBJ_LINE} + 2)
		echo "CUR_THRESH_LINE="${CUR_THRESH_LINE} # print to standard output

		# read threshold from file
		CUR_THRESH=$(sed "${CUR_THRESH_LINE}q;d" "${FN_THRESH}")
		echo "subject-specific threshold; CUR_THRESH="${CUR_THRESH} # print to standard output
	else
		CUR_THRESH=${FIX_THRESH}
		echo "fixed threshold; CUR_THRESH="${CUR_THRESH} # print to standard output
	fi

	echo "starting on subject = "${CUR_SUBJ} "; sort = "${CUR_SORT} # print to standard output
	
	echo "subject = "${CUR_SUBJ} "; sort = "${CUR_SORT} &>> "${LOG}" # print to log
	
	echo "############## ParseDb select ###########" &>> "${LOG}"

	# select only rows related to currect subject & sort
	# important not to set --regex: treat values as regular expressions and allow partial string matches
	# appends _parse-select.tab

	# SORT_2 (combines naive and memory) instead of SORT (separates naive and memory)
	# splits out .tsv if input is .tsv, unless -o specified otherwise
	ParseDb.py select \
		-d "${PATH_GENOTYPE}${CUR_SUBJ}${NAME_GENOTYPED}.tsv" \
		-f SORT_2 \
		-u ${CUR_SORT}\
		-o "${PATH_CHANGEO}${CUR_SUBJ}_${CUR_SORT}${NAME_GENOTYPED}_parse-select.tab" \
		&>> "${LOG}"


	echo "############## DefineClones ###########" &>> "${LOG}"

	# count (before clonal clustering)
    if [[ (-s ${COUNT_PATH}) ]]; then
		echo -e "---\t---" >> ${COUNT_PATH}
	else
		echo -e "SUBJECT\tCOUNT" > ${COUNT_PATH}
	fi

	COUNT_LINES=$(wc -l ${PATH_CHANGEO}${CUR_SUBJ}_${CUR_SORT}${NAME_GENOTYPED}_parse-select.tab | awk '{print $1}')
	COUNT_SEQS=$((COUNT_LINES - 1))
	echo -e "${CUR_SUBJ}_${CUR_SORT}_genotyped_filtered\t${COUNT_SEQS}" >> ${COUNT_PATH}
	
	# --act set (as opposed to first) accounts for ambiguous V-gene and J-gene 
	# calls when grouping similar sequences
	
	# because the ham distance model is symmetric, the --sym avg argument 
	# can be left as default

	# Because the threshold was generated using length normalized distances, 
	# the --norm len argument is selected

	echo "Threshold = " $CUR_THRESH &>> "${LOG}"

	# appends _clone-pass.tab, _clone-fail.tab (even with .tsv input)
	DefineClones.py \
		-d "${PATH_CHANGEO}${CUR_SUBJ}_${CUR_SORT}${NAME_GENOTYPED}_parse-select.tab" \
		--failed \
		--mode gene \
		--act set \
		--model ham \
		--dist "${CUR_THRESH}" \
		--norm len \
		--sym avg \
		--link single \
		--maxmiss 0 \
		--sf JUNCTION \
		--vf V_CALL_GENOTYPED \
		--jf J_CALL \
		--nproc ${NPROC} \
		--log "${CUR_SUBJ}_${CUR_SORT}_DefineClones.log" \
		--outdir ${PATH_CHANGEO} \
		--outname "${CUR_SUBJ}_${CUR_SORT}" \
		&>> "${LOG}"

	# count (after clonal clustering)
	COUNT_LINES=$(wc -l ${PATH_CHANGEO}${CUR_SUBJ}_${CUR_SORT}_clone-pass.tab | awk '{print $1}')
	COUNT_SEQS=$((COUNT_LINES - 1))
	echo -e "${CUR_SUBJ}_${CUR_SORT}_DefoneClones\t${COUNT_SEQS}" >> ${COUNT_PATH}


	echo "############## CreateGermlines ###########" &>> "${LOG}"

	#! important: if genotyping by tigger was performed, 
	#  add novel alleles to germline .fasta before running CreateGermlines

	#  if you have run the clonal assignment task prior to invoking CreateGermlines, 
	# then adding the --cloned argument is recommended, as this will generate a 
	# single germline of consensus length for each clone

	# no nproc

	# appends _germ-pass.tab, _germ-fail.tab
	CreateGermlines.py \
		-d "${PATH_CHANGEO}${CUR_SUBJ}_${CUR_SORT}_clone-pass.tab" \
		--failed \
		-r ${FN_IMGT_V_NOVEL} ${FN_IMGT_DJ} \
		-g full dmask vonly regions \
		--cloned \
		--vf V_CALL_GENOTYPED \
		--df D_CALL \
		--jf J_CALL \
		--sf SEQUENCE_IMGT \
		--log "${CUR_SUBJ}_${CUR_SORT}_CreateGermlines.log" \
		--outdir ${PATH_CHANGEO} \
		--outname "${CUR_SUBJ}_${CUR_SORT}" \
		&>> "${LOG}"

	# count
	COUNT_LINES=$(wc -l ${PATH_CHANGEO}${CUR_SUBJ}_${CUR_SORT}_germ-pass.tab | awk '{print $1}')
	COUNT_SEQS=$((COUNT_LINES - 1))
	echo -e "${CUR_SUBJ}_${CUR_SORT}_CreateGermlines\t${COUNT_SEQS}" >> ${COUNT_PATH}
	
	echo "finished subject = "$CUR_SUBJ" sort = "$CUR_SORT # print to standard output

done
