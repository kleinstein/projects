# Fine-tuning Pre-trained Antibody Language Models for Antigen Specificity Prediction

This repository contains the scripts for fine-tuning antibody language models for antigen specificity prediction to SARS-CoV-2 Spike Protein and influenza hemagglutinin. 

## 1. Prerequisites

Ensure you have all the required dependencies installed. You can set up the environment using the provided `environment.yml` file:

```bash
conda env create -f environment.yml
conda activate torch
```

## 2. Download the pre-trained models
Download the following models to the `models` folder.

| Model | Link |
|--|--|
| antiberty | https://pypi.org/project/antiberty/ |
| antiBERTa2 | https://huggingface.co/alchemab/antiberta2 |
| ESM2 | https://huggingface.co/facebook/esm2_t33_650M_UR50D |
| BALM-paired, ft-ESM2 | https://zenodo.org/record/8253367 |


## 3. Download the data files 
The specificity data has been deposited to [figshare](https://doi.org/10.6084/m9.figshare.25342924). Download the following data files into the `data` folder.

| File | Description |
|--|--|
| S_FULL.parquet | Sequence and labels for S binding prediction (full-length) |
| S_CDR3.parquet | Sequence and labels for S binding prediction (CDR3 only) |
| HA_FULL.parquet | Sequence and labels for HA binding prediction (full-length) |
| HA_CDR3.parquet | Sequence and labels for HA binding prediction (CDR3 only) |
| S_repertoires.parquet | Repertoires used for S binding prediction |
| HA_repertoires.parquet | Repertoires used for HA binding prediction |

## 4. Running the scripts

### Step 1: Generate the embeddings

First, we'll generate embeddings for each model type. Here are the list of scripts and example command run the scripts:

| Directory | File | Description |
|--|--|--|
| embedding | antiBERTy_embed.py | Generate antiBERTy embeddings |
| embedding | antiBERTa2_embed.py| Generate antiBERTa2 embeddings |
| embedding | ESM2_embed.py| Generate ESM2 (650M) embeddings |
| embedding | ft-ESM2_embed.py| Generate ft-ESM2 embeddings |
| embedding | BALM_paired_embed.py| Generate BALM-paired embeddings |

```bash
python embedding/<antiBERTy|antiberta2|...>_embed.py S_FULL.parquet <HL|H> output.pt
```

### Step 2: Train and evaluate SVM on Embeddings

Next, we'll train and evaluate SVM models on the generated embeddings:

| Directory | File | Description |
|--|--|--|
| embedding_specificity_prediction | COVID19_specificity_prediction_embedding.py | Train and evaluate SVM on embeddings to predict binding to SARS-CoV-2 S |
| embedding_specificity_prediction | Flu_specificity_prediction_embedding.py | Train and evaluate SVM on embeddings to predict binding to influenza HA |

```bash
python embedding_specificity_prediction/<COVID19|Flu>_specificity_prediction_embedding.py <antiberty|antiberta2|..._FULL|CDR3> <HL|H>
```

### Step 3: Fine-tune Language Models

Now, we'll fine-tune the language models for specificity prediction:

| Directory | File | Description |
|--|--|--|
| fine-tuning_specificity_prediction | COVID19_specificity_prediction_finetuning.py | Supervised fine-tuning of antibody language models to predict binding to SARS-CoV-2 S | 
| fine-tuning_specificity_prediction | Flu_specificity_prediction_finetuning.py | Supervised fine-tuning of antibody language models to predict binding to influenza HA |

```bash
python fine-tuning_specificity_prediction/<COVID19|Flu>_specificity_prediction_finetuning.py <antiberty|antiberta2|...> <HL|H> <fold number>
```

### Step 4: Save attention activations
To save attention activations from the language models:

| Directory | File | Description |
|--|--|--|
| fine-tuning_specificity_prediction | attention_matrix.py | Save attention activations from the language model |

```bash
python fine-tuning_specificity_prediction/attention_matrix.py <antiberty|antiberta2|...> <HL|H> S_FULL.parquet <antibody index> <checkpoint-number>
```

### Step 5: Predict on Repertoire Data

Finally, we'll use the trained models to predict class probabilities on repertoire data:

| Directory | File | Description |
|--|--|--|
| repertoire_prediction | Flu_specificity_repertoire_prediction_embedding.py | Use pre-trained embedding influenza SVM to predict class probability on repertoire data |
| repertoire_prediction | COVID19_specificity_repertoire_prediction_embedding.py | Use pre-trained embedding SARS-CoV-2 SVM to predict class probability on repertoire data |
| repertoire_prediction | Flu_specificity_repertoire_prediction_finetuning.py | Use fine-tuned influenza antibody language model to predict class probability on repertoire data |
| repertoire_prediction | COVID19_specificity_repertoire_prediction_finetuning.py | Use fine-tuned SARS-CoV-2 antibody language model to predict class probability on repertoire data |

```bash
python repertoire_prediction/COVID19_specificity_repertoire_prediction_embedding.py <antiberty|antiberta2|...> <HL|H> S_repertoires.parquet
```

After running all these scripts, you will have generated embeddings, trained SVM models, fine-tuned language models, and made predictions on repertoire data for both SARS-CoV-2 Spike protein and influenza hemagglutinin binding.