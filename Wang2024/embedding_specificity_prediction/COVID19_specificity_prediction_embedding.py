#--------------------------------------------
# Train SVM on pre-trained embeddings to predict
# S protein binding
# 
# Please generate corresponding embeddings first
# by running script from embedding/ folder
#
# Example usage: 
#
# python COVID19_specificity_prediction_embedding.py ESM2-650M HL
#--------------------------------------------

import pickle
import torch

import pandas as pd
import numpy as np
from sklearn.model_selection import StratifiedGroupKFold, GridSearchCV, cross_val_score
from sklearn.linear_model import LogisticRegression, Lasso
from sklearn.metrics import (
    precision_score,
    recall_score,
    f1_score,
    matthews_corrcoef,
    roc_auc_score,
    average_precision_score, 
    balanced_accuracy_score
)
from sklearn.svm import SVC
from collections import Counter
from sklearn.decomposition import PCA
import re
import time
import argparse
import joblib

parser = argparse.ArgumentParser(description="Train SVM on pre-trained embeddings to predict S protein binding")
parser.add_argument("embedding", type=str, help="Type of embedding (antiBERTy, antiBERTa2, BALM_paired, ESM2-650M, ft-ESM2)")
parser.add_argument("model", type=str, help="Type of model (HL, H)")
parser.add_argument("--random", type=bool, help="Shuffle the data matrix", default=False)
args = parser.parse_args()

BASE_DIR = "../data/"

def load_data(embedding, model = "HL"):
    if "FULL" in embedding:
        print(f"Loading full length data...")
        path = "S_FULL.parquet"
    else:
        print(f"Loading CDR3 data...")
        path = "S_CDR3.parquet"
    y = pd.read_parquet(BASE_DIR + path)
   
    if re.match('BALM_paired', embedding):
        print('Loading BALM_paired representation...')
        if "FULL" in embedding:
            X = torch.load(BASE_DIR + "S_FULL_" + model + "_BALM_paired.pt",
                          map_location=torch.device('cpu')).numpy()
        elif "CDR3" in embedding:
            X = torch.load(BASE_DIR + "S_CDR3_" + model + "_BALM_paired.pt",
                          map_location=torch.device('cpu')).numpy()
            
    elif re.match('ft-ESM2', embedding):
        if "FULL" in embedding:
            X = torch.load(BASE_DIR + "S_FULL_" + model + "_ft-ESM2.pt",
                          map_location=torch.device('cpu')).numpy()
        elif "CDR3" in embedding:
            X = torch.load(BASE_DIR + "S_CDR3_" + model + "_ft-ESM2.pt",
                          map_location=torch.device('cpu')).numpy()
    elif re.match('ESM2-650M', embedding):
        if "FULL" in embedding:
            X = torch.load(BASE_DIR + "S_FULL_" + model + "_ESM2.pt",
                          map_location=torch.device('cpu')).numpy()
        elif "CDR3" in embedding:
            X = torch.load(BASE_DIR + "S_CDR3_" + model + "_ESM2.pt",
                          map_location=torch.device('cpu')).numpy()
    
    elif re.match('antiBERTa2', embedding):
        print('Loading antiBERTa2 representation...')
        if "FULL" in embedding:
            X = torch.load(BASE_DIR + "S_FULL_" + model + "_antiBERTa2.pt",
                          map_location=torch.device('cpu')).numpy()
        elif "CDR3" in embedding:
            X = torch.load(BASE_DIR + "S_CDR3_" + model + "_antiBERTa2.pt",
                          map_location=torch.device('cpu')).numpy()
            
    elif re.match('antiBERTy', embedding):
        print('Loading antiBERTy representation...')
        if "FULL" in embedding:
            X = torch.load(BASE_DIR + "S_FULL_" + model + "_antiBERTy.pt",
                          map_location=torch.device('cpu')).numpy()
        elif "CDR3" in embedding:
            X = torch.load(BASE_DIR + "S_CDR3_" + model + "_antiBERTy.pt",
                          map_location=torch.device('cpu')).numpy()

    y_groups = y.subject.values
    y = np.isin(y.label.values, ["S+", "S1+", "S2+"]).astype(int)    
    assert X.shape[0] == len(y)   
    return X, y, y_groups

def compute_metrics(preds, probs, labs):
    return {
        "precision": precision_score(labs, preds, pos_label=1),
        "recall": recall_score(labs, preds, pos_label=1),
        "f1": f1_score(labs, preds, pos_label=1, average = "weighted"),
        "apr": average_precision_score(labs, probs, pos_label=1),
        "balanced_accuracy": balanced_accuracy_score(labs, preds), 
        "auc": roc_auc_score(labs, probs),
        "mcc": matthews_corrcoef(labs, preds),
    }

# 1. Load embeddings and labels
X, y, y_groups = load_data(args.embedding, args.model)
y_group_counts = Counter(y_groups)
print(f"Class size: {Counter(np.sort(y)).most_common()}")
    
print(f"In total, {len(y)} sequences from {len(np.unique(y_groups))} donors/studies.")
print(f"Class size: {Counter(np.sort(y)).most_common()}")

if args.random:
    print(f"Shuffling the embedding...")
    y = y[np.random.permutation(len(y))]

# 3. Nested cross validation (combine all data)
n_splits_outer = 4
n_splits_inner = 3
if "FULL" in args.embedding:
    random_state = 9
else:
    random_state = 7
outer_cv = StratifiedGroupKFold(n_splits=n_splits_outer, shuffle=True, random_state=random_state)
inner_cv = StratifiedGroupKFold(n_splits=n_splits_inner, shuffle=True, random_state=1)

p_grid = {"C": [1e-2, 1e-1, 10, 100]}

outer_cv_w_groups = outer_cv.split(X, y, y_groups)

metrics = []

i = 1
svc = SVC(kernel = "rbf", class_weight = "balanced", probability = True)
rank = np.zeros((n_splits_outer, 4))

for train_index, test_index in outer_cv_w_groups:
    print(f"##### Outer fold {i} #####")
    # get the cross validation score on the test
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]
    print(f"Train size: {len(train_index)}, test size: {len(test_index)}")
    print(f"% positive train: {np.mean(y_train)}, % positive test: {np.mean(y_test)}")
    # inner loop
    cur_time = time.time()
    search = GridSearchCV(estimator = svc, 
               param_grid = p_grid, 
               cv = inner_cv, scoring = "roc_auc", 
               n_jobs = -1,
               pre_dispatch = "1*n_jobs")
    search.fit(X_train, y_train, groups = y_groups[train_index])
    rank[i-1,:] = search.cv_results_['rank_test_score']
    print(f"[Time (Outer fold {i})]: {time.time() - cur_time} seconds")
    prediction = search.predict(X_test)
    prob = search.predict_proba(X_test)[:,-1]
    metric = compute_metrics(prediction, prob, y_test)
    print(metric)
    metrics.append(metric)
    i += 1

out_score = pd.DataFrame(metrics)

if args.random:
    is_random = "_random"
else:
    is_random = ""

filename = "../data/S_" + args.embedding + "_" + args.model + is_random + ".csv"
out_score.to_csv(filename)
print("Results saved at: " + filename)

print(out_score.agg(['mean', 'std']))

# 4. Write the final classifier trained on all the data
# Train the best model on the whole dataset
best_rank = np.argmin(rank.sum(axis = 0))
best_params = list(p_grid.values())[0][best_rank]
best_model = SVC(kernel = "rbf", class_weight = "balanced", probability = True, C = best_params)
best_model.fit(X, y)

# Save the trained model using joblib or pickle
model_filename = "../models/S_" + args.embedding + "_" + args.model + is_random + ".pkl"
joblib.dump(best_model, model_filename)

# Print the best hyperparameters
print(f"Best hyperparameters: {best_params}")

# Print the average performance metrics across outer folds
print("Train Performance Metrics:")

# Print the overall performance on the entire dataset
prediction = best_model.predict(X)
prob = best_model.predict_proba(X)[:,-1]
metric = compute_metrics(prediction, prob, y)
print(metric)