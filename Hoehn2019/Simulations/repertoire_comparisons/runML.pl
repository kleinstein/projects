#!/usr/bin/perl

use strict;
use warnings;
use Parallel::ForkManager;

my $pm=new Parallel::ForkManager($ARGV[0]);
open(IN,$ARGV[1]) or die();
my $sd = $ARGV[2];
my $omeans = $ARGV[3];
my $kmean = $ARGV[4];
my $hs = $ARGV[5];
my $id = $ARGV[6];
my $freeonly = $ARGV[7];

my @lines;
my $h=<IN>;
while(<IN>){
	my $line=$_;
	chomp($line);
	my @in=split("\\s+",$line);
	push(@lines,\@in);
}

print "ID: $id\n";
foreach my $f (@lines){
	$pm->start and next;
	print "@$f\n";
	my @fs = @$f;
	my $command;
	$command = "igphyml -m HLP -i $fs[0] -u $fs[1] --root $fs[2] --partfile $fs[3] -o lr --run_id free-$id";
	$command .= " --oformat tab";
	system($command);
	#die();
	$pm->finish;
}
$pm->wait_all_children;