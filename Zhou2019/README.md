# Assessing the light chain consistency of heavy chain-based B cell clones

This [vignette](https://bitbucket.org/kleinstein/projects/src/default/Zhou2019/vignette.pdf) provides a quick demo on the analysis of light chain consistency in heavy chain-based B cell clones
using single cell B cell receptor (BCR) sequences as described in [Zhou & Kleinstein (2019)](https://doi.org/10.4049/jimmunol.1900666). 

The associated functions and data used in the vignette are in [functions.R](https://bitbucket.org/kleinstein/projects/src/default/Zhou2019/functions.R) and [vignette_data_hs_pbmc_scoper.RData](https://bitbucket.org/kleinstein/projects/src/default/Zhou2019/vignette_data_hs_pbmc_scoper.RData) respectively.
