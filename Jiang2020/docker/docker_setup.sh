#!/bin/sh
# Ruoyi Jiang
# Building docker for replicating a publication

# cd to directory with Dockerfile

# Build
podman build -t ruoyijiang/ganymede:jiangjci2020 .

# Login
# podman login docker.io

# Push image
# podman push ruoyijiang/ganymede:jiangjci2020

# Pull image
# podman pull ruoyijiang/ganymede:jiangjci2020