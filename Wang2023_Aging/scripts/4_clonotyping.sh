#!/bin/bash
#SBATCH --partition=general
#SBATCH --job-name=clonotyping
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=10G
#SBATCH --time=1-00:00:00
#SBATCH --error=log/clonotyping.%A_%a.err
#SBATCH --output=log/clonotyping.%A_%a.out

taskID=${SLURM_ARRAY_TASK_ID}

infolder="/gpfs/gibbs/pi/kleinstein/shaw_flu/processed/BCR/3_filter"
outfolder="/gpfs/gibbs/pi/kleinstein/shaw_flu/processed/BCR/4_clonotyping"
cutoff=0.09
samples=(`ls $infolder/*_heavy.tab`)
sample=${samples[$taskID-1]}
sampleid=$(basename $sample "_heavy.tab")

echo "[$0 $(date +%Y%m%d-%H%M%S)] [start] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"

singularity exec /gpfs/ysm/project/kleinstein/mw957/software/immcantation/lab_devel.sif DefineClones.py -d $sample --act set --model ham \
    --norm len --dist $cutoff \
    --outdir $outfolder --outname $sampleid --format airr

singularity exec /gpfs/ysm/project/kleinstein/mw957/software/immcantation/lab_devel.sif light_cluster.py \
     -d $outfolder/$sampleid"_clone-pass.tsv" -e $infolder/$sampleid"_light.tab" \
     -o $outfolder/$sampleid"_light-cluster-pass.tsv" --format airr --doublets count

echo "[$0 $(date +%Y%m%d-%H%M%S)] [end] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"