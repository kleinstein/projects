#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 14-
#SBATCH --constraint E5-2660_v3
#SBATCH -c 1
#SBATCH --mem-per-cpu=100G

# nx360h (E5-2660_v3) has a total max memory of 121GB per node

# IN:
# - tigger/[subj]_IGHV_FUNCTIONAL-T_/F_genotyped.tsv
# OUT:
# - slide/[subj]_IGHV_FUNCTIONAL-T_genotyped_preClonal_germ-pass.tsv
# - slide/[subj]_IGHV_FUNCTIONAL-T_genotyped_preClonal_germ-pass_parse-rename.tab

# as of Oct 27 2018, the change-o commands in this file are intended to work with 0.4.3

LOG="/home/qz93/scratch60/briney2019/slide/createGermlines_preclonal_$(date '+%m%d%Y_%H%M%S').log"

PATH_ROOT="/home/qz93/scratch60/briney2019/"
PATH_TIGGER="${PATH_ROOT}tigger/"
PATH_SLIDE="${PATH_ROOT}slide/"

GERM_TAG="tag_201918-4_9May2019"
FN_IMGT_DJ="/home/qz93/germlines/${GERM_TAG}/human/vdj/imgt_human_IGH[DJ].fasta"
#FN_IMGT_V_NOVEL="/home/qz93/germlines/${GERM_TAG}/IGHV_novel_briney2019_20190628.fasta" #*
FN_IMGT_V_NOVEL="/home/qz93/germlines/${GERM_TAG}/human/vdj/imgt_human_IGHV.fasta" #*

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${LOG}"
echo "germline" $GERM_TAG &>> "${LOG}"
echo "bash" $BASH_VERSION &>> "${LOG}"
CreateGermlines.py --version &>> "${LOG}"
python3 -V &>> "${LOG}"

SUBJS=(326651 326713 316188 326650 326737 326780 326797 326907 327059 D103)
#SUBJS=(326713 316188 326650 326737 326780 326797 326907 327059 D103)

# whether to create germline 
# this can only be skipped if germline has been created 
CREATE_GERM=true #*

if ${CREATE_GERM}; then

	for SUBJ in ${SUBJS[@]}; do

		echo "############## CreateGermlines for " ${SUBJ} &>> "${LOG}"

		# create germline so that R script below can use it to count mutation
		# output filename: [outname]_germ-pass.tab, _germ-fail.tab
		# do not use --clone flag (this is pre-clonal clustering)
		# create column GERMLINE_IMGT_D_MASK

		#! important: if genotyping by tigger was performed, 
		#  add novel alleles to germline .fasta before running CreateGermlines

		# no nproc

		# change-o 0.4.1+ takes --df and --jf 
		# default values are:
		# --df D_CALL
		# --jf J_CALL

		if [[ (-s ${PATH_TIGGER}${SUBJ}_IGHV_FUNCTIONAL-T_genotyped.tsv ) ]]; then
			CreateGermlines.py \
				-d "${PATH_TIGGER}${SUBJ}_IGHV_FUNCTIONAL-T_genotyped.tsv" \
				-r ${FN_IMGT_V_NOVEL} ${FN_IMGT_DJ} \
				-g dmask \
				--vf V_CALL_GENOTYPED \
				--df D_CALL \
				--jf J_CALL \
				--sf SEQUENCE_IMGT \
				--failed \
				--log "${SUBJ}_IGHV_FUNCTIONAL-T_genotyped_preClonal.log" \
				--outname "${SUBJ}_IGHV_FUNCTIONAL-T_genotyped_preClonal" \
				--outdir "${PATH_SLIDE}" \
				&>> "${LOG}"

			# output filename: [outname]_parse-rename.tab
			ParseDb.py rename \
				-d "${PATH_SLIDE}${SUBJ}_IGHV_FUNCTIONAL-T_genotyped_preClonal_germ-pass.tab" \
				-f GERMLINE_IMGT_D_MASK \
				-k PRECLONAL_GERMLINE_IMGT_D_MASK \
				--outname "${SUBJ}_IGHV_FUNCTIONAL-T_genotyped_preClonal_germ-pass" \
				--outdir "${PATH_SLIDE}" \
				&>> "${LOG}"
		fi


		if [[ (-s ${PATH_TIGGER}${SUBJ}_IGHV_FUNCTIONAL-F_genotyped.tsv ) ]]; then
			CreateGermlines.py \
				-d "${PATH_TIGGER}${SUBJ}_IGHV_FUNCTIONAL-F_genotyped.tsv" \
				-r ${FN_IMGT_V_NOVEL} ${FN_IMGT_DJ} \
				-g dmask \
				--vf V_CALL_GENOTYPED \
				--df D_CALL \
				--jf J_CALL \
				--sf SEQUENCE_IMGT \
				--failed \
				--log "${SUBJ}_IGHV_FUNCTIONAL-F_genotyped_preClonal.log" \
				--outname "${SUBJ}_IGHV_FUNCTIONAL-F_genotyped_preClonal" \
				--outdir "${PATH_SLIDE}" \
				&>> "${LOG}"

			ParseDb.py rename \
				-d "${PATH_SLIDE}${SUBJ}_IGHV_FUNCTIONAL-F_genotyped_preClonal_germ-pass.tab" \
				-f GERMLINE_IMGT_D_MASK \
				-k PRECLONAL_GERMLINE_IMGT_D_MASK \
				--outname "${SUBJ}_IGHV_FUNCTIONAL-F_genotyped_preClonal_germ-pass" \
				--outdir "${PATH_SLIDE}" \
				&>> "${LOG}"
		fi

	done
	
fi
