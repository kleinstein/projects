#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=48:00:00
#SBATCH --mail-type=FAIL

module load Python/miniconda

source activate bpb

python --version

python simulateRepertoire.py $1 $2 $3 $4 $5

