#!/bin/bash
# set -eo pipefail

igblastn -germline_db_V databases/igblast_base/database/imgt_human_ig_v \
-germline_db_J databases/igblast_base/database/imgt_human_ig_j \
-germline_db_D databases/igblast_base/database/imgt_human_ig_d \
-organism human \
-query sim_repertoires/repA_BCR_shm_alpha2_5000singletons/sim_repertoire_repA.fasta \
-auxiliary_data databases/igblast_base/optional_file/human_gl.aux \
-show_translation \
-outfmt 19 > sim_repertoires/repA_BCR_shm_alpha2_5000singletons/repA_igblast_results.tab

igblastn -germline_db_V databases/igblast_base/database/imgt_human_ig_v \
-germline_db_J databases/igblast_base/database/imgt_human_ig_j \
-germline_db_D databases/igblast_base/database/imgt_human_ig_d \
-organism human \
-query sim_repertoires/repB_BCR_shm_uniform_5000singletons/sim_repertoire_repB.fasta \
-auxiliary_data databases/igblast_base/optional_file/human_gl.aux \
-show_translation \
-outfmt 19 > sim_repertoires/repB_BCR_shm_uniform_5000singletons/repB_igblast_results.tab

igblastn -germline_db_V databases/igblast_base/database/imgt_human_ig_v \
-germline_db_J databases/igblast_base/database/imgt_human_ig_j \
-germline_db_D databases/igblast_base/database/imgt_human_ig_d \
-organism human \
-query sim_repertoires/repC_BCR_shm_realtrees_singletons_2/sim_repertoire_repC.fasta \
-auxiliary_data databases/igblast_base/optional_file/human_gl.aux \
-show_translation \
-outfmt 19 > sim_repertoires/repC_BCR_shm_realtrees_singletons_2/repC_igblast_results.tab
