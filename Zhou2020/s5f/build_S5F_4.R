##### build updated S5F model
# compare with HH_S5F (2013)

#**************
#### setup ####
#**************

MAC = T
if (MAC) {
    pathRoot = "~/Dropbox (recherche)/yale/" # mac
} else {
    pathRoot = "/ysm-gpfs/home/qz93/" # hpc
}

pathScript = paste0(pathRoot, "projects/scripts/")
pathData = paste0(pathRoot, "projects/spatial/data/")
pathWork = paste0(pathRoot, "projects/spatial/newS5F/")

pathHS5F = paste0(pathWork, "shazam_data-raw/")

library(shazam) # v0.1.11 (alakazam v0.2.11)

source(paste0(pathScript, "spatial/helpers.R")) # getSpotFivemer

#### helper func ####

# transition bias defined as ratio of 
# highest substitution rate to second highest subst rate

compute.transition.bias = function(rates){
    # input: a column of a substitution model matrix (col=from, row=to)
    # output: a ratio of highest to second highest rate
    
    # if all NA or only 1 non-NA, return NA
    if ( sum(is.na(rates)) == length(rates) ){
        return(NA)
    } else if ( sum(is.na(rates)) == (length(rates)-1) ) {
        return(NA)
    } else {
        max.rate = max(rates, na.rm=T)
        second.max.rate = max(rates[-which.max(rates)], na.rm=T)
        ratio = max.rate/second.max.rate
        return(ratio)
    }
}

# testing
testing.sub.mtx = matrix(c(NA,NA,NA,
                           1.3,NA,4.7,
                           2.1,NA,NA,
                           1,2,3), byrow=F, nrow=3)
apply(testing.sub.mtx, 2, compute.transition.bias)

# usage: apply(substition_matrix, 2, compute.transition.bias)

#### setup ####

# generated from build_S5F_[13].R
saveName_sub_1 = "sub_1mer.RData"
saveName_sub_5 = "sub_5mer.RData"
saveName_mut = "mut_5mer.RData"
saveName_tar = "tar.RData"

saveName_HS5F = "HS5F_2013.RData"

# copied & pasted from shazam/data-raw/
fileName_HS5F_mut = "HS5F_Mutability.csv"
fileName_HS5F_sub = "HS5F_Substitution.csv"

COMPARE_2013 = F

#### HS5F (2013) - $Source ####

if (COMPARE_2013) {
    
    setwd(pathHS5F)
    
    ### mutability
    
    mut_HS5F = read.table(fileName_HS5F_mut, sep=",", header=T, stringsAsFactors=F)
    # 1024 x 5
    dim(mut_HS5F)
    #   Fivemer Mutability   Source    lower25    upper25
    # 1   TCGGG 0.02501445 Measured 0.11886268 0.05447608
    head(mut_HS5F)
    # 1024
    sum(mut_HS5F[["Mutability"]])
    # no NA 
    any(is.na(mut_HS5F))
    # normalize
    mut_HS5F[["Mutability"]] = mut_HS5F[["Mutability"]]/sum(mut_HS5F[["Mutability"]])
    # 1
    sum(mut_HS5F[["Mutability"]])
    
    # compare with shazam::HH_S5F
    shazam_mut = HH_S5F@mutability
    # 3125 (extend)
    length(shazam_mut)
    
    idx = match(mut_HS5F[["Fivemer"]], names(shazam_mut))
    stopifnot(all.equal(names(shazam_mut)[idx], mut_HS5F[["Fivemer"]]))
    
    # numerically identical
    all.equal(shazam_mut[idx], mut_HS5F[["Mutability"]], check.attributes = F)
    
    # can know which mutabilities in HH_S5F@mutability was measured/inferred
    # based on mut_HS5F[["Source"]]
    
    # Inferred Measured 
    #      468      556
    table(mut_HS5F[["Source"]])
    
    # 2019: measured 566 at minNumSeqMutations=500
    
    ### substitution rates
    
    sub_HS5F = read.table(fileName_HS5F_sub, sep=",", header=T, stringsAsFactors=F)
    # 1024x6
    dim(sub_HS5F)
    #   Fivemer A         C         G         T   Source
    # 1   AAAAA 0 0.3543814 0.4149485 0.2306701 Inferred
    head(sub_HS5F)
    
    # compare with shazam::HH_S5F
    shazam_sub = HH_S5F@substitution
    # 5 x 3125 (extended)
    dim(shazam_sub)
    
    # 5th row (corresponds to N) all NA
    all(is.na(shazam_sub[5, ]))
    
    idx = match(sub_HS5F[["Fivemer"]], colnames(shazam_sub))
    stopifnot( all.equal(sub_HS5F[["Fivemer"]], colnames(shazam_sub[, idx])) )
    
    shazam_sub_cf = data.frame(t(shazam_sub[-5, idx]))
    # convert NA to 0
    for (i in 1:nrow(shazam_sub_cf)) {
        idx_NA = which(is.na(shazam_sub_cf[i, ]))
        stopifnot(length(idx_NA)==1)
        shazam_sub_cf[i, idx_NA] = 0
    }
    rownames(shazam_sub_cf) = NULL
    
    # numerically identical
    all.equal(shazam_sub_cf, sub_HS5F[, c("A","C","G","T")])
    
    # Inferred Meausred
    #      740      284
    table(sub_HS5F[["Source"]])
    
    # 2019: 275 at minNumMutations=50
    
    setwd(pathWork)
    save(mut_HS5F, sub_HS5F, file=saveName_HS5F)
    
} else {
    setwd(pathWork)
    load(saveName_HS5F) # mut_HS5F, sub_HS5F
}

#### corr ####

setwd(pathWork)

### 1-mer substitution rates

load(saveName_sub_1) # sub_model_1mer_raw, sub_model_1mer_norm

stopifnot( all.equal( colnames(sub_model_1mer_norm), colnames(shazam::HH_S1F) ) )
stopifnot( all.equal( rownames(sub_model_1mer_norm), rownames(shazam::HH_S1F) ) )

# 0.986
cor(as.numeric(sub_model_1mer_norm), as.numeric(shazam::HH_S1F), method="pearson")
# 0.961
cor(as.numeric(sub_model_1mer_norm), as.numeric(shazam::HH_S1F), method="spearman")

### 5-mer mutability

load(saveName_mut) # mut_model_df

# 1024 x 3
dim(mut_model_df)
#       Fivemer  Mutability   Source
# AAAAA   AAAAA 0.001676912 Measured
head(mut_model_df)

# spot
mut_model_df[["spot"]] = sapply(mut_model_df[["Fivemer"]], getSpotFivemer)

# cf order
all.equal(mut_HS5F[["Fivemer"]], mut_model_df[["Fivemer"]])
# need to match
idx = match(mut_model_df[["Fivemer"]], mut_HS5F[["Fivemer"]])
stopifnot(all.equal(mut_model_df[["Fivemer"]], mut_HS5F[["Fivemer"]][idx]))
mut_HS5F = mut_HS5F[idx, ]

# binary for subsetting to measured in both
bool_m_both = mut_model_df[["Source"]]=="Measured" & mut_HS5F[["Source"]]=="Measured"
# F 471; T 553; no NA
# 553 measured in both
table(bool_m_both, useNA="ifany")

sum(bool_m_both) / sum(mut_model_df[["Source"]]=="Measured") # 0.9770318
sum(bool_m_both) / sum(mut_HS5F[["Source"]]=="Measured")     # 0.9946043

# all 5mers
# 0.8161538
cor(mut_model_df[["Mutability"]], mut_HS5F[["Mutability"]], method="pearson")
# 0.8401981
cor(mut_model_df[["Mutability"]], mut_HS5F[["Mutability"]], method="spearman")

# measured in both
mut_m_both_norm_2019 = mut_model_df[["Mutability"]][bool_m_both] / sum(mut_model_df[["Mutability"]][bool_m_both])
mut_m_both_norm_2013 = mut_HS5F[["Mutability"]][bool_m_both] / sum(mut_HS5F[["Mutability"]][bool_m_both])
names(mut_m_both_norm_2019) = mut_model_df[["Fivemer"]][bool_m_both]
names(mut_m_both_norm_2013) = mut_HS5F[["Fivemer"]][bool_m_both]
sum(mut_m_both_norm_2019)
sum(mut_m_both_norm_2013)
# 0.8117711
cor(mut_m_both_norm_2019, mut_m_both_norm_2013, method="pearson")
# 0.8497317
cor(log(mut_m_both_norm_2019), log(mut_m_both_norm_2013), method="pearson")
# 0.8613688
cor(mut_m_both_norm_2019, mut_m_both_norm_2013, method="spearman")
# 0.8613688 
# makes sense this is the same as un-logged since log preserves rank and spearman bases on rank
cor(log(mut_m_both_norm_2019), log(mut_m_both_norm_2013), method="spearman")

# differences
summary(mut_m_both_norm_2019-mut_m_both_norm_2013)
boxplot(mut_m_both_norm_2019-mut_m_both_norm_2013)

### visualize measured-only 2019

# 1024; normalized
length(mut_model_vec)
sum(mut_model_vec, na.rm=T)

# 1024; not re-normalized
length(mut_model_vec_m_only)
sum(mut_model_vec_m_only, na.rm=T)

# re-normalize measured-only vector
mut_model_vec_m_only_norm = mut_model_vec_m_only / sum(mut_model_vec_m_only, na.rm=T)
sum(mut_model_vec_m_only_norm, na.rm=T)

# same order; can use spot status from mut_model_df 
stopifnot( all.equal(mut_model_df[["Fivemer"]], names(mut_model_vec_m_only_norm)) )

### by-spot


uniqSpot = c("coldSYC/GRS", "neutral", "hotWA/TW", "hotWRC/GYW")
uniqSpotPrint = c("Cold\nSYC/GRS", "Neutral\n", "Hot\nWA/TW", "Hot\nWRC/GYW")
# for legend
uniqSpotPrint_2 = c("Cold\nSYC/GRS", "Neutral", "Hot\nWA/TW", "Hot\nWRC/GYW")

uniqSpotCol = c("hotWRC/GYW"="firebrick2", "hotWA/TW"="forestgreen", 
                "neutral"="gray50", "coldSYC/GRS"="dodgerblue2")

# legend
# horizontal
plotName = "mut_legend_horiz.pdf"
setwd(pathWork)
pdf(plotName, width=5, height=2)
par(mar=c(0,0,0,0))
plot(x=1:length(uniqSpot), y=rep(1, length(uniqSpot)), 
     pch=15, col=uniqSpotCol[uniqSpot], cex=2.5,
     xlim=c(0.95, length(uniqSpot)+0.75), ylim=c(0, 2.5), 
     xaxt="n", yaxt="n", xlab="", ylab="", bty="n")
text(x=(1:length(uniqSpot))+0.05, y=rep(1, length(uniqSpot)), 
     labels=uniqSpotPrint_2, pos=4)
dev.off()

# vertical
plotName = "mut_legend_vertical.pdf"
setwd(pathWork)
pdf(plotName, width=2, height=2)
par(mar=c(0,0,0,0))
plot(y=1:length(uniqSpot), x=rep(1, length(uniqSpot)), 
     pch=15, col=rev(uniqSpotCol[uniqSpot]), cex=2.5,
     ylim=c(0.9, length(uniqSpot)+0.275), xlim=c(0.9, 2), 
     xaxt="n", yaxt="n", xlab="", ylab="", bty="n")
text(y=(1:length(uniqSpot)), x=rep(1, length(uniqSpot))+0.05, 
     labels=rev(uniqSpotPrint_2), pos=4)
dev.off()

mut_model_vec_m_only_norm_lst = sapply(uniqSpot, function(s){ 
    idx = which(mut_model_df[["spot"]]==s & !is.na(mut_model_vec_m_only_norm))
    return(mut_model_vec_m_only_norm[idx])
}, simplify=F)

# hotWRC/GYW    hotWA/TW     neutral coldSYC/GRS 
#         70         131         291          74
uniqSpotCount = unlist(lapply(mut_model_vec_m_only_norm_lst, length))
# 566
sum(uniqSpotCount)
sum(uniqSpotCount) == sum(mut_model_df[["Source"]]=="Measured")
sum(uniqSpotCount) == sum(!is.na(mut_model_vec_m_only))

# add count to text label

ymax = max(mut_model_vec_m_only_norm, na.rm=T)

plotName = "mut_mOnly_boxplot.pdf"
setwd(pathWork)
pdf(plotName, width=7, height=5)
par(mar=c(3,5,1,1)) # BLTR
boxplot(mut_model_vec_m_only_norm_lst, ylim=c(0, ymax*1.1), 
        col=uniqSpotCol[uniqSpot], cex=0.75,
        xlab="", xaxt="n", las=2, ylab="")
axis(side=1, at=1:length(uniqSpot), labels=uniqSpotPrint, tick=F, line=0.5)
mtext(text="Mutability", side=2, line=3.5)
text(x=1:length(uniqSpot), y=ymax*1.1, labels=uniqSpotCount)
dev.off()

# rank plot

mut_model_vec_m_only_norm_sorted = sort(mut_model_vec_m_only_norm, decreasing=F)

plotName = "mut_mOnly_rank.pdf"
pdf(plotName, width=5, height=5)
par(mar=c(4,4,1,1)) # BLTR
plot(x=1:length(mut_model_vec_m_only_norm_sorted), 
     y=log(mut_model_vec_m_only_norm_sorted),
     pch=substr(names(mut_model_vec_m_only_norm_sorted), 3, 3),
     xlab="Index", ylab="Mutability (ln)", 
     col=scales::alpha(uniqSpotCol[sapply(names(mut_model_vec_m_only_norm_sorted), getSpotFivemer)], 0.65),
     cex=0.45)
dev.off()

### mutability, measured in both 2019 and 2013

commonRange_mut = range(c(log(mut_m_both_norm_2019), log(mut_m_both_norm_2013)))

plotName = "mut_m_both_2019_2013.pdf"
pdf(plotName, width=5, height=5)
par(mar=c(4,4,1,1)) # BLTR
plot(x=log(mut_m_both_norm_2019), y=log(mut_m_both_norm_2013),
     xlim=commonRange_mut, ylim=commonRange_mut,
     xlab="Mutability, updated model (ln)", ylab="Mutability, 2013 model (ln)",
     pch=substr(names(mut_m_both_norm_2019), 3, 3),
     col=scales::alpha(uniqSpotCol[sapply(names(mut_m_both_norm_2019), getSpotFivemer)], 0.8),
     cex=0.5)
abline(a=0, b=1, lty=2, col="black")
text(x=min(log(mut_m_both_norm_2019))*1.02, y=max(commonRange_mut), pos=4,
     labels=paste0("Spearman r: ", round(cor(log(mut_m_both_norm_2019), log(mut_m_both_norm_2013), method="spearman"), 2)))
text(x=min(log(mut_m_both_norm_2019))*1.02, y=max(commonRange_mut)*1.1, pos=4,
     labels=paste0("Pearson r: ", round(cor(log(mut_m_both_norm_2019), log(mut_m_both_norm_2013), method="pearson"), 2)))
dev.off()


#### transition bias ####

setwd(pathWork)
load(saveName_sub_5) # sub_model_5mer, sub_model_5mer_ex

# 4 x 1024
dim(sub_model_5mer)
#       AAAAA     AAAAC     AAAAG
# A        NA        NA        NA
# C 0.2866356 0.2866356 0.2866356
# G 0.4821701 0.4821701 0.4821701
# T 0.2311943 0.2311943 0.2311943
sub_model_5mer[, 1:3]

# same
all.equal(colnames(sub_model_5mer), sub_HS5F[["Fivemer"]])

# reshape 2013 substitution rates
sub_mtx_2013 = t(sub_HS5F[, rownames(sub_model_5mer)])
colnames(sub_mtx_2013) = sub_HS5F[["Fivemer"]]
# convert 0 to NA
for (i in 1:ncol(sub_mtx_2013)) {
    idx = which(sub_mtx_2013[, i]==0)
    stopifnot(length(idx)==1)
    sub_mtx_2013[idx, i] = NA
}

stopifnot(all.equal(colnames(sub_model_5mer), colnames(sub_mtx_2013)))

tb_2019 = apply(sub_model_5mer, 2, compute.transition.bias)
tb_2013 = apply(sub_mtx_2013, 2, compute.transition.bias)

# rank plot

tb_2019_sorted = sort(tb_2019, decreasing=F)

plotName = "tb_rank.pdf"
pdf(plotName, width=5, height=5)
par(mar=c(4,4,1,1)) # BLTR
plot(x=1:length(tb_2019_sorted), 
     y=log(tb_2019_sorted),
     pch=substr(names(tb_2019_sorted), 3, 3),
     xlab="Index", ylab="Transition Bias Rate (ln)", 
     col=scales::alpha(uniqSpotCol[sapply(names(tb_2019_sorted), getSpotFivemer)], 0.65),
     cex=0.45)
dev.off()

# 2019 vs 2013

commonRange_tb = range(c(log(tb_2019), log(tb_2013)))

plotName = "tb_2019_2013.pdf"
pdf(plotName, width=5, height=5)
par(mar=c(4,4,1,1)) # BLTR
plot(x=log(tb_2019), y=log(tb_2013),
     xlim=commonRange_tb, ylim=commonRange_tb,
     xlab="Transition bias rate, updated model (ln)", ylab="Transition bias rate, 2013 model (ln)",
     pch=substr(names(tb_2019), 3, 3),
     col=scales::alpha(uniqSpotCol[sapply(names(tb_2019), getSpotFivemer)], 0.65),
     cex=0.4)
abline(a=0, b=1, lty=2, col="black")
text(x=min(log(tb_2019))*1.01, y=max(commonRange_tb), pos=4,
     labels=paste0("Spearman: ", round(cor(log(tb_2019), log(tb_2013), method="spearman"), 2)))
text(x=min(log(tb_2019))*1.01, y=max(commonRange_tb)*0.95, pos=4,
     labels=paste0("Pearson: ", round(cor(log(tb_2019), log(tb_2013), method="pearson"), 2)))
dev.off()

