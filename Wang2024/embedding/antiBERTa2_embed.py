#--------------------------------------------
# Generate antiBERTa2 embedding
# 
# AntiBERTa2 is available on huggingface:
# https://huggingface.co/alchemab/antiberta2
#
# Example usage: 
#
# python antiBERTa2_embed.py S_FULL.parquet HL S_FULL_antiBERTa2.pt
#--------------------------------------------

import argparse
from datetime import date
import os
import warnings
warnings.simplefilter('ignore')
import torch
from transformers import (
    RoFormerModel,
    RoFormerForMaskedLM, 
    RoFormerTokenizer, 
    pipeline, 
    RoFormerForSequenceClassification
)
from Bio import SeqIO
import time
import math 
import pandas as pd

##--------------------------------------------
parser = argparse.ArgumentParser(description="Generate antiBERTa2 embedding. Example usage: \n python antiBERTa2_embed.py S_FULL.parquet HL S_FULL_antiBERTa2.pt")
parser.add_argument("parquet_file", type=str, help="Path to the BCR sequence parquet file.")
parser.add_argument("sequence", type=str, help="Column name of sequence to embed (e.g. H, L, HL)")
parser.add_argument("output_file", type=str, help="Output file path")
args = parser.parse_args()

##--------------------------------------------
def batch_loader(data, batch_size):
    num_samples = len(data)
    for i in range(0, num_samples, batch_size):
        end_idx = min(i + batch_size, num_samples)
        yield i, end_idx, data[i:end_idx]

def insert_space_every_other_except_cls(input_string):
    parts = input_string.split('[CLS]')
    modified_parts = [''.join([char + ' ' for char in part]).strip() for part in parts]
    result = ' [CLS] '.join(modified_parts)
    return result

##--------------------------------------------
## Format the sequence
dat = pd.read_parquet(args.parquet_file)
X = dat.loc[:,args.sequence]
max_length = 256
X = X.apply(lambda a: a[:max_length])
X = X.str.replace('<cls><cls>', '[CLS][CLS]')
X = X.apply(insert_space_every_other_except_cls)
X = X.str.replace('  ', ' ')
sequences = X.values

##--------------------------------------------
## Set up the model
tokenizer = RoFormerTokenizer.from_pretrained("alchemab/antiberta2")
model = RoFormerForMaskedLM.from_pretrained("alchemab/antiberta2")
model = model.to('cuda')
model_size = sum(p.numel() for p in model.parameters())
print(f"Model loaded. Size: {model_size/1e6:.2f}M")

##--------------------------------------------
## Embedding the sequences
start_time = time.time()
batch_size = 128
n_seqs = len(sequences)
dim = 1024
n_batches = math.ceil(n_seqs / batch_size)
embeddings = torch.empty((n_seqs, dim))

i = 1
for start, end, batch in batch_loader(sequences, batch_size):
    print(f'Batch {i}/{n_batches}\n')
    x = torch.tensor([
    tokenizer.encode(seq, 
                     padding="max_length",
                     truncation=True,
                     max_length=max_length,
                     return_special_tokens_mask=True) for seq in batch]).to('cuda')
    attention_mask = (x != tokenizer.pad_token_id).float().to('cuda')
    with torch.no_grad():
        outputs = model(x, attention_mask = attention_mask,
                       output_hidden_states = True)
        outputs = outputs.hidden_states[-1]
        outputs = list(outputs.detach())
    
    # aggregate across the residuals, ignore the padded bases
    for j, a in enumerate(attention_mask):
        outputs[j] = outputs[j][a == 1,:].mean(0)
        
    embeddings[start:end] = torch.stack(outputs)
    del x
    del attention_mask
    del outputs
    i += 1
    
end_time = time.time()
print(end_time - start_time)

torch.save(embeddings, args.output_file)