#!/usr/bin/env python
"""This is a script that will compile all of the files from a particular study in the OAS database
into one fasta file to then go into the docker igBLAST system."""
__author__ = "Kenneth Hoehn, Frederick Miller"
__credits__ = ["Kenneth Hoehn", "Frederick Miller"]
__status__ = "Development"
__version__ = "1.0"
import os
import json
import argparse
import sys

parser = argparse.ArgumentParser(description="Arguments for study")

parser.add_argument("--study",  required = True, help = "folder name that contains all the .fasta and .json files for the study")
parser.add_argument("--outdir", required = True, help = "where you want the final combined fasta to be stored ")
parser.add_argument("--outfile", required = True, help = "the name of the output file, including .fasta")


args = parser.parse_args()
#print(args)

#Creates folder for the result
if not os.path.exists("../processed/"+args.study):
    os.makedirs("../processed/"+args.study)
#print("outward directory os.listdr call: " + str(os.listdir(args.outdir)))



def getNewLineFirstHalf(jsonDict, fileName):
    """
    Returns a string that is the new line that is going to be added to the
    query line of the fasta files in the following format
    |study=Author_year|isotype=isotype|time=week|subject=subject

    Arguments:
    jsonDict - the first line of the json file dictionary, of the form
    {"Longitudinal": "week-38", "Chain": "Heavy", "Author": "Bhiman et al., (2015)", "Isotype": "IGHA", "Age": "no",
    "Size_igblastn": 30, "Disease": "HIV", "Link": "https://www.ncbi.nlm.nih.gov/pubmed/26457756",
    "BSource": "PBMC", "BType": "Unsorted-B-Cells", "Subject": "CAP256", "Species": "human", "Vaccine": "None", "Size": 19}
    fileName - the name of the fasta file, without its path (A string)

    Returns:
    String to append
    |study=Author_year|isotype=isotype|time=week|subject=subject|...|type=oas|file_name=
    Note, it does not include the Author, Size_igblastn, Link, or Size.
    """
    # todo: use everything except for what is exluded in google doc, toLowerCase everything before appending it to the result
    # make sure to keep the Liao_2013 type check for its subject
    keyList = list(jsonDict.keys())
    keyList.sort()
    result = ""
    if args.study == "Liao_2013": # assigns the subject correctly for the Liao study
        jsonDict["Subject"] = "CH505"


    for key in keyList:
        if key not in ["Author","Size_igblastn","Link","Size"]: # tags we dont need to include
            if key == "Longitudinal":
                result = result + "|timepoint=" + jsonDict.get(key)
            else:
                result = result + "|" + key.lower() +"=" +jsonDict.get(key)


    return "|study="+args.study+result + "|type=oas|file_name="+fileName



def getNewLineSecondHalf(jsonDicts, query):
    """
    Gets the second half of the line that is appended with extra information from the redundant json Queries.
    This function is only to be called with only redundant sequences.


    Arguments:
    jsonDict: the list of redundant dictionaries for that particular json file
    These contain sequence information
    query: the query of the sequence that was present more than once

    Returns: A string of the form
    |oas_v=___|oas_j=___|redundancy=n

    """
    #print("second half called")
    #print(jsonDicts)

    result = ""
    dictInQuestion = jsonDicts.get(query)
    oas_v ="|oas_v="+dictInQuestion.get("v")
    oas_j="|oas_j="+dictInQuestion.get("j")
    redundancy = "|redundancy="+str(dictInQuestion.get("redundancy"))
    result = oas_v + oas_j + redundancy + "\n" # newline character for the sequence to go after
    return result

def redundancyAboveOne(dicts):
    """
    Returns a list of all the redundant queries dictionaries. Redundant being the query is present more than once
    from the json file.

    Arguments:
    The list of dictionaries from the json files, containing information on every single
    sequence from the fasta file

    Return:
    A dictionary of the dictionaries of the sequences that are greater than 1 redundancy
    The key is the original name, and the value is the dictionary
    """
    # print("redundancy called")

    result = {}

    for dict in dicts[1:]:
        if dict.get("redundancy") > 1:
            #print(dict.get("original_name") + " has redundancy")
            result[dict.get("original_name")] = dict
    return result

def processFile(fileFasta, fileJson):
    """
    Processes each individual file of the Study by appending to the
    query lines the necessary information and appends it to the final file
    if the query line meets the necessary condition (redundancy > 1)
    """
    #print("processing " + str(fileFasta))
    #print("processFile called")
    #print(fileFasta.name.split("/")[-1])
    #print(fileJson)
    """
    Loads the json file into its list of dictionaries so that way they can
    properly processed.
    """
    jsonDicts = []
    for line in fileJson:
        jsonDicts.append(json.loads(line))
    textToAppendPart1 = getNewLineFirstHalf(jsonDicts[0], fileFasta.name.split("/")[-1])
    #print("loaded jsons")
    redundantQueriesDicts = redundancyAboveOne(jsonDicts)

    # Processes the fasta and writes those that are redundant into the final file
    for line in fileFasta:
        if line[0] ==">":
            if line[1:].strip() in redundantQueriesDicts.keys():
                textToAppendPart2 = getNewLineSecondHalf(redundantQueriesDicts, line[1:].strip())
                line = line.strip() + textToAppendPart1 + textToAppendPart2
                finalFile.write(line)
                finalFile.write(next(fileFasta))


    fileFasta.close()
    fileJson.close()

def getFiles():
    """
    Creates a dictionary of all the files of the heavy variant, with the key being the file name,
    and the value being a list, the first element is the fasta file, and the second element is the json file

    Arguments: None

    Returns:
    A dictionary where keys are file names
    and the values are a list of two elements, of the form [fasta, json] files that have the same name
    and therefore correspond to each other
    """
    #print("get files called")
    fastaPath = "../oas/"+args.study # specified as command line input
    jsonPath = "../oas_json/"
    fastaFileList = os.listdir(fastaPath)
    jsonFileList= os.listdir(jsonPath)
    fastaFiles = [] # list of fasta files
    jsonFiles = [] # list of json files
    fastaNameList = [] # list of fasta file names
    jsonNameList = [] # list of json file names
    fastaJsonpairs = [] # pairs for each fasta and json file

    # creates the list of json files in question
    # check if any files exist to process
    if not fastaFileList or not jsonFileList: # checks if lists are empty
        print("No fasta or json files are present. program terminating")
        sys.exit()


    for file in jsonFileList:
        if (args.study) in file:
            jsonNameList.append(file[:len(file)-5]) # -5 for the .json tag


    # Creates a list of the fasta files, note, this builds the pairs of fasta and json with only corresponding fastas
    for file in fastaFileList:
        fastaNameList.append(file[:len(file)-6]) #substract 6 to remove the .fasta tag
        fastaFiles.append(fastaPath+"/"+file)
        if file[:len(file)-6]+".json" in jsonFileList: # makes sure that the corresponding json file is present to create a pair
            jsonFiles.append(jsonPath+"/"+file[:len(file)-6]+".json")
            fastaJsonpairs.append([file[:len(file)-6],fastaPath+"/"+file,jsonPath+"/"+file[:len(file)-6]+".json"]) # (a,b,c) where a is the key, b and c are fasta and json files

    fastaFiles = list(set(fastaFiles)) # remove duplicates
    jsonFiles = list(set(jsonFiles)) # remove duplicates
    fastaNameList = list(set(fastaNameList)) # remove duplicates
    jsonNameList = list(set(jsonNameList)) # remove duplicates

    # error checking
    if not jsonNameList == fastaNameList:
        print("WARNING: There is an inconsistency between the json files and the fasta files" )
        print("only the following files were present in both")
        for name in list(set(jsonNameList) & set(fastaNameList)):
            print(name + "\n")
        print("The following files were not condensed")
        for name in list(set(jsonNameList).symmetric_difference(set(fastaNameList))):
            print(name)


    # Creates the dictionary where the file path is the key (without a .json or .fasta extension), and the value is a list:
    # list is of form [a,b] where a is the fasta, and b is the json file
    result = {}
    for pair in fastaJsonpairs:
        result[pair[0]] =[pair[1],pair[2]]



    return result

def filterOnlyHeavy(fileDict):
    """
    This function takes the resulting file dictionary and
    returns only those with the heavy chain

    """
    result = {}
    keyList = fileDict.keys()

    for key in keyList:
        jsonFile = fileDict.get(key)[1]
        firstLine = open(jsonFile,"r").readline()
        chainType = json.loads(firstLine).get("Chain")


        if chainType == "Heavy" or chainType == "heavy":
            result[key] = fileDict.get(key)

    return result

#This will create the new file, I specified the name here

finalFile = open(args.outdir+"/"+args.outfile,"w") # overwrites any file of the same name

fileDict = filterOnlyHeavy(getFiles()) # gets file dictionary

keyList = list(fileDict.keys()) # list of file names

# processes each file
for key in keyList:
    print("processing: " +key)
    processFile(open(fileDict.get(key)[0],"r"),open(fileDict.get(key)[1],"r")) # open the files here instead

finalFile.close()
