#!/bin/bash

#SBATCH --nodes=1
#SBATCH --cpus-per-task=50
#SBATCH --partition day
#SBATCH --time=1-0:00:00

module load R4.1.0

nprocs=$1
bcr_phylo=$2
build=$3

Rscript sim_bootstraps.R $nprocs $bcr_phylo $build