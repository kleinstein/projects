##-----------------------------
## 
## Script name: train_immune2vec.py
##
## Purpose of script: Script to train immune2vec on four single-cell BCR datasets
##
## Author: Mamie Wang
##
## Date Created: 2022-08-18
##
## Email: mamie.wang@yale.edu
##
## ---------------------------
## load the packages and inputs

import pickle
import sys

sys.path.append("/gpfs/ysm/project/mw957/repos/archive/immune2vec/embedding")
import sequence_modeling
codedir = "/gpfs/ysm/project/mw957/repos/bcr_embeddings/embedding"
sys.path.append(codedir)
import immune2vec

import pandas as pd
import numpy as np

import argparse

parser = argparse.ArgumentParser(description="Gene usage tasks")
parser.add_argument("chain", type=str, help="Chain type (H, L)")
parser.add_argument("input", type=str, help="Input sequence (FULL or CDR3 or FULL_CDR3)")
parser.add_argument("dim", type=int, help="Latent dimesnion of immune2vec")
args = parser.parse_args()

##-----------------------------
## Load data
base_dir = "/gpfs/ysm/project/kleinstein/mw957/repos/bcr_embeddings/data/"
out_dir = "/gpfs/gibbs/pi/kleinstein/mw957/data/BCR_embed/model/immune2vec/"

if args.chain == "H":
    if args.input == "CDR3":
        sequence_train = pd.read_csv(base_dir + "combined_cdr3_heavy.csv").heavy_cdr3
    elif args.input == "FULL":
        sequence_train = pd.read_csv(base_dir + "combined_distinct_heavy.csv").heavy
elif args.chain == "L": 
    if args.input == "CDR3":
        sequence_train = pd.read_csv(base_dir + "combined_cdr3_light.csv").light_cdr3
    elif args.input == "FULL":
        sequence_train = pd.read_csv(base_dir + "combined_distinct_light.csv").light

##-----------------------------
## Check input
seq_len = sequence_train.apply(len)

print("# sequences: " + str(sequence_train.shape[0]))

lengths = sequence_train.apply(len)
print("Length range: " + str(min(lengths)) + " - " + str(max(lengths)) + " AA.")

##-----------------------------
## Run immune2vec
out_id = args.chain + "_" + args.input + "_" + str(args.dim)
out_corpus_fname = out_dir + out_id

model = immune2vec.immune2vec(sequence_train, out_corpus_fname, args.dim)
filename = out_corpus_fname + ".immune2vec"
immune2vec.save_model(model, filename)

print("Immune2vec model saved at: " + filename)