

use strict;
use warnings;
use Parallel::ForkManager;

my $pm= new Parallel::ForkManager($ARGV[0]);

#open(IN,"boyd_flu_changeo.tab") or die();
my @folders=("034","097","095","093","083","074","073","065","064","063","054","049","033","026","025","023","021","018","016","015","014","013","011","008","004","003","001");


my %found;
foreach my $folder (@folders){
open(IN,"/ysm-gpfs/pi/kleinstein/boyd_flu/june_2017/changeo_v1only/s$folder\_germ-pass.tab") or die("/ysm-gpfs/pi/kleinstein/boyd_flu/june_2017/changeo_v1only/s$folder\_germ-pass.tab");
my $h=<IN>;
$h=~ s/\r//g;
chomp($h);

my $subind=-1;
my $platind=-1;
my $yearind=-1;
my $visitind=-1;
my @head=split("\t",$h);
for(my$i=0;$i<scalar(@head);$i++){
	if($head[$i] eq "SUBJECT"){
		$subind=$i;
	}
	if($head[$i] eq "PLATFORM"){
		$platind=$i;
	}
	if($head[$i] eq "YEAR"){
		$yearind=$i;
	}
	if($head[$i] eq "VISIT"){
		$visitind=$i;
	}
}
if($subind==-1){die()}
if($platind==-1){die()}
if($yearind==-1){die()}
if($visitind==-1){die()}

while(<IN>){
	my $line=$_;
	$h=~ s/\r//g;
	$h=~s/ /\ /g;
	chomp($line);
	my @in=split("\t");
	if(!exists($found{"$in[$subind]/$in[$platind]/$in[$visitind]"})){ #horribly inelegant and inefficient, but works
		system("mkdir $in[$subind]");
		system("mkdir $in[$subind]/$in[$platind]");
		system("mkdir $in[$subind]/$in[$platind]");
		system("mkdir $in[$subind]/$in[$platind]/$in[$visitind]");
		open(OUT,">$in[$subind]/$in[$platind]/$in[$visitind]/seqs.tab") or die("$in[$subind]/$in[$platind]/$in[$visitind]/seqs.tab");
		print OUT "$h\n";
		close(OUT);
		$found{"$in[$subind]/$in[$platind]/$in[$visitind]"}=1;
	}
	open(OUT,">>$in[$subind]/$in[$platind]/$in[$visitind]/seqs.tab") or die("$in[$subind]/$in[$platind]/$in[$visitind]/seqs.tab");
	print OUT "$line\n";
	close(OUT);
	#die();
}
#die();
}
foreach my $subject (keys %found){
	$pm->start and next;
	print "$subject\n";
	system("mkdir $subject/clones");
	my $command="BuildTrees.py -d $subject/seqs.tab --outdir $subject/ --log $subject/clones.log --collapse --md DUPCOUNT --failed --minseq 2 > $subject/console.log";
	system($command) ==0 or die($command);
	$command="checkCollapse.pl $subject/clones.log";
	system($command) ==0 or die($command);
	$pm->finish;
}
$pm->wait_all_children;
