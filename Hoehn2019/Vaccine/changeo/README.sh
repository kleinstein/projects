 #Kenneth B Hoehn
 #29/May/2018

#Split by subject
ParseDb.py -d InfluenzaVaccination_MiSeq_BCR_Export_2017-02-02.tsv -f SUBJECT --outname church_flu

#decided to use a threshold of 0.1 after looking at dist to nearest plots
sbatch defineClonesIB.sh
sbatch defineClonesFV.sh
sbatch defineClonesGMC.sh

#construct germline using the germlines repo downloaded on 29/May/2018
CreateGermlines.py -d church_flu_SUBJECT-GMC_clone-pass.tab -r  /ysm-gpfs/home/kbh35/project/germlines/IMGT/human/vdj/imgt_human_IGHV.fasta  /ysm-gpfs/home/kbh35/project/germlines/IMGT/human/vdj/imgt_human_IGHD.fasta  /ysm-gpfs/home/kbh35/project/germlines/IMGT/human/vdj/imgt_human_IGHJ.fasta -g dmask --cloned > germlines.log
CreateGermlines.py -d church_flu_SUBJECT-FV_clone-pass.tab -r  /ysm-gpfs/home/kbh35/project/germlines/IMGT/human/vdj/imgt_human_IGHV.fasta  /ysm-gpfs/home/kbh35/project/germlines/IMGT/human/vdj/imgt_human_IGHD.fasta  /ysm-gpfs/home/kbh35/project/germlines/IMGT/human/vdj/imgt_human_IGHJ.fasta -g dmask --cloned >> germlines.log
CreateGermlines.py -d church_flu_SUBJECT-IB_clone-pass.tab -r  /ysm-gpfs/home/kbh35/project/germlines/IMGT/human/vdj/imgt_human_IGHV.fasta  /ysm-gpfs/home/kbh35/project/germlines/IMGT/human/vdj/imgt_human_IGHD.fasta  /ysm-gpfs/home/kbh35/project/germlines/IMGT/human/vdj/imgt_human_IGHJ.fasta -g dmask --cloned >> germlines.log

#split into timepoints
ParseDb.py split -d church_flu_SUBJECT-GMC_clone-pass_germ-pass.tab -f TIME_POINT --outname GMC
ParseDb.py split -d church_flu_SUBJECT-FV_clone-pass_germ-pass.tab -f TIME_POINT --outname FV
ParseDb.py split -d church_flu_SUBJECT-IB_clone-pass_germ-pass.tab -f TIME_POINT --outname IB

mkdir logs
mv *log logs
mv *log.out logs