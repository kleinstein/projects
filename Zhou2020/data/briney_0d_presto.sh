#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 14-
#SBATCH --constraint E5-2660_v3
#SBATCH -c 1
#SBATCH --mem-per-cpu=30G

# IC: internal C isotype
# BP: productive or not (briney annotation)
# BI: isotype (briney annotation)
# SP: sample

# NOTE
# change run controls ("RUN_*") accordingly
# see briney_presto_stats.txt for reference stats
# for MaskPrimers, use multi cores; total memory <20G
# for ParseHeaders add, and concatenation, 1 core, memory <4G
# for CollapseSeq, use 1 core, 120 or 120+G memory

# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

PATH_ROOT="/home/qz93/scratch60/briney2019/"
PATH_PARSED="${PATH_ROOT}raw_parsed/"
PATH_PRESTO="${PATH_ROOT}presto/"

PATH_IC_PRIMER="${PATH_PRESTO}AbSeq_internalC_M_G.fasta" 

PATH_LOG="${PATH_PRESTO}presto_$(date '+%m%d%Y_%H%M%S').log"

#PATH_SCRIPT="/home/qz93/projects/scripts/briney2019/"

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
python3 -V &>> "${PATH_LOG}"
MaskPrimers.py --version &>> "${PATH_LOG}"

#SUBJ_ARRAY=(326651 326713 316188 326650 326737 326780 326797 326907 327059 D103)
#SUBJ_ARRAY=(326651 326713) # JSON only
#SUBJ_ARRAY=(316188 326650 326737 326780 326797 326907 327059 D103) # CSV only
#SUBJ_ARRAY=(326737) # testing

#SUBJ_ARRAY=(326651) 
#SUBJ_ARRAY=(326713)
#SUBJ_ARRAY=(316188 326650 326737)
#SUBJ_ARRAY=(326780 326797 326907)
#SUBJ_ARRAY=(327059 D103)


N_SAMPLES="18"
SAMPLE_ARRAY=($(seq 1 ${N_SAMPLES}))
#SAMPLE_ARRAY=(17) # testing

N_BIOREPS="6"
BIOREP_ARRAY=($(seq 1 ${N_BIOREPS}))

#*
RUN_MP=FALSE
RUN_PH_ADD=FALSE
RUN_CS_SEP=FALSE # not used
RUN_CONCAT_TECH=FALSE
RUN_PH_DEL=FALSE
RUN_CS_CONCAT=FALSE
RUN_PL=TRUE
RUN_SS=TRUE
RUN_CONCAT_BIO=TRUE

SS_NUM=2

# every subject has its own folder, with 18 samples/files inside

for SUBJ in ${SUBJ_ARRAY[@]}; do

	DIR_PARSED="${PATH_PARSED}${SUBJ}/"
	DIR_PRESTO="${PATH_PRESTO}${SUBJ}/"

	SUBJ_OUTNAME="${SUBJ}_all"
	SUBJ_CONCAT="${SUBJ_OUTNAME}_reheader.fasta"

	# if DIR_PRESTO does not exist, mkdir
	if [ ! -d "${DIR_PRESTO}" ]; then
		mkdir "${DIR_PRESTO}"
	fi

	# loop thru samples
	for SAMPLE in ${SAMPLE_ARRAY[@]}; do

		SAMPLE_PARSED="${DIR_PARSED}${SUBJ}_${SAMPLE}.fasta"
		SAMPLE_OUTNAME="${SUBJ}_${SAMPLE}"

		echo "***********************************************" &>> "${PATH_LOG}"

		if [[ (-s "${SAMPLE_PARSED}" ) ]]; then

			####################
			### mask primers ###
			####################

			if [[ ${RUN_MP} == "TRUE" ]]; then

				# The “cut” mode will remove both the primer region and the preceding sequence. 
				# The “mask” mode will replace the primer region with Ns and remove the preceding sequence. 
				# The “trim” mode will remove the region preceding the primer, but leave the primer region intact. 
				# The “tag” mode will leave the input sequence unmodified.

				# Internal C primers: 16nt
				# 1/16 = 0.0625
				# 2/16 = 0.125
				# 3/16 = 0.1875

				# output: [outname]_primers-pass.fasta


				echo "##### ${SAMPLE_OUTNAME}: MP" &>> "${PATH_LOG}"

				MaskPrimers.py align \
					-s "${SAMPLE_PARSED}" \
					-p "${PATH_IC_PRIMER}" \
					--outname "${SAMPLE_OUTNAME}" \
					--outdir "${DIR_PRESTO}" \
					--log "${DIR_PRESTO}${SAMPLE_OUTNAME}_MP.log" \
					--failed \
					--mode trim \
					--pf IC \
					--maxerror 0.1 \
					--nproc "${NPROC}" \
					&>> "${PATH_LOG}"

			fi

			####################
			### parse header ###
			####################

			if [[ ${RUN_PH_ADD} == "TRUE" ]]; then
				
				echo "##### ${SAMPLE_OUTNAME}: add sample info to header" &>> "${PATH_LOG}"

				# output: [outname]_reheader.fasta

				ParseHeaders.py add \
					-s "${DIR_PRESTO}${SAMPLE_OUTNAME}_primers-pass.fasta" \
					-f SP \
					-u "${SAMPLE}" \
					--outname "${SAMPLE_OUTNAME}" \
					--outdir "${DIR_PRESTO}" \
					--failed \
					--fasta \
					&>> "${PATH_LOG}"

			fi
			
			###################
			### collapseSeq ###
			###################

			if [[ ${RUN_CS_SEP} == "TRUE" ]]; then

				echo "##### ${SAMPLE_OUTNAME}: CS (separately for each sample)" &>> "${PATH_LOG}"

				# output: 
				# [outname]_collapse-duplicate.fasta
				# [outname]_collapse-undetermined.fasta
				# [outname]_collapse-unique.fasta

				CollapseSeq.py \
					-s "${DIR_PRESTO}${SAMPLE_OUTNAME}_reheader.fasta" \
					--outname "${SAMPLE_OUTNAME}" \
					--outdir "${DIR_PRESTO}" \
					--log "${DIR_PRESTO}${SAMPLE_OUTNAME}_CS.log" \
					--failed \
					--fasta \
					--uf IC \
					--cf BP BI \
					--act set set \
					&>> "${PATH_LOG}"

			fi
			
		else
			echo "${SAMPLE_PARSED} does not exist" &>> "${PATH_LOG}"
		fi

	done

	##############
	### concat ###
	##############

	if [[ ${RUN_CONCAT_TECH} == "TRUE" ]]; then

		echo "##### ${SUBJ}: concatenate post-MP technical replicates by biological replicate" &>> "${PATH_LOG}"

		cd "${DIR_PRESTO}"

		# 1, 7,  13
		cat ${SUBJ}_1_reheader.fasta ${SUBJ}_7_reheader.fasta ${SUBJ}_13_reheader.fasta > ${SUBJ}_b1.fasta
		
		# 2, 8,  14
		cat ${SUBJ}_2_reheader.fasta ${SUBJ}_8_reheader.fasta ${SUBJ}_14_reheader.fasta > ${SUBJ}_b2.fasta

		# 3, 9,  15
		cat ${SUBJ}_3_reheader.fasta ${SUBJ}_9_reheader.fasta ${SUBJ}_15_reheader.fasta > ${SUBJ}_b3.fasta

		# 4, 10, 16
		cat ${SUBJ}_4_reheader.fasta ${SUBJ}_10_reheader.fasta ${SUBJ}_16_reheader.fasta > ${SUBJ}_b4.fasta

		# 5, 11, 17
		cat ${SUBJ}_5_reheader.fasta ${SUBJ}_11_reheader.fasta ${SUBJ}_17_reheader.fasta > ${SUBJ}_b5.fasta

		# 6, 12, 18
		cat ${SUBJ}_6_reheader.fasta ${SUBJ}_12_reheader.fasta ${SUBJ}_18_reheader.fasta > ${SUBJ}_b6.fasta

		#cat ${SUBJ}_*_reheader.fasta > "${SUBJ_CONCAT}"

	fi

	
	# loop thru biological replicates

	for BIOREP in ${BIOREP_ARRAY[@]}; do

		echo "##### ${SUBJ} biological replicate ${BIOREP}" &>> "${PATH_LOG}"

		##########################
		### ParseHeader delete ###
		##########################

		if [[ ${RUN_PH_DEL} == "TRUE" ]]; then
			
			echo "##### ${SUBJ} biorep ${BIOREP}: ParseHeaders delete SEQORIENT" &>> "${PATH_LOG}"

			# output: [outname]_reheader.fasta

			# atleast-
			if [[ (-s "${DIR_PRESTO}${SUBJ}_b${BIOREP}.fasta" ) ]]; then
				ParseHeaders.py delete \
					-s "${DIR_PRESTO}${SUBJ}_b${BIOREP}.fasta" \
					-f SEQORIENT \
					--outname "${SUBJ}_b${BIOREP}" \
					--outdir "${DIR_PRESTO}" \
					--failed \
					--fasta \
					&>> "${PATH_LOG}"
			else
				echo "${DIR_PRESTO}${SUBJ}_b${BIOREP}.fasta does not exist" &>> "${PATH_LOG}"
			fi

		fi

		###################
		### collapseSeq ###
		###################

		if [[ ${RUN_CS_CONCAT} == "TRUE" ]]; then
			
			echo "##### ${SUBJ} biorep ${BIOREP}: CS (combining technical reps for each biorep)" &>> "${PATH_LOG}"

			# output: 
			# [outname]_collapse-duplicate.fasta
			# [outname]_collapse-undetermined.fasta
			# [outname]_collapse-unique.fasta

			if [[ (-s "${DIR_PRESTO}${SUBJ}_b${BIOREP}_reheader.fasta" ) ]]; then
				CollapseSeq.py \
					-s "${DIR_PRESTO}${SUBJ}_b${BIOREP}_reheader.fasta" \
					--outname "${SUBJ}_b${BIOREP}" \
					--outdir "${DIR_PRESTO}" \
					--log "${DIR_PRESTO}${SUBJ}_b${BIOREP}_CS.log" \
					--failed \
					--fasta \
					--uf IC \
					--cf BP BI SP \
					--act set set set \
					&>> "${PATH_LOG}"
			else
				echo "${SUBJ}_b${BIOREP}_reheader.fasta does not exist" &>> "${PATH_LOG}"
			fi

		fi

		################
		### ParseLog ###
		################

		if [[ ${RUN_PL} == "TRUE" ]]; then
			
			echo "##### ${SUBJ} biorep ${BIOREP}: ParseLog for combined CS output" &>> "${PATH_LOG}"

			# output: [outname].tab

			if [[ (-s "${DIR_PRESTO}${SUBJ}_b${BIOREP}_CS.log" ) ]]; then
				ParseLog.py \
					-l "${DIR_PRESTO}${SUBJ}_b${BIOREP}_CS.log" \
					-f DUPCOUNT \
					--outname "${SUBJ}_b${BIOREP}_CS" \
					--outdir "${DIR_PRESTO}" \
					&>> "${PATH_LOG}"
			else
				echo "${SUBJ}_b${BIOREP}_CS.log does not exist" &>> "${PATH_LOG}"
			fi

		fi

		################
		### SplitSeq ###
		################

		if [[ ${RUN_SS} == "TRUE" ]]; then
			
			echo "##### ${SUBJ} biorep ${BIOREP}: SplitSeq for combined CS output" &>> "${PATH_LOG}"

			# output: 
			# [outname]_under-2.fasta
			# [outname]_atleast-2.fasta

			if [[ (-s "${DIR_PRESTO}${SUBJ}_b${BIOREP}_collapse-unique.fasta" ) ]]; then
				SplitSeq.py group \
					-s "${DIR_PRESTO}${SUBJ}_b${BIOREP}_collapse-unique.fasta" \
					-f DUPCOUNT \
					--num "${SS_NUM}" \
					--fasta \
					--outname "${SUBJ}_b${BIOREP}" \
					--outdir "${DIR_PRESTO}" \
					&>> "${PATH_LOG}"
			else
				echo "${SUBJ}_b${BIOREP}_collapse-unique.fasta does not exist" &>> "${PATH_LOG}"
			fi

		fi

	done

	##############
	### concat ###
	##############

	if [[ ${RUN_CONCAT_BIO} == "TRUE" ]]; then

		echo "##### ${SUBJ}: concatenate post-CS/SS biological replicates" &>> "${PATH_LOG}"

		cd "${DIR_PRESTO}"

		cat ${SUBJ}_b*_atleast-${SS_NUM}.fasta > "${SUBJ}_all_atleast-${SS_NUM}.fasta"

	fi	

done

