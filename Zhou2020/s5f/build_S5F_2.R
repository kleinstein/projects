##### build updated S5F model
# parameter tuning & visualize 1-mer subsitution

#**************
#### setup ####
#**************

MAC = T
if (MAC) {
    pathRoot = "~/Dropbox (recherche)/yale/" # mac
} else {
    pathRoot = "/ysm-gpfs/home/qz93/" # hpc
}

pathScript = paste0(pathRoot, "projects/scripts/")
pathData = paste0(pathRoot, "projects/spatial/data/")
pathWork = paste0(pathRoot, "projects/spatial/newS5F/")

library(shazam) # v0.1.11 (alakazam v0.2.11)
library(corrplot) # v0.84

#### setup ####

#*
MIN_NUM_MUTATIONS = 50
MIN_NUM_SEQ_MUTATIONS = 500

VISUAL_COUNT_SUB = F
VISUAL_SUB_1 = F
VISUAL_COUNT_MUT = F

saveName_sub_count = "sub_count.RData"
saveName_sub_1 = "sub_1mer.RData"
saveName_mut_count = "mut_count.RData"

#### sub - count ####

if (VISUAL_COUNT_SUB) {
    setwd(pathWork)
    load(saveName_sub_count) # sub_count
    
    sub_count_tune_range = seq(from=10, to=100, by=10)
    
    sub_count_tune = minNumMutationsTune(subCount=sub_count, 
                                         minNumMutationsRange=sub_count_tune_range)
    
    # each col sums up to 1024
    # rows: 5mer, 3mer, 1mer
    colSums(sub_count_tune)
    
    #* 50 looks fine (5mer 275)
    sinkName = "sub_count_tune.txt"
    sink(sinkName)
    print(sub_count_tune)
    sink()
    
    # from bottom to top: 5mer, 3mer, 1mer
    col_sub = c("5mer"="black", "3mer"="gray60", "1mer"="white")
    
    plotName = "sub_count_tune.pdf"
    pdf(plotName, width=6, height=5)
    par(mar=c(4,4.5,1,1)) # BLTR
    barplot(sub_count_tune, las=2, col=col_sub, 
            xlab="minNumMutations", ylab="Number of five-mers", cex.lab=1.25)
    dev.off()
    
    plotName = "sub_count_tune_legend.pdf"
    pdf(plotName, width=4, height=2)
    par(mar=c(0,0,0,0))
    plot(1:3, rep(1,3), xlim=c(1,4), pch=22, bg=col_sub, col=c(NA,NA,"black"),
         xlab="", ylab="", xaxt="n", yaxt="n", bty="n", cex=2)
    text((1:3)+0.5, rep(1,3), labels=names(col_sub), cex=1.25)
    dev.off()
}

#### sub - 1mer ####

if (VISUAL_SUB_1) {
    setwd(pathWork)
    load(saveName_sub_1) # sub_model_1mer_norm
    
    # row=from, col=to (rowSums == 1)
    rowSums(sub_model_1mer_norm)
    
    # print
    sinkName = "sub_1mer_norm.txt"
    sink(sinkName)
    print(round(sub_model_1mer_norm, 3))
    sink()
    
    # plot
    plotName = "sub_1mer_norm.pdf"
    pdf(plotName, width=5, height=5)
    corrplot(corr=sub_model_1mer_norm, 
             col=colorRampPalette(c("white", "hotpink"))(7),
             method="circle", type='full', is.corr=F, diag=F, title="", 
             # coefficients
             addCoef.col="black", number.cex=0.9, number.digits=2,
             # A/T/G/C label
             tl.cex=1.5, tl.col="black", tl.srt=0, tl.offset=0.7,
             # color label
             cl.pos="n", cl.lim=c(0,1), cl.cex=1.1, cl.align.text='l', 
             # BLTR
             mar=c(0,0,0,0)) 
    dev.off()
}

#### mut - count ####

if (VISUAL_COUNT_MUT) {
    setwd(pathWork)
    load(saveName_mut_count) # mut_count
    
    mut_count_tune_range = seq(from=100, to=600, by=50)
    
    mut_count_tune = minNumSeqMutationsTune(mutCount=mut_count, 
                                            minNumSeqMutationsRange=mut_count_tune_range)
    
    # 2xn matrix
    # rows: # 5-mers for which mutability would be computed directly ("measured") and inferred ("inferred")
    # cols: parameter value
    
    # each col sums up to 1024
    colSums(mut_count_tune)
    
    #* 500 looks fine (measured 569)
    sinkName = "mut_count_tune.txt"
    sink(sinkName)
    print(mut_count_tune)
    sink()
    
    # # from bottom to top: measured, inferred
    col_mut = c("measured"="black", "inferred"="white")
    
    plotName = "mut_count_tune.pdf"
    pdf(plotName, width=6, height=5)
    par(mar=c(4,4.5,1,1)) # BLTR
    barplot(mut_count_tune, las=2, col=col_mut, 
            xlab="minNumSeqMutations", ylab="Number of five-mers", cex.lab=1.25)
    dev.off()
    
    plotName = "mut_count_tune_legend.pdf"
    pdf(plotName, width=3, height=2)
    par(mar=c(0,0,0,0))
    plot(1:2, rep(1,2), xlim=c(1,3), pch=22, bg=col_mut, col=c(NA,"black"),
         xlab="", ylab="", xaxt="n", yaxt="n", bty="n", cex=2)
    text((1:2)+0.5, rep(1,2), labels=names(col_mut), cex=1.25)
    dev.off()
    
}

