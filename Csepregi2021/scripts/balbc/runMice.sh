# Kenneth B. Hoehn
# 8/29/2022
# Run remaining clonal clustering and phylogenetic analyses


files=("A1" "D4" "E5" "F6" "H8" "I9")
threads=18

for file in "${files[@]}"
do
    sbatch --partition=pi_kleinstein --cpus-per-task=$threads \
        --mem-per-cpu=5gb --job-name=$file cloning.sh $file $threads 1000
done


