# Scripts for BCR analyses in Ota, Hoehn, Fernandes-Braga et al. (2024, Science Translational Medicine)
Kenneth B. Hoehn
kenneth.b.hoehn@dartmouth.edu
05/09/24

Publication: https://doi.org/10.1126/scitranslmed.adi0673

Preprint: https://doi.org/10.1101/2023.01.25.525506 

## Subject information

HTO to subject ID conversion table in `info` folder.

## Processing 10X VDJ sequencing data

Within `10X_5-21` and `10X_7-12`, run

```
bash processVDJ.sh
Rscript combineVDJData.R
```

These will align BCR sequences to the IMGT reference data and compile them into AIRR formatted tables.

## Processing PacBio BCR data

Within the `pacbio` folder, edit the script `presto_subisotype.sh` to choose one of the following blocks by commenting out one and leaving the other:

```
# First run
base="MO-06232021-scBCR.ccs"
F_SEQ=info/fprimer_run1.fasta

# Second run
base="Miyo-scBCR-07142021.ccs"
F_SEQ=info/fprimer_run2.fasta
```

Then run:

```
bash presto_subisotype.sh

```

Then repeat after swapping which block was commented out. This will do the align the F and R primers to all reads. To run the additional step of re-clustering the data to account for issues with barcode uniqueness, run:

```
bash presto_second_subisotype.sh
```

This will build the consensus sequences. To align sequences to the IMGT reference database, run:
```
bash runIgblast.sh

```

To assign consensus sequences to a particular well, plate, subject, etc, run:
```
Rscript analysisPlate.R

```

## Combined analysis and figure generation

All figures are generated roughly in order by the following three R scripts in the `integrated` directory. These use the outputs of the above two sections. To demultiplex the data and perform all gene-expression based analyses, run:

```
Rscript gexAnalysis.R
```

To combine the gene expression and 10X VDJ data, run:

```
Rscript combineVDJData.R

```

To run analyses using both 10X and PacBio VDJ sequences, run:

```
Rscript convergentClones.R

```


