


use strict;
use warnings;

#my $sub=$ARGV[0];
my $sub="N";
my @files = glob("shmulations/*");
open(OUT,">SHMulateResults.tsv") or die();
print OUT "Type\tP\tRID\tTime\tParam\tMLE\tLCI\tUCI\n";
foreach my $f(@files){
	print "trying $f\n";
	if($f =~ /shmulations\/(\S+?)_dist_(\d+)_ncdr_gy_lineages.tsv_igphyml_stats_(\S+).txt/){
		print "$f\n";
		my $n=$1;
		my $time=$2;
		my $ri=$3;
		my $t=$3;
		$t =~ s/\_//g;
		if($t eq ""){$t="N";}
		if($ri eq ""){$ri="NA"}
		open(IN,$f) or die($f);
		my $maxdiv = 0;
		my $sumdiv = 0;
		my $lincount = 0;
		while(<IN>){
			my $line=$_;
			chomp($line);
				if($line=~/\. Estimated substitutions in repertoire\:	(\S+)/){print OUT "$t\t$n\t$ri\t$time\tsubst\t$1\t$1\t$1\n"}
				if($line=~/\. Transition\/transversion ratio\: 	(\S+)/){print OUT "$t\t$n\t$ri\t$time\tkappa\t$1\t$1\t$1\n"}
				elsif($line=~/\. Transition\/transversion ratio\: 	(\S+)\s+\((\S+)\, (\S+)\)/){print OUT "$t\t$n\t$ri\t$time\tkappa\t$1\t$2\t$3\n"}
				if($line=~/\. Omega 0:	(\S+)\s+\((\S+)\, (\S+)\)/){print OUT "$t\t$n\t$ri\t$time\tomega0\t$1\t$2\t$3\n"}
				elsif($line=~/\. Omega 0:	(\S+)/){print OUT "$t\t$n\t$ri\t$time\tomega0\t$1\t$1\t$1\n"}
				if($line=~/\. Omega 1:	(\S+)\s+\((\S+)\, (\S+)\)/){print OUT "$t\t$n\t$ri\t$time\tomega1\t$1\t$2\t$3\n"}
				elsif($line=~/\. Omega 1:	(\S+)/){print OUT "$t\t$n\t$ri\t$time\tomega1\t$1\t$1\t$1\n"}
				if($line=~/Motif\:\s+(\S+)\s+\d+\s+\d+\s+(\S+)\s+\((\S+)\,\s+(\S+)\)/){print  OUT "$t\t$n\t$ri\t$time\t$1\t$2\t$3\t$4\n"}
				elsif($line=~/Motif\:\s+(\S+)\s+\d+\s+\d+\s+(\S+)/){print  OUT "$t\t$n\t$ri\t$time\t$1\t$2\t$2\t$2\n"}
				if($line=~/Position \d\:\s+f\((\S\d)\)\=(\S+)\s+f\((\S\d)\)\=(\S+)\s+f\((\S\d)\)\=(\S+)\s+f\((\S\d)\)\=(\S+)/){
					print "$line\n";
					print OUT "$t\t$n\t$ri\t$time\t$1\t$2\t$2\t$2\n";
					print OUT "$t\t$n\t$ri\t$time\t$3\t$4\t$4\t$4\n";
					print OUT "$t\t$n\t$ri\t$time\t$5\t$6\t$6\t$6\n";
					print OUT "$t\t$n\t$ri\t$time\t$7\t$8\t$8\t$8\n";
				}
				if($line =~ /Ind\s+Seq\s+TreeL/){
					while(<IN>){
						$line = $_;
						last if $line =~ /^$/;
						my @in = split("\\s+",$line);
						$lincount++;
						$sumdiv += $in[2];
						if($in[2] > $maxdiv){
							$maxdiv = $in[2];
						}
					}
					my $meandiv = $sumdiv/$lincount;
					print OUT "$t\t$n\t$ri\t$time\tMaxDiv\t$maxdiv\t$maxdiv\t$maxdiv\n";
					print OUT "$t\t$n\t$ri\t$time\tMeanDiv\t$meandiv\t$meandiv\t$meandiv\n";
				}
		}
	}else{
		print("Skipping $f\n");
	}
}
