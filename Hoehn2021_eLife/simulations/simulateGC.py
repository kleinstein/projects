from multiprocessing import Pool
import time
import os
import subprocess
import random
import sys
import scipy as sp

def runSimulator(args):

   motifs, naive, sel, clone, times, samples, subject, carry_cap, rseed = [args[i] for i in range(0,len(args))]
   outbase = "../../data/processed/gc_simulations/" + subject + "/" + str(clone)

   sim_args = ["simulator.py",
   "--mutability",motifs+"/Mutability_S5F.csv",
   "--substitution",motifs+"/Substitution_S5F.csv",
   "--outbase", outbase,
   "--lambda0", str(0.365),
   "--n_to_sample"," ".join(samples),
   "--naive_seq_file", naive,
   "--obs_times"," ".join(times),
   "--selection",
   "--target_count",str(1),
   "--selection_strength",str(sel),
   "--carry_cap",str(carry_cap),
   "--id",subject,
   "--n_tries",str(100),
   "--no_plot",
   "--dont_write_hists",
   "--random_seed",str(rseed),
   ">",outbase+".log"
   ]
   out = subprocess.check_call(" ".join(sim_args), shell=True, stdout=True)
   rm_files = [
  # ".selection_sim.runstats.pdf",
   "_collapsed_runstat_color_tree.p",
   "_collapsed_tree.p",
   #"_collapsed_tree.svg",
   #"_collapsed_tree_colormap.p",
   #"_collapsed_tree_colormap.tsv",
   "_lineage_tree.p",
   #"_lineage_tree.svg",
   #"_min_aa_target_hdists.p",
   #"_n_mutated_nuc_hdists.p",
   #"_sampled_min_aa_target_hdists.p",
   #"_collapsed_runstat_color_tree.svg",
   "_stats.tsv",
   "_targets.fa"]

   if out == 0:
      for file in rm_files:
         os.remove(outbase + file)

if __name__ == "__main__":
    
    args = list()

    cmd_args = sys.argv
    print(cmd_args)

    first_sample = 10
    nsample = 50
    second_sample = int(cmd_args[1])
    selection = float(cmd_args[2])
    carry_cap = int(cmd_args[3])
    reps = int(cmd_args[4])
    nproc = int(cmd_args[5])

    pool = Pool(nproc)
    
    motifs = "../../../programs/bcr-phylo-benchmark/motifs"
    naive = "../../../programs/bcr-phylo-benchmark/sequence_data/AbPair_naive_seqs.fa"
    subject = str(second_sample) + "-" + str(selection) + "-" + str(carry_cap)
    subprocess.call(["mkdir","../../data/processed/gc_simulations/"+subject])

    args = []
    runargs = []
    rseed = 1
    for clone in range(0,reps):
        times   = [str(first_sample), str(second_sample)]
        samples = [str(nsample), str(nsample)]
        run = [motifs, naive, selection, clone, times, samples, subject, carry_cap, rseed]
        rseed += 1
        args.append(run)

    results = pool.map(runSimulator,args)
