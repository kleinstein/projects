#!/usr/bin/bash

# simulate under different circumstances
sbatch --partition=pi_kleinstein --cpus-per-task=20 simulateRepertoire.sh 50 "097" pi_kleinstein 20 20 10
sbatch --partition=pi_kleinstein --cpus-per-task=20 simulateRepertoire.sh 50 "097" pi_kleinstein 20 20 N

#do repertoire wide parameter estiamtes
rm IB/-1h/sims/*igphyml*
rm IB/-1h/sims/sims_5/*igphyml*
threads=4
for index in {19..49}
do
	echo $index
	if [ $index -gt 30 ]
	then
		echo "General!"
		sbatch --partition=general --cpus-per-task=$threads fitModels.sh "097/sims/sim_10" $index $threads
		sbatch --partition=general --cpus-per-task=$threads fitModels.sh "097/sims/sim_N" $index $threads
	else
		echo "Kleinstein!"
		sbatch --partition=pi_kleinstein --cpus-per-task=$threads fitModels.sh "097/sims/sim_N" $index $threads
		sbatch --partition=pi_kleinstein --cpus-per-task=$threads fitModels.sh "097/sims/sim_10" $index $threads
	fi
done


#show distributions
Rscript distributions.R

#make tables for results
Rscript visualizeSimResults.R
