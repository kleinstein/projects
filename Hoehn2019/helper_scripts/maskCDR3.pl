#!/usr/bin/perl
use strict;
use warnings;
use hotSpotUtils;


my $linfile = $ARGV[0];
my $obase;
if($linfile =~ /(\S+?)\_lineages(.*)\.tsv/){
	$obase=$1."_ncdr_lineages$2.tsv";
	print "$obase\n";
}

open(IN,$linfile) or die();
open(OUT,">$obase") or die();

my $h=<IN>;
print OUT $h;
while(<IN>){
	my $line = $_;
	chomp($line);
	my @in = split("\t",$line);
	if($in[0] =~ /(\S+)\.(fa\S*)/){
		my $nf = "$1\_ncdr\.$2";
		#print "$nf\n";
		if($in[-1] =~ /(\S+)\.(part.txt)/){
			my $np = "$1\_ncdr\.$2";
			#print "$np\n";
			open(P,$in[-1]) or die($in[-1]);
			my @P = <P>;
			my @p=split(",",$P[-1]);
			my $seqs = getfasta($in[0]);
			open(FOUT,">$nf") or die($nf);
			open(POUT,">$np") or die($np);
			my $nl=0;
			foreach my $k (keys %$seqs){
				my $ns="";
				my $s = $seqs->{$k};
				for(my$i=0;$i<(length($s));$i+=3){
					if($p[int($i/3)] != 108){
						$ns .= substr($s,$i,3);
					}
				}
				if(length($ns)%3!=0){die($ns)}
				if($k=~/GERM/){
					$nl = int(length($ns)/3);
				}
				print FOUT ">$k\n$ns\n";
			}
			print POUT "2 $nl\n$P[1]$P[2]$P[3]$P[4]$p[0]";
			for(my$i=1;$i<scalar(@p);$i++){
				if($p[$i] != 108){print POUT ",$p[$i]";}
			}
			#print POUT "\n";
			print OUT "$nf\t$in[1]\t$in[2]\t$np\n";
		}else{
			die($in[-1]);
		}
	}else{
		die($in[0]);
	}
}

