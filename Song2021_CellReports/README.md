The code in this repository will reproduce the analysis and figures in Song et al. 2021 [doi.org/10.1016/j.xcrm.2021.100288](doi.org/10.1016/j.xcrm.2021.100288) using the appropriate software versions. 

This code requires that the 'podman' package be downloaded (https://podman.io/getting-started/installation.html) to use docker images. 

The overall structure of this directory which is also available on the docker: `notebooks` contains analysis notebooks, `docker` contains the files to make the docker image (see below).

### Download docker image

First download the image, and check that you can enter the shell. We show code for a dummy directory to be mounted `/path_to_mount_data_dir`. See https://docs.docker.com/storage/volumes/ for details on how volumes work. 

```{bash}
# use this command to download the image
podman pull ruoyijiang/ganymede:song2021

# run without mounting to test it is working, change port '8889' if occupied on your machine.
podman run -it  -p 8889:8889 ruoyijiang/ganymede:song2021 bash

# run the image with a mounted directory (dummy directory shown)
podman run -it  -p 8889:8889 -v /path_to_mount_data_dir:/data:z ruoyijiang/ganymede:song2021 bash
```

Now try launching a notebook from inside the interactive container shell

```{bash}
# to launch notebooks from within the container shell, run
jupyter notebook --no-browser --ip 0.0.0.0 --allow-root --port=8889
```

After this last step, type `localhost:8889` in your local web browser to connect and fill in the token from the container shell standard output. Control-C to kill the notebook if needed.

Click to go to `/git/projects/Song2021_CellReports` to find the notebooks relevant to this study. 




### Download the relevant files and arrange

The NCBI accession NNNNNN can be used to pull data (https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=NNNNNNNN). 

If a lot of difficulty is found trying to identify where these files are found, please contact the corresponding author who will contact me (Roy). The consolidated Change-O/AIRR table and the single cell gene expression matrices are the most important starting points for reproducing the main analysis notebooks. 

# Setup
We assume that there is a folder called `cellranger` in data/the mount point containing the output from running `cellranger`. 

# Processing
Please follow the instructions in `PROCESSING.ipynb` to reconstruct Change-O file from sequencing of single cell repertoires. 

# Analysis
Please follow the instructions in `ANALYSIS.ipynb` to generate the publication figures. 



