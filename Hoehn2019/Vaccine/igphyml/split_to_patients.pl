

use strict;
use warnings;
use Parallel::ForkManager;

my $pm= new Parallel::ForkManager($ARGV[0]);

my @subjects = ("IB","FV","GMC");
my @tps = ("+28d","+21d","+14d","+7d","+3d","+1h","+1d","-8d","-2d","-1h");
foreach my $sub (@subjects){

	my %found;
	foreach my $tp (@tps){
		if(!exists($found{"$sub/$tp"})){ #horribly inelegant and inefficient, but works
			system("mkdir $sub");
			system("mkdir $sub/$tp");
			system("cp ../changeo_ken_v4/$sub\_TIME_POINT-$tp.tab $sub/$tp/seqs.tab") == 0 or die();
			$found{"$sub/$tp"}=1;
		}
	}
	
	foreach my $subject (keys %found){
		$pm->start and next;
		print "$subject\n";
		my $file="$subject/seqs.tab";
		my $command="BuildTrees.py -d $file --outdir $subject/ --log $subject/clones.log --collapse --failed --minseq 2 > $subject/consolen.log";
		system($command) ==0 or die($command);
		$command="checkCollapse.pl $subject/clones.log >> $subject/console.log";
		system($command) ==0 or die($command);
		$pm->finish;
	}
	$pm->wait_all_children;	
}
