# nf-core/airrflow benchmarking code

This repository contains the code to reproduce the benchmarking analysis with simulated data of the [nf-core/airrflow](https://nf-co.re) pipeline. The nf-core/airrflow pipeline can be found on the nf-core GitHub repository [github.com/nf-core/airrflow](https://github.com/nf-core/airrflow), and the documentation for using the pipeline can be found on the [nf-core website](https://nf-co.re/airrflow).

## Simulation of BCR repertoires

The code to simulate the BCR repertoires can be found under `simulate-repertoires`. To generate simulated repertoires the following steps were taken:
1. Simulation of V(D)J rearrangements and clonal expansion (for B-cell repertoires) with `ImmuneSIM` and `shmulateTree`. Execute the `simulate_repertoire_BCR_withsingletons.ipynb` and `simulate_repertoire_BCR-realtrees-with-singletons.ipynb` (BCR) jupyter notebooks. You can use `jupyter lab` that is included in the conda environment. This will generate a tsv table and fasta file with the repertoire sequences.
2. Simulation of the library preparation and sequencing data with the protocols with and without UMI barcodes. For this, execute the `process_BCR_umi.sh` and `process_BCR.sh` scripts. This will call the required python scripts for the analysis that are provided within this repository.

The simulated repertoires to benchmark nf-core/airrflow have also been uploaded on Zenodo [10.5281/zenodo.10989592](https://doi.org/10.5281/zenodo.10989592).

## Processing the benchmarking data

The `airrflow-benchmark` directory contains the commands used to analyze the BCR simulated data with the nf-core/airrflow pipeline, as well as all the necessary input files for the analysis.

The `mixcr-benmchmark` directory contains the commands used to analyze the same data with the `MiXCR` tool, for a comparative analysis.

## Generating the benchmarking plots for the paper

The jupyter notebook `benchmark_results_analysis_BCR_airrflow30.ipynb` to reproduce the benchmark analysis can be found under `benchmark-analysis`.

## Processing the COVID / healthy datasets

The code for processing the COVID datasets with nf-core/airrflow and the convergence data validation can be found under `covid-datasets`.
