### Data and scripts to generate figures and tables for manuscript  
### *Platelet response to influenza vaccination reflects effects of aging*

The main subdirectory containing all associated code is in /scripts.  The code in this subdirectory requires data in the other subdirectories to run.  

The README in /scripts describes each script and the corresponding figures and/or tables in the manuscript.  

All scripts written by Anna Konstorum.  

Contact <anna.konstorum@yale.edu> with any questions or issues.
