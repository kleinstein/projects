# Kenneth B. Hoehn
# 8/29/2022
# Run IgBlast on each sample using BALB/c and IMGT germlines


module load Python/3.8.6-GCCcore-10.2.0

ids=(A1-1 A1-2 A1-3 A1-4 A1-5 A1-6 D4-1 D4-2 D4-3 D4-4 D4-5 D4-6 E5-1 E5-2 E5-3 E5-4 E5-5 E5-6 F6-1 F6-2 F6-3 F6-4 F6-5 F6-6 H8-1 H8-2 H8-3 H8-4 H8-5 H8-6 I9-1 I9-2 I9-3 I9-4 I9-5 I9-6)
threads=36

echo "" > blast_log.txt

for id in "${ids[@]}"
do
	bash runIgblastBalbc.sh $id $threads >> blast_log.txt
done
