Code for Alsoussi\*, Turner\*, Case\*, Zhao\*, Schmitz\* & Zhou\* et al., J. Immunol., 2020.

A Potently Neutralizing Antibody Protects Mice against SARS-CoV-2 Infection

<https://doi.org/10.4049/jimmunol.2000583>

BioProject PRJNA624801

------------------------------------------------------------------------------------

0_CR\*.sh preprocess FASTQ files using Cell Ranger (10x Genomics)

bcr_\*.sh/R process and analyze BCR sequences

gex_\*.sh/R process and analyze gene expression

julian_\*.R and susanna_\*.R contain helper functions called by bcr_\* and/or gex_\* scripts
