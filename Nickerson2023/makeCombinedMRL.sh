# Kenneth B. Hoehn
# 7/22/22
# Set up combined MRL germline

# combine initial and novel alleles
in="MRL_IGHV_combined"
export IGDATA=share/igblast
threads=7

# align combined files to IMGT
cat raw_naive/MRL_IGHV.fasta > raw_naive/${in}.fasta
cat raw_naive/results/novel_filtered_ungapped.fasta >> raw_naive/${in}.fasta

ncbi-igblast-1.18.0/bin/igblastn -query raw_naive/${in}.fasta -out raw_naive/${in}_igblast.fmt7 \
	-organism mouse -ig_seqtype Ig \
	-auxiliary_data share/igblast/optional_file/mouse_gl.aux \
	-germline_db_V share/igblast/database/imgt_mouse_ig_v \
	-germline_db_D share/igblast/database/imgt_mouse_ig_d \
	-germline_db_J share/igblast/database/imgt_mouse_ig_j \
	-domain_system imgt -outfmt '7 std qseq sseq btop'

MakeDb.py igblast -i raw_naive/${in}_igblast.fmt7 -s raw_naive/${in}.fasta \
  -r share/germlines/imgt/mouse/vdj/imgt_mouse_IGHV.fasta \
  share/germlines/imgt/mouse/vdj/imgt_mouse_IGHD.fasta \
  share/germlines/imgt/mouse/vdj/imgt_mouse_IGHJ.fasta \
  --extended --partial

# create fasta file of gapped MRL sequences
Rscript formatGermline.R ${in} >> other/formatGermline.log

seaview raw_naive/${in}_gapped_gl.fa

# make new MRL germline
echo "Setting up combined MRL germline"

# copy over mouse D and J segments
mkdir share/germlines/imgt/mrl-combined
mkdir share/germlines/imgt/mrl-combined/vdj
cp share/germlines/imgt/mouse/vdj/imgt_mouse_IGHD.fasta share/germlines/imgt/mrl-combined/vdj/imgt_mrl-combined_IGHD.fasta
cp share/germlines/imgt/mouse/vdj/imgt_mouse_IGHJ.fasta share/germlines/imgt/mrl-combined/vdj/imgt_mrl-combined_IGHJ.fasta

# copy over gapped MRL V gene segments
cp raw_naive/${in}_gapped.fasta share/germlines/imgt/mrl-combined/vdj/imgt_mrl-combined_IGHV.fasta

# copy over mouse aux file (corresponds to J segments, which are mouse)
cp share/igblast/optional_file/mouse_gl.aux share/igblast/optional_file/mrl-combined_gl.aux

#set up internal data dir
mkdir share/igblast/internal_data/mrl-combined

echo "Setting up IgBlast files"
# set up igblast databases
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in raw_naive/${in}.fasta \
	-out share/igblast/database/imgt_mrl-combined_ig_v
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in share/igblast/fasta/imgt_mouse_ig_j.fasta \
	-out share/igblast/database/imgt_mrl-combined_ig_j
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in share/igblast/fasta/imgt_mouse_ig_d.fasta \
	-out share/igblast/database/imgt_mrl-combined_ig_d
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in raw_naive/${in}.fasta \
	-out share/igblast/internal_data/mrl-combined/mrl-combined_V

# create internal_data file
Rscript makeInternalBlastFile.R raw_naive/${in}_gapped.fasta \
	share/igblast/internal_data/mrl-combined/mrl-combined.ndm.imgt >> other/makeInternalBlastFile.txt
