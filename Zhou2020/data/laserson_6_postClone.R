PATH_CHANGEO = "/ysm-gpfs/home/qz93/scratch60/spatial_data/church_flu/changeo/"
PATH_COLLAPSE = "/ysm-gpfs/home/qz93/scratch60/spatial_data/church_flu/collapseClone/"
N_PROC = 15 #* to match -c in shell launch script

# flu data is bulk unsorted
# use class-switched only (one most mutated seq per clone, regardless of isotype)
# Current implementation is that after collapsing, one gets, from each clone that
# contains class-switched sequences, the most mutated class-switched sequence.
# (In such a clone, even if the most mutated seq is non-class-switched, the collapsed
#  clone will be represented by the most mutated AND class-switched seq)

CS_ISOTYPES = paste0("Human-IGH", c("A", "E", "G")) #*

# collapse clones without considering isotype (i.e. 1 seq per clone)
ISOTYPE_BLIND = T #*
# collapse clones on a by-isotype basis (i.e. # seqs per clone = # isotypes in that clone)
ISOTYPE_SPECIFIC = F #* 


library(shazam) # v0.1.11

for (subj in c("FV", "IB", "GC")) { #*
#for (subj in c("GC")) { #*    
    cat(subj, "\n")
    
    ##### load db
    setwd(PATH_CHANGEO)
    db = read.table(paste0(subj, "_germ-pass.tab"), #*
                    sep="\t", header=T, stringsAsFactors=F)
    
    ##### filter out ones with V shorter than 312
    # note: this should have been performed already by *_igblast.sh
    # so really this ought to be a sanity check and 
    # 'no seq filtered' should be expected
    boolV312 <- db$V_GERM_LENGTH_IMGT >= 312
    stopifnot(any(boolV312))
    
    if (sum(boolV312)==nrow(db)) {
        cat("no seq filtered for V shorter than 312 \n")
    } else {
        cat(sum(!boolV312), "seqs filtered for V shorter than 312 \n")
        db = db[boolV312, ]
    }
    rm(boolV312)

    ##### clone size
    #* calc clone size before subsetting to class-switched so as to reflect true clone size
    # (in case clone includes non-CS sequences in addition to CS sequences)
    db$CLONE_SIZE = NA
    uniqCloneID = unique(db$CLONE)
    for (cloneID in uniqCloneID) {
        db$CLONE_SIZE[db$CLONE==cloneID] = sum(db$CLONE==cloneID)
    }
    
    ##### mutation frequency
    db = observedMutations(db, sequenceColumn="SEQUENCE_IMGT",
                           germlineColumn="GERMLINE_IMGT_D_MASK", 
                           regionDefinition=IMGT_V, 
                           mutationDefinition=NULL,
                           frequency=T, combine=T, nproc=N_PROC)
    
    dbMaster = db; rm(db)
    
    ##### subset to class-switched
    COL_ISOTYPE = "PRCONS" #*

    # check if any isotype is NA; remove if there's any
    isotypeNAbool = is.na(dbMaster[, COL_ISOTYPE])
    cat("number of NAs in isotype column:", sum(isotypeNAbool), "\n")
    if (any(isotypeNAbool)) {
        dbMaster = dbMaster[!isotypeNAbool, ]
    }
    
    # if $COL_ISOTYPE is NA, should evalute to FALSE via %in%
    # (though this becomes irrelavant after removing any NA first)
    db = dbMaster[dbMaster[, COL_ISOTYPE] %in% CS_ISOTYPES, ]
    
    
    ##### meta info
    setwd(PATH_CHANGEO)
    sink(paste0(subj, "_classSwitched_meta_before_collapsing.txt")) #*
    
    cat("total number of seqs \n")
    print(nrow(db))
    
    cat("sample & timepoint n") #*
    print(table(db$SAMPLE, db$TIMEPOINT, useNA="ifany"))
    
    cat("isotypes (", COL_ISOTYPE, ") \n") #*
    print(table(db[, COL_ISOTYPE], useNA="ifany"))
    
    cat("sample & timepoint & isotypes \n") #*
    print(table(db$SAMPLE, db$TIMEPOINT, db[, COL_ISOTYPE], useNA="ifany"))
    
    cat("number of unique clones \n")
    cat(paste0("note that clones containing only non-class-switched sequences, if any, have been excluded \n"))
    print(length(unique(db$CLONE)))
    
    cat("distribution of clone size \n")
    cat(paste0("note that clones containing only non-class-switched sequences, if any, have been excluded \n"))
    cat(paste0("note that clone size for a clone includes its non-class-switched sequences too, if any \n"))
    print(summary(as.numeric(table(db$CLONE))))
    
    cat("counts of clones of a particular size (upper row=clone size; lower row=# clones) \n")
    cat(paste0("note that clones containing only non-class-switched sequences, if any, have been excluded \n"))
    cat(paste0("note that clone size for a clone includes its non-class-switched sequences too, if any \n"))
    print(table(as.numeric(table(db$CLONE))))
    
    sink()
    
    setwd(PATH_CHANGEO)
    save(db, file=paste0(subj, "_classSwitched_germ-pass_mutFreqIMGTV.RData")) #*
    
    ##### collapse clones
    setwd(PATH_COLLAPSE)
    
    # make a copy of db
    dbCopy = db; rm(db)
    
    ### without considering isotype
    if (ISOTYPE_BLIND) {
        db = collapseClones(db=dbCopy, cloneColumn="CLONE",
                            sequenceColumn="SEQUENCE_IMGT",
                            germlineColumn="GERMLINE_IMGT_D_MASK",
                            muFreqColumn="MU_FREQ",
                            regionDefinition=IMGT_V,
                            method="mostMutated",
                            includeAmbiguous=F, breakTiesStochastic=F,
                            expandedDb=F, nproc=N_PROC)
        
        # sanity check
        stopifnot( nrow(db) == length(unique(db$CLONE)) )
        
        # meta info
        sink(paste0(subj, "_classSwitched_meta_after_collapsing_isotype-blind.txt")) #*
        
        cat("total number of seqs \n")
        print(nrow(db))
        
        cat("sample & timepoint n") #*
        print(table(db$SAMPLE, db$TIMEPOINT, useNA="ifany"))
        
        cat("isotypes (", COL_ISOTYPE, ") \n") #*
        print(table(db[, COL_ISOTYPE], useNA="ifany"))
        
        cat("sample & timepoint & isotypes \n") #*
        print(table(db$SAMPLE, db$TIMEPOINT, db[, COL_ISOTYPE], useNA="ifany"))
        
        cat("number of unique clones \n")
        cat(paste0("note that clones containing only non-class-switched sequences, if any, have been excluded \n"))
        cat("expect the same number as that of total number of seqs \n")
        print(length(unique(db$CLONE)))
        
        cat("distribution of clone size \n")
        cat("expect all density concentrating on size 1 \n")
        print(summary(as.numeric(table(db$CLONE))))
        
        cat("counts of clones of a particular size (upper row=clone size; lower row=# clones) \n")
        cat("expect all clones having size 1")
        print(table(as.numeric(table(db$CLONE))))
        
        sink()
        
        save(db, file=paste0(subj, "_classSwitched_collapseClones_isotype-blind.RData")) #*
        rm(db)
    }
    
    ### by-isotype
    if (ISOTYPE_SPECIFIC) {
        # all the unique isotypes 
        # NA shouldn't be a problem because any NA would have been removed earlier before collapsing
        uniqueIsotypes = unique(dbCopy[, COL_ISOTYPE])
        # hold collapsed db for each unique isotype in a list
        isotypeDbList = vector("list", length(uniqueIsotypes))
        # collapse for each isotype
        for (i in 1:length(uniqueIsotypes)) {
            isoDbFull = dbCopy[dbCopy[, COL_ISOTYPE]==uniqueIsotypes[i], ]
            isoDbCollapsed = collapseClones(db=isoDbFull, cloneColumn="CLONE",
                                            sequenceColumn="SEQUENCE_IMGT",
                                            germlineColumn="GERMLINE_IMGT_D_MASK",
                                            muFreqColumn="MU_FREQ",
                                            regionDefinition=IMGT_V,
                                            method="mostMutated",
                                            includeAmbiguous=F, breakTiesStochastic=F,
                                            expandedDb=F, nproc=N_PROC)
            isotypeDbList[[i]] = isoDbCollapsed
        }
        # combine collapsed db for each unique isotype
        db = do.call(rbind, isotypeDbList)
        
        
        # sanity check
        # from full db: sum of the number of unique isotypes in each clone 
        # should equal to
        # from collapse db: total number of rows
        numIsoPerClone = sapply(unique(dbCopy$CLONE), 
                                function(x){length(unique(dbCopy[dbCopy$CLONE==x, COL_ISOTYPE]))})
        stopifnot(sum(numIsoPerClone)==nrow(db))
        
        # meta info
        sink(paste0(subj, "_classSwitched_meta_after_collapsing_by-isotype.txt")) #*
        
        cat("total number of seqs \n")
        print(nrow(db))
        
        cat("sample & timepoint n") #*
        print(table(db$SAMPLE, db$TIMEPOINT, useNA="ifany"))
        
        cat("isotypes (", COL_ISOTYPE, ") \n") #*
        print(table(db[, COL_ISOTYPE], useNA="ifany"))
        
        cat("sample & timepoint & isotypes \n") #*
        print(table(db$SAMPLE, db$TIMEPOINT, db[, COL_ISOTYPE], useNA="ifany"))
        
        cat("number of unique clones \n")
        cat(paste0("note that clones containing only non-class-switched sequences, if any, have been excluded \n"))
        cat("expect this number to be <= total number of seqs \n")
        print(length(unique(db$CLONE)))
        
        cat("distribution of clone size \n")
        cat("density does not have to be all concentrating on size 1 \n")
        print(summary(as.numeric(table(db$CLONE))))
        
        cat("counts of clones of a particular size (upper row=clone size; lower row=# clones) \n")
        cat("this reflects the number of isotypes in each clone")
        print(table(as.numeric(table(db$CLONE))))
        
        sink()
        
        save(db, file=paste0(subj, "_classSwitched_collapseClones_by-isotype.RData")) #*
        rm(db)
    }
    
    rm(dbCopy)
    
    rm(dbMaster)
}

setwd(PATH_COLLAPSE)
sink("sessionInfo.txt")
sessionInfo()
sink()