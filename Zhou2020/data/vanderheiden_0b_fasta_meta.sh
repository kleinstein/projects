PATH_PRESTO_1="/ysm-gpfs/home/qz93/scratch60/spatial_data/oconnor_mg/presto_fastq/"
PATH_PRESTO_2="/ysm-gpfs/home/qz93/scratch60/spatial_data/oconnor_mg/presto_fastq/"

cd "${PATH_PRESTO_1}"

# add meta

ParseHeaders.py add -s "A79HP_HD07M-FIN_collapse-unique_atleast-2.fastq" -o "HD07M_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD07 Memory

ParseHeaders.py add -s "A79HP_HD07N-FIN_collapse-unique_atleast-2.fastq" -o "HD07N_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD07 Naive

ParseHeaders.py add -s "AB0RF_HD07U-FIN_collapse-unique_atleast-2.fastq" -o "HD07U_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD07 Unsorted

ParseHeaders.py add -s "AAYHL_HD09M-FIN_collapse-unique_atleast-2.fastq" -o "HD09M_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD09 Memory

ParseHeaders.py add -s "AB8KB_HD09N-FIN_collapse-unique_atleast-2.fastq" -o "HD09N_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD09 Naive

ParseHeaders.py add -s "AB0RF_HD09U-FIN_collapse-unique_atleast-2.fastq" -o "HD09U_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD09 Unsorted

ParseHeaders.py add -s "AAYHL_HD10M-FIN_collapse-unique_atleast-2.fastq" -o "HD10M_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD10 Memory

ParseHeaders.py add -s "AB8KB_HD10N-FIN_collapse-unique_atleast-2.fastq" -o "HD10N_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD10 Naive

ParseHeaders.py add -s "AB0RF_HD10U-FIN_collapse-unique_atleast-2.fastq" -o "HD10U_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD10 Unsorted

ParseHeaders.py add -s "AAYHL_HD13M-FIN_collapse-unique_atleast-2.fastq" -o "HD13M_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD13 Memory

ParseHeaders.py add -s "AB8KB_HD13N-FIN_collapse-unique_atleast-2.fastq" -o "HD13N_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD13 Naive

ParseHeaders.py add -s "AB0RF_HD13U-FIN_collapse-unique_atleast-2.fastq" -o "HD13U_collapse-unique_atleast-2.fastq" -f SUBJECT SORT -u HD13 Unsorted

# convert fastq to fasta

cat "HD07M_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD07M_collapse-unique_atleast-2.fasta"
cat "HD07N_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD07N_collapse-unique_atleast-2.fasta"
cat "HD07U_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD07U_collapse-unique_atleast-2.fasta"

cat "HD09M_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD09M_collapse-unique_atleast-2.fasta"
cat "HD09N_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD09N_collapse-unique_atleast-2.fasta"
cat "HD09U_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD09U_collapse-unique_atleast-2.fasta"

cat "HD10M_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD10M_collapse-unique_atleast-2.fasta"
cat "HD10N_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD10N_collapse-unique_atleast-2.fasta"
cat "HD10U_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD10U_collapse-unique_atleast-2.fasta"

cat "HD13M_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD13M_collapse-unique_atleast-2.fasta"
cat "HD13N_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD13N_collapse-unique_atleast-2.fasta"
cat "HD13U_collapse-unique_atleast-2.fastq" | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > "${PATH_PRESTO_2}HD13U_collapse-unique_atleast-2.fasta"

# concat
cd "${PATH_PRESTO_2}"
cat HD07*.fasta > HD07_collapse-unique_atleast-2.fasta
cat HD09*.fasta > HD09_collapse-unique_atleast-2.fasta
cat HD10*.fasta > HD10_collapse-unique_atleast-2.fasta
cat HD13*.fasta > HD13_collapse-unique_atleast-2.fasta
