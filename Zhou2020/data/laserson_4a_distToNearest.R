pathRoot = "/ysm-gpfs/home/qz93/scratch60/spatial_data/church_flu/"
pathData = paste0(pathRoot, "slide/")
pathWork = paste0(pathRoot, "changeo/distToNearest/")

library(alakazam) # v0.2.11
library(shazam) # v0.1.10+
library(doMC)
library(foreach)

#num.cores.foreach = detectCores() - 1 #*
num.cores.foreach = 10 #*

registerDoMC(num.cores.foreach) #* if using foreach loop

#### session info
setwd(pathWork)
sink("computingEnv.txt")
date()
sessionInfo()
sink()

#*
CALC_WITHIN=T
CALC_BETWEEN=T
FIND_THRESH=T
COL_SUBJECT="SUBJECT"
SUBSAMPLE_BETWEEN=NULL

SUBJECTS = c("FV", "IB", "GC")

#### within-subject distToNearest ####

if (CALC_WITHIN) {
    # loop thru subjects (nproc inside regular for loop)
    for (subj in SUBJECTS) {
        
        # load data
        setwd(pathData)
        cat("##################### subject: ", subj, "\n")
        load(paste0(subj, "_IGHV_FUNCTIONAL-T_genotyped_slideFiltered.RData")) #* db
        
        # run distToNearest
        setwd(pathWork)
        cat("# reads = ", nrow(db), "\n")
        cat("calculating dist to nearest \n")
        db = distToNearest(db, sequenceColumn="JUNCTION", 
                           vCallColumn="V_CALL_GENOTYPED", #*
                           jCallColumn = "J_CALL",
                           model="ham", 
                           normalize="len", first=F, 
                           nproc=num.cores.foreach, 
                           progress=T)
        save(db, file=paste0("distToNearest_", subj, ".RData"))
        rm(db)
    }
}


#### between-subject distToNearest ####

if (CALC_BETWEEN) {

    # concat all subjects
    for (i in 1:length(SUBJECTS)) {
        
        filename = paste0(SUBJECTS[i], "_IGHV_FUNCTIONAL-T_genotyped_slideFiltered.RData")
        pathSubject = paste0(pathData, filename)
        
        cat("loading", filename, "\n")
        load(pathSubject) # db

        if (i==1) {
            # initiate 
            db_all = db
        } else {
            # append new rows 
            db_all = rbind(db_all, db)
        }
        rm(db)
    }
    db = db_all; rm(db_all)
    # check
    cat("breakdown by subject:\n")
    print(table(db[[COL_SUBJECT]]))
    
    cat("calculating between-subject distToNearest... \n")
    
    db = distToNearest(db, sequenceColumn="JUNCTION", 
                       vCallColumn="V_CALL_GENOTYPED", #*
                       jCallColumn = "J_CALL",
                       model="ham", 
                       normalize="len", first=F, 
                       nproc=num.cores.foreach, 
                       cross = COL_SUBJECT, # between-subject
                       subsample=SUBSAMPLE_BETWEEN,
                       progress=T)
    
    setwd(pathWork)
    saveName = paste0("distToNearest_btwSubj_subsample-", 
                      ifelse(is.null(SUBSAMPLE_BETWEEN), "NULL", SUBSAMPLE_BETWEEN),
                      ".RData")
    save(db, file=saveName)
    
    rm(db) 
}


#### findThreshold ####

if (FIND_THRESH) {

    #* if using foreach loop
    # do this to prevent from getting this error on Farnam:
    # "Error in summary.connection(connection) : invalid connection Calls: %dopar% ... 
    # sendData.SOCKnode -> serialize -> summary -> summary.connection"
    registerDoSEQ()
    registerDoMC(num.cores.foreach)

    #! from Nima:
    # important to visually inspect for bi-modality first before running findTreshold
    # low seq count (say, <1000) and lack of bi-modality => won't work (stuck)

    #* if not using foreach loop
    #thresh.list = vector("list", length(SUBJECTS)) 
    #names(thresh.list) = SUBJECTS

    cat("##### findThreshold \n")

    thresh.list = foreach(subj=SUBJECTS) %dopar% {

        cat("\nsubject = ", subj, "\n")
        setwd(pathWork)
        load(paste0("distToNearest_", subj, ".RData")) # db

        threshObj = findThreshold(distances=db$DIST_NEAREST, method="density", 
                                  subsample=NULL, progress=T)

        # save for individual first in case any other individual failed
        # (since it can take a long time to run)
        setwd(pathWork)
        save(threshObj, file=paste0("thresh_", subj, "_density_full.RData"))
        
        return(list(threshObj=threshObj))
    }

    names(thresh.list) = SUBJECTS

    # save as a single object for all individuals
    setwd(pathWork)
    save(thresh.list, file=paste0("thresh_all_density_full.RData"))

    #### output thresholds ####

    # save as a plain text file
    # to be used by shell script for changeo threshold (if using a different threshold
    # for each subject)
    # the format is fixed -- DO NOT CHANGE

    thresh.list = thresh.list[!unlist(lapply(thresh.list, is.null))]
    thresh.vec = unlist(lapply(thresh.list, function(obj){ obj[["threshObj"]]@threshold }))

    sink(paste0("thresh_all_density_full.txt"))
    for (i in 1:length(thresh.vec)) {
        # sep="" essential to get rid of space that follows
        cat(names(thresh.vec)[i], sep="", "\n")
        cat(thresh.vec[i], sep="", "\n")
    }
    sink()

}

