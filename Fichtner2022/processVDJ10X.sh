#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --cpus-per-task=20
#SBATCH --mem-per-cpu=5gb
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=120:00:00
#SBATCH --mail-type=FAIL,END
#SBATCH --mail-user=kenneth.hoehn@gmail.com

module load Python/3.7.0-foss-2018b

dirs=(MYG274_BCR_VHT_cellranger MYG320_BCR_VHT_cellranger)

nproc=20

for wdir in ${dirs[@]}
do
	wdir="10x/$wdir"
	echo $wdir
	changeo-10x.sh -s $wdir/filtered_contig.fasta -a $wdir/filtered_contig_annotations.csv \
    	-g human -t ig -p 3 -o $wdir -b ~/share/igblast \
    	-r ~/share/germlines/imgt/human/vdj -p $nproc
done
