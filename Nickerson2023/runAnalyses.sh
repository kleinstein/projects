# Kenneth B. Hoehn
# 7/22/22
# Run analyses with transfer data

threads=7

# mrl-combined germline against naive mouse data
echo "Aligning MRL sequences to combined MRL reference"

export IGDATA=share/igblast
ncbi-igblast-1.18.0/bin/igblastn \
    -germline_db_V share/igblast/database/imgt_mrl-combined_ig_v\
    -germline_db_D share/igblast/database/imgt_mrl-combined_ig_d \
    -germline_db_J share/igblast/database/imgt_mrl-combined_ig_j \
    -domain_system imgt -ig_seqtype Ig -organism mrl-combined \
    -auxiliary_data share/igblast/optional_file/mouse_gl.aux \
    -outfmt '7 std qseq sseq btop' \
    -query raw_experimental/raw/filtered_contig.fasta \
    -out raw_experimental/processed/filtered_contig_mrl-combined\.fmt7 -num_threads $threads

MakeDb.py igblast -i raw_experimental/processed/filtered_contig_mrl-combined\.fmt7 \
    -s raw_experimental/raw/filtered_contig.fasta \
    -r \
    share/germlines/imgt/mrl-combined/vdj/imgt_mrl-combined_IGHV.fasta \
    share/germlines/imgt/mrl-combined/vdj/imgt_mrl-combined_IGHD.fasta \
    share/germlines/imgt/mrl-combined/vdj/imgt_mrl-combined_IGHJ.fasta \
    --extended --fail --asis-calls --outdir raw_experimental/processed

# merge annotations to data
python3 merge10x.py raw_experimental/processed/filtered_contig_mrl-combined_db-pass.tsv \
    raw_experimental/raw/filtered_contig_annotations_withMouseIdentity_WNNCluster0.6res_Oct2021.csv raw_experimental/processed/mrl_db-pass.tsv

echo "Identifying clonal clusters"

# combine BCR and Seurat annotations, perform clonal clustering, and reconstruct germlines
Rscript cloneGermline.R > other/cloneGermline.txt

echo "Performing MRL analyses"

# perform BCR analyses
Rscript analysisMRL.R > other/analysisMRL.txt
