# README #

Details scripts used for this paper: https://www.biorxiv.org/content/10.1101/558825v2

Consituent Perl and R scripts written by Kenneth B. Hoehn.

Each subdirectory contains a README.sh which details the order in which each script was used.

Contact kenneth.hoehn@yale.edu with any questions or issues.

