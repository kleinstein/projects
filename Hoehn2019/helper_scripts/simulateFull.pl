#!/usr/bin/perl

use strict;
use warnings;
use hotSpotUtilsPDL;
use Math::Random qw(:all);


#srand(1);

sub define_all_subs{
	my $oseq = $_[0];
	my $omegas = $_[1];
	my $regions = $_[2];
	my $kappa = $_[3];
	my $s5f = $_[4];

	my @sites;
	my @chars = ("T","C","A","G");
	my $s = $oseq;
	my $as = translate($s);
	#tally up mutability at each site
	my $l=length($s);
	my @muts = (0)x($l);
	$muts[0]=getMutability("NN".substr($s,0,3),$s5f);
	$muts[1]=getMutability("N".substr($s,0,4),$s5f);
	for(my $i=2;$i<($l-2);$i++){
		$muts[$i] = getMutability(substr($s,$i-2,5),$s5f);
	}
	$muts[$l-2]=getMutability(substr($s,-4)."N",$s5f);
	$muts[$l-1]=getMutability(substr($s,-3)."NN",$s5f);
	my $tmu=0;
	foreach my $m (@muts){$tmu+=$m};

	#normalize relative mutabilities of all codons
	for(my $i=0;$i<scalar(@muts);$i++){$muts[$i]/=$tmu};
	#calculate relative probability of substitution at each site given
	#mutability, omega, and kappa
	my $totalp=0;
	for(my$i=0;$i<$l;$i++){
		my $o = substr($s,$i,1);
		my $isum=0;
		my @subs;
		for(my$j=0;$j<scalar(@chars);$j++){
			my $c = $chars[$j];
			$subs[$j]=0; #same character subs have zero probability
			if($c eq $o){next;}
			my $temp = substr($s,0,$i).$c.substr($s,$i+1);
			if(hasPTC($temp)){next;} #don't allow PTCs
			my $ukappa=1; my $uomega=1;
			if($o eq "A" && $c eq "G"){$ukappa=$kappa;}
			if($o eq "G" && $c eq "A"){$ukappa=$kappa;}
			if($o eq "C" && $c eq "T"){$ukappa=$kappa;}
			if($o eq "T" && $c eq "C"){$ukappa=$kappa;}
			my $atemp = translate($temp); #translate substitution
			if($as ne $atemp){$uomega=$omegas->[$regions->[$i]];} #is substitution synonymous?
			$subs[$j] = $muts[$i]*$ukappa*$uomega;
			$totalp+=$subs[$j];#
			
		}
		push(@sites,\@subs);
	}
	for(my$i=0;$i<$l;$i++){
		my $isum=0;
		for(my$j=0;$j<scalar(@chars);$j++){
			$sites[$i]->[$j] /= $totalp;
			$isum += $sites[$i]->[$j];
		}
	}
	my @ret=(\@sites,1);
	return \@ret;
}

#define probability of all possible substitutions, then pick one
sub introduceOneMutation{
	my $s  = $_[0];
	my $omegas = $_[1];
	my $regions = $_[2];
	my $kappa = $_[3];
	my $s5f   = $_[4];
	my $ncdr3 = $_[5];
	my $fullregions = $_[6];
	my $ret = define_all_subs($s,$omegas,$regions,$kappa,$s5f);
	my $subs = $ret->[0];
	my $tp = $ret->[1];
	my $draw = rand($tp);
	my $l=length($s);
	my @chars = ("T","C","A","G");
	my $total=0;
	my $next = "";
	for(my$i=0;$i<$l;$i++){
		my $isum=0;
		for(my$j=0;$j<scalar(@chars);$j++){
			$total += $subs->[$i]->[$j];
			if($total >= $draw){
				$next = substr($s,0,$i).$chars[$j].substr($s,$i+1);
				#check if mutation is outside of CDR3
				if($fullregions->[int($i/3)] != 108){$$ncdr3 += 1;}
				return $next;
			}
		}
	}

	die("Found no appropriate subsitution for $s\n");
}

sub simulateFull{
	my $node = $_[0];
	my $omegas = $_[1];
	my $regions = $_[2];
	my $kappa = $_[3];
	my $s5f   = $_[4];
	my $seqs = $_[5];
	my $ratemultiplier = $_[6];
	my $id = $_[7];
	my $fullregions = $_[8];

	if(exists($node->{'up'})){
		my $s = $node->{"up"}->{"ntsequence"};
		my $l = length($s);
		my $ncdr3l = 0; #tally up non-CDR3 sites in alignment
		foreach my $r (@$fullregions){
			if($r != 108){
				$ncdr3l += 3;
			}
		}
		my $nsub = random_poisson(1,$node->{"dist"}*$l/3*$ratemultiplier);
		my $ncdr3 = 0;
		my $ns = $s;
		for(my $sub=0;$sub<$nsub;$sub++){ # add in each substitution
			$ns = introduceOneMutation($ns,$omegas,$regions,$kappa,$s5f,\$ncdr3,$fullregions);
		}
		$node->{"ntsequence"}=$ns;
		print SUBS "$id\t$nsub\t".($nsub)/($l/3)."\t".$ncdr3."\t".($ncdr3)/($ncdr3l/3).
			"\t".$node->{"subtaxa"}."\t".$node->{"dist"}."\n";
	}
	if(exists($node->{"left"})){
		$seqs = simulateFull($node->{"left"}, $omegas,$regions,$kappa,$s5f,$seqs,$ratemultiplier,$id,$fullregions);
		$seqs = simulateFull($node->{"right"},$omegas,$regions,$kappa,$s5f,$seqs,$ratemultiplier,$id,$fullregions);
	}else{
		$seqs .= ">".$node->{"id"}."\n".$node->{"ntsequence"}."\n";
	}
	return $seqs;
}



#Read in parameters from config file
my $config = $ARGV[0];
open(C,$config) or die("Couldn't open config file ($config)");

my $nsim;
my $kappa;
my @omegas;
my @motifs;
my $partfile;
my $treefile;
my @hs;
my $freqs="N";
my $length;
my $outdir;
my $igphyml;
my $seqfile;
my $context=0;
my $rooted=0;
my $rootid;
my $ancstate=0;
my $statsfile;
my $stem;
my $statslhood;
my $ambigfile;
my $aamodel;
my @aaints;
my $aaslope="N";
my $aatree = "N";
my $alrt=0;
my $hint = "N";
my $hslope = "N";
my $rootonly = 0;
my $rootpis = 0;
my $bstats;
my $recon="joint";
my $ratemultiplier=1;
my $maskcdr3=1;
my $s5f_file="N";
my $naivefile="NaiveData.tsv";
my $srand = "N";

while(<C>){
	my $line = $_;
	chomp($line);
	if($line =~ /nsim\s+(\S+)/){
		$nsim = $1;
	}
	if($line =~ /omegas\s+(\S+)/){
		@omegas = split(",",$1);
	}
	if($line =~ /kappa\s+(\S+)/){
		$kappa = $1;
	}
	if($line =~ /motifs\s+(\S+)/){
		@motifs = split(",",$1);
	}
	if($line =~ /hs\s+(\S+)/){
		@hs = split(",",$1);
	}
	if($line =~ /freqs\s+(\S+)/){
		$freqs = $1;
	}
	if($line =~ /^tree\s+(\S+)$/){
		$treefile = $1;
	}
	if($line =~ /fullcontext\s+(\S+)/){
		$context=$1;
	}
	if($line =~ /outdir\s+(\S+)/){
		$outdir=$1;
	}
	if($line =~ /rooted\s+(\S+)/){
		$rooted=$1;
	}
	if($line =~ /length\s+(\S+)/){
		$length=$1;
	}
	if($line =~ /rootid\s+(\S+)/){
		$rootid=$1;
	}
	if($line =~ /part\s+(\S+)/){
		$partfile=$1;
	}
	if($line =~ /igphyml\s+(\S+)/){
		$igphyml=$1;
	}
	if($line =~ /seqfile\s+(\S+)/){
		$seqfile=$1;
	}
	if($line =~ /ancstate\s+(\S+)/){
		$ancstate=$1;
	}
	if($line =~ /stats\s+(\S+)/){
		print "match $&\n";
		$statsfile=$1;
		print "statsfile $1 $statsfile\n";
	}
	if($line =~ /stem\s+(\S+)/){
		$stem=$1;
	}
	if($line =~ /ambigfile\s+(\S+)/){
		$ambigfile=$1;
	}
	if($line =~ /aatree\s+(\S+)/){
		$aatree=$1;
	}
	if($line =~ /alrt\s+(\S+)/){
		$alrt=$1;
	}
	if($line =~ /rootonly\s+(\S+)/){
		$rootonly=$1;
	}
	if($line =~ /rootpis\s+(\S+)/){
		$rootpis=$1;
	}
	if($line =~ /recon\s+(\S+)/){
		$recon=$1;
	}
	if($line =~ /ratemultiplier\s+(\S+)/){
		$ratemultiplier=$1;
	}
	if($line =~ /maskcdr3\s+(\S+)/){
		$maskcdr3=$1;
	}
	if($line =~ /s5f\s+(\S+)/){
		$s5f_file=$1;
	}
	if($line =~ /naivefile\s+(\S+)/){
		$naivefile=$1;
	}
}

#check to see if stats file was specified in command line
for(my $i = 1; $i < scalar(@ARGV); $i++){
	my $line = $ARGV[$i];
	if($line =~ /-stats/){
		$statsfile=$ARGV[$i+1];
	}
}

#Read in igphyml stats file, if specified
#print "stats: $statsfile\n";
if(defined $statsfile && $statsfile ne "N"){
	open(STATS,$statsfile)or die("Couldn't open $statsfile\n");
	my @stats = <STATS>;
	#print "@stats\n";
	@motifs = (0)x0;
	@omegas = (0)x0;
	@hs = (0)x0;
	if($freqs eq "N"){$freqs = "stats";}
	foreach my $l (@stats){
	  chomp($l);
	  #print "$l\n";
	  if($l =~ /Motif:\s+(\S+)#\d+\s+\d\s+\d\s+(\S+)/){
	  	push(@motifs,$1);
	  	push(@hs,$2);
	  	print "Read motif h $1 = $2 from $statsfile\n";
	  }elsif($l =~ /Motif:\s+(\S+)\s+\d\s+\d\s+(\S+)/){
	  	push(@motifs,$1);
	  	push(@hs,$2);
	  	print "Read motif h $1 = $2 from $statsfile\n";
	  }
	  if($l =~ /\. Omega\s+(\d+).*:\s+(\S+)/){
	  	$omegas[$1]=$2;
	  	print "Read omega $1 = $2 from $statsfile\n";
	  }
	  if($l =~ /. Nonsynonymous\/synonymous ratio:\s+(\S+)/){
	  	$omegas[0]=$1;
	  	print "Read old school omega 0 = $1 from $statsfile\n";
	  }
	  if($l =~ /\. Transition\/transversion ratio:\s+(\S+)/){
	  	$kappa=$1;
	  	print "Read kappa $kappa from $statsfile\n";
	  }
	  if($l =~ /\. Log-likelihood:\s+(\S+)/){
	  	$statslhood=$1;
	  	print "Read stats lhood $statslhood from $statsfile\n";
	  }
	  if($l =~ /\. Combined log-likelihood:\s+(\S+)/){
	  	$statslhood=$1;
	  	print "Read stats lhood $statslhood from $statsfile\n";
	  }
	  if($l =~ /. Partition file:\s+(\S+)/){
	  	$partfile=$1;
	  	print "Read partfile $partfile from $statsfile\n";
	  }
	  if($l =~ /.\s+(\S+)\s+aa_intercept\s+(\d+)\s+(\S+):\s+(\S+)/){
	  	$aamodel = $1;
	  	$aaints[$2]=$4;
	  	print "Read $aamodel $aaints[$2] from $statsfile\n";
	  }
	  if($l =~ /.\s+(\S+)\s+aa_slope:\s+(\S+)/){
	  	$aaslope = $2;
	  	print "Read $aamodel $aaslope from $statsfile\n";
	  }
	  if($l =~ /Motif:\s+S5F\s+h_intercept:\s+(\S+)/){
	  	$hint = $1;
	  	print "Read motif hint S5F = $1 from $statsfile\n";
	  }
	  if($l =~ /Motif:\s+S5F\s+h_slope:\s+(\S+)/){
	  	$hslope = $1;
	  	print "Read motif hslope S5F = $1 from $statsfile\n";
	  }
	  $bstats .= " $l";
	}
	print "Reading in frequency parameters from IgPhyML stats file\n";
}

#check command line args to see if any should be over-ridden
for(my $i = 1; $i < scalar(@ARGV); $i++){
	my $line = $ARGV[$i];
	if($line =~ /-nsim/){
		$nsim = $ARGV[$i+1];
	}
	if($line =~ /-omegas/){
		@omegas = split(",",$ARGV[$i+1]);
	}
	if($line =~ /-kappa/){
		$kappa = $ARGV[$i+1];
	}
	if($line =~ /-part/){
		$partfile = $ARGV[$i+1];
	}
	if($line =~ /-motifs/){
		@motifs = split(",",$ARGV[$i+1]);
	}
	if($line =~ /-hs/){
		@hs = split(",",$ARGV[$i+1]);
	}
	if($line =~ /-freqs/){
		$freqs = $ARGV[$i+1];
	}
	if($line =~ /-tree/){
		$treefile = $ARGV[$i+1];
	}
	if($line =~ /-fullcontext/){
		$context=$ARGV[$i+1];
	}
	if($line =~ /-outdir/){
		$outdir=$ARGV[$i+1];
	}
	if($line =~ /-rooted/){
		$rooted=$ARGV[$i+1];
	}
	if($line =~ /-length/){
		$length=$ARGV[$i+1];
	}
	if($line =~ /-rootid/){
		$rootid=$ARGV[$i+1];
	}
	if($line =~ /-igphyml/){
		$igphyml=$ARGV[$i+1];
	}
	if($line =~ /-seqfile/){
		$seqfile=$ARGV[$i+1];
	}
	if($line =~ /-ancstate/){
		$ancstate=$ARGV[$i+1];
	}
	if($line =~ /-stats/){
		$statsfile=$ARGV[$i+1];
	}
	if($line =~ /-stem/){
		$stem=$ARGV[$i+1];
	}
	if($line =~ /-ambigfile/){
		$ambigfile=$ARGV[$i+1];
	}
	if($line =~ /-aatree/){
		$aatree=$ARGV[$i+1];
	}
	if($line =~ /-alrt/){
		$alrt=$ARGV[$i+1];
	}
	if($line =~ /-rootonly/){
		$rootonly=$ARGV[$i+1];
	}
	if($line =~ /-rootpis/){
		$rootpis=$ARGV[$i+1];
	}
	if($line =~ /-recon/){
		$recon=$ARGV[$i+1];
	}
	if($line =~ /-ratemultiplier/){
		$ratemultiplier=$ARGV[$i+1];
	}
	if($line =~ /-maskcdr3/){
		$maskcdr3=$ARGV[$i+1];
	}
	if($line =~ /-s5f/){
		$s5f_file=$ARGV[$i+1];
	}
	if($line=~/naivefile/){
		$naivefile=$ARGV[$i+1];
	}
	if($line=~/-srand/){
		$srand=$ARGV[$i+1];
	}
}

#check that all necessary parameters are specified
#if(!defined $kappa){die("kappa needs to be specified")}
#if(scalar(@omegas)==0){die("omegas needs to be specified")}
#if(scalar(@motifs)==0){die("motifs needs to be specified")}
#if(scalar(@hs)==0 && $hint eq "N"){die("hs needs to be specified")}
if(!defined $freqs){die("freqs needs to be specified")}
if(!defined $outdir){die("outdir needs to be specified")}
if(!defined $rootid){die("rootid needs to be specified")}
if(!defined $igphyml){die("igphyml needs to be specified")}
if(!defined $stem){die("stem needs to be specified")}
#if(!defined $seqfile){die("seqfile needs to be specified");}
if(!defined $statsfile){$statsfile="N";}
if(!defined $partfile){$partfile="N";}
if(!defined $ambigfile){$ambigfile="N";}

print "\nReconstruction Settings\n";
print "kappa\t$kappa\n";
print "omegas\t@omegas\n";
print "motifs\t@motifs\n";
print "hs\t@hs\n";
print "freqs\t$freqs\n";
print "outdir\t$outdir\n";
#print "length\t$length\n";
print "rootid\t$rootid\n";
#print "seqfile\t$seqfile\n";
print "stats\t$statsfile\n";
print "ambigfile\t$ambigfile\n";
print "outfile format: $outdir/$stem\_\n";
print "aatree: $aatree\n";
print "alrt: $alrt\n";
#print "seqfile: $seqfile\n";
print "rootonly: $rootonly\n";
print "rootpis: $rootpis\n";
print "part: $partfile\n";
print "recon: $recon\n";
print "ratemult: $ratemultiplier\n";
print "srand: $srand\n";
print "s5f: $s5f_file\n";
print "\n";

if(srand ne "N"){
	srand($srand);
}

if(scalar(@hs) ne scalar(@motifs)){die(scalar(@hs)." h values but ".scalar(@motifs)." motifs!\n")}


####################################################

my $seqs;
if($rootid ne "NAIVE" && $rootid ne "test" && $rootid ne "uniform"){$seqs=getfasta($seqfile);}
if(!exists($seqs->{$rootid}) && $rootid ne "NAIVE" && $rootid ne "test" && $rootid ne "uniform"){die($rootid)}

#Read in mutability file
my $s5f;
if($s5f_file eq "N"){
	$s5f= getS5F();
}else{
	my %s5ft;
	open(SF,$s5f_file)or die($s5f_file);
	my $h=<SF>;
	while(<SF>){
		my $line=$_;
		chomp($line);
		my @in=split("\t",$line);
		$s5ft{$in[0]}=$in[1];
	}
	$s5f=\%s5ft;
}
print "AAAAA:\t".$s5f->{"AAAAA"}."\n";

#read in tree
open(TREE,$treefile)or die("Couldn't open $treefile");
my $tree = <TREE>;
if($tree =~ /NEXUS/){
	while(<TREE>){
		my $line=$_;
		if($line =~ /\[\&R\]\s+/){
			$tree=$';
			last;
		}
	}
}
chomp($tree);
my %root = %{readInRootedNewick($tree,0,0)};
my $t = printtreestring(\%root,"",0).";";
getSubTaxa(\%root);
assignparents(\%root,"NA");
setUnlabled(\%root);

#set up frequencies
my @transfreq;
my @codons;
my %codoni;
my $index = 0;
my @chars = ("t","c","a","g");
my %freqs;
my $fsum=0;
foreach my $a (@chars){
	foreach my $b (@chars){
		foreach my $c (@chars){
			if($a.$b.$c ne "tga" && $a.$b.$c ne "taa" && $a.$b.$c ne "tag"){
				push(@codons,$a.$b.$c);
				$codoni{$a.$b.$c}=$index;
				my $match = uc $a.$b.$c;
				if(defined $statsfile && $statsfile ne "N" && $freqs ne "uniform"){
					if($bstats =~ /f\($match\)=(\d+\.*\d*)/){
						$freqs{lc $match} = $1;
						if($freqs{lc $match} == 0){
							print "Zero frequency caught\n";
							$freqs{lc $match}=1e-10;
						}
					}else{die($match)}
				}elsif($freqs eq "uniform"){
					$freqs{lc $match} = 1/61;
				}else{
					die("freqs not set properly\n");
				}
				$fsum += $freqs{$a.$b.$c};
				$index++;
			}
		}
	}
}
foreach my $k (keys %freqs){
	$freqs{$k} = $freqs{$k}/$fsum;
	$transfreq[$codoni{$k}]=$freqs{$k};
}
print "Codon frequencies: @transfreq\n";

#if using random naive sequence as root
my @ndata;
if($rootid eq "NAIVE"){
	open(ND,$naivefile) or die("$naivefile not found!\n");
	my $h=<ND>;
	while(<ND>){
		my $line=$_;
		chomp($line);
		my @in=split("\t",$line);
		push(@ndata,\@in);
	}
}

open(SUBS,">$outdir/$stem\_subs.txt") or die("$outdir/$stem\_subs.txt");
print SUBS "id\ttotal_subs\ttotallength\tncdr3_subs\tncdr3_length\tsubtaxa\tbr_length\n";

print "Simulating $nsim datasets\n";
for(my $i = 0; $i < $nsim; $i++){
	print "On dataset $i $rootid\n";
	my @char = ("A","C","G","T");
	my $root;
	my @regions; #f using naive sequence
	my @fullregions;
	if($rootid ne "NAIVE" && $rootid ne "test" && $rootid ne "uniform"){
		$root = $seqs->{$rootid}; #draw root from seqfile
		if($partfile ne "N"){
			open(P,$partfile) or die($partfile);
			print "$root\n";
			my @pts=<P>;
			my @r = split(",",$pts[5]);
			@fullregions = @r;
			for(my$i=0;$i<scalar(@r);$i++){
				if($r[$i] == 30 || $r[$i] == 60 || $r[$i] == 108){
					push(@regions,1);push(@regions,1);push(@regions,1);
				}else{
					push(@regions,0);push(@regions,0);push(@regions,0);
				}
			}
			open(P,">$outdir/$stem\_$i.part.txt") or die("$outdir/$stem\_$i.part.txt");
			foreach my $p (@pts){print P $p}
			close(P);
			print "@regions\n";
		}
	}elsif($rootid eq "uniform"){ #use predefined root
		print "uniform root\n";
		for(my $j=0;$j<$length;$j++){
			my $n = rand(1);
			my $sum = 0;
			for(my $ind = 0; $ind < 61; $ind++){
				$sum += $freqs{$codons[$ind]};
				if($sum >= $n){
					$root .= $codons[$ind];
					last;
				}
			}
		}
		$root = lc "TCGGAGACCCTGTCCCTCACCTGCACTGTCTCTGGTGGCTCCATCAGTAGTTACTACTGGAGCTGGATCCGGCAGCCCCCAGGGAAGGGACTGGAGTGGATTGGGTATATCTATTACAGTGGGAGCACCAACTACAACCCCTCCCTCAAGAGTCGAGTCACCATATCAGTAGACACGTCCAAGAACCAGTTCTCCCTGAAGCTGAGCTCTGTGACCGCTGCGGACACGGCCGTGTATTACTGTGCGAGCTACTTTGACTACTGGGGCCAGGGAACCCTGGTCACCGTCTCCTCATCGGAGACCCTGTCCCTCACCTGCACTGTCTCTGGTGGCTCC";
		#$root = lc "TTTGGG";
		$root = uc $root;
		@regions = (0)x(length($root));
		@fullregions = (0)x(length($root));
	}elsif($rootid eq "test"){
		$root = "TCGTCG";
	}else{ #use random root from naive distribution
		my $rd = $ndata[rand @ndata];
		$root = $rd->[5];
		my $pt = $rd->[6];
		$partfile = "$outdir/$stem\_$i.part.txt"; #set up partition file
		$pt =~ s/;/\n/g;
		open(P,">$partfile") or die($partfile);
		print P "$pt\n";
		close(P);
		print "Using ".$rd->[0]."\t".$rd->[1]."\n";
		my @pts = split("\n",$pt);
		print "@pts\n";
		my @r = split(",",$pts[5]);
		print "@r\n";
		for(my$i=0;$i<scalar(@r);$i++){
			if($r[$i] == 30 || $r[$i] == 60 || $r[$i] == 108){
				push(@regions,1);push(@regions,1);push(@regions,1);
			}else{
				push(@regions,0);push(@regions,0);push(@regions,0);
			}
		}
		print "@regions\n";
		@fullregions = @r;
		#die();
	}
	#Fill in ambiguous spots in root
	my @ambig = (0)x(length($root));
	if(hasPTC($root)){die("$rootid has PTC!")}
	my $oriroot = $root;
	while($root=~/[N|-|\.]/){
		if($root =~ /[N|-|\.]/){
			my $oroot = $root;
			my $pre=$`; my $post = $';
			$root = $pre.($char[rand @char]).$post;
			if(hasPTC($root)){
				$root=$oroot
			}
			$ambig[length($pre)]=1;
		}
	}
	$root{"ntsequence"} = $root; #set root sequence
	my $troot = $root;

	#simulate sequences
	print "$root\n";

	my $sequences = simulateFull(\%root,\@omegas,\@regions,$kappa,$s5f,"",$ratemultiplier,$i,\@fullregions);
	if($maskcdr3){ #mask CDR3?
		print "MASKING CDR3\n";
		my $nroot="";
		open(P,$partfile) or die($partfile);
		my @p=<P>;
		my @imgt=split(",",$p[-1]);
		print "@imgt\n";
		foreach(my$j=0;$j<scalar(@imgt);$j++){
			if($imgt[$j] != 108){
				$nroot .= substr($root,$j*3,3);
			}else{
				$nroot .= "NNN";
			}
		}
		$root=$nroot;
	}else{
		for(my$i=0;$i<length($root);$i++){ #add ambiguous sites back into root
			if($ambig[$i]==1){
				if($i==0){$root="N".substr($root,1)}
				elsif($i==(length($root)-1)){$root=substr($root,0,-1)."N"}
				else{$root=substr($root,0,$i)."N".substr($root,$i+1)}
			}
		}
	}
	$root{"ntsequence"}=$root;
	$root{"right"}->{"ntsequence"} = $root;
	$sequences = getSeqs(\%root,"");
	open(OUT,">$outdir/$stem\_$i.fa") or die("Couldn't open $outdir/$stem\_$i.fa\n");
	print OUT uc "$sequences\n";
	close(OUT);

	my $sims = getfasta("$outdir/$stem\_$i.fa");
	my @ks = keys %$sims;
	for(my $ki=0;$ki<(scalar(@ks)-1);$ki++){
		for(my $kj=$ki;$kj<(scalar(@ks));$kj++){
			my $k  = $ks[$ki];
			my $k2 = $ks[$kj];
		}
	}
}



