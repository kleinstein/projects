#!/usr/bin/env python
# coding: utf-8

import os
import glob
import pandas as pd
import umap
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import silhouette_score, davies_bouldin_score, calinski_harabasz_score
import matplotlib.image as mpimg

labels = pd.read_table('../data/AIRR_antibody_info.tsv')
labels = labels.loc[:,['cell_id', 'Epitope Group']].drop_duplicates()

embedding_dir = '../data/amulety/*/*.tsv'
embedding_filepath = glob.glob(embedding_dir)
embedding_files = [pd.read_table(x) for x in embedding_filepath]
embeddings = [os.path.basename(os.path.dirname(x)) for x in embedding_filepath]


list = []
for i, embedding in enumerate(embeddings):
    print(embedding)
    umap_embeddings = umap.UMAP().fit_transform(embedding_files[i].iloc[:,1:].values)
    plot_data = pd.DataFrame({
        'embedding': embedding,
        'cell_id': embedding_files[i].iloc[:,0], 
        'umap_1': umap_embeddings[:,0],
        'umap_2': umap_embeddings[:,1],
        }).merge(labels)
    list.append(plot_data)
plot_data = pd.concat(list)

# Define base colors for main groups
base_colors = {
    'A': '#1f77b4',  # Blue
    'B': '#ff7f0e',  # Orange
    'C': '#2ca02c',  # Green
    'D': '#d62728',  # Red
    'E': '#9467bd',  # Purple
    'F': '#8c564b'   # Brown
}

# Generate shades for subgroups
custom_palette = {
    'A': base_colors['A'],
    'B': base_colors['B'],
    'C': base_colors['C'],
    'D1': sns.light_palette(base_colors['D'], n_colors=3)[1],  # Lighter Red
    'D2': sns.light_palette(base_colors['D'], n_colors=3)[2],  # Darker Red
    'E1': sns.light_palette(base_colors['E'], n_colors=5)[1],  # Lighter Purple
    'E2.1': sns.light_palette(base_colors['E'], n_colors=5)[2],  
    'E2.2': sns.light_palette(base_colors['E'], n_colors=5)[3],  
     'E3': sns.light_palette(base_colors['E'], n_colors=5)[4],  
    'F1': sns.light_palette(base_colors['F'], n_colors=4)[1],  
    'F2': sns.light_palette(base_colors['F'], n_colors=4)[2],  
    'F3': sns.light_palette(base_colors['F'], n_colors=4)[3]  
}

# Make sure the colors align with the unique epitope groups
unique_epitope_groups = plot_data['Epitope Group'].sort_values().unique()
embedding_name_map = {
    'antiberty': 'antiBERTy',
    'antiberta2': 'antiBERTa2',
    'balm_paired': 'BALM paired',
    'esm2': 'ESM2'
}

# Ensure embeddings appear in the correct order
embedding_order = ['antiberty', 'antiberta2', 'balm_paired', 'esm2']
plot_data['embedding'] = pd.Categorical(plot_data['embedding'], categories=embedding_order, ordered=True)

g = sns.relplot(
    data=plot_data,
    x='umap_1',
    y='umap_2',
    hue='Epitope Group',
    col='embedding', 
    kind='scatter',
    alpha=0.5,
    s=1,
    hue_order=unique_epitope_groups,
    col_order=embedding_order,
    palette=custom_palette,
    height=2,
    aspect=0.7,
    edgecolor='none',
    facet_kws={'sharey': False, 'sharex': False},
)
g.fig.subplots_adjust(wspace=0.1, hspace=0.1)
for ax, col_name in zip(g.axes.flat, embedding_order):
    ax.set_title(embedding_name_map[col_name], fontsize=8)

g.set_axis_labels("UMAP 1", "UMAP 2", fontsize=8)

# Modify each subplot
for ax in g.axes.flat:
    ax.set_xticks([])  # Remove x ticks
    ax.set_yticks([])  # Remove y ticks

# Move the legend to the bottom and arrange it horizontally
sns.move_legend(
    g,  # The FacetGrid object
    "lower center",  # Position
    bbox_to_anchor=(0.45, -0.01),  # Fine-tune placement (centered below plot)
    ncol=12,  # Arrange legend items in 3 columns
    title=None,  # Set legend title
    frameon=False,  # Optional: Remove legend box 
    markerscale=4,
    #mode='expand',
    columnspacing=0,
    handletextpad=0
)

for lh in g._legend.legend_handles: 
    lh.set_alpha(1)
    
g._legend.set_title('Epitope Group', prop={'size': 8})
for text in g._legend.texts:  
    text.set_fontsize(6)  # Set legend label size

plt.savefig('../figures/umap_antibody_embeddings.png', bbox_inches='tight', dpi=300)


# Define mapping for embedding names
embedding_order = ['antiBERTy', 'antiBERTa2', 'BALM paired', 'ESM2']

silhouette_score_list = []
davies_bouldin_score_list = []
calinski_harabasz_score_list = []
for i, embedding in enumerate(embedding_order):
    print(embedding)
    X = embedding_files[i].iloc[:,1:].values
    X_labels = labels.set_index('cell_id').loc[embedding_files[0].iloc[:,0].values]
    silhouette_score_list.append(silhouette_score(X, X_labels['Epitope Group'].values))
    davies_bouldin_score_list.append(davies_bouldin_score(X, X_labels['Epitope Group'].values))
    calinski_harabasz_score_list.append(calinski_harabasz_score(X, X_labels['Epitope Group'].values))


import numpy as np
# ---- PLOT B: Clustering Scores ---- #
# Replace embedding names in clustering score plots
embedding_names = embedding_order

# Convert scores to NumPy arrays
silhouette_scores = np.array(silhouette_score_list)
davies_bouldin_scores = np.array(davies_bouldin_score_list)
calinski_harabasz_scores = np.array(calinski_harabasz_score_list)

# Set up figure with three subplots for clustering metrics
fig, axes = plt.subplots(1, 3, figsize=(5.2, 2), constrained_layout=True)

# Define color palette
palette = sns.color_palette("coolwarm", len(embedding_names))

# Silhouette Score (Higher is better)
sns.barplot(x=embedding_names, y=silhouette_scores, palette=palette, ax=axes[0])
axes[0].set_title("Silhouette Score", fontsize=8)
axes[0].set_xticklabels(embedding_names, rotation=45, ha='right', fontsize=6)
axes[0].set_yticklabels(np.round(axes[0].get_yticks(), 2), fontsize=6)
axes[0].set_ylabel("Score", fontsize=8)

# Davies-Bouldin Index (Lower is better)
sns.barplot(x=embedding_names, y=davies_bouldin_scores, palette=palette, ax=axes[1])
axes[1].set_title("Davies-Bouldin Index", fontsize=8)
axes[1].set_xticklabels(embedding_names, rotation=45, ha='right', fontsize=6)
axes[1].set_yticklabels(np.round(axes[1].get_yticks(), 2), fontsize=6)
axes[1].set_ylabel("Score", fontsize=8)

# Calinski-Harabasz Index (Higher is better)
sns.barplot(x=embedding_names, y=calinski_harabasz_scores, palette=palette, ax=axes[2])
axes[2].set_title("Calinski-Harabasz Index", fontsize=8)
axes[2].set_xticklabels(embedding_names, rotation=45, ha='right', fontsize=6)
axes[2].set_yticklabels(np.round(axes[2].get_yticks(), 2), fontsize=6)
axes[2].set_ylabel("Score", fontsize=8)

# Adjust layout
plt.tight_layout()
plt.savefig('../figures/clustering_scores_antibody_embeddings.png', bbox_inches='tight', dpi=300)



# Load the PNG images
image1_path = "../figures/umap_antibody_embeddings.png"
image2_path = "../figures/clustering_scores_antibody_embeddings.png"

# Read the images using matplotlib's image module
image1 = mpimg.imread(image1_path)
image2 = mpimg.imread(image2_path)

# Create a vertical subplot layout
fig, axs = plt.subplots(2, 1, figsize=(5.2, 4.1), gridspec_kw={'height_ratios': [1.1, 1]}, constrained_layout=True)

# Display the first image in the first subplot
axs[0].imshow(image1)
axs[0].axis('off')  # Hide axes for the first subplot
axs[0].text(0.02, 0.98, "A", transform=axs[0].transAxes, fontsize=12,
            fontweight='bold', va='top', ha='left')  # Add label "A"

# Display the second image in the second subplot
axs[1].imshow(image2)
axs[1].axis('off')  # Hide axes for the second subplot
axs[1].text(0.015, 0.98, "B", transform=axs[1].transAxes, fontsize=12,
            fontweight='bold', va='top', ha='left')  # Add label "B"
plt.subplots_adjust(wspace=0, hspace=0)
# Adjust layout and save or display the plot
plt.tight_layout()
plt.savefig("../figures/combined_plot.png", dpi = 300)  # Save as a new PDF file
plt.show()

