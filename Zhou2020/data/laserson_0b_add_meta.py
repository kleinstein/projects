#!/usr/bin/env python3
"""
Add fields containing meta information to fasta header
"""

import re, sys, os

# inputFasta name format:
# 9-FIN_collapse-unique_atleast-2.fasta

# input header: 
# >ATAGTATACAAACCTCA|PRCONS=Human-IGL|SEQORIENT=F|CREGION=Human-IGLC-2-InternalC|CONSCOUNT=19|DUPCOUNT=4

# output header:
# >ATAGTATACAAACCTCA|PRCONS=Human-IGL|SEQORIENT=F|CREGION=Human-IGLC-2-InternalC|CONSCOUNT=19|DUPCOUNT=4|SAMPLE=|SUBJECT=|TIMEPOINT=

def addMetaToHeader(inputFasta, outputFasta):

    if os.path.exists(inputFasta):

        # extract SAMPLE as string and convert to integer
        curSample = re.findall(pattern="[0-9]+-FIN", string=inputFasta)[0]
        curSample = int(curSample[0:(len(curSample)-4)])

        # index corresponds to sample%10=0,1,2,...,9
        TPS = ("+4w", "-8d", "-2d", "-1h", "+1h", "+1d", "+3d", "+1w", "+2w", "+3w")
        # timepoint
        curTp = TPS[curSample % 10]

        # subject
        if curSample>=1 and curSample<=10:
            # darn, if re-run, this should be GC
            #curSubj = "GMC"
            curSubj = "GC"
        elif curSample>=11 and curSample<=20:
            curSubj = "IB"
        else:
            curSubj = "FV"

        print("sample = " + str(curSample) + " timepoint = " + curTp + " subject = " + curSubj)

    
        with open(inputFasta, "r") as in_handle:

            lines = [line.rstrip() for line in in_handle] # list
            # find index of lines containing ">"
            idxHeaders = [idx for idx in range(len(lines)) if ">" in lines[idx]]

            with open(outputFasta, 'w') as out_handle:

                for i in range(len(idxHeaders)):
                    idx = idxHeaders[i]

                    # current header
                    curHeader = lines[idx]

                    # new header
                    newHeader = curHeader + "|SUBJECT=" + curSubj + "|SAMPLE=" + str(curSample) + "|TIMEPOINT=" + curTp

                    out_handle.writelines(newHeader+"\n")

                    # if not the last header
                    if i!=(len(idxHeaders)-1):
                        nextIdx = idxHeaders[i+1]
                        # copy over lines containing current sequence
                        # idx+1 : nextIdx-1
                        for seqIdx in range(idx+1, nextIdx):
                            out_handle.writelines(lines[seqIdx]+"\n")
                    else:
                    # if the last header
                        # copy over remaining lines containing sequence
                        for seqIdx in range(idx+1, len(lines)):
                            out_handle.writelines(lines[seqIdx]+"\n")


# run
for i in range(30):
    # i+1 = 1..30
    inputFasta = str(i+1) + "-FIN_collapse-unique_atleast-2.fasta"
    outputFasta = str(i+1) + "-FIN_collapse-unique_atleast-2_meta.fasta"

    addMetaToHeader(inputFasta, outputFasta)
