#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=48:00:00

module load Python/miniconda

source activate bpb

python --version

python simulateGC.py $1 $2 $3 $4 $5

