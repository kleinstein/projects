
module load Python/3.7.0-foss-2018b

dirs=(201657034_B 201657036_B 201657037_B 201657083_B 201657785_B 201657787_B 201657790_B 2010113866_B 2010113873_B 2010113875_B 2010113904_B)

for wdir in ${dirs[@]}
do
	wdir="VDJ/cr_vdj/$wdir"
	echo $wdir
	changeo-10x.sh -s $wdir/filtered_contig.fasta -a $wdir/filtered_contig_annotations.csv \
    	-g human -t ig -p 3 -o $wdir -b ~/share/igblast \
    	-r ~/share/germlines/imgt/human/vdj -p 10
done
