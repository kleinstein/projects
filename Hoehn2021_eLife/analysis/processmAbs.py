
import sys
import re
from Bio import SeqIO

print(sys.argv[1])

study = sys.argv[1]

file = "../../data/mabs/" + study + "_mabs.fasta"
outfile = "../../data/mabs/" + study + "_processed.fasta"
out = open(outfile, "w")

seqs = SeqIO.parse(file, "fasta")


base = "|study=STUDY|age=no|bsource=mAb|btype=mAb|chain=Heavy|disease=DISEASE|isotype=mAb|timepoint=N|species=human|subject=SUBJECT|vaccine=VACCINE|type=mAb|file_name=FILE|oas_v=N|oas_j=N|redundancy=2"
base = base.replace("STUDY",study)
base = base.replace("FILE",study + "_mabs.fasta")

if study == "Davis_2019":
    base = base.replace("DISEASE","Ebola")
    base = base.replace("VACCINE","None")

if study == "Doria-Rose_2015":
    base = base.replace("DISEASE","HIV")
    base = base.replace("VACCINE","None")
    base = base.replace("SUBJECT","CAP256")

if study == "Huang_2016":
    base = base.replace("DISEASE","HIV")
    base = base.replace("VACCINE","None")
    base = base.replace("SUBJECT","Donor-Z258")

if study == "Johnson_2018":
    base = base.replace("DISEASE","HIV")
    base = base.replace("VACCINE","None")
    base = base.replace("SUBJECT","CAP256")

if study == "Liao_2013":
    base = base.replace("DISEASE","HIV")
    base = base.replace("VACCINE","None")
    base = base.replace("SUBJECT","CH505")

if study == "Wu_2015":
    base = base.replace("DISEASE","HIV")
    base = base.replace("VACCINE","None")
    base = base.replace("SUBJECT","Donor-45")

if study == "Landais_2017":
    base = base.replace("DISEASE","HIV")
    base = base.replace("VACCINE","None")
    base = base.replace("SUBJECT","PC064")     

print(base)
stimes = {}
for record in seqs:
    tbase = base
    if study == "Davis_2019":
        sub = re.compile("isolate (\\d+)\.(\\d+)\.(\\S+)-V([H|K|L])")
        subject = "EVD" + sub.search(record.description).group(1)
        time = sub.search(record.description).group(2) + "month"
        if time == "0month":
            time = "discharge"
        tbase = tbase.replace("SUBJECT",subject)
        tbase = tbase.replace("timepoint=N","timepoint="+time)
        print(subject + "\t" + time)
        if sub.search(record.description).group(4) != "H":
            continue
        stimes[subject + "_" + str(time)] = 1
        mab_id = sub.search(record.description).group(1) + "." + \
            sub.search(record.description).group(2) + "." + \
            sub.search(record.description).group(3)
        print(mab_id)
        tbase = tbase.replace("isotype=mAb","isotype="+mab_id)
    if study == "Doria-Rose_2015":
        sub = re.compile("cap256-(\\d+)-")
        if sub.search(record.description) is not None:
            timepoint = "timepoint=Week-" + str(int(sub.search(record.description).group(1)))
            tbase = tbase.replace("timepoint=N",timepoint)
            tbase = tbase.replace("btype=mAb","btype=curated")
            print(timepoint)
    if study == "Wu_2015": #https://ars.els-cdn.com/content/image/1-s2.0-S0092867415002573-mmc1.pdf
        sub = re.compile("45-VRC01.\\S+.(\\S)-")
        if sub.search(record.description) is not None:
            heading = str(sub.search(record.description).group(1))
            if heading == "O":
                time = "timepoint=Year-0"
            elif heading == "A":
                time = "timepoint=Year-6"
            elif heading == "B":
                time = "timepoint=Year-7"
            elif heading == "C":
                time = "timepoint=Year-11"
            elif heading == "D":
                time = "timepoint=Year-12"
            elif heading == "E":
                time = "timepoint=Year-12"
            elif heading == "F":
                time = "timepoint=Year-13"
            elif heading == "G":
                time = "timepoint=Year-13"
            elif heading == "H":
                time = "timepoint=Year-14"
            elif heading == "I":
                time = "timepoint=Year-14"
            else:
                exit("timepoint not recognized")
            tbase = tbase.replace("timepoint=N",time)
            tbase = tbase.replace("btype=mAb","btype=curated")

    tbase = record.id + tbase        
    print(tbase)

    out.write(">" + tbase + "\n")
    out.write(str(record.seq) + "\n")
print(stimes)