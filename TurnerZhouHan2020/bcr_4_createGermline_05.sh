#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 1:00:00
#SBATCH --constraint E5-2660_v3
#SBATCH -c 1
#SBATCH --mem-per-cpu=6G


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
# NPROC=${SLURM_CPUS_PER_TASK:-1}

# IN:
# - *_clone-pass.tab/tsv, either produced by scoper, or by DTN+DefineClones
# OUT:
# - [METHOD]_[SUBJ]_germ-pass.tab
# - [METHOD]_[SUBJ]_CreateGermlines.log


#*
VERSION="01092020"

PATH_ROOT="/home/qz93/project/ellebedy_10x_P05/bcr/${VERSION}/"
PATH_CHANGEO="${PATH_ROOT}changeo/"

#* allowed values: "scoper", "DTN"
#METHOD="scoper" 
METHOD="DTN"

if [[ ${METHOD} == "scoper" ]]; then
	PATH_CLUSTERD="${PATH_CHANGEO}/scoper/"
	FILENAME_SUFFIX_CLUSTERED="_clone-pass.tsv"
else
	PATH_CLUSTERD="${PATH_CHANGEO}/"
	FILENAME_SUFFIX_CLUSTERED="_clone-pass.tab"
fi

GERM_TAG="ref_201931-4_1Aug2019" #*
FN_IMGT_DJ="/home/qz93/germlines/${GERM_TAG}/human_IG/IGH[DJ].fasta"
FN_IMGT_V_NOVEL="/home/qz93/germlines/${GERM_TAG}/IGHV_novel_ellebedy_10x_P05_20200109.fasta" #* #N
#FN_IMGT_V_NOVEL="/home/qz93/germlines/${GERM_TAG}/human_IG/IGHV.fasta" #*

PATH_LOG=${PATH_CHANGEO}createGermlines_${METHOD}_$(date '+%m%d%Y_%H%M%S').log

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "germline" ${GERM_TAG} &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
CreateGermlines.py --version &>> "${PATH_LOG}"
python3 -V &>> "${PATH_LOG}"

SUBJS=(P05) #N

for SUBJ in ${SUBJS[@]}; do

	echo "############## CreateGermlines for ${SUBJ}" &>> "${PATH_LOG}"

	#! important: if genotyping by tigger was performed, 
	#  add novel alleles to germline .fasta before running CreateGermlines

	# if you have run the clonal assignment task prior to invoking CreateGermlines, 
	# then adding the --cloned argument is recommended, as this will generate a 
	# single germline of consensus length for each clone

	# no nproc

	# change-o 0.4.1+ takes --df and --jf 
	# default values are:
	# --df D_CALL 
	# --jf J_CALL 

	# expected input name:
	# [METHOD]_[SUBJ]_[FILENAME_SUFFIX_CLUSTERED]
	# e.g. scoper_P9_clone-pass.tsv
	#      DTN_P9_clone-pass.tab

	# appends _germ-pass.tab, _germ-fail.tab (even if input -d is .tsv)
	CreateGermlines.py \
		-d "${PATH_CLUSTERD}${METHOD}_${SUBJ}${FILENAME_SUFFIX_CLUSTERED}" \
		--failed \
		-r ${FN_IMGT_V_NOVEL} ${FN_IMGT_DJ} \
		-g full dmask vonly regions \
		--cloned \
		--vf V_CALL_GENOTYPED \
		--df D_CALL \
		--jf J_CALL \
		--sf SEQUENCE_IMGT \
		--log "${METHOD}_${SUBJ}_CreateGermlines.log" \
		--outdir "${PATH_CHANGEO}" \
		--outname "${METHOD}_${SUBJ}" \
		&>> "${PATH_LOG}"

	echo "############## Finished for ${SUBJ}" &>> "${PATH_LOG}"

done
