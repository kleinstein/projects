

use strict;
use warnings;
use hotSpotUtils;


my @folders=("GMC","FV","IB");
my @tps=("-8d","-2d","+1h","-1h","+1d","+3d","+7d","+14d","+21d","+28d");
open(OUT,">CloneSeqCounts.tsv") or die();
print OUT "Subject Clone @tps\n";
foreach my $f (@folders){
	my %clones;
	for(my$i=0;$i<scalar(@tps);$i++){
		print "$f\t$tps[$i]\n";
		open(IN,"$f/$tps[$i]/seqs_GY3000_lineages.tsv") or die("$f/$tps[$i]/seqs_GY3000_lineages.tsv");
		my $h=<IN>;
		while(<IN>){
			my $line=$_;
			my @in=split("\t",$line);
			my $seqs=getfasta($in[0]);
			my $nseq=0;
			my $id;
			foreach my $k (keys %$seqs){
				if($k=~/(\d+)_GERM/){
					$id=$1;
				}elsif($k !~ /\_1$/){
					$nseq++;
				}
			}
			if(!exists($clones{$id})){
				my @ar=(0) x scalar(@tps);
				$clones{$id}=\@ar;
			}
			$clones{$id}->[$i]=$nseq;#-1;
		}
	}
	foreach my $k (keys %clones){
		my @ts = @{$clones{$k}};
		print OUT "$f $k @ts\n";
	}
}