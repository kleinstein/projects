#!/usr/bin/env bash
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=6gb 
#SBATCH --time=24:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com

module load R

rate=$1
r01=$2
freq0=$3
rep=$4
nproc=$5

Rscript estimateTwostate.R $rate $r01 $freq0 $rep $nproc
Rscript estimateTwostate_downsample.R $rate $r01 $freq0 $rep $nproc



