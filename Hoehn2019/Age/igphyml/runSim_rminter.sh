#!/usr/bin/bash
#SBATCH --partition=$part
#SBATCH --job-name=$sub:$visit
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=48:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=kenneth.hoehn\@gmail.com
#SBATCH --exclude=c24n04

module load CBLAS/20110120-foss-2016b
module load ATLAS/3.10.2-GCC-5.4.0-2.26-LAPACK-3.6.1

maskCDR3.pl $1.tsv
igphyml --repfile $2.tsv -m GY --threads $3 --run_id GY --minseq 3 --outrep $2\_GY.tsv
igphyml --repfile $2\_GY.tsv -m HLP19 --motifs FCH --omegaOpt e,e --threads $3 --run_id HLP19ci -o lr --minseq 3 --cbmat

rm $4/simsrg/*\_$5\_0.fa*
rm $4/simsrg/*\_$5\_0.part*
rm $4/simsrg/*\_$5\_0_ncdr.fa*
rm $4/simsrg/*\_$5\_0_ncdr.part*

echo $4/simsrg/*\_$5\_0_ncdr.fa*
echo $4/simsrg/*\_$5\_0_ncdr.part*