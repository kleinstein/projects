#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --job-name=q2
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=18
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=24:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com
#SBATCH --exclude=c24n04

module load R

threads=18
igthreads=2
sims=50

mult=(1 2)
subj=HD1310
types=(split long)
for m in "${mult[@]}"
do
	perl makeFCH.pl $m muts/FCH_$m.tsv
	for type in "${types[@]}"
	do
		echo "$m $type"
		mkdir sims/$type
		mkdir sims/$type/$m
		mkdir sims/$m
		perl simulateRepertoire.pl $sims $threads trees/$type.tree sims/$type/$m  $type\_$m -naivefile $subj.tsv -omegas 0.5,0.7 -maskcdr3 1 -s5f muts/FCH_$m.tsv > simlog.txt
	done
done
