#!/bin/bash
set -euo pipefail

REP=( "rep000" "rep002" "rep003" "rep004" "rep005" )
LINMUT_1=( "0" "0.001" "0.001" "0.001" "0.001" )
LINMUT_2=( "0" "0.199" "0.499" "0.999" "1.999")
REF_REP="repA"
REF_REP_TYPE="alpha2_5000singletons"

for i in "${!REP[@]}"
do
    echo "Processing ${REP[i]} originating from ${REF_REP} with mutation rate: linear ${LINMUT_1[i]} ${LINMUT_2[i]}"

    parallel --tag "grinder -rf sim_repertoires/${REF_REP}_BCR_shm_${REF_REP_TYPE}/sim_repertoire_${REF_REP}.fasta \
    -cf 10 -fr {} -rd 300 uniform 0 -abundance_model uniform -fastq_output 1 \
    -qual_levels 30 10 -od {/.} -id 572 normal 5 -md linear ${LINMUT_1[i]} ${LINMUT_2[i]} -mr 100 0" \
    ::: primers/BCR_primers.fasta

    parallel --tag "python splitfastq.py -i {} -p GGGAAGGAAGTCCTGTGCGAGGCAGCC" ::: \
    BCR_primers/grinder-reads.fastq

    mv BCR_primers/grinder-reads_otherprimer_R1.fq "${REP[i]}_all_primers_reads_R1.fq"
    mv BCR_primers/grinder-reads_R2primer_R2.fq "${REP[i]}_all_primers_reads_R2.fq"

    python renamefastq.py -i "${REP[i]}_all_primers_reads_R2.fq"
    python renamefastq.py -i "${REP[i]}_all_primers_reads_R1.fq"

    mkdir ${REP[i]}_BCR_${REF_REP_TYPE}_linmut_linear_${LINMUT_1[i]}_${LINMUT_2[i]}
    mv "${REP[i]}_all_primers_reads_R1_renamed.fastq" ${REP[i]}_BCR_${REF_REP_TYPE}_linmut_linear_${LINMUT_1[i]}_${LINMUT_2[i]}/
    mv "${REP[i]}_all_primers_reads_R2_renamed.fastq" ${REP[i]}_BCR_${REF_REP_TYPE}_linmut_linear_${LINMUT_1[i]}_${LINMUT_2[i]}/
    rm -r BCR_primers
    rm -r "${REP[i]}_all_primers_reads_R1.fq"
    rm -r "${REP[i]}_all_primers_reads_R2.fq"
done

REP=( "rep001" "rep006" "rep007" "rep008" "rep009" )
LINMUT_1=( "0" "0.001" "0.001" "0.001" "0.001" )
LINMUT_2=( "0" "0.199" "0.499" "0.999" "1.999" )
REF_REP="repB"
REF_REP_TYPE="uniform_5000singletons"

for i in "${!REP[@]}"
do
    echo "Processing ${REP[i]} originating from ${REF_REP} with mutation rate: linear ${LINMUT_1[i]} ${LINMUT_2[i]}"

    parallel --tag "grinder -rf sim_repertoires/${REF_REP}_BCR_shm_${REF_REP_TYPE}/sim_repertoire_${REF_REP}.fasta \
    -cf 10 -fr {} -rd 300 uniform 0 -abundance_model uniform -fastq_output 1 \
    -qual_levels 30 10 -od {/.} -id 572 normal 5 -md linear ${LINMUT_1[i]} ${LINMUT_2[i]} -mr 100 0" \
    ::: primers/BCR_primers.fasta

    parallel --tag "python splitfastq.py -i {} -p GGGAAGGAAGTCCTGTGCGAGGCAGCC" ::: \
    BCR_primers/grinder-reads.fastq

    mv BCR_primers/grinder-reads_otherprimer_R1.fq "${REP[i]}_all_primers_reads_R1.fq"
    mv BCR_primers/grinder-reads_R2primer_R2.fq "${REP[i]}_all_primers_reads_R2.fq"

    python renamefastq.py -i "${REP[i]}_all_primers_reads_R2.fq"
    python renamefastq.py -i "${REP[i]}_all_primers_reads_R1.fq"

    mkdir ${REP[i]}_BCR_${REF_REP_TYPE}_linmut_linear_${LINMUT_1[i]}_${LINMUT_2[i]}
    mv "${REP[i]}_all_primers_reads_R1_renamed.fastq" ${REP[i]}_BCR_${REF_REP_TYPE}_linmut_linear_${LINMUT_1[i]}_${LINMUT_2[i]}/
    mv "${REP[i]}_all_primers_reads_R2_renamed.fastq" ${REP[i]}_BCR_${REF_REP_TYPE}_linmut_linear_${LINMUT_1[i]}_${LINMUT_2[i]}/
    rm -r BCR_primers
    rm -r "${REP[i]}_all_primers_reads_R1.fq"
    rm -r "${REP[i]}_all_primers_reads_R2.fq"
done

REP=( "rep101" "rep102" "rep103" "rep104" "rep105" )
LINMUT_1=( "0" "0.001" "0.001" "0.001" "0.001" )
LINMUT_2=( "0" "0.199" "0.499" "0.999" "1.999" )
REF_REP="repC"
REF_REP_TYPE="realtrees_singletons_2"

for i in "${!REP[@]}"
do
    echo "Processing ${REP[i]} originating from ${REF_REP} with mutation rate: linear ${LINMUT_1[i]} ${LINMUT_2[i]}"

    parallel --tag "grinder -rf sim_repertoires/${REF_REP}_BCR_shm_${REF_REP_TYPE}/sim_repertoire_${REF_REP}.fasta \
    -cf 10 -fr {} -rd 300 uniform 0 -abundance_model uniform -fastq_output 1 \
    -qual_levels 30 10 -od {/.} -id 572 normal 5 -md linear ${LINMUT_1[i]} ${LINMUT_2[i]} -mr 100 0" \
    ::: primers/BCR_primers.fasta

    parallel --tag "python splitfastq.py -i {} -p GGGAAGGAAGTCCTGTGCGAGGCAGCC" ::: \
    BCR_primers/grinder-reads.fastq

    mv BCR_primers/grinder-reads_otherprimer_R1.fq "${REP[i]}_all_primers_reads_R1.fq"
    mv BCR_primers/grinder-reads_R2primer_R2.fq "${REP[i]}_all_primers_reads_R2.fq"

    python renamefastq.py -i "${REP[i]}_all_primers_reads_R2.fq"
    python renamefastq.py -i "${REP[i]}_all_primers_reads_R1.fq"

    mkdir ${REP[i]}_BCR_${REF_REP_TYPE}_linmut_linear_${LINMUT_1[i]}_${LINMUT_2[i]}
    mv "${REP[i]}_all_primers_reads_R1_renamed.fastq" ${REP[i]}_BCR_${REF_REP_TYPE}_linmut_linear_${LINMUT_1[i]}_${LINMUT_2[i]}/
    mv "${REP[i]}_all_primers_reads_R2_renamed.fastq" ${REP[i]}_BCR_${REF_REP_TYPE}_linmut_linear_${LINMUT_1[i]}_${LINMUT_2[i]}/
    rm -r BCR_primers
    rm -r "${REP[i]}_all_primers_reads_R1.fq"
    rm -r "${REP[i]}_all_primers_reads_R2.fq"
done