# MaskPrimers.py: 0.5.11 2019.01.30

# Ran from /Users/jkewz/Dropbox (yale)/projects/ellebedy_murineCovid/bcr/nested_mAb

# default --maxerror 0.2 --maxlen 50

##### mAb HC forward

# all pass

MaskPrimers.py align -s clean_mAb_HC.fasta --failed --fasta -p primers_mAb_HC_forward.fasta --mode mask --pf FWR_PR --log ../MP/mAb_HC_forward.log --outname mAb_HC_forward --outdir ../MP --maxlen 1000

# all 0
grep -e "ERROR" ../MP/mAb_HC_forward.log
grep -e "PRSTART" ../MP/mAb_HC_forward.log

##### mAb HC reverse

# all pass

MaskPrimers.py align -s ../MP/mAb_HC_forward_primers-pass.fasta --failed --fasta -p primers_mAb_HC_reverse.fasta --mode mask --pf REV_PR --log ../MP/mAb_HC_forward_reverse.log --outname mAb_HC_forward_reverse --outdir ../MP --maxlen 1000

# all 0 except one 0.0213
grep -e "ERROR" ../MP/mAb_HC_forward_reverse.log
grep -e "PRSTART" ../MP/mAb_HC_forward_reverse.log

##### mAb LC forward

# all pass

MaskPrimers.py align -s clean_mAb_LC.fasta --failed --fasta -p primers_mAb_LC_forward.fasta --mode mask --pf FWR_PR --log ../MP/mAb_LC_forward.log --outname mAb_LC_forward --outdir ../MP --maxlen 1000

# all 0 except one 0.0167 and one 0.0172
grep -e "ERROR" ../MP/mAb_LC_forward.log
grep -e "PRSTART" ../MP/mAb_LC_forward.log

##### mAb LC reverse

MaskPrimers.py align -s ../MP/mAb_LC_forward_primers-pass.fasta --failed --fasta -p primers_mAb_LC_reverse.fasta --mode mask --pf REV_PR --log ../MP/mAb_LC_forward_reverse.log --outname mAb_LC_forward_reverse --outdir ../MP --maxlen 1000

# all 0 
grep -e "ERROR" ../MP/mAb_LC_forward_reverse.log
grep -e "PRSTART" ../MP/mAb_LC_forward_reverse.log

# 34; 34
grep -c "^>" ../MP/mAb*forward_reverse_primers-pass.fasta


##### cat

cat ../MP/*forward_reverse_primers-pass.fasta > ../MP/MP_mAb.fasta

# 68
grep -c "^>" ../MP/MP_mAb.fasta

cat ../MP/MP_mAb.fasta clean_nested_[HL]C.fasta > forIgblast_nested_mAb.fasta
# 268
grep -c "^>" forIgblast_nested_mAb.fasta
