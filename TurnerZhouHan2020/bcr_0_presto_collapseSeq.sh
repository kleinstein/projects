#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 4:00:00
#SBATCH --constraint E5-2660_v3
#SBATCH -c 1
#SBATCH --mem-per-cpu=20G


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

PATH_ROOT_1="/home/qz93/project/ellebedy_pairedBulk/" #*
#PATH_ROOT_2="/home/qz93/project/ellebedy_bulk/"
#PATH_PHIX="${PATH_ROOT_1}phix/"
PATH_PRESTO="${PATH_ROOT_1}presto/"
#PATH_SUPPORT="${PATH_ROOT_2}preprocess_support_files/"
#PATH_ABSEQ="${PATH_SUPPORT}AbSeq/"

#FILENAME_P1="AbSeq_R1_Human_IG_Primers.fasta"
#FILENAME_P2="AbSeq_R2_TS.fasta"
#FILENAME_IC="AbSeq_Human_IG_InternalCRegion.fasta"

#GERM_TAG="ref_201931-4_1Aug2019" #*
# same as "~/germlines/ref_201931-4_1Aug2019/human_IG/concat_imgt_human_IG_V.fasta" #*
#FILENAME_VREF="human_imgt_V_08012019.fasta" #*

#FILENAME_YAML="prestor.yaml"

#PATH_SCRIPT_ABSEQ="/home/qz93/projects/scripts/ellebedy_pairedBulk/immcantation_presto-abseq_10242018_JZ.sh" 
PATH_SCRIPT_Q2A="/home/qz93/projects/scripts/ellebedy_pairedBulk/immcantation_fastq2fasta.py"
#PATH_SCRIPT_RMV_INCONSISTENT="/home/qz93/projects/scripts/ellebedy_pairedBulk/JZ_remove_inconsistent_internalC.py" 
#PATH_SCRIPT_META="/home/qz93/projects/scripts/ellebedy_pairedBulk/JZ_add_meta.py" #* TBD

#chmod +x "${PATH_SCRIPT_ABSEQ}"
#chmod +x "${PATH_SCRIPT_Q2A}"
#chmod +x "${PATH_SCRIPT_RMV_INCONSISTENT}"
#chmod +x "${PATH_SCRIPT_META}" #*

# log (for lopping through samples)
PATH_LOG="${PATH_PRESTO}presto_allSamples_re-CollapseSeq_$(date '+%m%d%Y_%H%M%S').log"

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "germline" $GERM_TAG &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
parallel --version | head -n 1 &>> "${PATH_LOG}"
MaskPrimers.py --version &>> "${PATH_LOG}" # presto version
muscle -version &>> "${PATH_LOG}" # muscle version
usearch --version &>> "${PATH_LOG}" # usearch version
python3 -V &>> "${PATH_LOG}"

# phix directory structure
# phix/
# 	SampleName_R1_noPhix_selected.fastq
# 	SampleName_R2_noPhix_selected.fastq

#SUBJ_ARRAY=(S7) # testing 
SUBJ_ARRAY=(S7 S8 S9 S10 S11 S12) 

CS_MISS=0
CREGION_FIELD="CREGION"
STEP=0

for SUBJ in ${SUBJ_ARRAY[@]}; do

	echo "*********** STARTING ON ${SUBJ} ***********" &>> "${PATH_LOG}"

	# presto-abseq.sh creates a logs/ and report/ folder for each run
	# the log and report files inside have the same filename across runs
	# -> set up a separate folder within /presto for each sample

	cd "${PATH_PRESTO}"

	# create sample folder: SampleName
	DIR_SAMPLE="${SUBJ}/"
	cd "${PATH_PRESTO}${DIR_SAMPLE}"

	# if existing, remove first
	#if [ -d "${DIR_SAMPLE}" ]; then
	#	echo "Removed pre-exisitng folder for sample ${SUBJ}" &>> "${PATH_LOG}"
	#	rm -r "${DIR_SAMPLE}"
	#fi

	#echo "Created new folder for sample ${SUBJ}" &>> "${PATH_LOG}"
	#mkdir "${DIR_SAMPLE}"

	# start sample-specific log file
	PATH_LOG_SAMPLE="${PATH_PRESTO}${DIR_SAMPLE}presto_sample-${SUBJ}_re-CollapseSeq_$(date '+%m%d%Y_%H%M%S').log"
	dt=$(date '+%d/%m/%Y %H:%M:%S')
	echo "$dt" &> "${PATH_LOG_SAMPLE}"

	# NO --keepmiss
	OUTNAME="re_${SUBJ}"

	printf "  %2d: %-*s $(date +'%H:%M %D')\n" $((++STEP)) 24 "CollapseSeq" &>> "${PATH_LOG_SAMPLE}"

	CollapseSeq.py -s "${SUBJ}-final_total.fastq" -n $CS_MISS \
		--uf PRCONS $CREGION_FIELD --cf CONSCOUNT --act sum --inner \
		--outname "${OUTNAME}-final" \
		&>> "${PATH_LOG_SAMPLE}"

	# Filter to sequences with at least 2 supporting sources
	printf "  %2d: %-*s $(date +'%H:%M %D')\n" $((++STEP)) 24 "SplitSeq group" &>> "${PATH_LOG_SAMPLE}"
	SplitSeq.py group -s "${OUTNAME}-final_collapse-unique.fastq" \
		-f CONSCOUNT --num 2 \
		&>> "${PATH_LOG_SAMPLE}"

	# Create table of final repertoire
	printf "  %2d: %-*s $(date +'%H:%M %D')\n" $((++STEP)) 24 "ParseHeaders table" &>> "${PATH_LOG_SAMPLE}"
	#ParseHeaders.py table -s "${OUTNAME}-final_total.fastq" \
	#    -f ID PRCONS $CREGION_FIELD CONSCOUNT --outname "final-total" \
	#    --outdir ${LOGDIR} >> $PIPELINE_LOG 2> $ERROR_LOG
	ParseHeaders.py table -s "${OUTNAME}-final_collapse-unique.fastq" \
	    -f ID PRCONS $CREGION_FIELD CONSCOUNT DUPCOUNT \
	    --outname "${OUTNAME}_final-unique" \
	    &>> "${PATH_LOG_SAMPLE}"
	ParseHeaders.py table -s "${OUTNAME}-final_collapse-unique_atleast-2.fastq" \
	    -f ID PRCONS $CREGION_FIELD CONSCOUNT DUPCOUNT \
	    --outname "${OUTNAME}_final-unique-atleast2" \
	    &>> "${PATH_LOG_SAMPLE}"

	# convert fastq to fasta
	# creates ${SUBJ}-final_collapse-unique_atleast-2.fasta
	"${PATH_SCRIPT_Q2A}" "${OUTNAME}-final_collapse-unique_atleast-2.fastq" &>> "${PATH_LOG_SAMPLE}"

done

