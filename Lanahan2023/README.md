# Lanahan 2023 NP-OVA analysis

Author: Gisela Gabernet, 2023.
Data analysis code for the single-cell BCR NP-ova experiments of the article:

> **PI3Kg in B cells promotes antibody responses and generation of antibody-secreting cells**
>
> Stephen M. Lanahan, Lucas Yang, Kate M. Jones, Zhihong Qi, Emylette Cruz Cabrera,
> Lauren Y. Cominsky, Anjali Ramaswamy, Anis Barmada, Gisela Gabernet, Dinesh Babu,
> Uthaya Kumar, Lan Xu, Peiying Shan, Matthias P. Wymann, Steven Kleinstein, V. Koneti Rao,
> Peter Mustillo, Neil Romberg, Roshini S. Abraham, and Carrie L. Lucas.
>

## Analysis steps

1. Processing the 10x Genomics scBCRdata with `cellanger vdj`.
2. Demultiplexing of the AIRR rearrangement files with `airr_demultiplexing.R`.
3. Running nf-core/airrflow v3.1.0 (DOI: 10.5281/zenodo.800797) with the command specified in `run_airrflow.sh`.
4. Downstream analysis of the BCR NP-OVA experiments and figure generation with `np_ova_analysis.Rmd`.

The results and figure plots can be found under `results`.