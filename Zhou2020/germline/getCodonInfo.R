#*******************************
#### create DNA codon table ####
#*******************************

bases = c("A","C","G","T")
codons = expand.grid(list(bases, bases, bases), stringsAsFactors=F)
codons = apply(codons, 1, paste0, collapse="")
codons = sort(codons)
length(codons) == length(unique(codons)) # expect TRUE (64)
CODON_TAB = c("K","N","K","N", # AA*, *=A/C/G/T
             "T","T","T","T", # AC*
             "R","S","R","S", # AG*
             "I","I","M","I", # AT*
             "Q","H","Q","H", # CA*
             "P","P","P","P", # CC*
             "R","R","R","R", # CG*
             "L","L","L","L", # CT*
             "E","D","E","D", # GA*
             "A","A","A","A", # GC*
             "G","G","G","G", # GG*
             "V","V","V","V", # GT*
             "*","Y","*","Y", # TA*
             "S","S","S","S", # TC*
             "*","C","W","C", # TG*
             "L","F","L","F") # TT*
names(CODON_TAB) = codons
# sanity check; expect TRUE
all.equal(CODON_TAB, shazam:::AMINO_ACIDS[match(codons, names(shazam:::AMINO_ACIDS))])

#****************************************************
#### codons giving rise to silent-only mutations ####
#****************************************************

# each cell corresponds to a codon and a position in reading frame (1, 2, or 3)
# if TRUE, mutations at that position in that codon gives rise to silent mutations only
silent3merBinary = matrix(NA, nrow=length(CODON_TAB), ncol=3,
                          dimnames=list(names(CODON_TAB), paste0("pos_", 1:3)))

for (i in rownames(silent3merBinary)) {
    # pos_1
    # gather all codons that a mutation at pos_1 can lead to
    mutated = CODON_TAB[grepl(pattern=paste0("[ATGC]", substr(i,2,3)), x=names(CODON_TAB))]
    silent3merBinary[i, "pos_1"] <- length(unique(mutated))==1
    
    # pos_2
    # gather all codons that a mutation at pos_2 can lead to
    mutated = CODON_TAB[grepl(pattern=paste0(substr(i,1,1), "[ATGC]", substr(i,3,3)), x=names(CODON_TAB))]
    silent3merBinary[i, "pos_2"] <- length(unique(mutated))==1
    
    # pos_3
    # gather all codons that a mutation at pos_3 can lead to
    mutated = CODON_TAB[grepl(pattern=paste0(substr(i,1,2), "[ATGC]"), x=names(CODON_TAB))]
    silent3merBinary[i, "pos_3"] <- length(unique(mutated))==1
}

# only 3rd position mutation gives rise to silent-only mutations
# pos_1 pos_2 pos_3 
#     0     0    32 
colSums(silent3merBinary)

# this can be verified against the biological codon table 
( CODONS_SILENT_POS3 = rownames(silent3merBinary)[silent3merBinary[, "pos_3"]] )
# e.g. all amino acid "T"
CODON_TAB["ACA"]
CODON_TAB["ACT"]
CODON_TAB["ACG"]
CODON_TAB["ACC"]

#*************************************
##### theoretical "silent" 5mers #####
#*************************************

# this does not take into account of the reading frame of the VDJ sequence
theoSilentFivemers = expand.grid(list(CODONS_SILENT_POS3, bases, bases), stringsAsFactors=F)
theoSilentFivemers = apply(theoSilentFivemers, 1, paste0, collapse="")
theoSilentFivemers = sort(theoSilentFivemers)
length(theoSilentFivemers) # 512
length(theoSilentFivemers) == length(CODONS_SILENT_POS3) * 4 * 4
length(theoSilentFivemers) == length(unique(theoSilentFivemers))

# note: in practice, when considering a 5-mer in the context of a VDJ sequence,
# even if the 5-mer is in theoSilentFivemers, it could be excluded because 
# the reading frame of the VDJ sequence is such that the central nucleotide is not
# the 3rd position in a codon in the reading frame

#***********************
#### CODON_MUT_LIST ####
#***********************

CODON_MUT_LIST = vector("list", length=length(CODON_TAB))
names(CODON_MUT_LIST) = names(CODON_TAB)

# produces CODON_MUT_LIST
# - length: 64; each entry in the list corresponds to a codon
# - each entry contains a 10-element vector;
#   each element corresponds to a codon 1-AA away from the entry or 0-AA away (itself)
#   e.g. entry: AAA
#        elements: AAA TAA GAA CAA
#                      ATA AGA ACA
#                      AAT AAG AAC
# - each element contains one of the following information:
#   NM: no change in codon nucleotides (no mutation)
#   R: replacement (codon A [stop or non-stop] to codon B [non-stop & different from A])
#   S: silent (incl. stop to stop codon)
#   P: stop mutation (non-stop to stop codon)

for (codonOld in names(CODON_MUT_LIST)) {
    aaOld = CODON_TAB[codonOld]
    # codons to mutate to
    # consider each position "independently in its germline codon context"
    newCodons = c( paste0(c("A","T","G","C"), substr(codonOld,2,3)),
                   paste0(substr(codonOld,1,1), c("A","T","G","C"), substr(codonOld,3,3)),
                   paste0(substr(codonOld,1,2), c("A","T","G","C")) )
    # there could be duplicates
    # e.g. TTT
    newCodons = unique(newCodons)
    
    curVec = vector("character", length=length(newCodons))
    names(curVec) = newCodons
    
    for (codonNew in newCodons) {
        # no mutation (same codon nucleotide composition)
        if (codonNew==codonOld) {
            curVec[codonNew] = "NM"
        } else {
            aaNew = CODON_TAB[codonNew]
            # silent mutation 
            # this includes * => *
            if (aaOld==aaNew) {
                curVec[codonNew] = "S"
            } else {
                # stop mutation (non-* => *)
                if (aaOld!="*" & aaNew=="*") {
                    curVec[codonNew] = "P"
                } else {
                    # replacement mutation (_ => non-*, where non-* is different from _)
                    # this includes * => non-*
                    curVec[codonNew] = ifelse(aaOld==aaNew, "S", "R")
                }
            }
        }
    }
    CODON_MUT_LIST[[codonOld]] = curVec
}

#***************
#### export ####
#***************

versionTag = "tag_201918-4_9May2019" #*
# here, versionTag only makes a difference as to where to save the file
setwd(paste0("/Users/jkewz/Dropbox (recherche)/yale/projects/spatial/germlineLandscape_", versionTag))
save(CODON_TAB, CODONS_SILENT_POS3, CODON_MUT_LIST, file="codon_info.RData")
