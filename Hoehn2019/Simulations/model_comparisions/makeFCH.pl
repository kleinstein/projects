

use strict;
use warnings;
use hotSpotUtils;

my @motifs = ((0)x6);
my @counts = ((0)x6);

my $mult = $ARGV[0];
my $gyw=3*$mult;
my $wrc=2*$mult;
my $wa=2*$mult;
my $tw=1*$mult;
my $syc=-0.6;
my $grs=-0.6;

open(OUT,">$ARGV[1]") or die();



my %iupac = (
	"A" => "A",
	"C" => "C",
	"G" => "G",
	"T" => "T",
	"R" => "[A|G]",
	"Y" => "[C|T]",
	"S" => "[G|C]",
	"W" => "[A|T]",
	"K" => "[G|T]",
	"M" => "[A|C]",
	"B" => "[C|G|T]",
	"D" => "[A|G|T]",
	"H" => "[A|C|T]",
	"V" => "[A|C|G]",
	"N" => "[A|C|G|T]"
);

my @ms = ("WRC_2","SYC_2","WA_1","GYW_0","GRS_0","TW_0");
my $total=0;
my $tcount=0;
open(IN,"Mutability.csv") or die();
my $h=<IN>;

print OUT "$h";

while(<IN>){
	my $line=$_;
	chomp($line);
	$line =~ s/\"//g;
	my @in = split("\\s+",$line);
	if(substr($in[0],0,3)=~/[A|T][A|G]C/){
		print OUT "$in[0]\t".($wrc+1)."\n";
	}
	elsif(substr($in[0],0,3)=~/[G|C][C|T]C/){
		print OUT "$in[0]\t".($syc+1)."\n";
	}
	elsif(substr($in[0],1,2)=~/[A|T]A/){
		print OUT "$in[0]\t".($wa+1)."\n";
	}
	elsif(substr($in[0],2,3)=~/G[C|T][A|T]/){
		print OUT "$in[0]\t".($gyw+1)."\n";
	}
	elsif(substr($in[0],2,3)=~/G[A|G][G|C]/){
		print OUT "$in[0]\t".($grs+1)."\n";
	}
	elsif(substr($in[0],2,2)=~/T[A|T]/){
		print OUT "$in[0]\t".($tw+1)."\n";
	}else{
		print OUT "$in[0]\t".(1)."\n";
	}
}
#my $th=$total/$tcount;
#print "th: $th\n";
#for(my $i=0;$i<6;$i++){
#
#	my $h=$motifs[$i]/$counts[$i];
#	my $er=$h/$th-1; #convert S5F to HLP17 h
#	print "$ms[$i]\t$h\t$er\t$h\t$th\n";
#}
#
#print "\n";

#for(my $i=0;$i<6;$i++){
#	my $h=$motifs[$i]/$counts[$i];
#	my $omut=$total;
#	my $ocount=$tcount;
#	for(my $j=0;$j<6;$j++){ #count all other motifs
#		#if($j==$i){next;}
#		$omut+=$motifs[$j];
#		$ocount+=$counts[$j];
#	}
#	my $th=$omut/$ocount;
#	my $er=$h/$th-1; #convert S5F to HLP17 h
#	print "$ms[$i]\t$h\t$er\t$omut\t$ocount\n";
#}
