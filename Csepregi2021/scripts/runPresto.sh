#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=48:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com

# Kenneth B. Hoehn
# 8/24/2021
# (formatData.R) Format data from long file
# collapse sequences and discard all sequences
# with < 3 duplicates. Then run IgBlast

module load R/4.0.3-foss-2020b
module load Python/3.7.0-fosscuda-2018b

id=$1
threads=$2

# Get fasta files from long table
Rscript formatData.R $id

# May not be necessary because sequences are already corrected
# collapse identical sequences, only keep those with > 1 copy
CollapseSeq.py -s ../fasta/$id\.fasta --uf PRIMER --inner -n 1

SplitSeq.py group -s ../fasta/$id\_collapse-unique.fasta --num 3 \
	-f DUPCOUNT
