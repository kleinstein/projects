#!/usr/bin/bash
# Kenneth B. Hoehn
# 3/18/22
# kenneth.hoehn@yale.edu
# Run presto initial presto pipeline

module load Python/3.7.0-foss-2018b

# select one or the other set
#base="MO-06232021-scBCR.ccs"
#F_SEQ=info/fprimer_run1.fasta
# Note run 1 used P3 primers labeled as P2 primers

base="Miyo-scBCR-07142021.ccs"
F_SEQ=info/fprimer_run2.fasta

R1_READS=CCS/$base.fastq
FS_QUAL=20
FS_LEN=500
NPROC=36
PIPELINE_LOG="presto/pipeline_run_$base.txt"
R_SEQ=info/rprimer_all2.fasta
C_SEQ=info/subG_shortened.fasta

# remove sequences that are < 500bp
FilterSeq.py quality -s $R1_READS -q $FS_QUAL --nproc $NPROC \
    --outdir presto --log "presto/quality_$base-1_2.log" \
    >> $PIPELINE_LOG  
OUTNAME=presto/$base\_quality-pass.fastq

# align forward primers to each sequence
MaskPrimers.py align -s $OUTNAME -p $F_SEQ \
        --maxlen 1000 --maxerror 0 --mode cut --barcode \
        --log "presto/forward_$base_2.log" --nproc $NPROC \
        >> $PIPELINE_LOG
OUTNAME=presto/$base\_quality-pass_primers-pass.fastq

# align IgG subisotype constant region primers
MaskPrimers.py align -s $OUTNAME -p $C_SEQ \
        --maxlen 1000 --maxerror 0.1 --mode tag --fail --pf SUBPRIMER \
        --log "presto/cregion_$base_3.log" --nproc $NPROC >> $PIPELINE_LOG

# edit failed file, then concatenate, since it will contain non-IgG sequences
ParseHeaders.py add -s presto/$base\_quality-pass_primers-pass_primers-fail.fastq -f SUBPRIMER -u "N"
cat presto/$base\_quality-pass_primers-pass_primers-pass.fastq \
    presto/$base\_quality-pass_primers-pass_primers-fail_reheader.fastq \
    > presto/$base\_quality-pass_primers-pass_combined.fastq    
OUTNAME=presto/$base\_quality-pass_primers-pass_combined.fastq

# align reverse primer to sequences
MaskPrimers.py align -s $OUTNAME -p $R_SEQ \
        --maxlen 1000 --maxerror 0 --mode cut \
        --log "presto/cregion_$base_2.log" --nproc $NPROC \
        >> $PIPELINE_LOG

OUTNAME=presto/$base\_quality-pass_primers-pass_combined_primers-pass.fastq
ParseHeaders.py expand -s $OUTNAME -f PRIMER \
    --outdir presto >> $PIPELINE_LOG

OUTNAME=presto/$base\_quality-pass_primers-pass_combined_primers-pass_reheader.fastq
ParseHeaders.py merge -s $OUTNAME -f PRIMER1 PRIMER2 -k BR --act cat >> $PIPELINE_LOG

# remove sequences that still have fewer than 500bp
OUTNAME=presto/$base\_quality-pass_primers-pass_combined_primers-pass_reheader_reheader.fastq
FilterSeq.py length -s $OUTNAME -n $FS_LEN --nproc $NPROC \
    --outdir presto --log "presto/length_$base-1_2.log" \
    >> $PIPELINE_LOG



