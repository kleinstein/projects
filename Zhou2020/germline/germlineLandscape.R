##### germline k-mer landscape

versionTag = "tag_201918-4_9May2019" #*

pathRoot = "~/Dropbox (recherche)/yale/projects/"
pathWork = paste0(pathRoot, "spatial/germlineLandscape_", versionTag, "/")
pathGermline = paste0(pathRoot, "germ/", versionTag, "/")
pathScript = paste0(pathRoot, "scripts/spatial/")

load(paste0(pathWork, "codon_info.RData")) 
source(paste0(pathScript, "helpers.R")) # removeGaps, getKmers, getTargetKmers
source(paste0(pathRoot, "scripts/auxiliary/vioplotLst.R"))

library(tibble)
library(seqinr)
library(ggplot2)

setwd(pathWork)

load(paste0(pathGermline, "imgt_human_IGHV_IGHJ.RData")) # germIMGT_V/J

#****************************
##### remove duplicates #####
#****************************

RUN = F

if (RUN) {
    # from data_mg_tiggerCorrection.R
    # tag_201918-4_9May2019
    # len_320
    # IGHV1-69*01 IGHV1-69D*01 
    # IGHV3-23*01 IGHV3-23D*01 
    # IGHV3-30*02 IGHV3-30-5*02 
    # IGHV3-30*04 IGHV3-30-3*03 
    # IGHV3-30*18 IGHV3-30-5*01 
    # len_322 
    # IGHV2-70*04 IGHV2-70D*04 
    # IGHV3-29*01 IGHV3-30-42*01
    
    lst = list(c("IGHV1-69*01", "IGHV1-69D*01"), 
               c("IGHV3-23*01", "IGHV3-23D*01"), 
               c("IGHV3-30*02", "IGHV3-30-5*02"), 
               c("IGHV3-30*04", "IGHV3-30-3*03"), 
               c("IGHV3-30*18", "IGHV3-30-5*01"), 
               c("IGHV2-70*04", "IGHV2-70D*04"), 
               c("IGHV3-29*01", "IGHV3-30-42*01"))
    
    for (i in 1:length(lst)) {
        cur_1 = lst[[i]][1]
        cur_2 = lst[[i]][2]
        # since germIMGT_V is filered, and lst is baed on pre-filtered
        # the duplicate pair may not be in filtered germIMGT_V
        # but if one is missing, the other in the pair must necessarily be missing to
        if (!cur_1 %in% names(germIMGT_V)) {
            stopifnot( !cur_2 %in% names(germIMGT_V) )
            cat("not in filtered:", cur_1, cur_2, "\n")
        }
        # if in, the pair must necessarily have identical seqs
        # remove 2nd entry in pair
        if (cur_1 %in% names(germIMGT_V)) {
            stopifnot( germIMGT_V[cur_1] == germIMGT_V[cur_2] )
            germIMGT_V = germIMGT_V[-which(names(germIMGT_V)==cur_2)]
            cat("removed from filtered:", cur_2, "\n")
        }
    }
    
    # removed from filtered: IGHV1-69D*01 
    # removed from filtered: IGHV3-23D*01 
    # removed from filtered: IGHV3-30-5*02 
    # removed from filtered: IGHV3-30-3*03 
    # removed from filtered: IGHV3-30-5*01 
    # removed from filtered: IGHV2-70D*04 
    # not in filtered: IGHV3-29*01 IGHV3-30-42*01
    
    # 212
    # 218 (filtered) - 6 (removed from filtered) = 212
    length(germIMGT_V)
    
    # 11
    length(germIMGT_J)
    # 11
    length(unique(germIMGT_J))
    
    save(germIMGT_V, germIMGT_J,
         file=paste0(pathGermline, "imgt_human_IGHV_IGHJ_noDup.RData"))
} else {
    load(paste0(pathGermline, "imgt_human_IGHV_IGHJ_noDup.RData"))
}


#*********************************
##### trim germIMGT_V to 312 #####
#*********************************

germIMGT_V = substr(germIMGT_V, 1, 312)
table(nchar(germIMGT_V))


#*****************
##### concat #####
#*****************

germIMGTAll = tibble(allele=c(names(germIMGT_V), names(germIMGT_J)),
                     seq=c(germIMGT_V, germIMGT_J),
                     type=c(rep("V", length(germIMGT_V)),
                            rep("J", length(germIMGT_J))),
                     novel=c(rep(F, length(germIMGT_V)),
                             rep(F, length(germIMGT_J)))
)
table(nchar(germIMGTAll$seq))


#*****************************************
##### hot spot density in FWR vs CDR #####
#*****************************************

# Supp Fig 8A

# IGHV 1-312
# 212 alleles; all 312 nts
table(nchar(germIMGT_V))

# with IMGT gaps
# CDR FWR 
#  66 246
table(shazam::IMGT_V@boundaries)

# helper function
.countHotSpots = function(seq){
    # gapped vs non-gapped correspondence
    seqInfo = getSqNoGapInfo(sq=seq)
    # since curated, expect length to be multiple of 3
    stopifnot( nrow(seqInfo) %% 3 ==0 )
    
    # sanity check; expect match 
    seqNoGap = removeGaps(germ=seq)[["germNoGaps"]]
    stopifnot( seqNoGap == paste(seqInfo[["sqChar"]], collapse="") )
    
    # get kmers
    kmers = getKmers(seq=seqNoGap, k=5, uniq=F)
    seqInfo[["kmer"]] = NA
    # first 2 positions and last 2 positions ignored
    kmer_start_idx = 3
    kmer_end_idx = nrow(seqInfo)-2
    stopifnot( length(kmer_start_idx:kmer_end_idx) == length(kmers) )
    
    seqInfo[["kmer"]][kmer_start_idx:kmer_end_idx] = kmers
    stopifnot( all.equal( substr(kmers, 3, 3), seqInfo[["sqChar"]][kmer_start_idx:kmer_end_idx] ) )
    
    # region def: FWR vs. CDR
    seqInfo[["region"]] = as.character(shazam::IMGT_V@boundaries[seqInfo[["idxGapped"]]])
    
    # spot classification
    seqInfo[["spot"]] = NA
    seqInfo[["spot"]][kmer_start_idx:kmer_end_idx] = sapply(kmers, getSpotFivemer)
    # sanity check: if kmer is NA, spot should be NA
    stopifnot( sum(is.na(seqInfo[["spot"]])) == sum(is.na(seqInfo[["kmer"]])) )
    
    # whether spot is hot or not
    seqInfo[["hot"]] = NA
    seqInfo[["hot"]][kmer_start_idx:kmer_end_idx] = seqInfo[["spot"]][kmer_start_idx:kmer_end_idx] %in% c("hotWA/TW", "hotWRC/GYW")
    # sanity check: if kmer is NA, hot should be NA
    stopifnot( sum(is.na(seqInfo[["hot"]])) == sum(is.na(seqInfo[["kmer"]])) )
    
    # tabulate number of hot spots in CDR vs FWR
    #     FALSE TRUE
    # CDR 
    # FWR
    tab = table(seqInfo[["region"]], seqInfo[["hot"]], useNA="no")
    
    # convert into %
    perc_hot = tab[, 2] / rowSums(tab)*100
    
    return(perc_hot)
}

# each row is an allele
# cols = % hot spots in CDR, FWR
germIMGT_V_percHot_FWRvsCDR = data.frame(do.call(rbind, sapply(germIMGT_V, .countHotSpots, simplify=F)))

# paired test
germIMGT_V_percHot_FWRvsCDR_test = wilcox.test(x=germIMGT_V_percHot_FWRvsCDR[["CDR"]],
                                               y=germIMGT_V_percHot_FWRvsCDR[["FWR"]],
                                               alternative="g", paired=T)
#* 7.788152e-37
( germIMGT_V_percHot_FWRvsCDR_test[["p.value"]] )

# visualize
plotName = "germIMGT_V_percHot_FWRvsCDR.pdf"
setwd(pathWork)
pdf(plotName, width=5, height=5)
par(mar=c(2,4.5,0.4,0.2)) # BLTR
ptCol = scales::alpha("gray60", 0.7)
plot(x=0, y=0, xlim=c(0.5, 2.5), ylim=c(0, 100), 
     xaxt="n", xlab="", ylab="Hot Spot Percentage",
     cex.lab=1.5, cex.axis=1.5)
axis(side=1, at=c(1,2), labels=c("CDR", "FWR"), tick=F, cex.axis=1.5, cex.lab=2)
# paired points
for(i in 1:nrow(germIMGT_V_percHot_FWRvsCDR)) {
    points(x=c(1,2), y=c(germIMGT_V_percHot_FWRvsCDR[["CDR"]][i], germIMGT_V_percHot_FWRvsCDR[["FWR"]][i]),
           type="p", col=ptCol, cex=0.5)
    lines(x=c(1,2), y=c(germIMGT_V_percHot_FWRvsCDR[["CDR"]][i], germIMGT_V_percHot_FWRvsCDR[["FWR"]][i]),
          col=ptCol)
}
# boxplots
boxplot(germIMGT_V_percHot_FWRvsCDR[["CDR"]], add=T, col="transparent", outline=F, at=1, ann=F, yaxt="n", xaxt="n")
boxplot(germIMGT_V_percHot_FWRvsCDR[["FWR"]], add=T, col="transparent", outline=F, at=2, ann=F, yaxt="n", xaxt="n")
dev.off()


#****************************************
##### spot distribution in germline #####
#****************************************

germToPlot = germIMGTAll[germIMGTAll$type=="V", ]

# order of 1:4 needs to be kept consistent with that in getColFivemer
spotColors = c("firebrick2", "darkgoldenrod2", "dodgerblue2", "gray50", "transparent")
names(spotColors) = c("hotWRC/GYW", "hotWA/TW", "coldSYC/GRS", "neutral", "na")

for (boolSilent in c(T,F)) {
    for (boolAtLeastTwice in c(T,F)) {
        
        cat("boolSilent:", boolSilent, "; boolAtLeastTwice:", boolAtLeastTwice, "\n")
        
        ##### set-up
        # "long" format (for ggplot)
        spotDistrV = data.frame(matrix(NA, nrow=(nrow(germToPlot)*length(3:310)), ncol=6,
                                       dimnames=list(NULL, 
                                                     c("allele","pos","kmer","ctr","spot","col")) ))
        spotDistrV$allele = rep(germToPlot$allele, each=length(3:310))
        spotDistrV$pos = rep(3:310, times=nrow(germToPlot))
        
        ##### get
        for (i in 1:nrow(germToPlot)) {
            curAllele = germToPlot$allele[i]
            #cat("i=", i, "; allele=", curAllele, "\n")
            curDf = getGermVGappedSpots(vSeq=germToPlot$seq[i], k=5, spotCols=spotColors[1:4], 
                                        silent=boolSilent,
                                        atLeastTwice=boolAtLeastTwice)$df
            
            if (!is.null(curDf)) {
                # sanity check
                if (boolAtLeastTwice) {
                    stopifnot(all(table(curDf$kmers)>1))
                }
                
                for (j in 1:nrow(curDf)) {
                    #if (j%%100==0) {cat("j=", j, "\n")}
                    idxSpotDistrV = which(spotDistrV$allele==curAllele & 
                                          spotDistrV$pos==as.integer(curDf$idxGapped[j]))
                    spotDistrV[idxSpotDistrV, c("kmer", "ctr", "spot", "col")] = 
                        curDf[j, c("kmers", "fivemerCtrs", "fivemerSpots", "fivemerCols")]
                }
            }
            
        }
        spotDistrV$spot[is.na(spotDistrV$spot)] = "na" 
        
        setwd(pathWork)
        save(spotDistrV, file=paste0("spotDistrV_silent_", boolSilent, 
                                     "_atLeastTwice_", boolAtLeastTwice, ".RData"))

        ##### clean
        rm(spotDistrV)
        
    }
}

#*******************
##### distance #####
#*******************

for (boolSilent in c(T,F)) {
    
    targetFivemers_V = vector("list", length=nrow(germToPlot))
    names(targetFivemers_V) = germToPlot[["allele"]]
    
    for (i in 1:nrow(germToPlot)) {
        #cat(names(targetFivemers_V)[i], "\n")
        targetFivemers_V[[i]] = getTargetFivemerInfo(getSqNoGapInfo(germToPlot[["seq"]][i]), 
                                                     silent=boolSilent, atLeastTwice=TRUE)
    }
    
    # count unique target 5-mers for each VJ
    # if NA, nrow() gives NULL
    targetFivemersTab_V = lapply(targetFivemers_V, 
                                 function(x){ if(!is.null(nrow(x))) table(x$kmer) })
    
    # entries in targetFivemers_V could be NA
    # corresponding entries in targetFivemersTab_V could be NULL
    numPairsPerAllele = lapply(targetFivemersTab_V, 
                               function(x){ if (!is.null(x)) sum(sapply(x,choose,k=2)) })
    stopifnot(!any(is.na(numPairsPerAllele)))
    # NULL in targetFivemersTab_V remains as NULL; removed when unlist()
    numPairsTotal = sum(unlist(numPairsPerAllele))
    
    distMtxCols = c("germID", "kmer", "totalNumInstances", "distNoGap", 
                    "idxNoGap_1", "idxNoGap_2", "idxGapped_1", "idxGapped_2",
                    "distGappedNearestCDR_1", "nearestCDR_1", 
                    "distGappedNearestCDR_2", "nearestCDR_2")
    distMtx = data.frame(matrix(NA, nrow=numPairsTotal, ncol=length(distMtxCols)))
    colnames(distMtx) = distMtxCols
    
    counter=1
    for (i in 1:length(targetFivemers_V)) {
        
        if (!is.na(targetFivemers_V[i])) {
            curAllele = names(targetFivemers_V)[i]
            cat("cur allele:", curAllele, "\n")
            curDf = targetFivemers_V[[i]]
            
            curUniqKmers = unique(curDf$kmer)
            
            for (j in 1:length(curUniqKmers)) {
                
                curKmer = curUniqKmers[j]
                curKmerIdx = which(curDf$kmer==curKmer)
                stopifnot(length(curKmerIdx)>=2)
                curKmerIdxPairs = combn(x=curKmerIdx, m=2, simplify=T)
                
                for (k in 1:ncol(curKmerIdxPairs)) {
                    distMtx[counter, "idxNoGap_1"] = curDf[curKmerIdxPairs[1,k], "idxNoGap"]
                    distMtx[counter, "idxNoGap_2"] = curDf[curKmerIdxPairs[2,k], "idxNoGap"]
                    distMtx[counter, "idxGapped_1"] = curDf[curKmerIdxPairs[1,k], "idxGapped"]
                    distMtx[counter, "idxGapped_2"] = curDf[curKmerIdxPairs[2,k], "idxGapped"]
                    distMtx[counter, "distNoGap"] = curDf[curKmerIdxPairs[2,k], "idxNoGap"] - curDf[curKmerIdxPairs[1,k], "idxNoGap"]
                    distMtx[counter, "germID"] = curAllele
                    distMtx[counter, "kmer"] = curKmer
                    distMtx[counter, "totalNumInstances"] = sum(curDf$kmer == curKmer)
                    
                    nearestCDR_1 = getDistFromClosestCDR(ctrPosGapped=distMtx[counter, "idxGapped_1"])
                    nearestCDR_2 = getDistFromClosestCDR(ctrPosGapped=distMtx[counter, "idxGapped_2"])
                    distMtx[counter, "distGappedNearestCDR_1"] = nearestCDR_1$dist
                    distMtx[counter, "nearestCDR_1"] = nearestCDR_1$cdr
                    distMtx[counter, "distGappedNearestCDR_2"] = nearestCDR_2$dist
                    distMtx[counter, "nearestCDR_2"] = nearestCDR_2$cdr
                    
                    counter = counter+1
                }
            }
        }
        
    }
    stopifnot(counter==(numPairsTotal+1))
    sum(is.na(distMtx))
    
    distMtx$spot = sapply(distMtx$kmer, getSpotFivemer)
    # novel?
    distMtx$novel <- distMtx$germID %in% unique(germIMGTAll[["allele"]][germIMGTAll$novel])
    
    # sanity check for $totalNumInstances
    distMtxPairs = paste(distMtx$germID, distMtx$kmer, sep="@")
    distMtxUniqPairs = unique(distMtxPairs)
    distMtxUniqPairsIdx = match(distMtxUniqPairs, distMtxPairs)
    stopifnot( all.equal(distMtxUniqPairs, distMtxPairs[distMtxUniqPairsIdx]) )
    
    
    setwd(pathWork)
    save(distMtx, file=paste0("distMtxV_silent_", boolSilent, ".RData")) 
    
    rm(distMtx)
}

# allele-level
uniqAlleles = sort(unique(distMtx$germID))
length(uniqAlleles) # 210 (silent+atLeast2); 212 (atLeast2)

# gene-level
uniqGenes = sort(unique( sapply(uniqAlleles, function(s){ strsplit(s,"\\*")[[1]][1] }) ))
length(uniqGenes) # 51 (silent+atLeast2; atLeast2)

# spot
uniqMotifs = unique(distMtx[["kmer"]])
length(uniqMotifs) # 44 (silent+atLeast2); 389 (atLeast2)
uniqMotifs_spot = sapply(uniqMotifs, function(m){ distMtx[["spot"]][ which(distMtx[["kmer"]]==m)[1] ] })
table(uniqMotifs_spot)
# silent+atLeast2
# coldSYC/GRS    hotWA/TW  hotWRC/GYW     neutral 
#          12           3           3          26
# atLeast2
# coldSYC/GRS    hotWA/TW  hotWRC/GYW     neutral 
#          55          67          51         216
    
# number of MAPs per allele
tab1 = table(distMtx[["germID"]])
# silent+atLeast2
#  Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 1.000   3.000   4.000   4.171   5.000   8.000
# atLeast2
#  Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
# 45.00   54.00   58.50   59.92   64.25   88.0
summary(as.vector(tab1))

tab2 = table(tab1)
# 1st row: number of MAPs
# 2nd row: number of V alleles with that many MAPs
# silent+atLeast2
# 1  2  3  4  5  6  7  8 
# 1 34 41 51 32 39 10  2
# atLeast2
# 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 76 77 78 80 82 83 84 88 
#  3  4  2  6  2 10  5 10  9 15 10 11  6 13 12  5  7  8 11 10  5  5  2  6  5  5  3  4  2  2  1  2  2  2  3  2  1  1

tab1[ which(tab1==8) ]
# silent+atLeast2: IGHV3-64*03 IGHV4-30-2*03

tab1[ which(tab1>=80) ]
# atLeast2
# IGHV3-21*03 IGHV3-48*01 IGHV3-48*02 IGHV3-48*04 IGHV3-64*01 IGHV3-64*02 IGHV5-51*01 IGHV5-51*03 IGHV5-51*04 
#          80          82          84          83          82          88          83          80          82 

