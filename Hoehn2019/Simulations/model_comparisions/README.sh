### Simulation analyses for different models
# Kenneth Hoehn
# 11/1/2019
# Do not run this as a shell script! Execute each command individually
# and wait for it to complete. This is just an explanation of the pipeline


## Generates simulated data and runs IgPhyML on each repertoire
# makeFCH.pl creates mutability file, uses Mutability.csv
# simulateRepertoire.pl generates files and runs IgPhyML
# uses simulateFull.pl, HD1310.tsv, runq.sh, blank.txt
sbatch runSimulations.sh

# Summarize results
perl parseStats.pl

#clean up
mv slurm* old