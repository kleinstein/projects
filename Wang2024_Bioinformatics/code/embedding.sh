#!/bin/bash
# download data file
if [ -f ../data/antibody_info.xlsx ]; then
    echo "File already downloaded. "
else
    echo "File does not exist. Downloading data file. "
    wget https://github.com/jianfcpku/convergent_RBD_evolution/raw/main/antibody_info.xlsx -P ../data
fi

# cleanup the file
if [ -f ../data/AIRR_antibody_info.tsv ]; then
    echo "Already converted to AIRR file"
else
    echo "Creating AIRR file"
    python format_file.py ../data/antibody_info.xlsx ../data/AIRR_antibody_info.tsv
fi

# run nextflow embedding pipeline
nextflow run ../../../nf-amulet/main.nf -profile singularity --input ../data/ -w ../data/work --outdir ../data/results
