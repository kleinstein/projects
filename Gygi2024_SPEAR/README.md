# SPEAR_manuscript
Code utilized for both data analysis and figure generation of the SPEAR manuscript.

**For the SPEAR R-package, see the [SPEAR Bitbucket repository here](https://bitbucket.org/kleinstein/spear/src/main/)**

Note:: SPEAR objects themselves are quite large in size (given that they store the multi-omic assay data). These scripts depend upon those SPEAR objects which can be obtained by contacting jeremy.gygi@yale.edu .
