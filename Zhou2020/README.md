Code for Zhou & Kleinstein, J. Immunol., 2020

[Position-Dependent Differential Targeting of Somatic Hypermutation](https://doi.org/10.4049/jimmunol.2000496)


```
├── README.md
├── analysis
│   ├── analyze_1.R
│   ├── analyze_2.R
│   ├── analyze_3.R
│   ├── analyze_4.R
│   ├── analyze_distToNearestCDR.R
│   ├── analyze_mtby_freq.R
│   ├── analyze_mtby_window_calc.R
│   ├── analyze_mtby_window_tune.R
│   ├── analyze_mtby_window_tunePlot.R
│   ├── analyze_regression.R
│   ├── analyze_sim.R
│   ├── fig_seqLogo_Supp10CD.R
│   └── sim_generateSeqsBySubj.R
├── auxiliary
│   ├── additional_qc_briney.py
│   ├── additional_qc_rest.py
│   ├── briney_groupGenes.R
│   ├── briney_nearestDist.R
│   ├── briney_parse_csv.py
│   ├── briney_parse_json.py
│   ├── helpers.R
│   ├── mcnemar.R
│   ├── plotCrossHam.R
│   ├── remove_inconsistent_internalC.py
│   ├── split_chains.py
│   └── vioplotLst.R
├── data
│   ├── all_non-productive.R
│   ├── all_productive.R
│   ├── all_subjOSmeta.R
│   ├── briney_0a_download.sh
│   ├── briney_0b_decompress.sh
│   ├── briney_0c_parse.sh
│   ├── briney_0d_presto.sh
│   ├── briney_1_igblast.sh
│   ├── briney_2a_tigger.R
│   ├── briney_3a_createGermline.sh
│   ├── briney_3b_calcMut.R
│   ├── briney_3c_slide.R
│   ├── briney_3d_filter.R
│   ├── briney_4a_distToNearest.R
│   ├── briney_4b_findThreshold.R
│   ├── briney_4c_distToNearestPlot.R
│   ├── briney_5a_defineClones.R
│   ├── briney_5b_createGermline.sh
│   ├── briney_6a_collapseDups.R
│   ├── briney_6b_muFreq.R
│   ├── briney_6c_collapseClones.R
│   ├── briney_7_dedup_mask.R
│   ├── fraussen_1_igblast.sh
│   ├── fraussen_2a_tigger.R
│   ├── fraussen_2b_dedup.R
│   ├── fraussen_3a_slideFilter.sh
│   ├── fraussen_3b_slideFilter.R
│   ├── fraussen_4a_distToNearest.R
│   ├── fraussen_4b_distToNearestPlot.R
│   ├── fraussen_5_changeo.sh
│   ├── fraussen_6_postClone.R
│   ├── laserson_0a_fastq2fasta.sh
│   ├── laserson_0b_add_meta.py
│   ├── laserson_1_igblast.sh
│   ├── laserson_2a_tigger.R
│   ├── laserson_2b_dedup.R
│   ├── laserson_3a_slideFilter.sh
│   ├── laserson_3b_slideFilter.R
│   ├── laserson_4a_distToNearest.R
│   ├── laserson_4b_distToNearestPlot.R
│   ├── laserson_5_changeo.sh
│   ├── laserson_6_postClone.R
│   ├── rubelt_0_separate.py
│   ├── rubelt_1_igblast.sh
│   ├── rubelt_2a_tigger.R
│   ├── rubelt_2b_dedup.R
│   ├── rubelt_3a_slideFilter.sh
│   ├── rubelt_3b_slideFilter.R
│   ├── rubelt_4a_distToNearest.R
│   ├── rubelt_4b_distToNearestPlot.R
│   ├── rubelt_5_changeo.sh
│   ├── rubelt_6_postClone.R
│   ├── vanderheiden_0a_getPresto.sh
│   ├── vanderheiden_0b_fasta_meta.sh
│   ├── vanderheiden_1_igblast.sh
│   ├── vanderheiden_2a_tigger.R
│   ├── vanderheiden_2b_dedup.R
│   ├── vanderheiden_3a_slideFilter.sh
│   ├── vanderheiden_3b_slideFilter.R
│   ├── vanderheiden_4a_distToNearest.R
│   ├── vanderheiden_4b_distToNearestPlot.R
│   ├── vanderheiden_5_changeo.sh
│   └── vanderheiden_6_postClone.R
├── germline
|   ├── fig_MAPs_genotypes.R
│   ├── germlineLandscape.R
│   ├── getCodonInfo.R
│   └── getGermIMGT.R
└── s5f
    ├── build_S5F_1.R
    ├── build_S5F_2.R
    ├── build_S5F_3.R
    └── build_S5F_4.R
```


## Overview

The `auxiliary` directory contains various auxiliary scripts and helper functions.

The `germline` directory contains the following:

* `getGermIMGT.R` processes IMGT germline reference sequences and should be run before data processing.

* `getCodonInfo.R` assesses theoretical 5-mers and should be run before analysis.

* `germlineLandscape.R` surveys motif-allele pairs (MAPs) in the germline landscape.

* `fig_MAPs_genotypes.R` visualizes the germline landscape and individualizes genotypes.  

The `data` directory contains scripts for reprocessing of various datasets. The filenames of the scripts for individual datasets indicate the order in which they are run (e.g. `_1_` >> `_2a_` >> `_2b_` >> `_3a_`...).

* For reprocessing Briney et al. 2019: `briney_*`.

* For reprocessing Fraussen et al. 2019: `fraussen_*`.

* For reprocessing Laserson et al. 2014 / Gupta et al. 2017: `laserson_*`.

* For reprocessing Rubelt et al. 2016: `rubelt_*`.

* For reprocessing Vander Heiden et al. 2017: `vanderheiden_*`.

* For aggregating datasets:

	* `all_subjOSmeta.R`: specifies paths to various files for each dataset.

	* `all_productive.R`: aggregates producitvely rearranged sequences across datasets, subsetting to memory/class-switched if applicable. 
	
	* `all_non-productive.R`: aggregates across datasets sequences that are non-producitvely rearranged due to out-of-frame junctions, subsetting to memory/class-switched if applicable. 

The `s5f` directory contains scripts for building an updated version of the Synonymous, 5-mer, Functional (S5F) targeting model for somatic hypmermutation. The filenames indicate the order in which the scripts are run.

The `analysis` directory contains scripts for performing various analyses.

* The basis of all subsequent analyses is built by running `analyze_1.R` followed by `analyzed_2.R`.

* Simulation analysis: `sim_generateSeqsBySubj.R` performs the simulation, which is then analyzed by `analyze_sim.R`.

* Meta analysis: `analyze_3.R`.

* Additional analyses. These scripts are independent of each other, but are all dependent of the results from the meta analysis.
	* `analyze_4.R`
	* `analyze_distToNearestCDR.R`
	* `analyze_regression.R`
	* `analyze_mtby_window_calc.R` >> `analyze_mtby_window_tune.R` >> `analyze_mtby_window_tunePlot.R` >> `analyze_mtby_freq.R`

* Response to reviewers' comments: `fig_seqLogo_Supp10CD.R`.


## Correspondence between tables & figures and scripts

Manuscript    | Script                                | File Produced by Script
----------    | -------                               | --------
Fig 1A        | `germline/fig_MAPs_genotypes.R`       | `fig1_silent-TRUE_atLeastTwice-TRUE.pdf`
Fig 1B        | `germline/fig_MAPs_genotypes.R`       | `num_MAPs_per_allele_silent-TRUE_v2.pdf`
Fig 1C        | `germline/fig_MAPs_genotypes.R`       | `distV_noGap_silent_TRUE_v2.pdf`
Fig 2A        | `analysis/analyze_4.R`                | `fig_visual_mutFreq_*.pdf`
Fig 2B        | `analysis/analyze_2.R`                | `mag_sigf_v3.pdf`
Fig 2C        | `analysis/analyze_sim.R`              | `perc_sigf_pr_npr.pdf`
Fig 3         | `analysis/analyze_3.R`                | `pr_MT1_mtx_rv.pdf`
Fig 4A-D      | `analysis/analyze_4.R`                | `pr_allele_byVH_IGHV[1234].pdf` 
Fig 4E        | `analysis/analyze_4.R`                | `pr_allele_multiVH.pdf` 
Fig 5A        | `analysis/analyze_mtby_freq.R`        | `fig_supp_pos_mutFreq_pos_neg.pdf`
Fig 5B        | `analysis/analyze_regression.R`       | `DIST_NUC_MAPs-pos_neg.pdf`
Fig 5C        | `analysis/analyze_mtby_freq.R`        | `fig_percHot_mutFreq_WRC_GYW_WA_TW_16_27.pdf`
Fig 5D        | `analysis/analyze_mtby_freq.R`        | `fig_avgMutability_mutFreq_16_27.pdf`
Fig 5E        | `analysis/analyze_mtby_freq.R`        | `fig_avgMutability_mutFreq_23_27.pdf`
Supp Fig 1A   | `germline/fig_MAPs_genotypes.R`       | `fig1_supp_spotDistrV_silent_TRUE_atLeastTwice_TRUE.pdf`
Supp Fig 1B   | `germline/fig_MAPs_genotypes.R`       | `fig1_supp_spotDistrV_silent_FALSE_atLeastTwice_TRUE.pdf`
Supp Fig 2A   | `germline/fig_MAPs_genotypes.R`       | `fig1_silent-FALSE_atLeastTwice-TRUE.pdf`
Supp Fig 2B   | `germline/fig_MAPs_genotypes.R`       | `num_MAPs_per_allele_silent-FALSE_v2.pdf`
Supp Fig 2C   | `germline/fig_MAPs_genotypes.R`       | `distV_noGap_silent_FALSE_v2.pdf`
Supp Fig 3    | `germline/fig_MAPs_genotypes.R`       | `genoTested_*.pdf`
Supp Fig 4    | `analysis/analyze_2.R`                | `mag_sigf_v3.pdf`
Supp Fig 5A   | `s5f/build_S5F_3.R`                   | `hedgehog_noEx_[ATGC].pdf`
Supp Fig 5B   | `s5f/build_S5F_4.R`                   | `mut_m_both_2019_2013.pdf`
Supp Fig 5C   | `s5f/build_S5F_4.R`                   | `mut_mOnly_boxplot.pdf`
Supp Fig 5D   | `s5f/build_S5F_2.R`                   | `sub_1mer_norm.pdf`
Supp Fig 6    | `analysis/analyze_3.R`                | `pr_MT1_mtx_rv_xaxis.tsv`
Supp Fig 7AB  | `analysis/anallyze_4.R`               | `motif_MT1_*.txt` (input for pLogo)
Supp Fig 7C   | `analysis/anallyze_4.R`               | `motif_MT1_spot_sigfPos_sigfNeg.pdf`
Supp Fig 7D   | `analysis/anallyze_4.R`               | `motif_MT1_VH_sigfPos_sigfNeg.pdf`
Supp Fig 8A   | `germline/germlineLandscape.R`        | `germIMGT_V_percHot_FWRvsCDR.pdf`
Supp Fig 8B   | `analysis/analyze_distToNearestCDR.R` | `distGappedNearestCDR_MAPs-pos_neg_1.pdf`
Supp Fig 9    | `analysis/analyze_mtby_freq.R`        | `fig_percHot_mutFreq_*_16_27.pdf`
Supp Fig 10AB | `analysis/analyze_2.R`                | `mag_sifg_v3_supp.pdf`
Supp Fig 10CD | `analysis/fig_seqLogo_Supp10CD.R`     | `gr2_combined.pdf`
Table I       | `data/all_productive.R`               | `pr_seqCount.txt`
Table I       | `data/all_non-productive.R`           | `np_seqCount.txt`
Table II      | `analysis/analyze_2.R`                | `tab_detected_tested.txt`


## Saved core data objects

Select core data objects are provided in `RData` format [here](https://drive.google.com/drive/folders/1m8VWLmuumn2JVBT_HSfcstGP1En31qnd?usp=sharing).

* Generated by scripts in `germline`:
	* `codon_info.RData`
	* `imgt_human_*.RData`
	* `distMtx_*.RData`
	* `spotDistrV_*.RData`

* Generated by scripts in `data` and served as the starting point of analysis:
	* `pr_aggregate.RData`
	* `np_aggregate.RData`

* Generated by scripts in `s5f` and constitute the updated S5F model. `_m_only` = based on directly measured 5-mers only. `_ex` = extended.
	* `sub_1mer.RData`: updated 1-mer substitution model
	* `sub_5mer.RData`: updated 5-mer substitution model
	* `mut_5mer.RData`: updated 5-mer mutability model
	* `tar.RData`: updated targeting model

* Generated by scripts in `analysis`:
	* `diffBySubj_test_*.RData`: produced at the end of `analyze_2.R`
	* `pr_crossSubj_objects.RData`: produced at the end of `analyze_3.R`
