

# vecLst: a list of vectors (names of list entries go into x axis)
# yHoriz: numeric value for horizontal dashed line
# xLas: 2 = vertical, 1=horizontal
# yLas: 3 = vertical, 2=horizontal

# useBoxplot: use boxplot instead of violin plot
# showPoints: show raw points (not recommended to use with useBoxplot=T if there're lots of points)
# configs for showPoints=T:
# - pointsPch: VECTORS matching length of vecLst
# - pointsCol: VECTORS matching length of vecLst (recommen using transparency via scales::alpha)
# - pointsCex: single value, cex of points
# - pointsJitterAmount: single value, passed to `amount` in `jitter``
# - pointsJitterBool: if TRUE, jitter; if FALSE, do not jitter
vioplotLst = function(vecLst, vioYmax=NULL, vioYmin=0, xLas=2, yLas=3, 
                      xNames=NULL, xTick=TRUE, xlab="", ylab="", title="", 
                      countCex=0.7, xaxisCex=1, yaxisCex=1,
                      yHoriz=NULL, yHorizCol="firebrick2",
                      vioBkgCol=NA, vioMedCol="hotpink", vioRecCol="darkgoldenrod2",
                      vioMedPch=18, vioMedStyle="point", vioBoxWidth=0.2, vioWhiskerCol="black",
                      useBoxplot=F, showPoints=F, 
                      pointsPch, pointsCol, pointsCex=0.3, 
                      pointsJitterAmount=0.4, pointsJitterBool=TRUE) {
    nVec = length(vecLst)
    
    if (is.null(vioYmax)) {
        vioYmax = max(unlist(vecLst), na.rm=T)*1.05
    }
    
    # base plot
    plot(x=0:(nVec+1), y=rep(0, nVec+2),
         col=NA, ylim=c(vioYmin, vioYmax), xlim=c(0.5, nVec+0.5), 
         xaxt="n", yaxt="n", xlab=xlab, ylab=ylab, main=title, 
         cex.lab=1.2, cex.axis=1.2, cex.main=1.5)
    axis(side=1, at=1:nVec, labels=xNames, las=xLas, cex.axis=xaxisCex, tick=xTick)
    axis(side=2, las=yLas, cex.axis=yaxisCex)
    
    # individual plots
    for (i in 1:nVec) {
        
        if (!useBoxplot) {
            
            # do not attemp,t to plot if NULL or if not NULL but of length 0
            if (!is.null(vecLst[[i]]) && length(vecLst[[i]])>0) {
                # vioplot may break if all values are the same
                # e.g. vioplot(rep(0, 113))
                if ( length(unique(vecLst[[i]])) > 1) {
                    
                    # if adding raw points
                    # do this BEFORE drawing vioplot
                    if (showPoints) {
                        if (pointsJitterBool) {
                            pts_x = jitter(x=rep(i, length=length(vecLst[[i]])), amount=pointsJitterAmount)
                        } else {
                            pts_x = rep(i, length=length(vecLst[[i]]))
                        }
                        points(x=pts_x, y=vecLst[[i]], cex=pointsCex, col=pointsCol[i], pch=pointsPch[i])
                    }
                    
                    if (is.null(xNames)) {
                        xName_i = ""
                    } else {
                        xName_i = xNames[i]
                    }
                    
                    vioplot.julian(vecLst[[i]], names=xName_i, add=T, at=i,
                                   col=vioBkgCol, rectCol=vioRecCol, whiskerCol=vioWhiskerCol,
                                   colMed=vioMedCol, pchMed=vioMedPch, medStyle=vioMedStyle, cexMed=1.5, 
                                   drawRect=T, boxWidth=vioBoxWidth)
                    #vioplot(vecLst[[i]], add=T, at=i,
                    #        col=vioBkgCol, rectCol=vioRecCol,
                    #        colMed=vioMedCol, pchMed=vioMedPch,
                    #        drawRect=T)                    
                    
                } else {
                    points(x=i, y=unique(vecLst[[i]]), col=vioMedCol, pch=vioMedPch, cex=1.5)
                }
                text(x=i, y=vioYmax, labels=length(vecLst[[i]]), cex=countCex)
            } else {
                text(x=i, y=vioYmax, labels="0", cex=countCex)
            }
        } else {
            # boxplot
            # do not attemp,t to plot if NULL or if not NULL but of length 0
            if (!is.null(vecLst[[i]]) && length(vecLst[[i]])>0) {

                if (is.null(xNames)) {
                    xName_i = ""
                } else {
                    xName_i = xNames[i]
                }
                
                # do not plot outliers if showPoints is T
                # otherwise will end up double-plotting
                boxplot(vecLst[[i]], add=T, at=i, col=vioBkgCol, yaxt="n",
                        names=xName_i, outline=!showPoints)  
                
                # if adding raw points
                # do this AFTER drawing vioplot
                # in general, doesn't turn out well if there're too many points even if drawing 
                # afterwards
                if (showPoints) {
                    if (pointsJitterBool) {
                        pts_x = jitter(x=rep(i, length=length(vecLst[[i]])), amount=pointsJitterAmount)
                    } else {
                        pts_x = rep(i, length=length(vecLst[[i]]))
                    }
                    points(x=pts_x, y=vecLst[[i]], cex=pointsCex, col=pointsCol[i], pch=pointsPch[i])
                }
                
                text(x=i, y=vioYmax, labels=length(vecLst[[i]]), cex=countCex)
            } else {
                text(x=i, y=vioYmax, labels="0", cex=countCex)
            }
        }

    }
    
    # threshold
    if (!is.null(yHoriz)) {
        abline(h=yHoriz, col=2, lty=2, lwd=1.5)
    }
}
