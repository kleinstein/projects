#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 14-
#SBATCH --constraint E5-2660_v3
#SBATCH -c 16
#SBATCH --mem-per-cpu=6G


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

# IN: 
# - presto/[subj]_all_atleast-2_reheader.fasta
# OUT: 
# - igblast/[subj].fmt7
# - igblast/[subj]_db-pass.tab
# - igblast/[subj]_IG-H/KL/HKL/NA.tab
# - igblast/[subj]_IG-H_QC1.tab
# - igblast/[subj]_IGHV_FUNCTIONAL-T/F.tab
# - igblast/[subj]_IGHV_FUNCTIONAL-T_QC2.tab

# Change-o v0.4.3+

PATH_ROOT="/home/qz93/scratch60/briney2019/"
PATH_PRESTO="${PATH_ROOT}presto/"
PATH_IGBLAST="${PATH_ROOT}igblast/"

PATH_SCRIPT="/home/qz93/projects/scripts/spatial/auxiliary/"
PATH_SCRIPT_SPLITCHAIN="${PATH_SCRIPT}split_chains.py"
PATH_SCRIPT_ADDTNLQC="${PATH_SCRIPT}additional_qc_briney.py"

PATH_LOG="${PATH_IGBLAST}igblast_allSubjs_$(date '+%m%d%Y_%H%M%S').log"
PATH_COUNT="${PATH_IGBLAST}igblast_bySubj_count_$(date '+%m%d%Y_%H%M%S').txt"

GERM_TAG="tag_201918-4_9May2019"
FN_IMGTVDJ="/home/qz93/germlines/${GERM_TAG}/human/vdj/imgt_human_IG*.fasta"

DIR_IGDATA="/home/qz93/apps/ncbi-igblast-1.14.0_tag-201918-4-9May2019"
PATH_EXEC="/home/qz93/apps/ncbi-igblast-1.14.0_tag-201918-4-9May2019/bin/igblastn"

#*
FILENAME_SUFFIX_PRESTO="_all_atleast-2.fasta"
#FILENAME_SUFFIX_PRESTO="_all_atleast-2_first1000.fasta" # testing

#*
#SUBJ_ARRAY=(326651 326713 316188 326650 326737 326780 326797 326907 327059 D103)
#SUBJ_ARRAY=(326651 326713) # JSON only
#SUBJ_ARRAY=(316188 326650 326737 326780 326797 326907 327059 D103) # CSV only
#SUBJ_ARRAY=(316188) # testing
#SUBJ_ARRAY=(316188 326650 326737 326780) # CSV only, p1
#SUBJ_ARRAY=(326797 326907 327059 D103)   # CSV only, p2

#*
# TRUE/FALSE
RUN_IGBLAST=TRUE
RUN_MAKEDB=FALSE
RUN_SPLITCHAIN=FALSE
RUN_ADDTNLQC_1=FALSE
RUN_SPLITNPR=FALSE
RUN_ADDTNLQC_2=FALSE

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "germline" $GERM_TAG &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
"${PATH_EXEC}" -version &>> "${PATH_LOG}"
MakeDb.py --version &>> "${PATH_LOG}"
python3 -V &>> "${PATH_LOG}"

# every subject has its own folder

for SUBJ in ${SUBJ_ARRAY[@]}; do

	echo "*********** STARTING ON ${SUBJ} ***********" &>> "${PATH_LOG}" # main log

	DIR_PRESTO="${PATH_PRESTO}${SUBJ}/"
	DIR_IGBLAST="${PATH_IGBLAST}${SUBJ}/"

	# if DIR_IGBLAST does not exist, mkdir
	if [ ! -d "${DIR_IGBLAST}" ]; then
		mkdir "${DIR_IGBLAST}"
	fi

	# path to presto input fasta (not fastq)
	PATH_PRESTO_SAMPLE="${DIR_PRESTO}${SUBJ}${FILENAME_SUFFIX_PRESTO}"

	# start sample-specific log file
	PATH_LOG_SAMPLE="${DIR_IGBLAST}igblast_subject-${SUBJ}_$(date '+%m%d%Y_%H%M%S').log"
	dt=$(date '+%d/%m/%Y %H:%M:%S')
	echo "$dt" &> "${PATH_LOG_SAMPLE}"

	# initiate count file
	if [[ (-s ${PATH_COUNT}) ]]; then
		echo -e "---\t---" >> ${PATH_COUNT}
	else
		echo -e "SUBJECT\tCOUNT" > ${PATH_COUNT}
	fi


	#######################
	##### run igblast #####
	#######################

	if [[ ${RUN_IGBLAST} == "TRUE" ]]; then

		if [[ (-s "${PATH_PRESTO_SAMPLE}" ) ]]; then

			# count reads in presto-abseq fasta
			COUNT_PRESTO=`grep -c '^>' ${PATH_PRESTO_SAMPLE}`
			echo -e "${SUBJ}_presto\t${COUNT_PRESTO}" >> ${PATH_COUNT}

			echo "##### run IgBLAST #####" &>> "${PATH_LOG_SAMPLE}"

			# output: [outname]_igblast.fmt7

			# vdb, ddb, jdb: name of the custom V/D/J reference in IgBLAST database folder
			# if unspecified, default to imgt_<organism>_<loci>_v/d/j
			AssignGenes.py igblast \
				-s "${PATH_PRESTO_SAMPLE}" \
				-b "${DIR_IGDATA}" \
				--exec "${PATH_EXEC}" \
				--organism "human" \
				--loci "ig" \
				--vdb "imgt_human_ig_v" \
				--ddb "imgt_human_ig_d" \
				--jdb "imgt_human_ig_j" \
				--format "blast" \
				--outname "${SUBJ}" \
				--outdir "${DIR_IGBLAST}" \
				--nproc "${NPROC}" \
				&>> "${PATH_LOG_SAMPLE}"

		else
			echo "${PATH_PRESTO_SAMPLE} does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi

	#######################################
	##### process output from IgBLAST #####
	#######################################

	if [[ ${RUN_MAKEDB} == "TRUE" ]]; then

		PATH_IGBLAST_SAMPLE="${DIR_IGBLAST}${SUBJ}_igblast.fmt7"

		if [[ (-s "${PATH_IGBLAST_SAMPLE}" ) ]]; then
			
			echo "##### process output from IgBLAST #####" &>> "${PATH_LOG_SAMPLE}"

			# default: partial=False; asis-id=False
			# --partial: if specified, include incomplete V(D)J alignments in the pass file instead of the fail file

			# IMPORTANT: do not put "" around FN_IMGTVDJ (otherwise * will be interpreted as is)
			# output: [outname]_db-pass.tab
			MakeDb.py igblast \
				-i "${PATH_IGBLAST_SAMPLE}" \
				-s "${PATH_PRESTO_SAMPLE}" \
				-r ${FN_IMGTVDJ} \
				--regions \
				--cdr3 \
				--scores \
				--failed \
				--partial \
				--format changeo \
				--outname "${SUBJ}" \
				--outdir "${DIR_IGBLAST}" \
				&>> "${PATH_LOG_SAMPLE}"

			# count
			COUNT_LINES=$(wc -l ${DIR_IGBLAST}${SUBJ}_db-pass.tab | awk '{print $1}')
			COUNT_SEQS=$((COUNT_LINES - 1))
			echo -e "${SUBJ}_MakeDb\t${COUNT_SEQS}" >> ${PATH_COUNT}

		else
			echo "${PATH_IGBLAST_SAMPLE} does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi

	############################################
	##### split based on chains from V_CALL ####
	############################################

	if [[ ${RUN_SPLITCHAIN} == "TRUE" ]]; then
		
		if [[ (-s "${DIR_IGBLAST}${SUBJ}_db-pass.tab" ) ]]; then

			echo "##### split seqs based on chains from V_CALL & J_CALL #####" &>> "${PATH_LOG_SAMPLE}"

			# outputs:
			# [SUBJ]_IG-H.tab (if any)
			# [SUBJ]_IG-KL.tab (if any)
			# [SUBJ]_IG-HKL.tab (if any)
			# [SUBJ]_IG-NA.tab (if any)
			python3 "${PATH_SCRIPT_SPLITCHAIN}" \
				"${DIR_IGBLAST}${SUBJ}_db-pass.tab" \
				"${DIR_IGBLAST}${SUBJ}" \
				&>> "${PATH_LOG_SAMPLE}"

			# count
			# these numbers should add up to MakeDb-pass exactly
			if [[ (-s ${DIR_IGBLAST}${SUBJ}_IG-H.tab ) ]]; then
				COUNT_LINES=$(wc -l ${DIR_IGBLAST}${SUBJ}_IG-H.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_IG-H\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_IG-H\t0" >> ${PATH_COUNT}
			fi

			if [[ (-s ${DIR_IGBLAST}${SUBJ}_IG-KL.tab ) ]]; then
				COUNT_LINES=$(wc -l ${DIR_IGBLAST}${SUBJ}_IG-KL.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_IG-KL\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_IG-KL\t0" >> ${PATH_COUNT}
			fi

			if [[ (-s ${DIR_IGBLAST}${SUBJ}_IG-HKL.tab ) ]]; then
				COUNT_LINES=$(wc -l ${DIR_IGBLAST}${SUBJ}_IG-HKL.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_IG-HKL\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_IG-HKL\t0" >> ${PATH_COUNT}
			fi

			if [[ (-s ${DIR_IGBLAST}${SUBJ}_IG-NA.tab ) ]]; then
				COUNT_LINES=$(wc -l ${DIR_IGBLAST}${SUBJ}_IG-NA.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_IG-NA\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_IG-NA\t0" >> ${PATH_COUNT}
			fi

		else
			echo "${SUBJ}_db-pass.tab does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi	

	###########################
	##### additional QC 1 #####
	###########################

	if [[ ${RUN_ADDTNLQC_1} == "TRUE" ]]; then

		# only reads igblasted to heavy chain V & J genes are used

		if [[ (-s "${DIR_IGBLAST}${SUBJ}_IG-H.tab" ) ]]; then

			echo "##### perform additional QC 1 #####" &>> "${PATH_LOG_SAMPLE}"

			# check for 
			# - V length must >=312
			# - % of N's in V up to & including V_GERM_LENGTH_IMGT must < 10%
			# - num of IMGT gaps in V up to and including 
			#   min(312, V_GERM_LENGTH_IMGT) must <= 35 (accommodates entire FWR missing)
			# - no "None" value in GERMLINE_IMGT
			# - no missing value in columns IC, GERMLINE_IMGT, JUNCTION
			# - NOT DONE HERE (to be done later): JUNCTION length multiple of 3
			python3 "${PATH_SCRIPT_ADDTNLQC}" \
				"${DIR_IGBLAST}${SUBJ}_IG-H.tab" \
				"${DIR_IGBLAST}${SUBJ}_IG-H_QC1.tab" \
				notRun run run run run run notRun \
				&>> "${PATH_LOG_SAMPLE}"

			# count
			if [[ (-s ${DIR_IGBLAST}${SUBJ}_IG-H_QC1.tab ) ]]; then
				COUNT_LINES=$(wc -l ${DIR_IGBLAST}${SUBJ}_IG-H_QC1.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_IG-H_QC1\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_IG-H_QC1\t0" >> ${PATH_COUNT}
			fi

		else
			echo "${SUBJ}_IG-H.tab does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi	

	####################################################
	##### split into productive vs. non-productive #####
	####################################################

	if [[ ${RUN_SPLITNPR} == "TRUE" ]]; then

		if [[ (-s "${DIR_IGBLAST}${SUBJ}_IG-H_QC1.tab" ) ]]; then
			
			echo "##### separate productively rearranged seqs from non-productively ones #####" &>> "${PATH_LOG_SAMPLE}"

			# terminology
			# igblast: functional/non-functional, equivalent to
			# IMGT: productively/non-productively rearranged

			# output: [outname]_FUNCTIONAL-T/F.tab

			ParseDb.py split \
		    	-d "${DIR_IGBLAST}${SUBJ}_IG-H_QC1.tab" \
		    	-f FUNCTIONAL \
		    	--outname "${SUBJ}_IGHV" \
		    	--outdir "${DIR_IGBLAST}" \
		    	&>> "${PATH_LOG_SAMPLE}"
			
		    # count
		    # IGHV productively rearranged
			if [[ -s "${DIR_IGBLAST}${SUBJ}_IGHV_FUNCTIONAL-T.tab" ]]; then
				COUNT_LINES=$(wc -l ${DIR_IGBLAST}${SUBJ}_IGHV_FUNCTIONAL-T.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_productive\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_productive\t0" >> ${PATH_COUNT}
			fi

			# IGHV non-productively rearranged
			if [[ -s "${DIR_IGBLAST}${SUBJ}_IGHV_FUNCTIONAL-F.tab" ]]; then
				COUNT_LINES=$(wc -l ${DIR_IGBLAST}${SUBJ}_IGHV_FUNCTIONAL-F.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_non-productive\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_non-productive\t0" >> ${PATH_COUNT}
			fi

		else
			echo "${SUBJ}_IG-H_QC1.tab does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi

	###########################
	##### additional QC 2 #####
	###########################

	if [[ ${RUN_ADDTNLQC_2} == "TRUE" ]]; then

		# only productively rearranged heavy chain reads that have passed QC1

		if [[ (-s "${DIR_IGBLAST}${SUBJ}_IGHV_FUNCTIONAL-T.tab" ) ]]; then

			echo "##### perform additional QC 2 #####" &>> "${PATH_LOG_SAMPLE}"

			# check for 
			# - JUNCTION length multiple of 3
			python3 "${PATH_SCRIPT_ADDTNLQC}" \
				"${DIR_IGBLAST}${SUBJ}_IGHV_FUNCTIONAL-T.tab" \
				"${DIR_IGBLAST}${SUBJ}_IGHV_FUNCTIONAL-T_QC2.tab" \
				notRun notRun notRun notRun notRun notRun run \
				&>> "${PATH_LOG_SAMPLE}"

			# count
			if [[ (-s ${DIR_IGBLAST}${SUBJ}_IGHV_FUNCTIONAL-T_QC2.tab ) ]]; then
				COUNT_LINES=$(wc -l ${DIR_IGBLAST}${SUBJ}_IGHV_FUNCTIONAL-T_QC2.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_productive_QC2\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_productive_QC2\t0" >> ${PATH_COUNT}
			fi

		else
			echo "${SUBJ}_IGHV_FUNCTIONAL-T.tab does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi	

	echo "*********** FINISHED FOR ${SUBJ} ***********" &>> "${PATH_LOG}" # main log

done

