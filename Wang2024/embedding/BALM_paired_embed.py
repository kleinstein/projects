#--------------------------------------------
# Generate BALM_paired embedding
# 
# BALM_paired is available on zenodo:
# https://zenodo.org/record/8253367
#
# Please download the model weights to model/ folder
# 
# Example usage: 
#
# python BALM_paired_embed.py S_FULL.parquet HL S_FULL_BALM_paired.pt
#--------------------------------------------

import argparse
from datetime import date
import os
import warnings
warnings.simplefilter('ignore')
import torch
from transformers import (
    RobertaConfig,
    RobertaTokenizer,
    RobertaForMaskedLM
)
from Bio import SeqIO
import time
import math 
import pandas as pd
#--------------------------------------------
parser = argparse.ArgumentParser(description="Generate BALM_paired embedding.")
parser.add_argument("parquet_file", type=str, help="Path to the parquet file")
parser.add_argument("sequence", type=str, help="Column name of sequence to embed (e.g. H, HL)")
parser.add_argument("output_file", type=str, help="Output file path")
args = parser.parse_args()
#--------------------------------------------
def batch_loader(data, batch_size):
    num_samples = len(data)
    for i in range(0, num_samples, batch_size):
        end_idx = min(i + batch_size, num_samples)
        yield i, end_idx, data[i:end_idx]

##--------------------------------------------
## Read the sequences
dat = pd.read_parquet(args.parquet_file)
sequences = dat.loc[:,args.sequence].values

##--------------------------------------------
## Set up the model 
BASEDIR = '../models/'
MODEL_DIR = BASEDIR + 'BALM-paired_LC-coherence_90-5-5-split_122222'
tokenizer = RobertaTokenizer.from_pretrained(MODEL_DIR)
model = RobertaForMaskedLM.from_pretrained(MODEL_DIR)
model = model.to('cuda')

model_size = sum(p.numel() for p in model.parameters())
print(f"Model size: {model_size/1e6:.2f}M")

##--------------------------------------------
## Embedding
start_time = time.time()
batch_size = 64 # 128
n_seqs = len(sequences)
dim = 1024
n_batches = math.ceil(n_seqs / batch_size)
embeddings = torch.empty((n_seqs, dim))
max_length = 512 - 2

# Use the batch_loader function to iterate through batches
i = 1
for start, end, batch in batch_loader(sequences, batch_size):
    print(f'Batch {i}/{n_batches}\n')
    x = torch.tensor([
    tokenizer.encode(seq[:max_length], 
                     padding="max_length",
                     truncation=True,
                     max_length=max_length,
                     return_special_tokens_mask=True) for seq in batch]).to('cuda')
    attention_mask = (x != tokenizer.pad_token_id).float().to('cuda')
    with torch.no_grad():
        outputs = model(x, attention_mask = attention_mask,
                       output_hidden_states = True)
        outputs = outputs.hidden_states[-1]
        outputs = list(outputs.detach())
    
    # aggregate across the residuals, ignore the padded bases
    for j, a in enumerate(attention_mask):
        outputs[j] = outputs[j][a == 1,:].mean(0)
        
    embeddings[start:end] = torch.stack(outputs)
    del x
    del attention_mask
    del outputs
    i += 1
    
end_time = time.time()
print(end_time - start_time)

torch.save(embeddings, args.output_file)