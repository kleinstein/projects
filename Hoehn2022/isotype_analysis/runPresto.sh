#!/usr/bin/bash
#SBATCH --ntasks=1 --nodes=1
#SBATCH --partition=pi_kleinstein
#SBATCH --time=48:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com
#SBATCH --exclude c22n04

id=$1
nproc=$2

bash presto-assemble.sh -1 ../raw/$id.fastq \
	-j ../info/boyd_rprimers.fasta \
	-v ../info/boyd_fprimers.fasta \
	-c ../info/boyd_rprimers_internalC_rc.fasta \
	-o ../presto/$id -p $nproc

AssignGenes.py igblast -s ../presto/$id/$id\_reheader.fasta \
	-b ~/share/igblast --organism human --loci ig \
	--format blast --nproc $nproc

MakeDb.py igblast -i ../presto/$id/$id\_reheader_igblast.fmt7 \
	-s ../presto/$id/$id\_reheader.fasta \
	-r ~/share/germlines/imgt/human/vdj/imgt_human_IGHV.fasta \
	   ~/share/germlines/imgt/human/vdj/imgt_human_IGHD.fasta \
	   ~/share/germlines/imgt/human/vdj/imgt_human_IGHJ.fasta \
	--outname $id --regions --scores --outdir ../changeo/db-pass
