### SHMulation simulation analysis
# Kenneth Hoehn
# 11/1/2019
# Do not run this as a shell script! Execute each command individually
# and wait for it to complete. This is just an explanation of the pipeline

## Generate SHMulated data and run IgPhyML
# Runs SHMulate_dist.R, which depends on updated_functions/Lineage.R, a custom
# version of Alakazam that treats branch lengths differently from the default 
# setting, and implements shmulateTreeG, which SHMulates down a tree starting 
# from the germline sequnce. Naive germline sequences are taken from 
# Heavy_NaiveMemory_SORT-Naive_SUBJECT-HD10.tsv. Also runs runq.sh which
# uses BuildTrees.py and maskCDR3.pl to convert data to igphyml input, and does
# igphyml analysis
sbatch runSHMulations.sh

# Summarize results
# generates SHMulateResults.tsv
perl parseStats.pl