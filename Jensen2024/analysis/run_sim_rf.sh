#!/bin/bash
#SBATCH --partition week

sbatch get_full_rf_results.sh "pratchet" 32
sbatch get_full_rf_results.sh "pml" 32
sbatch get_full_rf_results.sh "raxml" 32
sbatch get_full_rf_results.sh "raxml_p" 32
sbatch get_full_rf_results.sh "dnapars" 32
sbatch get_full_rf_results.sh "dnaml" 32
sbatch get_full_rf_results.sh "igphyml" 32
sbatch get_full_rf_results.sh "igphyml_p" 32
