
library(alakazam)
library(shazam)
library(dplyr)
library(parallel)
library(Matrix)
library(dowser)
library(tidyr)

options(error=function()traceback(2))

files = list.files("estimates",full=TRUE)

files = files[grepl("four",files)]

props = tibble()
pss = tibble()
pcounts = tibble()
for(file in files){
	print(file)
	fs = strsplit(file,split="_")[[1]]
	true = c(rate=fs[2],mode=fs[3],
		rep=fs[4],model=fs[5])

	rate = as.numeric(true["rate"])
	mode = true["mode"]
	rep = as.numeric(true["rep"])
	if(rep > 20){next}
	if(!true["mode"] %in% c("association","mix") &&
	 true["model"] == "rev"){next}

	t = readRDS(file)
	if(mode %in% c("association","mix")){
		prop = testSP(t$switches$switches,
			permuteAll=TRUE,pseudocount=0,alternative="greater")$means		
	}else{
		prop = testSP(t$switches$switches,
			permuteAll=TRUE,to="D",alternative="greater")$means
	}
	prop$rate = rate
	prop$mode = mode
	prop$rep = rep
	prop$model = true["model"]
	prop$type = "between"
	props = bind_rows(props,prop)

	if(mode %in% c("association","mix")){
		prop = testSP(t$switches$switches,
			permuteAll=FALSE,pseudocount=0,alternative="greater")$means
	}else{
		prop = testSP(t$switches$switches,
			permuteAll=FALSE,to="D",alternative="greater")$means
	}
	prop$rate = rate
	prop$mode = mode
	prop$rep = rep
	prop$model = true["model"]
	prop$type = "within"
	props = bind_rows(props,prop)

}

saveRDS(props,file="intermediates/props4_g.rds")
saveRDS(pcounts,file="intermediates/counts4_g.rds")
saveRDS(pss,file="intermediates/pss4_g.rds")

props = readRDS("intermediates/props4_g.rds")
pcounts = readRDS("intermediates/counts4_g.rds")
pss = readRDS("intermediates/pss4_g.rds")

#props = readRDS("intermediates/props4.rds")
props$mode = factor(props$mode,levels=c(
	"association","mix","direct","sequential",
	"progressive","unconstrained"))

fstate = props %>%
	filter(mode %in% c("association","mix")) %>%
	group_by(FROM,TO,mode,type,model) %>%
	summarize(sp = mean(PGT < 0.025),mdiff = mean(DELTA))

pdf("results/assoc_mixed.pdf",width=7,height=6)
ggplot(fstate,aes(y=FROM,x=TO,fill=sp,label=signif(sp,digits=2)))+
	geom_tile()+geom_text()+facet_grid(type~mode+model)+theme_bw()+
	scale_fill_distiller(palette="RdYlBu")
dev.off()

mixstats = props %>%
	filter(mode %in% c("mix")) %>%
	mutate(SWITCH = paste0(FROM,"->",TO)) %>%
	group_by(SWITCH,type) %>%
	summarize(nsig = sum(PGT < 0.05)) %>%
	spread(type,nsig)

write.csv(mixstats,file="results/mix_stats.csv",row.names=FALSE)

fprops = 
	props %>%
	mutate(SWITCH = paste0(FROM,"->",TO)) %>%
	filter(SWITCH %in% c("A->B","B->A","C->D","D->C"))

g = ggplot(filter(fprops,mode=="mix"),aes(x=SWITCH,y=PGT))+geom_boxplot(outlier.size=0.5)+
facet_wrap(~type,labeller=label_both)+theme_bw()+
	scale_color_brewer(palette="Dark2")+geom_hline(yintercept=0.05)+
	theme(text = element_text(size=8))
ggsave(g,file="results/mixed_fourstate.pdf",useDingbats=FALSE,width=2.5,height=1.5)

cprops = 
	props %>%
	filter(!mode %in% c("association","mix"))

g = ggplot(filter(cprops,type=="between"),aes(x=FROM,y=PGT))+geom_boxplot(outlier.size=0.5)+
facet_wrap(~mode,labeller=label_both)+theme_bw()+
	scale_color_brewer(palette="Dark2")+geom_hline(yintercept=0.05)+
	theme(text = element_text(size=8))
ggsave(g,file="results/constrained_fourstate.pdf",useDingbats=FALSE,width=2.5,height=2.6)	

g = ggplot(filter(cprops,type=="between"),aes(x=FROM,y=PGT))+geom_boxplot(outlier.size=0.5)+
facet_wrap(~mode,labeller=label_both)+theme_bw()+
	scale_color_brewer(palette="Dark2")+geom_hline(yintercept=0.05)
ggsave(g,file="results/constrained_fourstate.pdf",useDingbats=FALSE,width=6,height=3.5)

cstate = props %>%
	filter(!mode %in% c("association","mix")) %>%
	group_by(FROM,mode,model,type) %>%
	summarize(sp = mean(PGT < 0.025),
		n = length(unique(rep)))

pdf("results/constrained.pdf",width=7,height=6)
ggplot(cstate,aes(x=FROM,y=mode,fill=sp,label=signif(sp,digits=2)))+
	geom_tile()+geom_text()+facet_grid(model~type)+theme_bw()+
	scale_fill_distiller(palette="RdYlBu")	
dev.off()


# 20/20 simulations showed correct pattern from C to D, none from D to C
filter(props, type=="within", FROM=="C", TO=="D", mode=="mix")
filter(props, type=="within", FROM=="D", TO=="C", mode=="mix")

# 20/20 as expected from direct vs sequential switching
filter(cprops, type=="between", FROM=="A", TO=="D", mode=="direct")
filter(cprops, type=="between", FROM=="B", TO=="D", mode=="sequential")

# 20/20 from B and C to D, but not A
filter(cprops, type=="between", TO=="D", mode=="progressive") %>%
group_by(FROM) %>% summarize(sum(PGT < 0.05), n())
# similar
filter(cprops, type=="between", TO=="D", mode=="unconstrained") %>%
group_by(FROM) %>% summarize(sum(PGT < 0.05), n())

