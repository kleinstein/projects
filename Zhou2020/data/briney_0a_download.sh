#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 1-
#SBATCH -c 1
#SBATCH --mem-per-cpu=4G

PATH_SAVE="/home/qz93/scratch60/briney2019/consensus_csv/"
PATH_LOG="${PATH_SAVE}download.log"
cd "${PATH_SAVE}"

# 326651
LINK_1="http://burtonlab.s3.amazonaws.com/sequencing-data/hiseq_2016-supplement/326651_HC5LVBCXY_consensus_UID18-cdr3nt-90_jsons_071817.tar.gz"

# 326713
LINK_2="http://burtonlab.s3.amazonaws.com/sequencing-data/hiseq_2016-supplement/326713_HJLLNBCXY_consensus_UID18-cdr3nt-90_jsons_071817.tar.gz"

# 316188
LINK_3="http://burtonlab.s3.amazonaws.com/sequencing-data/hiseq_2016-supplement/316188_HNCHNBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz"

# 326650
LINK_4="http://burtonlab.s3.amazonaws.com/sequencing-data/hiseq_2016-supplement/326650_HCGCYBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz"

# 326737
LINK_5="http://burtonlab.s3.amazonaws.com/sequencing-data/hiseq_2016-supplement/326737_HNKVKBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz"

# 326780
LINK_6="http://burtonlab.s3.amazonaws.com/sequencing-data/hiseq_2016-supplement/326780_HLH7KBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz"

# 326797
LINK_7="http://burtonlab.s3.amazonaws.com/sequencing-data/hiseq_2016-supplement/326797_HCGNLBCXY%2BHJLN5BCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz"

# 326907
LINK_8="http://burtonlab.s3.amazonaws.com/sequencing-data/hiseq_2016-supplement/326907_HLT33BCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz"

# 327059
LINK_9="http://burtonlab.s3.amazonaws.com/sequencing-data/hiseq_2016-supplement/327059_HCGTCBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz"

# D103
LINK_10="http://burtonlab.s3.amazonaws.com/sequencing-data/hiseq_2016-supplement/D103_HCGCLBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz"


# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"

echo "start: LINK_1" &>> "${PATH_LOG}"
wget "${LINK_1}"
mv "326651_HC5LVBCXY_consensus_UID18-cdr3nt-90_jsons_071817.tar.gz" "326651_jsons.tar.gz"

echo "start: LINK_2" &>> "${PATH_LOG}"
wget "${LINK_2}"
mv "326713_HJLLNBCXY_consensus_UID18-cdr3nt-90_jsons_071817.tar.gz" "326713_jsons.tar.gz"

echo "start: LINK_3" &>> "${PATH_LOG}"
wget "${LINK_3}"
mv "316188_HNCHNBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz" "316188_csv.tar.gz"

echo "start: LINK_4" &>> "${PATH_LOG}"
wget "${LINK_4}"
mv "326650_HCGCYBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz" "326650_csv.tar.gz"

echo "start: LINK_5" &>> "${PATH_LOG}"
wget "${LINK_5}"
mv "326737_HNKVKBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz" "326737_csv.tar.gz"

echo "start: LINK_6" &>> "${PATH_LOG}"
wget "${LINK_6}"
mv "326780_HLH7KBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz" "326780_csv.tar.gz"

echo "start: LINK_7" &>> "${PATH_LOG}"
wget "${LINK_7}"
# "%2B" in link is actually "+"
mv "326797_HCGNLBCXY+HJLN5BCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz" "326797_csv.tar.gz"

echo "start: LINK_8" &>> "${PATH_LOG}"
wget "${LINK_8}"
mv "326907_HLT33BCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz" "326907_csv.tar.gz"

echo "start: LINK_9" &>> "${PATH_LOG}"
wget "${LINK_9}"
mv "327059_HCGTCBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz" "327059_csv.tar.gz"

echo "start: LINK_10" &>> "${PATH_LOG}"
wget "${LINK_10}"
mv "D103_HCGCLBCXY_consensus_UID18-cdr3nt-90_minimal_071817.tar.gz" "D103_csv.tar.gz"

echo "DONE!" &>> "${PATH_LOG}"
