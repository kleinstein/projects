#!/usr/bin/env python3
"""
Perform QC steps after IgBLAST and MakeDb.py
"""
import re, sys, os
from os import path
from sys import argv
import pandas as pd

# command line input: 
# inputTab, outname, 
# vIdentity, vLen, vN, vGap, noneCheck, nanCheck, mod3Check

# outputs: 
# - outname (a .tab or .tsv, depending on file extension specified in `outname`)

### parameters

THRESH_V_IDENTITY = 0.9 # must >=; filtered if <
THRESH_V_LEN = 312      # must >=; filtered if <
THRESH_V_N = 10         # must <;  filtered if >= (in %; 10 means 10%)

# based on germs/tag_2018-03-05/germlineV_gaps.pdf
# Btw IMGT 1-312, there are maximum 1+4+3+1=9 IMGT gaps
THRESH_V_GAP = 10       # must <;  filtered if >=

COL_V_IDENTITY = "V_IDENTITY"
COL_SEQ = "SEQUENCE_IMGT"
COL_V_LEN = "V_GERM_LENGTH_IMGT"

COL_NONE_CHECK = ["GERMLINE_IMGT"]

# ok if one or more of these columns are absent from .tab
# code will check existence of columns and skip if absent
COL_NAN_CHECK = ["PRCONS","CREGION","VPRIMER","CPRIMER","INTERNAL_C","GERMLINE_IMGT", "JUNCTION"]

COL_MOD3_CHECK = ["JUNCTION"]

COL_V_CALL = "V_CALL"
COL_ISOTYPE = "PRCONS"

### count the number of N's in seqFull up to and including position indicated by seqVlen
# return as percentage
def countNinV(seqFull, seqVlen):

    seqV = seqFull[0:seqVlen]
    #seqNcount = seqV.count("N")
    seqNcount = len(re.findall("[Nn]", seqV))

    seqNperc = seqNcount/seqVlen*100

    return seqNperc

### count the number of N's in seqFull up to and including position 
#   indicated by seqVlen or 312, whichever is smaller
def countIMGTgapInV(seqFull, seqVlen):

    minLen = min(seqVlen, 312)
    seq312 = seqFull[0:minLen]
    # number of IMGT gaps
    seq312GapCount = len(re.findall("\.\.\.", seq312))

    return seq312GapCount

### compare to a threshold
# with dtype=object, int and float64 are represented as str
def compareInt(strVal, thresh):
    return int(strVal)>=thresh

def compareFloat(strVal, thresh):
    return float(strVal)>=thresh

### if str is "None"
def isNone(value):
    
    if not pd.isnull(value):
        # check value has type str
        if not isinstance(value, str):
            sys.exit("Value being checked for `None` is not a str. Exited.")

        return value=="None"
    else:
        return False

### if length of string is multiple of 3
def isMod3(inputStr):

    # inputStr should be a string
    if not isinstance(inputStr, str):
        sys.exit("inputStr being checked for `isMod3` is not a str. Exited.")

    return (len(inputStr) % 3)==0

### detect IGHV in v_call 
def detectIGHV(v_call):
    match = re.findall("[iI][gG][hH][vV]", v_call)
    return(len(match)>0)

### detect IG[KL] in isotype
def detectIGKL(isotype):
    match = re.findall("[iI][gG][kKlL]", isotype)
    return(len(match)>0)

### given an inputTab .tab/tsv file, output a new .tab file called outname,
# (`outname` should contain the appropriate file extension, like .tab or .tsv)
### after performing various QC steps:

# - vIdentity: if `run`, filter based on % of V call identity
# - vLen: if `run`, filter based on length of V region
# - vN: if `run`, filter based on the number of N's in V region
# - vGap: if 'run', filter based on the number of IMGT gaps up to and including
#         COL_V_LEN or 312, whichever is smaller
# - noneCheck: if 'run', filter sequences whose specified column is `None`
# - nanCheck: if `run`, filter if a row has missing value (NaN) in specified columns
# - mod3Check: if `run`, filter if a row has JUNCTION length not a multiple of 3

def performQC(inputTab, outname, vIdentity, vLen, vN, vGap, noneCheck, nanCheck, mod3Check, heavyChainIsoCheck):

    df = pd.read_table(inputTab, dtype=object)

    # if statements:
    # `if vIdentity:` (with the expectation that vIdentity being supplied as True or False)
    # doesn't work on Farnam (Python 3.5); though weirdly it works on Jupyter with Python 3.6
    # use this instead `if vIdentity=="run"` (vIdentity interpreted as string from sys.argv[])

    ### V identity
    # retain seqs with v identity >= 0.9
    if vIdentity=="run":
        # true if >= 
        bool_V_identity = df[COL_V_IDENTITY].apply(compareFloat, args=(THRESH_V_IDENTITY,))
        #bool_V_identity = df[COL_V_IDENTITY] >= THRESH_V_IDENTITY
        print("- # seqs filtered due to V identity: {0}; # seqs remaining: {1}".format(sum(~bool_V_identity), sum(bool_V_identity)))
        print("  column used: " + COL_V_IDENTITY + "; threshold (<): " + str(THRESH_V_IDENTITY))
        df = df[bool_V_identity]
        df.reset_index(inplace=True, drop=True)

    ### V length
    # retain seqs with length >= threshold
    if vLen=="run":
        # true if >=
        bool_V_len = df[COL_V_LEN].apply(compareInt, args=(THRESH_V_LEN,))
        #bool_V_len = df[COL_V_LEN] >= THRESH_V_LEN
        print("- # seqs filtered due to V length: {0}; # seqs remaining: {1}".format(sum(~bool_V_len), sum(bool_V_len)))
        print("  column used: " + COL_V_LEN + "; threshold (<): " + str(THRESH_V_LEN))
        df = df[bool_V_len]
        df.reset_index(inplace=True, drop=True)

    ### % of N's in V
    # retain seqs with < threshold % of N's in V
    if vN=="run":
        Nperc = pd.Series([countNinV(df[COL_SEQ][i], int(df[COL_V_LEN][i])) for i in range(df.shape[0])])
        bool_V_N = Nperc < THRESH_V_N
        print("- # seqs filtered due to % N in V: {0}; # seqs remaining: {1}".format(sum(~bool_V_N), sum(bool_V_N)))
        print("  column used: " + COL_SEQ + "; threshold (>=): " + str(THRESH_V_N) + "%")
        df = df[bool_V_N]
        df.reset_index(inplace=True, drop=True)

    ### number of IMGT gaps in V up to and including the smaller of COL_V_LEN and 312
    if vGap=="run":
        numGap = pd.Series([countIMGTgapInV(df[COL_SEQ][i], int(df[COL_V_LEN][i])) for i in range(df.shape[0])])
        bool_V_gap = numGap < THRESH_V_GAP
        print("- # seqs filtered due to # IMGT gaps in V up to IMGT 312: {0}; # seqs remaining: {1}".format(sum(~bool_V_gap), sum(bool_V_gap)))
        print("  column used: " + COL_SEQ + "; threshold (>=): " + str(THRESH_V_GAP))
        df = df[bool_V_gap]
        df.reset_index(inplace=True, drop=True)

    ### check whether column in COL_NONE_CHECK has value "None"
    if noneCheck=="run":
        for curCol in COL_NONE_CHECK:
            # check if column is present; if not, skip to next
            if curCol in df.columns:
                bool_none = df[curCol].apply(isNone)
                print("- # seqs filtered due to 'None' in {0}: {1}; # seqs remaining: {2}".format(curCol, sum(bool_none), sum(~bool_none)))
                df = df[~bool_none]
                df.reset_index(inplace=True, drop=True)
            else:
                print(curCol + "not found as a column - skipped")

    ### check NaN in columns
    if nanCheck=="run":
        for curCol in COL_NAN_CHECK:
            # check if column is present; if not, skip to next
            if curCol in df.columns:
                bool_nan = pd.isnull(df[curCol])
                print("- # seqs filtered due to NaN in {0}: {1}; # seqs remaining: {2}".format(curCol, sum(bool_nan), sum(~bool_nan)))
                df = df[~bool_nan]
                df.reset_index(inplace=True, drop=True)
            else:
                print(curCol + "not found as a column - skipped")

    ### check columns like JUNCTION is of length multiple of 3
    # important: isMod3 will fail if inputStr is NaN
    # call isMod3 after doing nanCheck on columns to be checked
    if mod3Check=="run":
        for curCol in COL_MOD3_CHECK:
            # check if column is present; if not, skip to next
            if curCol in df.columns:
                bool_mod3 = df[curCol].apply(isMod3)
                print("- # seqs filtered due to length of {0} not being a multiple of 3: {1}; # seqs remaining: {2}".format(curCol, sum(~bool_mod3), sum(bool_mod3)))
                df = df[bool_mod3]
                df.reset_index(inplace=True, drop=True)
            else:
                print(curCol + "not found as a column - skipped")

    ### check for seqs with IGHV as V_CALL but IgK/L as isotype
    # mainly intended for heavy chain seqs

    # $PRCONS:
    # Ig[ADEGM]
    # Human-IG[KL], Human-IGH[ADEGM]

    # $CREGION:
    # Human-IGKC-IGKJ-InternalC, Human-IGLC-[1234]-InternalC, Human-IGH[ADEGM]-InternalC

    if heavyChainIsoCheck=="run":
        if (COL_V_CALL in df.columns) and (COL_ISOTYPE in df.columns):
            # find rows to filter
            bool_v_call = df[COL_V_CALL].apply(detectIGHV)
            bool_isotype = df[COL_ISOTYPE].apply(detectIGKL)
            bool_vcall_iso = bool_v_call & bool_isotype

            print("# seqs filtered due to having IGHV as {0} and IGK/L as {1}: {2}; # seqs remaining: {3}".format(COL_V_CALL, COL_ISOTYPE, sum(bool_vcall_iso), sum(~bool_vcall_iso)))
            df = df[~bool_vcall_iso]
            df.reset_index(inplace=True, drop=True)
        else:
            print(COL_V_CALL + "and/or" + COL_ISOTYPE + "not found as a column - skipped")

    ### export
    if df.shape[0] > 0:
        df.to_csv(outname, sep="\t", index=False)
    else:
        print("no seq passed QC; no output exported.")

### run
# just using sys.argv[1:] deosn't seem to work on Farnam
performQC(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6], sys.argv[7], sys.argv[8], sys.argv[9], sys.argv[10])
