


use strict;
use warnings;
use hotSpotUtils;

open(IN,$ARGV[0]) or die($ARGV[0]); #repertoire file
open(OUT,">$ARGV[1]") or die($ARGV[1]);
my $append = "";


my $h=<IN>;
my @lines=<IN>;
my $total=0;
my $max=0;
foreach my $l (@lines){
	chomp($l);
	my @in=split("\t",$l);
	my $seqs=getfasta($append.$in[0]);
	my $nseq=scalar(keys %$seqs);
	if($nseq<3){next;}
	$total += ($nseq-1);
	if($nseq>$max){$max=$nseq}
	print OUT "$l\t$nseq\n";
}
print "$total\n$max\n";

