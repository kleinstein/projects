#!/bin/bash
#SBATCH --partition=scavenge
#SBATCH --job-name=DE
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=32G
#SBATCH --time=02:00:00
#SBATCH --error=log/DE.%A_%a.err
#SBATCH --output=log/DE.%A_%a.out
#SBATCH --array=1-4

module load miniconda
conda activate r_4.0

taskID=$SLURM_ARRAY_TASK_ID

cell_types=("Naive B" "RMB" "ABC" "PB")
cell_type=${cell_types[$taskID-1]}
echo $cell_type

echo "[$0 $(date +%Y%m%d-%H%M%S)] [start] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"

Rscript 11_DE.R "$cell_type"

echo "[$0 $(date +%Y%m%d-%H%M%S)] [end] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"
