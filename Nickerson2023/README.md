# Scripts for performing MRL mouse analyses

Kenneth B. Hoehn
7/22/22
kenneth.hoehn@yale.edu

# Create initial MRL germline database, then find novel alleles
```bash
bash makeInitialMRL.sh

```

# Create combined MRL germline database
```bash
bash makeCombinedMRL.sh

```

# Run analyses on MRL experimental data
```bash
bash runAnalyses.sh

```
