#!/usr/bin/bash
#SBATCH --mem=100gb
#SBATCH --partition=pi_kleinstein
#SBATCH --cpus-per-task=6
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=100:00:00
#SBATCH --mail-type=FAIL,END
#SBATCH --mail-user=kenneth.hoehn@gmail.com

module load R/4.1.0-foss-2020b

Rscript filter_pca_umap.R
Rscript cloneGermline10X.R
Rscript analyze10X.R

