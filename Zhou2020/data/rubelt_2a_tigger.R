##### genotyping
pathRoot = "/ysm-gpfs/home/qz93/scratch60/spatial_data/davis_twins/"
pathData = paste0(pathRoot, "igblast/")
pathWork = paste0(pathRoot, "tigger/")

library(tigger) # v0.3.1
library(alakazam) # for writeChangeoDb

#*
RUN_NOVEL = F

setwd(pathWork)
sinkName = paste0("computingEnv_", Sys.Date(), "-", 
                  format(Sys.time(), "%H%M%S"), '.txt')
sink(sinkName)
cat("RUN_NOVEL:", RUN_NOVEL, "\n")
sessionInfo()
sink()

##### germIMGT_V_full
#load("/ysm-gpfs/home/qz93/germlines/tag_2018_03_05/imgt_human_IGHV_full.RData")
load("/ysm-gpfs/home/qz93/germlines/tag_201918-4_9May2019/imgt_human_IGHV_full.RData")

# germIMGT_V_full (tag_201918-4_9May2019) is more up-to-date than TIGGER built-in (201408-4)
cat("all tigger germline in germIMGT_V_full? \n")
bool1 = names(GermlineIGHV) %in% names(germIMGT_V_full)
print(all(bool1)) # expect TRUE; if not, investigate
names(GermlineIGHV)[!bool1] # IGHV3-43D*01

cat("all germIMGT_V_full in tigger germline? \n")
bool2 = names(germIMGT_V_full) %in% names(GermlineIGHV)
print(all(bool2)) # expect FALSE (unless Tigger constantly updates its GermlineIGHV)
names(germIMGT_V_full)[!bool2]

# Tigger's GermlineIGHV has IGHV3-43D*01
# IMGT's germIMGT_V_full has IGHV3-43D*03
# same seq? (TRUE as of Oct 27 2018)
GermlineIGHV[!bool1] == germIMGT_V_full["IGHV3-43D*03"]

rm(bool1, bool2)

##### load data
SUBJECTS = paste0(rep("TW", 10), rep(paste0("0", 1:5), each=2), rep(c("A","B"), 5)) 

for (subj in SUBJECTS) {
    setwd(pathData)

    filename_F = paste0("6_", subj, "_IGHV_FUNCTIONAL-F.tab")
    filename_T = paste0("6_", subj, "_IGHV_FUNCTIONAL-T_QC2.tab")
    
    ##### load data (IGHV, both functional and non-functional)
    cat("##################### subject: ", subj, "\n")

    db_F = read.table(filename_F, sep="\t", header=T, stringsAsFactors=F)
    db_T = read.table(filename_T, sep="\t", header=T, stringsAsFactors=F)
    
    db = rbind(db_F, db_T)
    rm(db_F, db_T)
    
    # sanity check
    # no IGKV nor IGLV in V_CALL
    stopifnot(!any(grepl(pattern="IG[KL]V", x=db$V_CALL)))

    cat("nrow (IGHV, both functinoal and non-functional) \n")
    print(nrow(db))

    cat("function vs. non-functional \n")
    print(table(db$FUNCTIONAL))
    
    cat("number of unique SEQUENCE_ID per subject \n")
    print(length(unique(db$SEQUENCE_ID)))
    
    cat("SORT \n")
    print(table(db$SUBSET))
    
    ##### remove JUNCTION_LENGTH==0 (TIGGER fails)
    cat("summary on distribution of junction lengths \n")
    print(summary(db$JUNCTION_LENGTH))
    # FUNCTIONAL-F could contribute JUNCTION_LENGTH of NA
    cat("how many junction lengths of 0? \n")
    numJL0 = sum(db$JUNCTION_LENGTH==0, na.rm=T)
    print(numJL0)
    cat("how many junction lengths of NA? \n")
    numJLna = sum(is.na(db$JUNCTION_LENGTH))
    print(numJLna)
    
    if (numJL0>0) {
        cat("which sequences are junction lengths of 0? \n")
        print(table(db[db$JUNCTION_LENGTH==0, c("FUNCTIONAL")]))
    }
    
    if (numJLna>0) {
        cat("which sequences are junction lengths of NA? \n")
        print(table(db[is.na(db$JUNCTION_LENGTH), c("FUNCTIONAL")]))
    }
    
    # subset
    db = db[(db$JUNCTION_LENGTH>0 & !is.na(db$JUNCTION_LENGTH)), ]
    
    cat("nrow, after removing JUNCTION_LENGTH of 0 and NA \n")
    print(nrow(db))
    
    ##### TIGGER parameters 
    # default values unless otherwise noted
    param.germline_min = 150 #* default is 200
    param.min_seqs = 50
    param.y_intercept = 0.125
    param.j_max = 0.15
    param.min_frac = 0.75
    param.nproc = 12 #*
    param.fraction_to_explain = 0.875
    param.gene_cutoff = 1e-04
    
    ##### run TIGGER
    setwd(pathWork)
    cat("running tigger... \n")
    
    ### detect novel alleles

    if (RUN_NOVEL) {

        cat("findNovelAlleles() \n")
        novelDf = findNovelAlleles(data=db, germline_db=germIMGT_V_full,
                                   v_call="V_CALL",
                                   germline_min = param.germline_min,
                                   min_seqs = param.min_seqs,
                                   y_intercept = param.y_intercept,
                                   j_max = param.j_max,
                                   min_frac = param.min_frac,
                                   nproc = param.nproc)

        # extract and view the rows that contain successful novel allele calls
        cat("selectNovel() \n")
        novel = selectNovel(novelDf, keep_alleles=F) # F is the default parameter
        
        # visualize evidence
        cat("plotNovel() \n")
        if (nrow(novel)>0) {
            pdf(paste0("novelEvidence_", subj, ".pdf"), width=5, height=9)
            for (i in 1:nrow(novel)) {
                a=plotNovel(data=db, novel_row=novel[i, ], ncol=1, v_call="V_CALL")
                print(a)
                rm(a)
            }
            dev.off()
        }

    } else {
        novelDf = NA # to be passed on to inferGenotype()
        novel = NULL
    }
    
    ### genotyping
    # Infer the individual's genotype, using only unmutated sequences and checking
    # for the use of the novel alleles inferred in the earlier step.
    cat("inferGenotype() \n")
    geno = inferGenotype(data=db, v_call="V_CALL",
                         fraction_to_explain=param.fraction_to_explain,
                         gene_cutoff=param.gene_cutoff,
                         find_unmutated=TRUE,
                         germline_db=germIMGT_V_full, novel=novelDf)
    # Save the genotype sequences to a vector
    cat("genotypeFasta() \n")
    genoSeqs = genotypeFasta(genotype=geno, 
                             germline_db=germIMGT_V_full, novel=novelDf)
    # export genotype sequences
    cat("writeFasta() \n")
    writeFasta(named_sequences=genoSeqs, 
               file=paste0("genotypeSeqs_", subj, ".fasta"),
               append=F)
    # genotype and sequence counts
    sink(paste0("genotypeCounts_", subj, ".txt"))
    print(geno)
    sink()
    # visualize - bars indicate presence, not proportion.
    cat("plotGenotype() \n")
    pdf(paste0("genotype_", subj, ".pdf"), width=6, height=6)
    plotGenotype(genotype=geno, gene_sort="name", text_size=12)
    dev.off()
    
    ### corrected allele calls
    # Use the personlized genotype to determine corrected allele assignments
    # currently only hamming and keep_gene=TRUE are implemented
    # returns a single-column data.frame
    cat("reassignAlleles() \n")
    # previously using personal bug-fix version
    # with v0.3.1, bug should have been fixed
    # with v0.3.1, returned object is the original df with corrected calls appended
    db = reassignAlleles(data=db, genotype_db=genoSeqs, v_call="V_CALL", 
                         method="hamming", keep_gene="gene")
    
    ### save & cleanup
    save(novelDf, novel, geno, genoSeqs, 
         file=paste0("tiggerObjects_", subj, ".RData"))
    rm(novelDf, novel, geno, genoSeqs)

    # IGHV functional
    tmp = db; rm(db)
    db = tmp[tmp$FUNCTIONAL, ]
    cat("nrow, IGHV, productively rearranged, QC2, genotyped: \n")
    print(nrow(db))

    writeChangeoDb(data=db, file=paste0(subj, "_IGHV_FUNCTIONAL-T_genotyped.tsv"))
    #save(db, file=paste0(subj, "_IGHV_FUNCTIONAL-T_genotyped.RData"))
    rm(db)
    
    # IGHV non-functional
    db = tmp[!tmp$FUNCTIONAL, ]
    cat("nrow, IGHV, non-productively rearranged, genotyped: \n")
    print(nrow(db))

    writeChangeoDb(data=db, file=paste0(subj, "_IGHV_FUNCTIONAL-F_genotyped.tsv"))
    #save(db, file=paste0(subj, "_IGHV_FUNCTIONAL-F_genotyped.RData"))
    rm(db)

    cat("FINISHED for", subj, "\n")
    
}
