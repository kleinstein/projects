from Bio import SeqIO
from Bio.Seq import Seq
import os
import sys
import argparse
import re
import random

parser = argparse.ArgumentParser(description='Find subsequence (primer) in Fasta sequence, split at the end of the subsequence and add UMI barcode.')
parser.add_argument('-p','--primer', type=str,
                    help='Subsequence (primer, possibly needs reversion) to find',
                    default = '')
parser.add_argument('-pt', '--primer_type', type=str,
                    help='Type of primer: forward or reverse.',
                    default='reverse')
parser.add_argument('-i', '--input', type=str,
                    help='Input fasta file')
parser.add_argument('-u', '--umi_length', type=int,
                    help='UMI length', default=8)
parser.add_argument('-o', '--output', type=str,
                    help='Output filename. Default equals input filename with suffix _withUMI.',
                    default='')
parser.add_argument('-s', '--seed', type=int,
                    help='Random seed for the UMI nucleotide selection.',
                    default=42)
args = parser.parse_args()

random.seed(args.seed)

input_fasta = args.input
if args.output == "":
    output_fasta = os.path.splitext(input_fasta)[0]+"_withUMI.fasta"
else:
    output_fasta = args.output
primer = args.primer
umi_length = args.umi_length
primer_type = args.primer_type

print("Adding UMIs to sequences...")

def add_umi_rev(seq, umi_length=umi_length):
    letters = ["A", "T", "C", "G"]
    umi_list = [ random.choice(letters) for i in range(0,umi_length) ]
    umi_str = ''.join(umi_list)
    seq_umi = seq + umi_str
    umi_revcomp = str(Seq(umi_str).reverse_complement())
    return (seq_umi, umi_revcomp)

def add_umi_for(seq,umi_length=umi_length):
    letters = ["A", "T", "C", "G"]
    umi_list = [ random.choice(letters) for i in range(0,umi_length) ]
    umi_str = ''.join(umi_list)
    seq_umi = umi_str + seq
    return(seq_umi, umi_str)

umi_list = []

if primer_type == "reverse":
    with open (output_fasta, "w") as fout:
        with open(input_fasta, "r") as f:
            for record in SeqIO.parse(f, "fasta"):
                seq = str(record.seq)
                primer_end = re.search(primer,seq).end()
                seq_until_primer = seq[0:primer_end]
                (seq_until_primer_with_umi, umi_seq) = add_umi_rev(seq_until_primer, umi_length)
                record.seq = Seq(seq_until_primer_with_umi)
                record.description = record.description + " UMI:" + umi_seq
                SeqIO.write(record,fout,"fasta")
                umi_list.append(umi_seq)
elif primer_type == "forward":
    with open (output_fasta, "w") as fout:
        with open(input_fasta, "r") as f:
            for record in SeqIO.parse(f, "fasta"):
                seq = str(record.seq)
                primer_start = re.search(primer,seq).start()
                seq_from_primer = seq[primer_start:]
                (seq_from_primer_with_umi, umi_seq) = add_umi_for(seq_from_primer, umi_length)
                record.seq = Seq(seq_from_primer_with_umi)
                record.description = record.description + " UMI:" + umi_seq
                SeqIO.write(record,fout,"fasta")
                umi_list.append(umi_seq)
else:
    sys.exit("Wrong primer type, please select 'forward'  or 'reverse'.")

umi_set = set(umi_list)

print("Total number of UMI barcodes: {}".format(len(umi_list)))
print("Total number of unique UMI barcodes: {}".format(len(umi_set)))




