
library(alakazam)
library(dplyr)
library(dowser)

minrange = 3 * (7*24)

trees = readRDS("../../data/processed/Gupta_2017/datastates_cluster_all_genotyped.rds")

trees$clone_range = unlist(lapply(trees$data,function(x)max(x@data$time)-
            min(x@data$time) ))

trees = filter(trees,subject == "Fv" & clone_range >= minrange &
 	seqs >= 15 & min_p < 0.05)

samples = tibble()
for(i in 1:nrow(trees)){
	temp = trees[i,]
	clone_id = temp$clone_id
	counts = table(temp$data[[1]]@data$time)
	times = names(counts)
	counts = counts
	samples = bind_rows(samples,
		bind_cols(clone_id=clone_id,times=times,
			counts=counts))
}

samples$subject = "Fv"

write.csv(samples,row.names=FALSE,quote=FALSE,
	file="intermediates/fv_clonecounts.csv")
