

use strict;
use warnings;
use hotSpotUtils;


my $reps = 20;
open(OUT,">simResults.tsv") or die();

print OUT "id\trep\tmodel\tnseq\tparam\tmle\ttrue\n";

my @ms = ("HLP19");
for(my$rep=0;$rep<$reps;$rep++){
	my %lengths;
	my %real;
	my %nseqs;
	open(R,"097/mrna/V1/simulatedrg_ncdr_lineages_$rep.tsv") or die();	
	my $h=<R>;
	while(<R>){
		my $line=$_;
		chomp($line);
		my @in=split("\t",$line);
		my $f = $in[0];
		my $id="";
		print "$f\n";
		if($f =~ /097\/mrna\/V1\/simsrg\/(\d+).fa_$rep\_0_ncdr.fa/){
			$id = $1;
		}else{die($id)}
		my $seq=getfasta($f);
		my @ks=keys %$seq;
		my $nseq=scalar(@ks);
		$nseqs{$id}=$nseq;
		#true number of substitutions
		open(T,"097/mrna/V1/simsrg//$id.fa\_$rep\_subs.txt") or die("097/mrna/V1/simsrg//$id.fa\_$rep\_subs.txt");
		my $curlength=0;
		while(<T>){
			my $line=$_;
			chomp($line);
			my @in=split("\t",$line);
			if(!exists($lengths{$in[0]})){
				$lengths{$in[0]}=0;
			}
			$real{"$in[0]:$in[3]"}=$in[2];
			$curlength+=$in[2];
			if($in[3] =~ /(\d+)_GERM/){
				$lengths{$1}=$curlength;
				$curlength=0;
			}
		}

		foreach my $m (@ms){
			open(S,"$in[0]\_igphyml_stats_$m"."indlr.txt") or die("$in[0]\_igphyml_stats_$m"."indlr.txt");# open(S,"$in[0]\_igphyml_stats_$m"."indlr.txt") or die("$in[0]\_igphyml_stats_$m"."indlr.txt");# do{print("simulations/$id.fa_igphyml_stats_HLPindlr.txt FAILED");next;};
			while(<S>){
				my $line=$_;
				chomp($line);
				if($line=~/\. Transition\/transversion ratio\: 	(\S+)/){print OUT "$id\t$rep\t$m"."i\t$nseq\tkappa\t$1\t2\n"}
				if($line=~/\. Omega 0:	(\S+)/){print OUT "$id\t$rep\t$m"."i\t$nseq\tomega0\t$1\t0.5\n"}
				if($line=~/\. Omega 1:	(\S+)/){print OUT "$id\t$rep\t$m"."i\t$nseq\tomega1\t$1\t0.7\n"}
				if($line=~/Motif\:\s+(\S+)\s+\d+\s+\d+\s+(\S+)/){
					my $mot=$1; my $val=$2; my $true;
					if($mot=~/WRC/){$true=4}
					if($mot=~/GYW/){$true=6}
					if($mot=~/WA/){$true=3}
					if($mot=~/TW/){$true=1}
					if($mot=~/SYC/){$true=-0.6}
					if($mot=~/GRS/){$true=-0.6}
					print  OUT "$id\t$rep\t$m"."i\t$nseqs{$id}\t$mot\t$val\t$true\n"
				}
				if($line=~/Ind\s+Seq/){
					while(<S>){
						$line=$_;
						#print $line;
						chomp($line);
						if($line =~ /oooooooo/){last;}
						my @in=split("\\s+",$line);
						if($line=~/(\d+)\_GERM/){
							print OUT "$1\t$rep\t$m"."i\t$nseqs{$id}\tlength\t$in[2]\t$lengths{$1}\n"
						}
					}
				}
			}
		}

	}
	foreach my $m (@ms){
	my $nm = lc $m;
	open(S2,"097/mrna/V1/simulatedrg_ncdr_lineages_$rep\_GY.tsv_igphyml_stats_HLP19ci.txt") or die();
	while(<S2>){
		my $line=$_;
		chomp($line);
		if($line=~/\. Transition\/transversion ratio\: 	(\S+)/){print OUT "-1\t$rep\t$m"."r\t-1\tkappa\t$1\t2\n"}
		if($line=~/\. Omega 0:	(\S+)/){print OUT "-1\t$rep\t$m"."r\t-1\tomega0\t$1\t0.5\n"}
		if($line=~/\. Omega 1:	(\S+)/){print OUT "-1\t$rep\t$m"."r\t-1\tomega1\t$1\t0.7\n"}
		if($line=~/Motif\:\s+(\S+)\s+\d+\s+\d+\s+(\S+)/){
			my $mot=$1; my $val=$2; my $true;
			if($mot=~/WRC/){$true=4}
			if($mot=~/GYW/){$true=6}
			if($mot=~/WA/){$true=3}
			if($mot=~/TW/){$true=1}
			if($mot=~/SYC/){$true=-0.6}
			if($mot=~/GRS/){$true=-0.6}
			print  OUT "-1\t$rep\t$m"."r\t-1\t$mot\t$val\t$true\n"
		}
		if($line=~/Ind\s+Seq/){
			while(<S2>){
				$line=$_;
				#print $line;
				chomp($line);
				if($line =~ /oooooooo/){last;}
				my @in=split("\\s+",$line);
				if($line=~/(\d+)\_GERM/){
					my $id = $1;
					print OUT "$1\t$rep\t$m"."r\t$nseqs{$1}\tlength\t$in[2]\t$lengths{$1}\n";
					#my $ttree = "097seqs/$id.fa\_igphyml_tree_H0tree.txt";
					#my $otree;
					#if($m eq "HLP19"){
					#	$otree ="sims/$id.fa_$rep\_0.fa\_igphyml_tree_HLP19tlme.txt";
					#}else{
					#	$otree ="sims/$id.fa_$rep\_0.fa\_igphyml_tree_hlp17tl.txt";
					#}
					#my $rf = `Rscript compRF.R $ttree $otree`;
					##print "\n\nRF: $rf";
					#if($rf =~ /\[1\] (\d+)/){
					#	print OUT "$id\t$rep\t$m"."r\t$nseqs{$id}\tRF\t$1\t0\n";
					#}else{
					#	print OUT "$id\t$rep\t$m"."r\t$nseqs{$id}\tRF\tNA\t0\n";
					#}
				}
			}
		}
	}
	}
}






