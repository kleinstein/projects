# Bootstrap the simulated data

library(ape)
library(dplyr)
library(dowser)
library(ggtree)
library(ggplot2)
library(ggpubr)
library(ggtree)
library(parallel)
library(PairedData)


args = commandArgs(trailingOnly=TRUE)
nprocs = as.numeric(args[1])
bcr_phylo = as.character(args[2])
build = as.character(args[3])

print(build)
if(build=="dnapars"){
  exec <- paste(".../.../phylip-3.697/exe/dnapars")
}else if (build =="dnaml"){
  exec <- paste(".../.../phylip-3.697/exe/dnaml")
}else if(build == "igphyml" || build == "igphyml_p"){
  exec <- paste(".../.../igphyml/src/igphyml")
}else if(build == "raxml" || build == "raxml_p"){
  exec <- paste(".../.../raxml-ng/bin/raxml-ng")
}else{
  exec <- NULL
}



print("reading in the data")
hclones <- readRDS(paste0(".../.../hclones_", bcr_phylo, "_1.rds"))
hlclones <- readRDS(paste0(".../.../hlclones_", bcr_phylo, "_1.rds"))
print("data successfully read in")

comp_name <- paste0(".../.../comp_", build, "_", bcr_phylo, ".csv")

print("making starting trees")
if(build %in% c("dnaml", "dnapars", "pratchet", "pml")){
  hclones <- getTrees(hclones, nproc = nprocs, build=build, exec = exec)
  hlclones <- getTrees(hlclones, nproc = nprocs, build=build, exec = exec)
} else if(build == "raxml"){
  hclones <- getTrees(hclones, nproc = nprocs, build=build, exec = exec)
  hlclones <- getTrees(hlclones, nproc = nprocs, build=build, exec = exec)
} else if(build == "igphyml_p"){
  hclones <- getTrees(hclones, nproc = nprocs, build="igphyml", exec = exec,optimize="tlr")
  hlclones <- getTrees(hlclones, build="igphyml", partition="hl", dir="temp_p_bootstrap",id="hl",optimize="tlr",
                       exec=exec,nproc=nprocs, omega="e,e", rates="0,1")
} else if(build == "igphyml"){
  hclones <- getTrees(hclones, nproc = nprocs, build="igphyml", exec = exec,optimize="tlr")
  hlclones <- getTrees(hlclones, build="igphyml", dir="temp_bs", exec=exec,nproc=nprocs, optimize="tlr")
} else if(build == "raxml_p"){
  hclones <- getTrees(hclones, nproc = nprocs, build="raxml", exec = exec)
  hlclones <- getTrees(hlclones, nproc = nprocs, build="raxml", partition = TRUE, exec = exec) 
}

print('starting the hclones bootstrapping')
if(!build %in% c("igphyml", "igphyml_p", "raxml_p")){
  hclones <- getBootstraps(hclones, bootstraps = 100, nproc = nprocs, bootstrap_nodes = TRUE, quiet=5, build=build, exec = exec)
  print('starting the hlclones bootstrapping')
  hlclones <- getBootstraps(hlclones, bootstraps = 100, nproc = nprocs, bootstrap_nodes = TRUE, quiet=5, build=build, exec = exec)
} else if(build == "igphyml"){
  hclones <- getBootstraps(hclones, bootstraps = 100, nproc = nprocs, bootstrap_nodes = TRUE, quiet=5, build=build, exec = exec)
  print('starting the hlclones bootstrapping')
  hlclones <- getBootstraps(hlclones, bootstraps = 100, nproc = nprocs, bootstrap_nodes = TRUE, quiet=5, build=build, exec = exec, optimize="tlr")
} else if(build == "igphyml_p"){
  hclones <- getBootstraps(hclones, bootstraps = 100, nproc = nprocs, bootstrap_nodes = TRUE, quiet=5, build="igphyml", exec = exec)
  print('starting the hlclones bootstrapping')
  hlclones <- getBootstraps(hlclones, bootstraps = 100, nproc = nprocs, bootstrap_nodes = TRUE, quiet=5, build="igphyml", exec = exec, id="hl",optimize="tlr",
                            omega="e,e", rates="0,1")
} else if(build == "raxml_p"){
  hclones <- getBootstraps(hclones, bootstraps = 100, nproc = nprocs, bootstrap_nodes = TRUE, quiet=5, build="raxml", exec = exec)
  print('starting the hlclones bootstrapping')
  hlclones <- getBootstraps(hlclones, bootstraps = 100, nproc = nprocs, bootstrap_nodes = TRUE, quiet=5, build="raxml", exec = exec, partition = TRUE)
}


# filter down to only include the clones in both sets just in case
hclones <- filter(hclones, clone_id %in% hlclones$clone_id)
hlclones <- filter(hlclones, clone_id %in% hclones$clone_id)

# find the amounts for the table and what not
print('Getting the aveage bootstrapping score by clone id')
hclones_avg <- c()
hclones$bootstrap_values <- rep(0, nrow(hclones))
for(clone in 1:nrow(hclones)){
  matches_from_nodes <- c()
  for(i in 1:length(hclones[clone,]$trees[[1]]$nodes)){
    to_bind <- hclones[clone,]$trees[[1]]$nodes[[i]]$bootstrap_value
    print(to_bind)
    matches_from_nodes <- append(matches_from_nodes, to_bind)
  }
  hclones$bootstrap_values[clone] <- mean(matches_from_nodes, na.rm = TRUE)
  hclones_avg <- rbind(hclones_avg, mean(matches_from_nodes, na.rm = TRUE))
  print(mean(matches_from_nodes, na.rm = TRUE))
}

hlclones_avg <- c()
hlclones$bootstrap_values <- rep(0, nrow(hlclones))
for(clone in 1:nrow(hlclones)){
  matches_from_nodes <- c()
  for(i in 1:length(hlclones[clone,]$trees[[1]]$nodes)){
    to_bind <- hlclones[clone,]$trees[[1]]$nodes[[i]]$bootstrap_value
    print(to_bind)
    matches_from_nodes <- append(matches_from_nodes, to_bind)
  }
  hlclones$bootstrap_values[clone] <- mean(matches_from_nodes, na.rm = TRUE)
  hlclones_avg <- rbind(hlclones_avg, mean(matches_from_nodes, na.rm = TRUE))
  print(mean(matches_from_nodes, na.rm = TRUE))
}

# make the comp file 
comp <- cbind(data.frame(hclones_avg), data.frame(hlclones_avg))

for(i in 1:length(hclones$clone_id)){
  if((comp$hclones_avg[i] > comp$hlclones_avg[i]) == T){
    comp$larger_value[i] <- "hclones"
    comp$l_value[i] <- comp$hclones_avg[i]
  }
  else if((comp$hlclones_avg[i] > comp$hclones_avg[i]) == T){
    comp$larger_value[i] <- "hlclones"
    comp$l_value[i] <- comp$hlclones_avg[i]
  }
  else{
    comp$larger_value[i] <- "same"
    comp$l_value[i] <- comp$hlclones_avg[i]
  }
}

comp$tree_num <- 1:nrow(hlclones)
comp$seq_size <- hlclones$seqs

# save updated comp file 
write.csv(comp, file = comp_name, row.names=FALSE)
print("DONE")
