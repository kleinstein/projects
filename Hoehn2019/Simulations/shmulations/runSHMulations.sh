#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --job-name=shmulate
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=96:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=kenneth.hoehn\@gmail.com

module load R 
folders=("034" "097" "095" "093" "083" "074" "073" "065" "064" "063" "054" "049" "033" "026" "025" "023" "021" "018" "016" "015" "014" "013" "011" "008" "004" "003" "001")
naive=Heavy_NaiveMemory_SORT-Naive_SUBJECT-HD10.tsv
reps=2
startingrep=0
threads=1
count=0

for f in "${folders[@]}"
do
	echo $f
	echo Rscript SHMulate_dist.R ../june_2017/changeo_v1only/s$f\_germ-pass.tab shmulations/s$f\_dist $reps $naive 1 FALSE $startingrep
	Rscript SHMulate_dist.R ../june_2017/changeo_v1only/s$f\_germ-pass.tab shmulations/s$f\_dist $reps $naive 1 FALSE $startingrep
	for ((i=$startingrep;i<=$reps;i++));
	do
		echo $i
		if [ $(($count%3)) == 0 ]
		then
			echo TRUE
			sbatch --cpus-per-task=$threads --job-name=$f:$i --partition=pi_kleinstein runq.sh shmulations/s$f\_dist_$i $threads
			#sbatch --cpus-per-task=$threads --job-name=$f:$i --partition=pi_kleinstein runq_split.sh shmulations/s$f\_rg\_$i $threads
		else
			echo FALSE
			sbatch --cpus-per-task=$threads --job-name=$f:$i --partition=general runq.sh shmulations/s$f\_dist_$i $threads
			#sbatch --cpus-per-task=$threads --job-name=$f:$i --partition=general runq_split.sh shmulations/s$f\_rg\_$i $threads
		fi
		count=$((count + 1))
	done
done
#mv slurm* old
