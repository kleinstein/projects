


use strict;
use warnings;

my $depth=3000;

my @folders=glob("*/*/*/*simulated_0_lineages_GY.tsv_igphyml_stats_HLP19.txt");
open(OUT,">RepResultsSim.tsv") or die();
print OUT "Type\tP\tRID\tTime\tParam\tMLE\tLCI\tUCI\n";

my $c=0;
foreach my $f (@folders){
	print "$f\n";
	if($f=~/(\S+)\/(\S+)\/V1\/simulated_0_lineages_GY.tsv_igphyml_stats_(HLP19).txt/){
	my $n=$1;
	my $time=$2;
	my $ri=$3;
	my $t=$time;
	if($time =~ /([-|+])(\d+)d/){
		if($1 eq "+"){
			$time=$2;
		}else{
			$time=$1.$2;
		}
	}elsif($time =~ /([-|+])(\d+)h/){
		if($1 eq "+"){
			$time=($2/24);
		}else{
			$time=$1.($2/24);
		}
	}
	if($ri eq ""){$ri="NA"}
	if($f !~ /FCH/ && $f !~ /HLP19/){next;}
	my $maxdiv = 0;
	my $sumdiv = 0;
	my $lincount = 0;
	open(IN,$f) or die($f);
	while(<IN>){
		my $line=$_;
		chomp($line);
			if($line=~/\. Estimated substitutions in repertoire\:	(\S+)/){print OUT "$t\t$n\t$ri\t$time\tsubst\t$1\t$1\t$1\n"}
			if($line=~/\. Transition\/transversion ratio\: 	(\S+)\s+\((\S+)\, (\S+)\)/){print OUT "$t\t$n\t$ri\t$time\tkappa\t$1\t$2\t$3\n"}
			elsif($line=~/\. Transition\/transversion ratio\: 	(\S+)/){print OUT "$t\t$n\t$ri\t$time\tkappa\t$1\t$1\t$1\n"}
			if($line=~/\. Omega 0:	(\S+)\s+\((\S+)\, (\S+)\)/){print OUT "$t\t$n\t$ri\t$time\tomega0\t$1\t$2\t$3\n"}
			elsif($line=~/\. Omega 0:	(\S+)/){print OUT "$t\t$n\t$ri\t$time\tomega0\t$1\t$1\t$1\n"}
			if($line=~/\. Omega 1:	(\S+)\s+\((\S+)\, (\S+)\)/){print OUT "$t\t$n\t$ri\t$time\tomega1\t$1\t$2\t$3\n"}
			elsif($line=~/\. Omega 1:	(\S+)/){print OUT "$t\t$n\t$ri\t$time\tomega1\t$1\t$1\t$1\n"}
			if($line=~/Motif\:\s+(\S+)\s+\d+\s+\d+\s+(\S+)\s+\((\S+)\,\s+(\S+)\)/){print  OUT "$t\t$n\t$ri\t$time\t$1\t$2\t$3\t$4\n"}
			elsif($line=~/Motif\:\s+(\S+)\s+\d+\s+\d+\s+(\S+)/){print  OUT "$t\t$n\t$ri\t$time\t$1\t$2\t$2\t$2\n"}
			if($line=~/Position \d\:\s+f\((\S\d)\)\=(\S+)\s+f\((\S\d)\)\=(\S+)\s+f\((\S\d)\)\=(\S+)\s+f\((\S\d)\)\=(\S+)/){
				print "$line\n";
				print OUT "$t\t$n\t$ri\t$time\t$1\t$2\t$2\t$2\n";
				print OUT "$t\t$n\t$ri\t$time\t$3\t$4\t$4\t$4\n";
				print OUT "$t\t$n\t$ri\t$time\t$5\t$6\t$6\t$6\n";
				print OUT "$t\t$n\t$ri\t$time\t$7\t$8\t$8\t$8\n";
			}
			if($line =~ /Ind\s+Seq\s+TreeL/){
				while(<IN>){
					$line = $_;
					last if $line =~ /oooooooooooooooo/;
					my @in = split("\\s+",$line);
					$lincount++;
					$sumdiv += $in[2];
					if($in[2] > $maxdiv){
						$maxdiv = $in[2];
					}
				}
				my $meandiv = $sumdiv/$lincount;
				print OUT "$t\t$n\t$ri\t$time\tMaxDiv\t$maxdiv\t$maxdiv\t$maxdiv\n";
				print OUT "$t\t$n\t$ri\t$time\tMeanDiv\t$meandiv\t$meandiv\t$meandiv\n";
			}
		}
	}else{
		print("Skipping $f\n");
	}
}
