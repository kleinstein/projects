

reps=1
repe=20
rates=(10 25 50 100)
r01s=(1 10 0.1)
freqs=(0.5 1)


threads=1

for freq in "${freqs[@]}"
do
	for rate in "${rates[@]}"
	do 
		for r01 in "${r01s[@]}"
		do
			for i in $(seq $reps $repe);
			do
				if [ "$i" -lt 21 ]; then
					sbatch --partition=pi_kleinstein --cpus-per-task=$threads \
						--job-name="${freq}_${rate}_${r01}_$i" estimateTwostate.sh $rate $r01 $freq $i $threads
				else
					sbatch --partition=general --cpus-per-task=$threads \
						--job-name="${freq}_${rate}_${r01}_$i" estimateTwostate.sh $rate $r01 $freq $i $threads
				fi					
			done
		done
	done
done


rates=(10)
repe=20
modes=("association" "mix" "direct" "sequential" \
	"progressive" "unconstrained")

for rate in "${rates[@]}"
do
	for mode in "${modes[@]}"
	do 
		for i in $(seq $reps $repe);
		do
			if [ "$i" -lt 21 ]; then
			sbatch --partition=pi_kleinstein --cpus-per-task=$threads \
				estimateFourstate.sh $rate $mode $i $threads
			else
			sbatch --partition=general --cpus-per-task=$threads \
				estimateFourstate.sh $rate $mode $i $threads
			fi
		done
	done
done
