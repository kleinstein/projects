#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --cpus-per-task=20
#SBATCH --mem-per-cpu=5gb
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=48:00:00

module load R/3.6.1-foss-2018b

first=$1
second=$2
nproc_in=$3
nproc_out=$4

Rscript runPresto.R $first $second $nproc_in $nproc_out