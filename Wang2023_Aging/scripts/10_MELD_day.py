## ---------------------------
##
## Script name: 10_MELD_day.py
##
## Purpose of script: Run MELD on days on data
##
## Author: Mamie Wang
##
## Date Created: 2022-07-25
##
## Email: mamie.wang@yale.edu
##
## ---------------------------
## load the packages and functions

import pandas as pd
import numpy as np
import meld
import graphtools
import sklearn
import argparse

##---------------------------
## load the arguments

parser = argparse.ArgumentParser(description="Run MELD on cell subsets.")
parser.add_argument("cell_type", help="Cell subset (Naive B, RMB, ABC, PB, PB proliferating")
args = parser.parse_args()
cell_type = args.cell_type

##---------------------------
## load the data

indir = "/gpfs/ysm/project/mw957/repos/shaw_flu/data/outs/8_B_clustering/"
outdir = "/gpfs/ysm/project/mw957/repos/shaw_flu/data/outs/10_d7_MELD/"
B_cell_data = pd.read_csv(indir + "B_counts_scaled.csv", index_col=0)

metadata = pd.read_csv(indir + "B_meta_data.csv", index_col=0)

metadata.loc[:,'is_d7'] = np.where(metadata.day == "D7", 1, 0)
n_clusters = [2, 3, 4, 5]
patients = ["Y1", "Y2", "Y3", "O1", "O2", "O3"]

##---------------------------
## compute the day 7 MELD score
if cell_type == "full":
    day_RL = pd.DataFrame(np.empty(shape=(metadata.shape[0],2), dtype='float'),
                          index=metadata.index, 
                          columns=["D0", "D7"])
    for i, patient in enumerate(patients):
        is_target = metadata.ID == patient
        patient_meta = metadata.loc[is_target,:]
        patient_data = B_cell_data.loc[:, is_target]
        curr_sample_labels = patient_meta.is_d7
        curr_G = graphtools.Graph(patient_data.values.T, n_pca=50, use_pygsp=True, knn=20)

        meld_op = meld.MELD(beta = 67, knn = 7)
        sample_density = meld_op.fit_transform(curr_G, sample_labels = curr_sample_labels)
        curr_likelihood = sklearn.preprocessing.normalize(sample_density, norm='l1')
        day_RL.iloc[np.where(is_target)[0], :] = curr_likelihood
    
else:
    is_cell_type = metadata.annotated_cluster == cell_type

    day_RL = pd.DataFrame(np.empty(shape=(sum(is_cell_type),len(n_clusters) + 1), dtype='float'),
                          index=metadata.index[is_cell_type], 
                          columns = ["D7", "k = 2", "k = 3", "k = 4", "k = 5"])
    cell_type_meta = metadata.loc[is_cell_type,:]
    cell_type_data = pd.DataFrame(B_cell_data).loc[:,is_cell_type]

    for i, patient in enumerate(patients):
        is_target = cell_type_meta.ID == patient
        patient_meta = cell_type_meta.loc[is_target,:]
        patient_data = cell_type_data.loc[:, is_target]
        curr_sample_labels = patient_meta.is_d7

        curr_G = graphtools.Graph(patient_data.values.T, n_pca=50, use_pygsp=True, knn=20)

        meld_op = meld.MELD(beta = 67, knn = 7)
        sample_density = meld_op.fit_transform(curr_G, sample_labels = curr_sample_labels)
        curr_likelihood = sklearn.preprocessing.normalize(sample_density, norm='l1')[:,1]
        day_RL.iloc[np.where(is_target)[0], 0] = curr_likelihood

        for j, n_cluster in enumerate(n_clusters): 
            curr_vfc = meld.VertexFrequencyCluster(n_clusters = n_cluster)
            curr_vfc.fit_transform(curr_G, curr_sample_labels, curr_likelihood)
            day_RL.iloc[np.where(is_target)[0], j + 1] = curr_vfc.predict(n_cluster)
        
##---------------------------
## save the object

day_RL.to_csv(outdir + "d7_RL_" + cell_type + ".csv")
