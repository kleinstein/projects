#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 10:00:00
#SBATCH --constraint E5-2660_v3
#SBATCH -c 20
#SBATCH --mem-per-cpu=1G


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

PATH_ROOT_1="/home/qz93/project/ellebedy_pairedBulk/"
PATH_ROOT_2="/home/qz93/project/ellebedy_bulk/"
PATH_RAW="${PATH_ROOT_1}data/FASTQs/"
PATH_PHIX="${PATH_ROOT_1}phix/"

PATH_SCRIPT_PHIX="/home/qz93/projects/scripts/turnerZhouHan_2020/immcantation_preprocess-phix_10242018_JZ.sh" 
PATH_SCRIPT_FASTQ2FASTA="/home/qz93/projects/scripts/turnerZhouHan_2020/"


chmod +x "${PATH_SCRIPT_PHIX}"

# notes on getting blastn to work properly

# need run makeblastdb on phix174.fasta
# three p174.n* files will be generated
# these are passed to blastn -db

# need to set environmental variable $BLASTDB 
# point it to directory containing p174.n*
# so that blastn can find them when p174 is passed to -db

PATH_REF_PHIX_DIR="${PATH_ROOT_2}preprocess_support_files/phix_ref/"
PATH_REF_PHIX_FASTA="${PATH_REF_PHIX_DIR}phix174_ncbi_06-JUL-2018.fasta" 
NAME_PHIX_DB="p174"

# make blastn index
cd "${PATH_REF_PHIX_DIR}"
# commented out b/c done for ellebedy_bulk already
#makeblastdb -in "${PATH_REF_PHIX_FASTA}" -input_type fasta -dbtype nucl -out "${NAME_PHIX_DB}"

export BLASTDB="${PATH_REF_PHIX_DIR}"

# log
PATH_LOG="${PATH_PHIX}phix_$(date '+%m%d%Y_%H%M%S').log"

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
#echo "germline" $GERM_TAG &>> "${PATH_LOG}"
echo "phix ref" $(ls ${PATH_REF_PHIX_DIR}/*fasta) &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
#MaskPrimers.py --version &>> "${PATH_LOG}" # presto version
#muscle -version &>> "${PATH_LOG}" # muscle version
#usearch --version &>> "${PATH_LOG}" # usearch version
blastn -version &>> "${PATH_LOG}" # blastn version
python3 -V &>> "${PATH_LOG}"

# raw data directory structure
# /home/qz93/project/ellebedy_pairedBulk/data/FASTQs
#   SampleName_R1.fastq
#   SampleName_R2.fastq

#SUBJ_ARRAY=(Md1718_03) # testing
SUBJ_ARRAY=(S7 S8 S9 S10 S11 S12)

for SUBJ in ${SUBJ_ARRAY[@]}; do

	echo "*********** STARTING ON ${SUBJ} ***********" &>> "${PATH_LOG}"

	cd "${PATH_RAW}"
	
	# get sample fastq names
	FASTQ_1="${SUBJ}_R1.fastq"
	FASTQ_2="${SUBJ}_R2.fastq"

	#   -s  FASTQ sequence file.
	#   -r  Directory containing phiX174 reference db.
	#   -n  Sample name or run identifier which will be used as the output file prefix.
	#       Defaults to a truncated version of the input filename.
	#   -o  Output directory.
	#       Defaults to the sample name.
	#   -p  Number of subprocesses for multiprocessing tools.
	#       Defaults to the available processing units.

	# remove phix for R1
	# output: [subj]_R1_noPhix_selected.fastq
	"${PATH_SCRIPT_PHIX}" \
		-s "${PATH_RAW}${FASTQ_1}" \
		-r "${NAME_PHIX_DB}" \
		-n "${SUBJ}_R1_noPhix" \
		-o "${PATH_PHIX}" \
		-p "${NPROC}" \
		-t "${PATH_SCRIPT_FASTQ2FASTA}" \
		&>> "${PATH_LOG}"

	# restore header to illumina format
	# output: [subj]_R1_noPhix_reheader.fastq
	# python3 "${PATH_SCRIPT_RESTORE_HEADER}" \
	# 	"${PATH_PHIX}${SUBJ}_R1_noPhix_selected.fastq" \
	# 	"${PATH_PHIX}${SUBJ}_R1_noPhix_reheader.fastq" \
	# 	"fastq" &>> "${PATH_LOG}"

	# remove phix for R2
	# output: [subj]_R2_noPhix_selected.fastq
	"${PATH_SCRIPT_PHIX}" \
		-s "${PATH_RAW}${FASTQ_2}" \
		-r "${NAME_PHIX_DB}" \
		-n "${SUBJ}_R2_noPhix" \
		-o "${PATH_PHIX}" \
		-p "${NPROC}" \
		-t "${PATH_SCRIPT_FASTQ2FASTA}" \
		&>> "${PATH_LOG}"

	# restore header to illumina format
	# output: [subj]_R2_noPhix_reheader.fastq
	# python3 "${PATH_SCRIPT_RESTORE_HEADER}" \
	# 	"${PATH_PHIX}${SUBJ}_R2_noPhix_selected.fastq" \
	# 	"${PATH_PHIX}${SUBJ}_R2_noPhix_reheader.fastq" \
	# 	"fastq" &>> "${PATH_LOG}"

	# notes on preprocess-phix_10242018_JZ.sh
	# adapted from Susana's immancation/scripts/preprocess-phix.sh

	# If, # reads in fasta - # IDs in *phixhits.txt != # reads in selected.fastq
	#     that is because some of the IDs appear more than once in *phixhits.txt

done

