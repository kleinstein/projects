### Description of code for manuscript
### *Platelet response to influenza vaccination reflects effects of aging*

#### RNASeq_platelet_study_functions.R
Custom functions used in downstream RNASeq analysis of platelet transcriptome data  

#### RNASeq_platelet_prepare.R
Preprocess RNASeq count and flow data for use in downstream analyses  

#### RNASeq_platelet_baseline.Rmd
Perform baseline analysis of RNASeq data    
Generates plots for Figures 1-2 and Extended Data Figure 1  
Generates results for Table 1, and Supplementary Tables 1-10  

#### RNASeq_platelet_tensor_prep.Rmd
Filter and reformat data into gene by subject by time dataframes for tensor decomposition  

#### CPOPT_platelet.m
Perform non-negative cp tensor decomposition on time-course platelet data  

#### TD_postprocess_functions.py
Functions to analyze and visualize output of tensor decompositions in Jupyter Notebook  

#### Platelet_TD_postprocess.ipynb
Analyze and visualize tensor decomposition results using Python in Jupyter Notebook  
Generates plots for Figures 3,4,6 and Supplementary Figure 2  
(Visualize output of notebook in Platelet_TD_postprocess.html)

#### RNASeq_timecourse_marker.genes.Rmd
Analyze correlations of marker genes across all timepoints
Generates plots for Supplementary Figures 3-4.


#### RNASeq_timecourse_HvL_Young.Rmd
Perform time-course DE analysis of Young High v. Low-Responders  
Generates plots for Figure 6

#### Marker_analysis.Rmd
Compare marker activity across days and groups for both RNASeq and flow cytometry  
Generates plots for Figure 5

#### Platelet_strain.Rmd
Perform maxRBA calculation for HAI titers
Generates plots for Supplementary Figure 1
