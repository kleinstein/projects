#!/bin/bash
#SBATCH --partition=general
#SBATCH --job-name=clustering
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=60G
#SBATCH --time=02:00:00
#SBATCH --error=log/clustering.%A.err
#SBATCH --output=log/clustering.%A.out
#SBATCH --mail-user=mamie.wang@yale.edu

module load miniconda
conda activate r_4.0

echo "[$0 $(date +%Y%m%d-%H%M%S)] [start] $SLURM_JOBID"

Rscript 6_clustering.R

echo "[$0 $(date +%Y%m%d-%H%M%S)] [end] $SLURM_JOBID"
