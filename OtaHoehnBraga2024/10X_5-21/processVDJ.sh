#!/usr/bin/bash

module load Python/3.7.0-foss-2018b

dirs=("MALA01_PA_1_B-v1" "MALA01_HC_2_B-v1")

for wdir in ${dirs[@]}
do
	wdir="../sent_out_MALA01-Peanut_Allergy_Miyo_VDJ_2021-04-01/$wdir"
	echo $wdir
	changeo-10x.sh -s $wdir/filtered_contig.fasta -a $wdir/filtered_contig_annotations.csv \
    	-g human -t ig -p 3 -o $wdir -b ~/share/igblast \
    	-r ~/share/germlines/imgt/human/vdj -p 10
done
