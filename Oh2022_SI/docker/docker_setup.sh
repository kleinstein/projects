#!/bin/sh
# Ruoyi Jiang
# Building docker for replicating a publication

# cd to directory with Dockerfile

# Build
podman build -t ruoyijiang/ganymede:oh2022 .

# Login
# podman login docker.io

# Push image
# podman push ruoyijiang/ganymede:oh2022

# Pull image
# podman pull ruoyijiang/ganymede:oh2022