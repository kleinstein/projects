#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 1:00:00
#SBATCH --constraint E5-2660_v3
#SBATCH -c 20
#SBATCH --mem-per-cpu=4G


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

# IN:
# - slide/[subj]_FUNCTIONAL-T_slideFiltered_mutThresh_[]_windowSize_[].tsv
# OUT:
# - changeo/DTN_[subj]_clone-pass.tab
# - changeo/DTN_[subj]_DefineClones.log

# This script is designed to work when defining clones on a by-subject basis using 
# either a different threshold for each subject
# or a fixed threshold for all subjects
# The threshold(s) should have been produced by the dist-to-nearest (DTN) method

# references
# https://stackoverflow.com/questions/9458752/variable-for-number-of-lines-in-a-file
# https://stackoverflow.com/questions/6022384/bash-tool-to-get-nth-line-from-a-file
# https://www.cyberciti.biz/faq/bash-loop-over-file/
# https://unix.stackexchange.com/questions/88216/bash-variables-in-for-loop-range


#*
VERSION="01092020"

PATH_ROOT="/home/qz93/project/ellebedy_10x_P05/bcr/${VERSION}/"
PATH_CHANGEO="${PATH_ROOT}changeo/"
PATH_DTN="${PATH_CHANGEO}distToNearest/"
#PATH_SLIDE="${PATH_ROOT}slide/"
PATH_TIGGER="${PATH_ROOT}tigger/"

METHOD="DTN"
#MUT_THRESH="6"
#WINDOW_SIZE="10"
#FILENAME_SUFFIX_SLIDE="_FUNCTIONAL-T_slideFiltered_mutThresh_${MUT_THRESH}_windowSize_${WINDOW_SIZE}.tsv"

MAX_MISS="0" #*

PATH_LOG=${PATH_CHANGEO}defineClones_${METHOD}_$(date '+%m%d%Y_%H%M%S').log

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
#echo "germline" ${GERM_TAG} &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
DefineClones.py --version &>> "${PATH_LOG}"
echo "maxmiss: ${MAX_MISS}" &> "${PATH_LOG}"
python3 -V &>> "${PATH_LOG}"

#* IMPORTANT: CHECK THIS BEFORE RUNNING

#* flexible threshold from file or fixed threshold?
# can also set this to true but use a file filled with manual thresholds
#FLEX_THRESH=true #* 
FLEX_THRESH=false

FIX_THRESH=0.1 #*

#* must set if FLEX_THRESH=true
#  file must be present even if FLEX_THRESH=false and using FIX_THRESH
#  because subjects are provided via this file
FN_THRESH="${PATH_DTN}thresh_all_density_full.txt" #*

N_LINES=$(wc -l < "${FN_THRESH}")
echo "N_LINES="$N_LINES &>> "${PATH_LOG}"
# number of subjects = number of lines/2 (1 line each for subject and threshold)
N_SUBJ=$(expr ${N_LINES} "/" 2) 
echo "N_SUBJ="$N_SUBJ &>> "${PATH_LOG}"

for ((SUBJ_IDX=1;SUBJ_IDX<=${N_SUBJ};SUBJ_IDX++)); do
# next line won't work; brace-expansion occurs before parameter expansion
#for SUBJ_IDX in {1..${N_SUBJ}}; do 
	
	echo "SUBJ_IDX="$SUBJ_IDX &>> "${PATH_LOG}"

	# calculate line number containing subject: (i-1)*2+1
	CUR_SUBJ_LINE=$(expr '(' ${SUBJ_IDX} - 1 ')' '*' 2 + 1)
	echo "CUR_SUBJ_LINE="${CUR_SUBJ_LINE} &>> "${PATH_LOG}"

	# read subject from file
	CUR_SUBJ=$(sed "${CUR_SUBJ_LINE}q;d" "${FN_THRESH}")
	echo "CUR_SUBJ="${CUR_SUBJ} &>> "${PATH_LOG}"

	# get threshold
	if ${FLEX_THRESH}; then
		# calculate line number containing threshold: (i-1)*2+2
		CUR_THRESH_LINE=$(expr ${CUR_SUBJ_LINE} + 1)
		echo "CUR_THRESH_LINE="${CUR_THRESH_LINE} &>> "${PATH_LOG}"

		# read threshold from file
		CUR_THRESH=$(sed "${CUR_THRESH_LINE}q;d" "${FN_THRESH}")
		echo "subject-specific threshold; CUR_THRESH="${CUR_THRESH} &>> "${PATH_LOG}"
	else
		CUR_THRESH=${FIX_THRESH}
		echo "fixed threshold; CUR_THRESH="${CUR_THRESH} &>> "${PATH_LOG}"
	fi
	
	echo "############## DefineClones for ${CUR_SUBJ}" &>> "${PATH_LOG}"

	#*
	if [[ ${CUR_SUBJ} == "P04" || ${CUR_SUBJ} == "P05" ]]; then
		#PATH_TSV_T="${PATH_TIGGER}corrected_${CUR_SUBJ}_IGHV_FUNCTIONAL-T_genotyped.tsv"
		# _2 for P05 has d0_PBMC_2 with identical cell barcode and SEQUENCE_IMGT as d5_PBMC_2 removed
		PATH_TSV_T="${PATH_TIGGER}corrected_${CUR_SUBJ}_IGHV_FUNCTIONAL-T_genotyped_2.tsv"
	else
		PATH_TSV_T="${PATH_TIGGER}${CUR_SUBJ}_IGHV_FUNCTIONAL-T_genotyped.tsv"
	fi
	
	# --act set (as opposed to first) accounts for ambiguous V-gene and J-gene 
	# calls when grouping similar sequences
	
	# because the ham distance model is symmetric, the --sym avg argument 
	# can be left as default

	# Because the threshold was generated using length normalized distances, 
	# the --norm len argument is selected

	# appends _clone-pass.tab, _clone-fail.tab (even with .tsv input)
	DefineClones.py \
		-d "${PATH_TSV_T}" \
		--failed \
		--mode gene \
		--act set \
		--model ham \
		--dist "${CUR_THRESH}" \
		--norm len \
		--sym avg \
		--link single \
		--maxmiss "${MAX_MISS}" \
		--sf JUNCTION \
		--vf V_CALL_GENOTYPED \
		--jf J_CALL \
		--nproc "${NPROC}" \
		--log "${PATH_CHANGEO}${METHOD}_${CUR_SUBJ}_DefineClones.log" \
		--outdir "${PATH_CHANGEO}" \
		--outname "${METHOD}_${CUR_SUBJ}" \
		&>> "${PATH_LOG}"
	
	echo "############## Finished for ${CUR_SUBJ}" &>> "${PATH_LOG}"

done
