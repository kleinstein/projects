#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --job-name=repsim
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=48:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com
#SBATCH --exclude=c24n04

array=("GMC/+1h" "GMC/+3d" "GMC/+14d" "GMC/+1d" "GMC/+28d" "GMC/+21d" "GMC/-1h" "GMC/-2d" "GMC/-8d" "GMC/+7d" "FV/+1h" "FV/+3d" "FV/+14d" "FV/+1d" "FV/+28d" "FV/+21d" "FV/-1h" "FV/-2d" "FV/-8d" "FV/+7d" "IB/+1h" "IB/+3d" "IB/+14d" "IB/+1d" "IB/+28d" "IB/+21d" "IB/-1h" "IB/-2d" "IB/-8d" "IB/+7d")

sims=20
count=0
threads=4

rm simmasked.log
for i in "${array[@]}"
do
	echo $i
	echo $count
	igphyml --repfile $i/seqs_GY3000_lineages.tsv -m HLP19 -o n --run_id H0tree --outrep $i/seqs_full_lineages.HLP.tsv --threads 18
	mkdir $i/sims_rg
	rm $i/sims_rg/*
	if [ $(($count%3)) == 0 ]
	then
		echo "TRUE" 
		#sbatch --partition=pi_kleinstein --cpus-per-task=8 --job-name=$i runSim.sh $i/simulated_$rep 8
		#perl simulateRepertoire.pl $sims 18 $i/seqs_full_lineages.HLP.tsv 0.7 $i HD1310.tsv simulatedrg pi_kleinstein >> simmasked.log
		#perl simulateRepertoire.pl $sims 18 $i/seqs_full_lineages.HLP.tsv 0.7 $i N simulatedrg pi_kleinstein 5 >> simmasked.log
		sbatch --cpus-per-task=$threads --partition=pi_kleinstein --job-name=$i simulateRepertoire.sh $sims $i pi_kleinstein 10 $threads
	else
		echo "FALSE"
		#sbatch --partition=general --cpus-per-task=8 --job-name=$i runSim.sh $i/simulated_$rep 8
		#perl simulateRepertoire.pl $sims 18 $i/seqs_full_lineages.HLP.tsv 0.7 $i HD1310.tsv simulatedrg general >> simmasked.log
		#perl simulateRepertoire.pl $sims 18 $i/seqs_full_lineages.HLP.tsv 0.7 $i N simulatedrg general 5 >> simmasked.log
		sbatch --cpus-per-task=$threads --partition=general --job-name=$i simulateRepertoire.sh $sims $i general 10 $threads
	fi
	count=$((count + 1))
done



