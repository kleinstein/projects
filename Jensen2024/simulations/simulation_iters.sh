#!/bin/bash

# set the number of nodes
#SBATCH --nodes=1

# memory requirement
#SBATCH --mem=60g
#SBATCH --cpus-per-task=20

module load R/4.1.0-foss-2020b
nprocs=$1
quiet=$2
iterations=$3

Rscript simulation_simulations.R $nprocs $quiet $iterations