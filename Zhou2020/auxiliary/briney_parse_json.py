#!/usr/bin/env python3
"""
Parse and extract essential seq info from JSON
"""

import re, sys, os
from os import path
from sys import argv
import json

# inputJSON name format:
# 326651/16_consensus.json

# output header format: 
# >34f631d6-6dfb-4146-a52f-93dfc5aa3786_1|BP=yes|BI=IgM

# goals:
# - parse JSON, line by line
# - extract these fields: seq_id, prod, isotype, raw_input
# - output as FASTA

def parseJSON(inputJSON, outputFasta):

	if os.path.exists(inputJSON):
		print("Processing " + inputJSON)

		with open(inputJSON, "r") as in_handle:
			lines = [line.rstrip() for line in in_handle] # list

			with open(outputFasta, 'w') as out_handle:

				for i in range(len(lines)):
					#print(i)
					curLine = json.loads(lines[i])
					curSeqId = curLine["seq_id"]
					curProd = curLine["prod"]
					curIsotype = curLine["isotype"]
					curRaw = curLine["raw_input"]

					curHeader = ">" + curSeqId + "|BP=" + curProd + "|BI=" + curIsotype

					out_handle.writelines(curHeader+"\n")
					out_handle.writelines(curRaw+"\n")

				print("Outputted as: ", outputFasta)
	else:
		print(inputJSON + " does not exist.")

# run
parseJSON(sys.argv[1], sys.argv[2])
