### Vaccine dataset analysis
# Kenneth Hoehn
# 11/1/2019
# Do not run this as a shell script! Execute each command individually
# and wait for it to complete. This is just an explanation of the pipeline


# Generate figures summarizing simulation analyses
# Supplemental file 3 - model comparisons
# Supplemental file 4 - repertoire wide comparisons
# Supplemental file 6 - shmulate simulations
# input:
# simulations/RepResults_startingseq.tsv
# simulations/simResults.tsv
# simulations/SHMulateResults.tsv
# simulations/SplitRepResults.tsv
#
# outputs:
# figures/omegas1.pdf (Figure 1)
# figures/simgrid_mean.pdf Supplemental File 3
# figures/simgrid.pdf (Supplemental File 3)
# figures/omega_shmulaterepdist_treelength_sim.pdf (Supplemental File 6)
# figures/omega_shmulateregdist_treelength_sim.pdf (Supplemental File 6)
# 
Rscript simulationAnalyses.R

# Generate main figures and do empirical repertoire analyis
# input files: age/RepResults.tsv vaccine/RepResults.tsv
# information: age/patient_data.tsv
# 
# outputs (tidy up in Illustrator): 
# figures/omega_treelength.pdf (Figure 5)
# figures/regressions.pdf (Figure 3)
# figures/FoldChangeOmegaTree.pdf (Figure 4)
# figures/Combined_Parameters.pdf (Figure 2)
Rscript analyzeRepertoireResults.R

# Generate files for Supplemental File 5
# input files: age/RepResultsSim_rg.tsv vaccine/RepResults_sim_rg.tsv
# information: age/patient_data.tsv
# 
# outputs (tidy up in Illustrator): 
# figures/omega_treelength_sim.pdf (Supplemental File 5)
# figures/omegalength_comp_FCH.pdf (Supplemental File 5)
# figures/regression_comp_FCH.pdf (Supplemental File 5)
# figures/foldchange_simulations.pdf (Supplemental File 5)
# figures/FoldChangeOmegaTree_sim.pdf (Supplemental File 5)
Rscript simulationResults.R

# Generate stream plots for Supplemental File 7 and split repertoire files
# for Supplemental File 8
# input: vaccine/CloneSeqCounts.tsv
# vaccine/linSummary.tsv
# output: figures/streams.0.01.pdf (Supplemental File 7)
# split_repertoire.pdf (Supplemental File 8)
Rscript makeStreamPlots.R

# For Supplemental File 4a-c, move to the Simulations/repertoire_comparisons
# direcotry, run the following commands, and check out the results directory
cd ../Simulations/repertoire_comparisons
Rscript visualizeSimResults.R

