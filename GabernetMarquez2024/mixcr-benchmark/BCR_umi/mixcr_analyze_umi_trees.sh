#!/bin/bash
set -euo pipefail

REP=( "rep010" "rep011" "rep012" "rep013" "rep014" "rep015" "rep016" "rep017" "rep018" "rep019" "rep111" "rep112" "rep113" "rep114" "rep115" )

echo "Start"
date

for i in "${!REP[@]}"
do
  echo "Processing ${REP[i]}"
  date
  mixcr analyze generic-bcr-amplicon-umi \
    --threads 20 \
    --library imgt \
    --species human \
    --rna \
    --tag-pattern "^(UMI:N{"12"})gtacaaaaa(R1:*)\^(R2:*)" \
    --rigid-left-alignment-boundary \
    --floating-right-alignment-boundary C \
    --assemble-clonotypes-by VDJRegion \
    --dont-split-clones-by C \
    --remove-step exportClones \
    "../../sim_repertoires/${REP[i]}_all_primers_reads_R1_renamed.fastq.gz" \
    "../../sim_repertoires/${REP[i]}_all_primers_reads_R2_renamed.fastq.gz" \
    "results/${REP[i]}"
  mixcr findAlleles \
    --threads 20 \
    --report results/${REP[i]}.findAlleles.report.txt \
    --json-report results/${REP[i]}.findAlleles.report.json \
    --export-alleles-mutations results/${REP[i]}_alleles.tsv \
    --export-library results/${REP[i]}_alleles.json \
    --output-template results/${REP[i]}.reassigned.clns \
    results/${REP[i]}.clns
  mixcr exportAirr results/${REP[i]}.vdjca results/${REP[i]}_AIRR_alignments.tsv
  mixcr exportAirr results/${REP[i]}.reassigned.clns results/${REP[i]}_AIRR_clones.tsv
  mixcr findShmTrees \
    --threads 20 \
    --report results/${REP[i]}_trees.log \
    results/${REP[i]}.reassigned.clns \
    results/${REP[i]}.shmt
  mixcr exportShmTreesWithNodes \
      results/${REP[i]}.shmt \
      results/${REP[i]}_trees.tsv
done

mixcr exportQc align results/*.vdjca results/alignQc.pdf

echo "End"
date