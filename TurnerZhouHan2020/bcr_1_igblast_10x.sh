#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu

# -c 16
# --mem-per-cpu=6G
# --constraint E5-2660_v3


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

# change-o v0.4.6

PATH_ROOT="/home/qz93/scratch60/" #*
PATH_10x="${PATH_ROOT}cr_bcr/"
PATH_IGBLAST="${PATH_ROOT}bcr_igblast/"

#TODO
PATH_LOG="${PATH_IGBLAST}igblast_allSamples_$(date '+%m%d%Y_%H%M%S').log"
PATH_COUNT="${PATH_IGBLAST}igblast_bySample_count_$(date '+%m%d%Y_%H%M%S').txt"

GERM_TAG="ref_201931-4_1Aug2019" #*
FN_IMGTVDJ="/home/qz93/germlines/ref_201931-4_1Aug2019/human_IG/IG*.fasta" #*

DIR_IGDATA="/home/qz93/apps/ncbi-igblast-1.14.0_ref_201931-4_1Aug2019" #*
PATH_EXEC="/home/qz93/apps/ncbi-igblast-1.14.0_ref_201931-4_1Aug2019/bin/igblastn" #*

SUBJ="P05" #*
PATH_SAMPLE_LIST="${PATH_IGBLAST}preprocessed_bcr_sampleList.txt" #*

#* run control
# TRUE/FALSE
RUN_IGBLAST=TRUE
RUN_MAKEDB=TRUE
RUN_ADDMETA=TRUE

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "germline" $GERM_TAG &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
#parallel --version | head -n 1 &>> "${PATH_LOG}"
"${PATH_EXEC}" -version &>> "${PATH_LOG}"
MakeDb.py --version &>> "${PATH_LOG}"
python3 -V &>> "${PATH_LOG}"

# loop thru
echo "sample/library list: ${PATH_SAMPLE_LIST}" &>> "${PATH_LOG}"

N_LINES=$(wc -l < "${PATH_SAMPLE_LIST}")
echo "N_LINES="$N_LINES &>> "${PATH_LOG}"

for ((IDX=1;IDX<=${N_LINES};IDX++)); do
	
	echo "IDX=${IDX}" &>> "${PATH_LOG}"

	# read Library Name from file
	CUR_LIB=$(sed "${IDX}q;d" "${PATH_SAMPLE_LIST}")
	echo "CUR_LIB=${CUR_LIB}" &>> "${PATH_LOG}"

	# cell ranger vdj output
	# eg: /home/qz93/scratch60/cr_bcr/d0_BCell-lib2/outs
	PATH_FASTA="${PATH_10x}${CUR_LIB}/outs/filtered_contig.fasta"
	PATH_CSV="${PATH_10x}${CUR_LIB}/outs/filtered_contig_annotations.csv"

	# d0_BCell-lib2
	# d0_BMPC_2-lib2
	# remove -lib2, -lib3
	SAMPLE="${CUR_LIB%-lib[23]}"
	echo "SAMPLE=${SAMPLE}" &>> "${PATH_LOG}"

	TIMEPOINT="$(eval echo ${SAMPLE} | cut -d "_" -f1)"
	SUBSET="$(eval echo ${SAMPLE} | cut -d "_" -f2)"

	# d0_BCell
	# d0_BMPC_2
	# count number of underscores
	NUM_UNDERSCORE="$(eval echo ${SAMPLE} | tr -cd "_" | wc -c)"
	# replicate number
	if [[ ${NUM_UNDERSCORE} == "1" ]]; then
		REP="1"
	else
		REP="$(eval echo ${SAMPLE} | cut -d "_" -f3)"
	fi

	echo "${SUBJ} ${TIMEPOINT} ${SUBSET} ${REP}" &>> "${PATH_LOG}"

	# create sample-specifc igblast folder: sample_[sample number]
	cd "${PATH_IGBLAST}"
	DIR_SAMPLE="${SAMPLE}/"

	# no error if existing
	mkdir -p "${DIR_SAMPLE}"

	# lib-specific log
	PATH_LOG_SAMPLE="${PATH_IGBLAST}${DIR_SAMPLE}igblast_${SAMPLE}_$(date '+%m%d%Y_%H%M%S').log"

	#######################
	##### run igblast #####
	#######################

	if [[ ${RUN_IGBLAST} == "TRUE" ]]; then

		if [[ (-s "${PATH_FASTA}" ) ]]; then

			# count reads in cell ranger filtered contig fasta
			COUNT_CR=`grep -c '^>' ${PATH_FASTA}`
			echo -e "${SAMPLE}_CR\t${COUNT_CR}" >> ${PATH_COUNT}

			echo "##### run IgBLAST #####" &>> "${PATH_LOG_SAMPLE}"

			# output: [outname]_igblast.fmt7

			# vdb, ddb, jdb: name of the custom V/D/J reference in IgBLAST database folder
			# if unspecified, default to imgt_<organism>_<loci>_v/d/j
			AssignGenes.py igblast \
				-s "${PATH_FASTA}" \
				-b "${DIR_IGDATA}" \
				--exec "${PATH_EXEC}" \
				--organism "human" \
				--loci "ig" \
				--vdb "imgt_human_ig_v" \
				--ddb "imgt_human_ig_d" \
				--jdb "imgt_human_ig_j" \
				--format "blast" \
				--outname "${SAMPLE}" \
				--outdir "${PATH_IGBLAST}${DIR_SAMPLE}" \
				--nproc "${NPROC}" \
				&>> "${PATH_LOG_SAMPLE}"

		else
			echo "${PATH_FASTA} does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi

	#######################################
	##### process output from IgBLAST #####
	#######################################

	if [[ ${RUN_MAKEDB} == "TRUE" ]]; then

		PATH_IGBLAST_SAMPLE="${PATH_IGBLAST}${DIR_SAMPLE}${SAMPLE}_igblast.fmt7"

		if [[ (-s "${PATH_IGBLAST_SAMPLE}" ) ]]; then
			
			echo "##### process output from IgBLAST #####" &>> "${PATH_LOG_SAMPLE}"

			# default: partial=False; asis-id=False
			# --partial: if specified, include incomplete V(D)J alignments in the pass file instead of the fail file

			# v0.4.6: Combined the extended field arguments of all subcommands 
			#   (--scores, --regions, --cdr3, and --junction) into a single --extended argument

			# IMPORTANT: do not put "" around FN_IMGTVDJ (otherwise * will be interpreted as is)
			# output: [outname]_db-pass.tab
			MakeDb.py igblast \
				-i "${PATH_IGBLAST_SAMPLE}" \
				-s "${PATH_FASTA}" \
				-r ${FN_IMGTVDJ} \
				--10x "${PATH_CSV}" \
				--extended \
				--failed \
				--partial \
				--format changeo \
				--outname "${SAMPLE}" \
				--outdir "${PATH_IGBLAST}${DIR_SAMPLE}" \
				&>> "${PATH_LOG_SAMPLE}"

			# count
			COUNT_LINES=$(wc -l ${PATH_IGBLAST}${DIR_SAMPLE}${SAMPLE}_db-pass.tab | awk '{print $1}')
			COUNT_SEQS=$((COUNT_LINES - 1))
			echo -e "${SAMPLE}_MakeDb\t${COUNT_SEQS}" >> ${PATH_COUNT}

		else
			echo "${PATH_IGBLAST_SAMPLE} does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi

	#########################
	##### Add meta info #####
	#########################

	if [[ ${RUN_ADDMETA} == "TRUE" ]]; then

		if [[ (-s "${PATH_IGBLAST}${DIR_SAMPLE}${SAMPLE}_db-pass.tab" ) ]]; then
			
			echo "##### add meta info #####" &>> "${PATH_LOG_SAMPLE}"

			echo "meta: ${SUBJ}_${TIMEPOINT}_${SUBSET}_${REP}" &>> "${PATH_LOG_SAMPLE}"

			# output: [outname]_parse-add.tab

			ParseDb.py add \
				-d "${PATH_IGBLAST}${DIR_SAMPLE}${SAMPLE}_db-pass.tab" \
				-f "SAMPLE" "SUBJECT" "TIMEPOINT" "CELL_TYPE" "REPLICATE" \
				-u "${SAMPLE}" "${SUBJ}" "${TIMEPOINT}" "${SUBSET}" "${REP}" \
				--outname "${SAMPLE}_db-pass" \
				--outdir "${PATH_IGBLAST}${DIR_SAMPLE}" \
				&>> "${PATH_LOG_SAMPLE}"

		else
			echo "${SAMPLE}_db-pass.tab does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi


done

echo "DONE" &>> "${PATH_LOG}"
