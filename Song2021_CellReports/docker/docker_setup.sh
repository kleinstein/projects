#!/bin/sh
# Ruoyi Jiang
# Building docker for replicating a publication

# cd to directory with Dockerfile

# Build
podman build -t ruoyijiang/ganymede:song2021 .

# Login
# podman login docker.io

# Push image
# podman push ruoyijiang/ganymede:song2021

# Pull image
# podman pull ruoyijiang/ganymede:song2021