#!/bin/bash
#SBATCH --partition=general
#SBATCH --job-name=clustering
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=60G
#SBATCH --time=02:00:00
#SBATCH --error=log/annotation.%A.err
#SBATCH --output=log/annotation.%A.out

module load miniconda
conda activate r_4.0

echo "[$0 $(date +%Y%m%d-%H%M%S)] [start] $SLURM_JOBID"

Rscript 8_annotation.R

echo "[$0 $(date +%Y%m%d-%H%M%S)] [end] $SLURM_JOBID"
