#!/usr/bin/bash
#SBATCH --job-name=q2
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=24:00:00
#SBATCH --exclude=c24n04

type=$1
masked=$2
threads=$3


maskCDR3.pl $1.tsv
igphyml --repfile $2.tsv     -m GY    --threads $threads --run_id GY --outrep $2\_GY.tsv
igphyml --repfile $2\_GY.tsv -m HLP17 --threads $threads --motifs FCH --run_id hlp17  --omegaOpt e,e --cbmat -o lr 
igphyml --repfile $2\_GY.tsv -m HLP17 --threads $threads --hotness 0 -f empirical --rootpi --run_id h0  --omegaOpt e,e --cbmat -o lr 
igphyml --repfile $2\_GY.tsv -m HLP19 --threads $threads --motifs FCH --run_id first  --omegaOpt e,e --cbmat -o lr
