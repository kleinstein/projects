%{
CP-OPT non-negative decomposition of platelet data

A. Konstorum
anna.konstorum@yale.edu
%}

%% Read in data
cd('~/data/')
day0=readtable('slice_day_0_r.csv','ReadRowNames',true,'Headerlines',1);
day0=table2array(day0);

day2=readtable('slice_day_2_r.csv','ReadRowNames',true,'Headerlines',1);
day2=table2array(day2);

day7=readtable('slice_day_7_r.csv','ReadRowNames',true,'Headerlines',1);
day7=table2array(day7);

day28=readtable('slice_day_28_r.csv','ReadRowNames',true,'Headerlines',1);
day28=table2array(day28);


%% Create tensor
% T_full is a multi-dimensional array in Matlab
% T_fullt is a tensor object for use with Tensor Toolbox

T_full_t(:,:,1)=day0;
T_full_t(:,:,2)=day2;
T_full_t(:,:,3)=day7; 
T_full_t(:,:,4)=day28;
T_fullt=tensor(T_full_t); % for use with Tensor toolbox

%% Perform multiple iterations of decompositions and ranks (cp_opt; Tensor Toolbox)
%{
Perform CP OPT decomposition with non-negativity constraints
using the Tensor Toolbox package.  
Need to install separate package: https://github.com/stephenbeckr/L-BFGS-B-C
Docs: https://www.tensortoolbox.org/cp_opt_doc.html
https://github.com/andrewssobral/tensor_toolbox/blob/master/cp_opt.m
%}  

% Implementation of the similarity score, as described in Williams et al.
% (https://doi.org/10.1016/j.neuron.2018.05.015), Equation 14
% Doc for score function: https://gitlab.com/tensors/tensor_toolbox/-/blob/master/@ktensor/score.m
% Error is normalized reconstruction error using Frobenius norm (Equation
% 13, same reference as above)


% initialize error and score matrices
max_rank=15;
rand_starts=100;
Merr_tot = cell(1,max_rank);
Mscore_tot = cell(1,max_rank);
M_all=cell(1, max_rank);
M_min_ind = [];

parfor i=[1:max_rank] % Rank
	Merr=[];
	Mscore=[];
    M = cell(1,rand_starts);
	for k=1:rand_starts % number trials
        i % track progress
        k % track progress
        rng(k)
		M{k}=cp_opt(T_fullt, i,'lower',0);
        err = norm(T_fullt-full(M{k}))/norm(T_fullt);
		Merr=[Merr err];
    end
    M_all{i} = M;
	[max_err,ind] = min(Merr);
    M_min_ind = [M_min_ind ind];
	Mopt=M{ind};
	for l=1:rand_starts
		if l~=ind
			score_M=score(M{l},Mopt);
		Mscore=[Mscore score_M];
		end
	end
	Merr_tot{i}=Merr;
	Mscore_tot{i}=Mscore;
end

Merr_tot_mat=zeros(max_rank,rand_starts);
Mscore_tot_mat=zeros(max_rank,rand_starts-1);

for i = [1:max_rank]
	Merr_tot_mat(i,:)=Merr_tot{i};
	Mscore_tot_mat(i,:)=Mscore_tot{i};
end

% These matrices hold quality metrics for rank

csvwrite('Merr_tot_mat_platelet.csv',Merr_tot_mat)
csvwrite('Mscore_tot_mat_platelet.csv',Mscore_tot_mat)


%% Save Decomposition with lowest error at rank 5

% Write each variable to file with normalized component scores 
csvwrite('genes_M5opt80.csv',M_all{5}{80}.U{1})
csvwrite('samples_M5opt80.csv',M_all{5}{80}.U{2})
csvwrite('study_M5opt80.csv',M_all{5}{80}.U{3})