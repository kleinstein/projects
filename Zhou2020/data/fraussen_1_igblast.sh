#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 3:00:00
#SBATCH --constraint E5-2660_v3
#SBATCH -c 3
#SBATCH --mem-per-cpu=20G

NPROC=3 #* this should match sbatch -c

# 3c x 20G
# Job Wall-clock time: 00:55:06
# Memory Utilized: 0.00 MB (estimated maximum)


# nx360h (E5-2660_v3) has a total max memory of 121GB per node

# IN: 
# - presto_2017-08-18_jason/[subj]_Memory_collapse-unique_atleast-2.fasta
# OUT: 
# - presto_2017-08-18_jason/1_[subj]_consistent.fasta
# - igblast/2_[subj].fmt7
# - igblast/3_[subj]_db-pass.tab
# - igblast/4_[subj]_parse-add.tab
# - igblast/5_[subj]_IGV-H/KL/HKL/NA.tab
# - igblast/5_[subj]_IGV-H/KL_QC.tab
# - igblast/6_[subj]_IGHV/IGKLV_FUNCTIONAL-T/F.tab, .RData

# the change-o commans in this file are intended to work with 0.4.5

PATH_PRESTO="/ysm-gpfs/home/qz93/scratch60/spatial_data/oconnor_dnHC/presto_2017-08-18_jason/"
PATH_IGBLAST="/ysm-gpfs/home/qz93/scratch60/spatial_data/oconnor_dnHC/igblast/"
# for remove_inconsistent_internalC.py, additional_qc_rest.py, split_chains.py
PATH_SCRIPT="/ysm-gpfs/home/qz93/projects/scripts/spatial/auxiliary/"


PRESTO_FILENAME="_Memory_collapse-unique_atleast-2"

#FN_IMGTVDJ="/ysm-gpfs/home/qz93/germlines/tag_2018_03_05/human/vdj/imgt_human_IGH[VDJ].fasta"
FN_IMGTVDJ_IGHKL="/ysm-gpfs/home/qz93/germlines/tag_201918-4_9May2019/human/vdj/imgt_human_IG*.fasta"
GERM_TAG="tag_201918-4_9May2019"

DIR_IGDATA="/home/qz93/apps/ncbi-igblast-1.14.0_tag-201918-4-9May2019"
PATH_EXEC="/home/qz93/apps/ncbi-igblast-1.14.0_tag-201918-4-9May2019/bin/igblastn"

LOG_PATH="${PATH_IGBLAST}igblast_$(date '+%m%d%Y_%H%M%S').log"
COUNT_PATH="${PATH_IGBLAST}igblast_bySubj_count_$(date '+%m%d%Y_%H%M%S').txt"

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${LOG_PATH}"
echo "germline" $GERM_TAG &>> "${LOG_PATH}"
echo "bash" $BASH_VERSION &>> "${LOG_PATH}"
"${PATH_EXEC}" -version &>> "${LOG_PATH}"
MakeDb.py --version &>> "${LOG_PATH}"
python3 -V &>> "${LOG_PATH}"


# for each subject

SUBJS=(HC024 HC038 HC263)

for SUBJ in ${SUBJS[@]}; do

	echo "############## STARTING ON " ${SUBJ} &>> "${LOG_PATH}"

	#################################################################
	##### 1: remove reads with mismatched C primer & internal C #####
	#################################################################

	echo "##### Remove reads with mismatched C primer & internal C #####" &>> ${LOG_PATH}

	# outputs:
	# ${SUBJ}_Memory_collapse-unique_atleast-2_consistent.fasta
	# ${SUBJ}_Memory_collapse-unique_atleast-2_inconsistent.fasta (if any)
	# ${SUBJ}_Memory_collapse-unique_atleast-2_inconsistent_count.txt (if any)
	python3 "${PATH_SCRIPT}remove_inconsistent_internalC.py" \
		"${PATH_PRESTO}${SUBJ}${PRESTO_FILENAME}.fasta"

	# count
	if [[ (-s ${COUNT_PATH}) ]]; then
		echo -e "---\t---" >> ${COUNT_PATH}
	else
		echo -e "SUBJECT\tCOUNT" > ${COUNT_PATH}
	fi

	mv "${PATH_PRESTO}${SUBJ}${PRESTO_FILENAME}_consistent.fasta" "${PATH_PRESTO}1_${SUBJ}_consistent.fasta"

	COUNT_FASTA=`grep -c '^>' ${PATH_PRESTO}1_${SUBJ}_consistent.fasta`
	echo -e "${SUBJ}_consistent\t${COUNT_FASTA}" >> ${COUNT_PATH}

	if [[ (-s "${PATH_PRESTO}${SUBJ}${PRESTO_FILENAME}_inconsistent.fasta") ]]; then
		mv "${PATH_PRESTO}${SUBJ}${PRESTO_FILENAME}_inconsistent.fasta" "${PATH_PRESTO}1_${SUBJ}_inconsistent.fasta"

		COUNT_FASTA=`grep -c '^>' ${PATH_PRESTO}1_${SUBJ}_inconsistent.fasta`
		echo -e "${SUBJ}_inconsistent\t${COUNT_FASTA}" >> ${COUNT_PATH}
	fi

	if [[ (-s "${PATH_PRESTO}${SUBJ}${PRESTO_FILENAME}_inconsistent_count.txt") ]]; then
		mv "${PATH_PRESTO}${SUBJ}${PRESTO_FILENAME}_inconsistent_count.txt" "${PATH_PRESTO}1_${SUBJ}_inconsistent_count.txt"
	fi	

	
	##########################
	##### 2: run IgBLAST #####
	##########################

	echo "***** run IgBLAST *****" &>> "${LOG_PATH}"

	# output: [outname]_igblast.fmt7

	# vdb, ddb, jdb: name of the custom V/D/J reference in IgBLAST database folder
	# if unspecified, default to imgt_<organism>_<loci>_v/d/j
	AssignGenes.py igblast \
		-s "${PATH_PRESTO}1_${SUBJ}_consistent.fasta" \
		-b "${DIR_IGDATA}" \
		--exec "${PATH_EXEC}" \
		--organism "human" \
		--loci "ig" \
		--vdb "imgt_human_ig_v" \
		--ddb "imgt_human_ig_d" \
		--jdb "imgt_human_ig_j" \
		--format "blast" \
		--outname "1_${SUBJ}_consistent" \
		--outdir "${PATH_IGBLAST}" \
		--nproc "${NPROC}" \
		&>> "${LOG_PATH}"


	mv "${PATH_IGBLAST}1_${SUBJ}_consistent_igblast.fmt7" "${PATH_IGBLAST}2_${SUBJ}.fmt7"

	##########################################
	##### 3: process output from IgBLAST #####
	##########################################

	echo "***** process output from IgBLAST *****" &>> "${LOG_PATH}"

	# default: partial=False; noparse=False
	# --partial: if specified, include incomplete V(D)J alignments in the pass file instead of the fail file

	# IMPORTANT: do not put "" around FN_IMGTVDJ_IGHKL (otherwise * will be interpreted as is)
	MakeDb.py igblast \
		-i "${PATH_IGBLAST}2_${SUBJ}.fmt7" \
		-s "${PATH_PRESTO}1_${SUBJ}_consistent.fasta" \
		-r ${FN_IMGTVDJ_IGHKL} \
		--regions \
		--cdr3 \
		--scores \
		--failed \
		--partial \
		--format changeo \
		--outname "3_${SUBJ}" \
		--outdir "${PATH_IGBLAST}" \
		&>> "${LOG_PATH}"

	# count
	COUNT_LINES=$(wc -l ${PATH_IGBLAST}3_${SUBJ}_db-pass.tab | awk '{print $1}')
	COUNT_SEQS=$((COUNT_LINES - 1))
	echo -e "${SUBJ}_MakeDb\t${COUNT_SEQS}" >> ${COUNT_PATH}


	###########################
	#### 4: add annotation ####
	###########################

	echo "***** add annotation *****" &>> "${LOG_PATH}"

	# add "_parse-add.tab" to outname
	ParseDb.py add \
		-d "${PATH_IGBLAST}3_${SUBJ}_db-pass.tab" \
		-f SUBJ \
		-u ${SUBJ} \
		--outname "4_${SUBJ}" \
		--outdir "${PATH_IGBLAST}" \
		&>> "${LOG_PATH}"
	

	############################################
	##### 5: split based on chains from V_CALL ####
	############################################

	# outputs:
	# 5_[SUBJ]_IG-H.tab (if any)
	# 5_[SUBJ]_IG-KL.tab (if any)
	# 5_[SUBJ]_IG-HKL.tab (if any)
	# 5_[SUBJ]_IG-NA.tab (if any)
	python3 "${PATH_SCRIPT}split_chains.py" \
		"${PATH_IGBLAST}4_${SUBJ}_parse-add.tab" \
		"${PATH_IGBLAST}5_${SUBJ}" \
		&>> "${LOG_PATH}"

	# count
	# these numbers should add up to MakeDb exactly
	if [[ (-s ${PATH_IGBLAST}5_${SUBJ}_IG-H.tab ) ]]; then
		COUNT_LINES=$(wc -l ${PATH_IGBLAST}5_${SUBJ}_IG-H.tab | awk '{print $1}')
		COUNT_SEQS=$((COUNT_LINES - 1))
		echo -e "${SUBJ}_IG-H\t${COUNT_SEQS}" >> ${COUNT_PATH}
	else
		echo -e "${SUBJ}_IG-H\t0" >> ${COUNT_PATH}
	fi

	if [[ (-s ${PATH_IGBLAST}5_${SUBJ}_IG-KL.tab ) ]]; then
		COUNT_LINES=$(wc -l ${PATH_IGBLAST}5_${SUBJ}_IG-KL.tab | awk '{print $1}')
		COUNT_SEQS=$((COUNT_LINES - 1))
		echo -e "${SUBJ}_IG-KL\t${COUNT_SEQS}" >> ${COUNT_PATH}
	else
		echo -e "${SUBJ}_IG-KL\t0" >> ${COUNT_PATH}
	fi

	if [[ (-s ${PATH_IGBLAST}5_${SUBJ}_IG-HKL.tab ) ]]; then
		COUNT_LINES=$(wc -l ${PATH_IGBLAST}5_${SUBJ}_IG-HKL.tab | awk '{print $1}')
		COUNT_SEQS=$((COUNT_LINES - 1))
		echo -e "${SUBJ}_IG-HKL\t${COUNT_SEQS}" >> ${COUNT_PATH}
	else
		echo -e "${SUBJ}_IG-HKL\t0" >> ${COUNT_PATH}
	fi

	if [[ (-s ${PATH_IGBLAST}5_${SUBJ}_IG-NA.tab ) ]]; then
		COUNT_LINES=$(wc -l ${PATH_IGBLAST}5_${SUBJ}_IG-NA.tab | awk '{print $1}')
		COUNT_SEQS=$((COUNT_LINES - 1))
		echo -e "${SUBJ}_IG-NA\t${COUNT_SEQS}" >> ${COUNT_PATH}
	else
		echo -e "${SUBJ}_IG-NA\t0" >> ${COUNT_PATH}
	fi

    #################
	##### 5+ QC #####
	#################

	# QC and count
	# these number should match log output from qc.py
	if [[ (-s ${PATH_IGBLAST}5_${SUBJ}_IG-H.tab ) ]]; then

		echo "QC for IG-H" &>> "${LOG_PATH}"

		# check for IGHV as V_CALL and IGK/L as isotype
		python3 "${PATH_SCRIPT}additional_qc_rest.py" \
			"${PATH_IGBLAST}5_${SUBJ}_IG-H.tab" \
			"${PATH_IGBLAST}5_${SUBJ}_IG-H_QC1.tab" \
			notRun run run run run run notRun run \
			&>> "${LOG_PATH}"

		if [[ (-s ${PATH_IGBLAST}5_${SUBJ}_IG-H_QC1.tab ) ]]; then
			COUNT_LINES=$(wc -l ${PATH_IGBLAST}5_${SUBJ}_IG-H_QC1.tab | awk '{print $1}')
			COUNT_SEQS=$((COUNT_LINES - 1))
			echo -e "${SUBJ}_IG-H_QC1\t${COUNT_SEQS}" >> ${COUNT_PATH}
		else
			echo -e "${SUBJ}_IG-H_QC1\t0" >> ${COUNT_PATH}
		fi
	else
		echo -e "${SUBJ}_IG-H_QC1\t0" >> ${COUNT_PATH}
	fi

	# not intending to use IG-KL, IG-HKL or IG-NA so skipping QC for these
	
	#######################################################
	##### 6: split into functional vs. non-functional #####
	#######################################################


	echo "***** separate functional seqs from non-functional ones *****" &>> "${LOG_PATH}"

	# attaches _FUNCTIONAL-T/F.tab to --outname

	if [[ (-s ${PATH_IGBLAST}5_${SUBJ}_IG-H_QC1.tab ) ]]; then
		ParseDb.py split \
		    -d "${PATH_IGBLAST}5_${SUBJ}_IG-H_QC1.tab" \
		    -f FUNCTIONAL \
		    --outname "6_${SUBJ}_IGHV" \
		    --outdir "${PATH_IGBLAST}" \
		    &>> "${LOG_PATH}"
	fi

    # count
    # IGHV functional
	if [[ -s "${PATH_IGBLAST}6_${SUBJ}_IGHV_FUNCTIONAL-T.tab" ]]; then
		COUNT_LINES=$(wc -l ${PATH_IGBLAST}6_${SUBJ}_IGHV_FUNCTIONAL-T.tab | awk '{print $1}')
		COUNT_SEQS=$((COUNT_LINES - 1))
		echo -e "${SUBJ}_IGHV_functional\t${COUNT_SEQS}" >> ${COUNT_PATH}
	else
		echo -e "${SUBJ}_IGHV_functional\t0" >> ${COUNT_PATH}
	fi

	# IGHV non-functional
	if [[ -s "${PATH_IGBLAST}6_${SUBJ}_IGHV_FUNCTIONAL-F.tab" ]]; then
		COUNT_LINES=$(wc -l ${PATH_IGBLAST}6_${SUBJ}_IGHV_FUNCTIONAL-F.tab | awk '{print $1}')
		COUNT_SEQS=$((COUNT_LINES - 1))
		echo -e "${SUBJ}_IGHV_non-functional\t${COUNT_SEQS}" >> ${COUNT_PATH}
	else
		echo -e "${SUBJ}_IGHV_non-functional\t0" >> ${COUNT_PATH}
	fi

	###################
	##### 6+: QC2 #####
	###################

	# only productively rearranged heavy chain reads that have passed QC1

	if [[ (-s "${PATH_IGBLAST}6_${SUBJ}_IGHV_FUNCTIONAL-T.tab" ) ]]; then

		echo "##### perform additional QC 2 #####" &>> "${LOG_PATH}"

		# check for 
		# - JUNCTION length multiple of 3
		python3 "${PATH_SCRIPT}additional_qc_rest.py" \
			"${PATH_IGBLAST}6_${SUBJ}_IGHV_FUNCTIONAL-T.tab" \
			"${PATH_IGBLAST}6_${SUBJ}_IGHV_FUNCTIONAL-T_QC2.tab" \
			notRun notRun notRun notRun notRun notRun run notRun \
			&>> "${LOG_PATH}"

		# count
		if [[ (-s ${PATH_IGBLAST}6_${SUBJ}_IGHV_FUNCTIONAL-T_QC2.tab ) ]]; then
			COUNT_LINES=$(wc -l ${PATH_IGBLAST}6_${SUBJ}_IGHV_FUNCTIONAL-T_QC2.tab | awk '{print $1}')
			COUNT_SEQS=$((COUNT_LINES - 1))
			echo -e "${SUBJ}_productive_QC2\t${COUNT_SEQS}" >> ${COUNT_PATH}
		else
			echo -e "${SUBJ}_productive_QC2\t0" >> ${COUNT_PATH}
		fi

	else
		echo "6_${SUBJ}_IGHV_FUNCTIONAL-T.tab does not exist" &>> "${LOG_PATH}"
	fi


	echo "############## FINISHED FOR " ${SUBJ} &>> "${LOG_PATH}"


done

