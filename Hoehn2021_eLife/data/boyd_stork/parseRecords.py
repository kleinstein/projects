
import os
import re

f = open("info/biosample_result-2.txt","r")
o = open("info/run_metadata.csv","w")

o.write("BioSample,SampleName,SRA,subject_id,sample_timepoint\n")

ids = re.compile("Identifiers: BioSample: (SAM\S+); Sample name: (\S+); SRA: (\S+)")
subj = re.compile(".*subject_id=\"(\S+)\".*")
time = re.compile(".*sample_timepoint=\"(\d+) days\".*")
for l in f:
    m = ids.match(l)
    ms = subj.match(l)
    mt = time.match(l)
    if m is not None:
        o.write(m.group(1) + "," + m.group(2) + "," + m.group(3))
    if ms is not None:
        o.write("," + ms.group(1))
    if mt is not None:
        o.write("," + mt.group(1))
    if not l.strip():
        o.write("\n")
