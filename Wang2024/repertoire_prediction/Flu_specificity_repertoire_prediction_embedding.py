#--------------------------------------------
# Predict class probability of HA protein binding
# 
# Example usage: 
#
# python Flu_specificity_repertoire_prediction_embedding.py ft-ESM2 HL HA_repertoires.parquet
#--------------------------------------------

import joblib
import torch
import argparse
import scipy
import pandas as pd
import numpy as np

parser = argparse.ArgumentParser(description="Predict class probability of HA protein binding. ")
parser.add_argument("embedding", type=str, help="Type of model (antiBERTy, antiBERTa2, BALM_paired, ft-ESM2)")
parser.add_argument("model", type=str, help="Type of model (HL, H)")
parser.add_argument("dataset", type=str, help="Dataset")
parser.add_argument("--random", type=bool, help="Use model trained with random labels", default=False)
args = parser.parse_args()

embedding = args.embedding
input = args.model
dataset = args.dataset

# 1. Load model and data
print(f"### Loading model and embeddings... ")
base_dir = "../data/"
random_suffix = ""
if args.random:
    random_suffix = "_random"
model_filename = base_dir + f"results/HA_{embedding}_FULL_{input}{random_suffix}.pkl"
model = joblib.load(model_filename)
sequence_filename = f"{base_dir}/{dataset}.parquet"
embedding_filename = base_dir + f"embeddings/{dataset}_{input}_{embedding}.pt"

seqs = pd.read_parquet(sequence_filename)
X = torch.load(embedding_filename, 
               map_location=torch.device('cpu')).numpy()
print(f"Loaded {model_filename} and {embedding_filename}")

# 2. Predicting
print(f"### Predicting...")
pred = model.predict_proba(X)[:,-1]
print(f"Predicted the binding probability of {pred.shape[0]} sequences.")

# 3. Load labels
seqs.loc[:,"pred_prob"] = pred

# 4. save the outputs as tsv
print(f"### Saving outputs...")
data_path = dataset.split('.', 1)[0]
out_path = base_dir + f"{data_path}_{input}_{embedding}_flu_embedding_prediction.csv"
seqs.to_csv(out_path)
print(f"Saved the output to {out_path}.")