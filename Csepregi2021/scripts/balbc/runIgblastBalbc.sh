# Kenneth B. Hoehn
# 8/26/2022
# Run IgBlast on a sample using BALB/c and IMGT mouse germlines

id=$1
threads=$2

echo $id $threads

export IGDATA=share/igblast 

# BALBc germline
ncbi-igblast-1.18.0/bin/igblastn \
    -germline_db_V share/igblast/database/imgt_balbc_ig_v\
    -germline_db_D share/igblast/database/imgt_balbc_ig_d \
    -germline_db_J share/igblast/database/imgt_balbc_ig_j \
    -domain_system imgt -ig_seqtype Ig -organism balbc \
    -auxiliary_data share/igblast/optional_file/mouse_gl.aux \
    -outfmt '7 std qseq sseq btop' \
    -query ../../fasta/$id\_collapse-unique_atleast-3.fasta \
    -out igblast_out/$id\_balbc\.fmt7 -num_threads $threads

MakeDb.py igblast -i igblast_out/$id\_balbc\.fmt7 \
    -s ../../fasta/$id\_collapse-unique_atleast-3.fasta \
    -r \
    share/germlines/imgt/balbc/vdj/imgt_balbc_IGHV.fasta \
    share/germlines/imgt/balbc/vdj/imgt_balbc_IGHD.fasta \
    share/germlines/imgt/balbc/vdj/imgt_balbc_IGHJ.fasta \
    --extended --fail --asis-calls --outdir processed

# IMGT germline
ncbi-igblast-1.18.0/bin/igblastn \
    -germline_db_V share/igblast/database/imgt_mouse_ig_v\
    -germline_db_D share/igblast/database/imgt_mouse_ig_d \
    -germline_db_J share/igblast/database/imgt_mouse_ig_j \
    -domain_system imgt -ig_seqtype Ig -organism mouse \
    -auxiliary_data share/igblast/optional_file/mouse_gl.aux \
    -outfmt '7 std qseq sseq btop' \
    -query ../../fasta/$id\_collapse-unique_atleast-3.fasta \
    -out igblast_out/$id\_mouse.fmt7 -num_threads $threads

MakeDb.py igblast -i igblast_out/$id\_mouse.fmt7 \
    -s ../../fasta/$id\_collapse-unique_atleast-3.fasta \
    -r \
    share/germlines/imgt/mouse/vdj/imgt_mouse_IGHV.fasta \
    share/germlines/imgt/mouse/vdj/imgt_mouse_IGHD.fasta \
    share/germlines/imgt/mouse/vdj/imgt_mouse_IGHJ.fasta \
    --extended --fail --outdir processed
