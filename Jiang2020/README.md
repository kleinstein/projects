The code in this repository will reproduce the analysis and figures in Jiang et al. 2020 (JCI Insight, https://pubmed.ncbi.nlm.nih.gov/32573488/) using the appropriate software versions. 

This code requires that the 'podman' package be downloaded (https://podman.io/getting-started/installation.html) to use docker images. 

The overall structure of this directory which is also available on the docker: `notebooks` contains analysis notebooks, `docker` contains the files to make the docker image, `musk_tree_code` contains code for drawing the trees in the publication, `data_example` contains meta data csv tables (e.g. AIRR_sample.csv) that should be filled in and deposited in the directory to be mounted when running the docker image (see below).

### Download docker image

First download the image, and check that you can enter the shell. We show code for a dummy directory to be mounted `/path_to_mount_data_dir`. See https://docs.docker.com/storage/volumes/ for details on how volumes work. 

```{bash}
# use this command to download the image
podman pull ruoyijiang/ganymede:jiangjci2020

# run without mounting to test it is working, change port '8889' if occupied.
podman run -it  -p 8889:8889 ruoyijiang/ganymede:jiangjci2020 bash

# run the image with a mounted directory (dummy directory shown)
podman run -it  -p 8889:8889 -v /path_to_mount_data_dir:/data:z ruoyijiang/ganymede:jiangjci2020 bash
```

Now try launching a notebook from inside the interactive container shell

```{bash}
# to launch notebooks from within the container shell, run
jupyter notebook --no-browser --ip 0.0.0.0 --allow-root --port=8889
```

After this last step, type `localhost:8889` in your local web browser to connect and fill in the token from the container shell standard output. Control-C to kill the notebook if needed.

Click to `/hg/projects/Jiang2020` to find the notebooks relevant to this study. 




### Download the relevant files and arrange

The NCBI accession GSE149133 can be used to pull data (https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE149133). 

If a lot of difficulty is found trying to identify where these files are found, please contact the corresponding author who will contact me (Roy). The consolidated Change-O/AIRR table, GEX matrices and the meta-data are the most important for reproducing the two main analysis notebooks. 

- meta_information: please fill this out and deposit this as shown in your directory that will be mounted. Examples are in the `data_example` folder. 

```{bash}
/path_to_mount_data_dir
	/AIRR_sample.csv
	
/path_to_mount_data_dir
	/SingleCell_sample.csv
```

- raw presto reads: please deposit this as shown, where MG001 is the name of the sample. Make the appropriate directories as shown. 
	
```{bash}
/path_to_mount_data_dir
	/raw
		/MG001
			/R1.fastq.gz
			/R2.fastq.gz
```
	
- sanger input csv: please deposit this as shown. Make the appropriate directories as shown. 

```{bash}
/path_to_mount_data_dir
	/sanger
		/sanger.csv
```
		
- cellranger 10X VDJ outputs: please deposit this as shown. Make the appropriate directories as shown. 

```{bash}
/path_to_mount_data_dir
	/10x_bcr_vdj
		/MG139B_NOIGHD_BCRVDJ_VHT
			/outs
				/filtered_contig.fasta
				...
```

- cellranger 10X filtered matrix outputs: please deposit this as shown. Make the appropriate directories as shown. 

```{bash}
/path_to_mount_data_dir
	/10x_gex
		/MG139B_NOIGHD_HHT
			/outs
				/filtered_feature_bc_matrix
				...
```

### Bulk sequencing pre-processing

To process IGH presto sequences, run the following script for each sample. This takes a long time without HPC support so we recommend you avoid this step by starting with the downloaded Change-O/AIRR format table instead (this should be uploaded along with GSE149133 to NCBI if it is not there already). `Processing_Bulk.ipynb` provides a print-out of an iterated script for doing this assuming the files were downloaded and assembled as described above at the mount point (`/path_to_mount_data_dir` dummy directory). 

```{bash}
# The name of the RAW_DIR directory will be used as the name of the sample. Please use the exact sample names from meta_information
RAW_DIR = /data/raw/MG001
RUN_DIR = /data/presto

# run the following command
image presto_nocregion.sh $RAW_DIR $RUN_DIR

# To save space...
rm -rf $RUN_DIR/*/presto 
```

Next, use `Processing_Bulk.ipynb` to read-in files and output the combined Change-O dataframe. This depends on `AIRR_sample.csv`. A file `/path_to_mount_data_dir/bulk_unique.tab` should be generated.



### 10X VDJ pre-processing

Follow the instructions in `Processing_10X.ipynb`. This assumes that `SingleCell_sample.csv` has been filled out correctly. A `changeo` directory should be generated with a path like this in each of the 10X default output directories:

`/path_to_mount_data_dir/10x_bcr_vdj/MG139B_NOIGHD_BCRVDJ_VHT/outs/changeo`



### Processing Sanger-derived V(D)J sequences

Follow the instructions in `Processing_Sanger.ipynb`. These sequences are manually curated from Sanger sequencing experiments. 

This should output two files, `MUSK_db-pass.tab`, `MUSK_db-pass_germ-pass.tab` for further analysis. 


### Analysis

For analysis of bulk repertoires, start with `R_PreparePlot.ipynb`. This will generate files that can then be used for `R_GEX-Seurat.ipynb`. 

`R_CD20CITESeq.ipynb` was used for generating a figure correlating surface CD20 and mRNA from a published dataset. 

For generating trees, please refer to the `musk_tree_code` folder. 
