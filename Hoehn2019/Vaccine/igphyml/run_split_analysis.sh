#/usr/bin/bash


module load R

bases=("IB/+7d/seqs_3000")

for base in "${bases[@]}"
do
	perl  splitRepertoire.pl $base\_ncdr_lineages.tsv $base\_linSize.txt
	Rscript splitD7.R $base\_linSize.txt $base\_ncdr
	
	qs=(1 2 3 4)
	for q in "${qs[@]}"
	do
		sbatch --cpus-per-task=16 --partition=general runq.sh $base\_ncdr\_q$q 16
	done
done
