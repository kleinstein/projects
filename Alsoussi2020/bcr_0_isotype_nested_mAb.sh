# BLAST IGH seqs from P04 & P05 GC that passed IMGT alignment & MakeDb
# against IMGT reference IGHC

# note on paths: 
# Dropbox path did not work due to presence of space, despite using quote marks
# created a tmp "blast/" folder on Desktop/ that mirrors igblast/IGH_QC_FAIL/

PATH_BIN="/Users/jkewz/apps/ncbi-blast-2.9.0+/bin/"
PATH_BLASTN="${PATH_BIN}blastn"

"${PATH_BLASTN}" -version # blast 2.9.0, build Mar 11 2019 16:04:57
# "${PATH_BLASTN}" -help > "blastn.txt"


PATH_ROOT="/Users/jkewz/Desktop/blast_constant/"
PATH_IMGT="${PATH_ROOT}"
PATH_BL="${PATH_ROOT}"

# query files
QUERY_ARRAY=(clean_nested_mAb)

# task (what type of blast to run)
#BLAST_TYPE_ARRAY=(megablast blastn blastn-short)
BLAST_TYPE_ARRAY=(blastn)
#BLAST_TYPE_ARRAY=(blastn-short)

# do not allow reverse complement match
# STRAND="plus"
STRAND="both"

for QUERY in ${QUERY_ARRAY[@]}; do

	# make blastn index

	PATH_DB_FASTA="${PATH_IMGT}C57BL6_IG_constant_ref_202011-3.fasta"
	NAME_DB="C57BL6_IG_constant"

	#* note that this is stored in bcr/nested_mAb/ and only copied into blast folder when running blast
	NAME_QUERY="${QUERY}.fasta"
	PATH_QUERY="${PATH_ROOT}${NAME_QUERY}"
	
	cd "${PATH_BL}"
	"${PATH_BIN}makeblastdb" -in "${PATH_DB_FASTA}" -input_type fasta -dbtype nucl -out "${NAME_DB}"
	
	
	for BLAST_TYPE in ${BLAST_TYPE_ARRAY[@]}; do
		
		NAME_OUT="${BLAST_TYPE}_${STRAND}_${QUERY}.fmt6"

		if [[ (-s "${PATH_QUERY}" ) ]]; then

			echo "*** Running: ${NAME_QUERY}, ${BLAST_TYPE}, strand=${STRAND}..."

			# task: blastn, blastn-short, megablast (default), dc-megablast, rmblastn
			# query: input file name
			# db: BLAST database name

			# qseqid: Query Seq-id
			# qseq: Aligned part of query sequence
			# sseq: Aligned part of subject sequence
			# qstart: Start of alignment in query
			# qend: End of alignment in query
			# sstart: Start of alignment in subject
			# send: End of alignment in subject
			# evalue: Expect value
			# bitscore: Bit score
			# length: Alignment length
			# pident: Percentage of identical matches
			# nident: Number of identical matches
			# mismatch: Number of mismatches
			# gaps: Total number of gaps

			"${PATH_BLASTN}" \
				-task "${BLAST_TYPE}" \
				-strand "${STRAND}" \
				-query "${PATH_QUERY}" \
				-db "${PATH_BL}${NAME_DB}" \
				-max_target_seqs 3 \
				-out  "${PATH_BL}${NAME_OUT}" \
				-outfmt "6 qseqid sseqid qseq sseq pident nident mismatch gaps qstart qend sstart send evalue bitscore length" \
				-num_threads 7

		else
			echo "${NAME_QUERY} does not exist"
		fi
	done
done


#wc -l "${PATH_OUT}"*.fmt6

