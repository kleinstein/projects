Scripts for Nouri et al, 2020. PlosCB
------------------------------------------------------------------------

This directory contains scripts for Nouri et al, 2020. PlosCB. 
The data is available in ig.med.yale.edu:/mnt/data2/projects/nn282_sim. 
The analysis was conducted in following scripts:

1. `clone.R` infer the clonal relationships
2. `stats.R` calculates the accuracy measurements
3. `plot_stats.R` plots the accuracy summary
4. `plot_summary.R` plots supplementary figure
5. `qgraph.R` plots qgraph fig6
6. `plot_timing.R` plots timing figure 
7. `dengue_cross_spc_test.R` calculates specificity for the cross-sample dengue test
8. `random_pairwise_shared_mutation.R` claculates the pairwise mutatins in random-arbitrary clones
