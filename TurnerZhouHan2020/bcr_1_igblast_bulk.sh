#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 1-
#SBATCH --constraint E5-2660_v3
#SBATCH -c 20
#SBATCH --mem-per-cpu=2G


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

# IN: 
# - presto/[sampleName]/[sampleName]-final_collapse-unique_atleast-2.fasta
# OUT: 
# - igblast/[subj].fmt7
# - igblast/[subj]_db-pass.tab
# - igblast/[subj]_db-pass_parse-add.tab
# - igblast/[subj]_IG-H/KL/HKL/NA.tab
# - igblast/[subj]_IG-H_QC.tab
# - igblast/[subj]_IGHV_FUNCTIONAL-T/F.tab


PATH_ROOT="/home/qz93/project/ellebedy_pairedBulk/" #*
PATH_PRESTO="${PATH_ROOT}presto/"
PATH_IGBLAST="${PATH_ROOT}igblast/"

PATH_SCRIPT_SPLITCHAIN="/home/qz93/projects/scripts/turnerZhouHan_2020/JZ_split_chains.py"
PATH_SCRIPT_ADDTNLQZ="/home/qz93/projects/scripts/turnerZhouHan_2020/JZ_additional_qc.py"

PATH_LOG="${PATH_IGBLAST}igblast_allSamples_$(date '+%m%d%Y_%H%M%S').log"
PATH_COUNT="${PATH_IGBLAST}igblast_bySample_count_$(date '+%m%d%Y_%H%M%S').txt"

GERM_TAG="ref_201931-4_1Aug2019" #*
FN_IMGTVDJ="/home/qz93/germlines/ref_201931-4_1Aug2019/human_IG/IG*.fasta" #*

DIR_IGDATA="/home/qz93/apps/ncbi-igblast-1.14.0_ref_201931-4_1Aug2019" #*
PATH_EXEC="/home/qz93/apps/ncbi-igblast-1.14.0_ref_201931-4_1Aug2019/bin/igblastn" #*

FILENAME_SUFFIX_PRESTO="-final_collapse-unique_atleast-2.fasta" #*

# subjects/samples
#SUBJ_ARRAY=(S8) # testing
SUBJ_ARRAY=(S7 S8 S9 S10 S11 S12)


#* run control
# TRUE/FALSE
RUN_IGBLAST=TRUE
RUN_MAKEDB=TRUE
RUN_ADDMETA=TRUE
RUN_SPLITCHAIN=TRUE
RUN_ADDTNLQC=TRUE
RUN_SPLITNPR=TRUE

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "germline" $GERM_TAG &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
parallel --version | head -n 1 &>> "${PATH_LOG}"
"${PATH_EXEC}" -version &>> "${PATH_LOG}"
MakeDb.py --version &>> "${PATH_LOG}"
python3 -V &>> "${PATH_LOG}"

# for each subject
for SUBJ in ${SUBJ_ARRAY[@]}; do
	
	echo "*********** STARTING ON ${SUBJ} ***********" &>> "${PATH_LOG}" # main log

	# path to presto input fasta (not fastq)
	PATH_PRESTO_SAMPLE="${PATH_PRESTO}${SUBJ}/re_${SUBJ}${FILENAME_SUFFIX_PRESTO}"

	# create sample-specifc igblast folder: sample_[sample number]
	cd "${PATH_IGBLAST}"
	DIR_SAMPLE="${SUBJ}/"

	# no error if existing
	mkdir -p "${DIR_SAMPLE}"

	# start sample-specific log file
	PATH_LOG_SAMPLE="${PATH_IGBLAST}${DIR_SAMPLE}igblast_sample-${SUBJ}_$(date '+%m%d%Y_%H%M%S').log"
	dt=$(date '+%d/%m/%Y %H:%M:%S')
	echo "$dt" &> "${PATH_LOG_SAMPLE}"

	# initiate count file
	if [[ (-s ${PATH_COUNT}) ]]; then
		echo -e "---\t---" >> ${PATH_COUNT}
	else
		echo -e "SAMPLE\tCOUNT" > ${PATH_COUNT}
	fi

	#######################
	##### run igblast #####
	#######################

	if [[ ${RUN_IGBLAST} == "TRUE" ]]; then

		if [[ (-s "${PATH_PRESTO_SAMPLE}" ) ]]; then

			# count reads in presto-abseq fasta
			COUNT_PRESTO=`grep -c '^>' ${PATH_PRESTO_SAMPLE}`
			echo -e "${SUBJ}_presto\t${COUNT_PRESTO}" >> ${PATH_COUNT}

			echo "##### run IgBLAST #####" &>> "${PATH_LOG_SAMPLE}"

			# output: [outname]_igblast.fmt7

			# vdb, ddb, jdb: name of the custom V/D/J reference in IgBLAST database folder
			# if unspecified, default to imgt_<organism>_<loci>_v/d/j
			AssignGenes.py igblast \
				-s "${PATH_PRESTO_SAMPLE}" \
				-b "${DIR_IGDATA}" \
				--exec "${PATH_EXEC}" \
				--organism "human" \
				--loci "ig" \
				--vdb "imgt_human_ig_v" \
				--ddb "imgt_human_ig_d" \
				--jdb "imgt_human_ig_j" \
				--format "blast" \
				--outname "${SUBJ}" \
				--outdir "${PATH_IGBLAST}${DIR_SAMPLE}" \
				--nproc "${NPROC}" \
				&>> "${PATH_LOG_SAMPLE}"

		else
			echo "${PATH_PRESTO_SAMPLE} does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi

	#######################################
	##### process output from IgBLAST #####
	#######################################

	if [[ ${RUN_MAKEDB} == "TRUE" ]]; then

		PATH_IGBLAST_SAMPLE="${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_igblast.fmt7"

		if [[ (-s "${PATH_IGBLAST_SAMPLE}" ) ]]; then
			
			echo "##### process output from IgBLAST #####" &>> "${PATH_LOG_SAMPLE}"

			# default: partial=False; asis-id=False
			# --partial: if specified, include incomplete V(D)J alignments in the pass file instead of the fail file

			# IMPORTANT: do not put "" around FN_IMGTVDJ (otherwise * will be interpreted as is)
			# output: [outname]_db-pass.tab
			MakeDb.py igblast \
				-i "${PATH_IGBLAST_SAMPLE}" \
				-s "${PATH_PRESTO_SAMPLE}" \
				-r ${FN_IMGTVDJ} \
				--regions \
				--cdr3 \
				--scores \
				--failed \
				--partial \
				--format changeo \
				--outname "${SUBJ}" \
				--outdir "${PATH_IGBLAST}${DIR_SAMPLE}" \
				&>> "${PATH_LOG_SAMPLE}"

			# count
			COUNT_LINES=$(wc -l ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_db-pass.tab | awk '{print $1}')
			COUNT_SEQS=$((COUNT_LINES - 1))
			echo -e "${SUBJ}_MakeDb\t${COUNT_SEQS}" >> ${PATH_COUNT}

		else
			echo "${PATH_IGBLAST_SAMPLE} does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi

	#########################
	##### Add meta info #####
	#########################

	# From A Scjhmitz
	# Sample  Study_Subject_Timepoint_CellType
	# 7		  321_04_d05_ASC
    # 8		  321_05_d05_ASC
	# 9   	  321_11_d06_ASC
	# 10	  321_04_d0_PBMCs
	# 11	  321_05_d0_PBMCs
	# 12	  321_11_d0_PBMCs

	if [[ ${RUN_ADDMETA} == "TRUE" ]]; then

		if [[ (-s "${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_db-pass.tab" ) ]]; then
			
			echo "##### add meta info #####" &>> "${PATH_LOG_SAMPLE}"

			# sample
			META_SAMP="${SUBJ}"

			# subject, timepoint, cell type
			if [[ ${SUBJ} == "S7" ]]; then
				META_SUBJ="P04"
				META_TP="d5"
				META_CELL="PB"
			elif [[ ${SUBJ} == "S8" ]]; then
				META_SUBJ="P05"
				META_TP="d5"
				META_CELL="PB"
			elif [[ ${SUBJ} == "S9" ]]; then
				META_SUBJ="P11"
				META_TP="d6"
				META_CELL="PB"
			elif [[ ${SUBJ} == "S10" ]]; then
				META_SUBJ="P04"
				META_TP="d0"
				META_CELL="PBMC"
			elif [[ ${SUBJ} == "S11" ]]; then
				META_SUBJ="P05"
				META_TP="d0"
				META_CELL="PBMC"
			elif [[ ${SUBJ} == "S12" ]]; then
				META_SUBJ="P11"
				META_TP="d0"
				META_CELL="PBMC"			
			fi

			echo "${META_SAMP}_${META_SUBJ}_${META_TP}_${META_CELL}" &>> "${PATH_LOG_SAMPLE}"

			# output: [outname]_parse-add.tab

			ParseDb.py add \
				-d "${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_db-pass.tab" \
				-f "SAMPLE" "SUBJECT" "TIMEPOINT" "CELL_TYPE" \
				-u "${META_SAMP}" "${META_SUBJ}" "${META_TP}" "${META_CELL}" \
				--outname "${SUBJ}_db-pass" \
				--outdir "${PATH_IGBLAST}${DIR_SAMPLE}" \
				&>> "${PATH_LOG_SAMPLE}"

		else
			echo "${SUBJ}_db-pass.tab does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi


	############################################
	##### split based on chains from V_CALL ####
	############################################

	if [[ ${RUN_SPLITCHAIN} == "TRUE" ]]; then
		
		if [[ (-s "${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_db-pass_parse-add.tab" ) ]]; then

			echo "##### split seqs based on chains from V_CALL & J_CALL #####" &>> "${PATH_LOG_SAMPLE}"

			# outputs:
			# [SUBJ]_IG-H.tab (if any)
			# [SUBJ]_IG-KL.tab (if any)
			# [SUBJ]_IG-HKL.tab (if any)
			# [SUBJ]_IG-NA.tab (if any)
			python3 "${PATH_SCRIPT_SPLITCHAIN}" \
				"${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_db-pass_parse-add.tab" \
				"${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}" \
				&>> "${PATH_LOG_SAMPLE}"			

			# count
			# these numbers should add up to MakeDb-pass exactly
			if [[ (-s ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-H.tab ) ]]; then
				COUNT_LINES=$(wc -l ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-H.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_IG-H\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_IG-H\t0" >> ${PATH_COUNT}
			fi

			if [[ (-s ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-KL.tab ) ]]; then
				COUNT_LINES=$(wc -l ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-KL.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_IG-KL\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_IG-KL\t0" >> ${PATH_COUNT}
			fi

			if [[ (-s ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-HKL.tab ) ]]; then
				COUNT_LINES=$(wc -l ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-HKL.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_IG-HKL\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_IG-HKL\t0" >> ${PATH_COUNT}
			fi

			if [[ (-s ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-NA.tab ) ]]; then
				COUNT_LINES=$(wc -l ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-NA.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_IG-NA\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_IG-NA\t0" >> ${PATH_COUNT}
			fi

		else
			echo "${SUBJ}_db-pass.tab does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi

	#########################
	##### additional QC #####
	#########################

	if [[ ${RUN_ADDTNLQC} == "TRUE" ]]; then

		# only reads igblasted to heavy chain V & J genes are used

		if [[ (-s "${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-H.tab" ) ]]; then

			echo "##### perform additional QC #####" &>> "${PATH_LOG_SAMPLE}"

			# check for 
			# - V length must >=310
			# - % of N's in V up to & including V_GERM_LENGTH_IMGT must < 10%
			# - num of IMGT gaps in V up to and including 
			#   min(312, V_GERM_LENGTH_IMGT) must < 10
			# - no "None" value in GERMLINE_IMGT
			# - no missing value in columns PRCONS, CREGION, GERMLINE_IMGT, JUNCTION
			# - JUNCTION length multiple of 3
			python3 "${PATH_SCRIPT_ADDTNLQZ}" \
				"${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-H.tab" \
				"${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-H_QC.tab" \
				notRun run run run run run run \
				&>> "${PATH_LOG_SAMPLE}"

			# count
			if [[ (-s ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-H_QC.tab ) ]]; then
				COUNT_LINES=$(wc -l ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-H_QC.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_IG-H_QC\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_IG-H_QC\t0" >> ${PATH_COUNT}
			fi

		else
			echo "${SUBJ}_IG-H.tab does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi

	####################################################
	##### split into productive vs. non-productive #####
	####################################################

	if [[ ${RUN_SPLITNPR} == "TRUE" ]]; then

		if [[ (-s "${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-H_QC.tab" ) ]]; then
			
			echo "##### separate productively rearranged seqs from non-productively ones #####" &>> "${PATH_LOG_SAMPLE}"

			# terminology
			# igblast: functional/non-functional, equivalent to
			# IMGT: productively/non-productively rearranged

			# output: [outname]_FUNCTIONAL-T/F.tab

			ParseDb.py split \
		    	-d "${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IG-H_QC.tab" \
		    	-f FUNCTIONAL \
		    	--outname "${SUBJ}_IGHV" \
		    	--outdir "${PATH_IGBLAST}${DIR_SAMPLE}" \
		    	&>> "${PATH_LOG_SAMPLE}"
			
		    # count
		    # IGHV productively rearranged
			if [[ -s "${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IGHV_FUNCTIONAL-T.tab" ]]; then
				COUNT_LINES=$(wc -l ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IGHV_FUNCTIONAL-T.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_productive\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_productive\t0" >> ${PATH_COUNT}
			fi

			# IGHV non-productively rearranged
			if [[ -s "${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IGHV_FUNCTIONAL-F.tab" ]]; then
				COUNT_LINES=$(wc -l ${PATH_IGBLAST}${DIR_SAMPLE}${SUBJ}_IGHV_FUNCTIONAL-F.tab | awk '{print $1}')
				COUNT_SEQS=$((COUNT_LINES - 1))
				echo -e "${SUBJ}_non-productive\t${COUNT_SEQS}" >> ${PATH_COUNT}
			else
				echo -e "${SUBJ}_non-productive\t0" >> ${PATH_COUNT}
			fi

		else
			echo "${SUBJ}_IG-H_QC.tab does not exist" &>> "${PATH_LOG_SAMPLE}"
		fi

	fi

	echo "*********** FINISHED FOR ${SUBJ} ***********" &>> "${PATH_LOG}" # main log

done

