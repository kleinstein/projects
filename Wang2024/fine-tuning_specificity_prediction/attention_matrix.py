#--------------------------------------------
# Save attention activations given a BCR sequence
#
# Example usage:
#
# python attention_matrix.py ft-ESM2 HL S_FULL.parquet 1 checkpoint-7290
#--------------------------------------------
import pickle
import argparse
import re
import time
from datetime import date
import random
import os
from collections import Counter

import torch
import pandas as pd
import numpy as np
from Bio import SeqIO
import antiberty
import transformers
from transformers import (
    AutoTokenizer,
    AutoModelForSequenceClassification,
    Trainer,
    TrainingArguments
)
from torch.utils.data import DataLoader
from datasets import (
    Dataset,
    DatasetDict,
    Sequence,
    ClassLabel
)
from sklearn.metrics import (
    precision_score,
    recall_score,
    f1_score,
    matthews_corrcoef,
    roc_auc_score,
    average_precision_score,
    balanced_accuracy_score
)
from sklearn.model_selection import (
    StratifiedGroupKFold
)
import json
import scipy
import numpy as np
#--------------------------------------------
parser = argparse.ArgumentParser(description="Gene usage tasks")
parser.add_argument("embedding", type=str, help="Type of model (BALM_paired)")
parser.add_argument("input", type=str, help="Type of model (HL, H)")
parser.add_argument("dataset", type=str, help="Dataset parquet file")
parser.add_argument("antibody", type=str, help="Index to the antibody")
parser.add_argument("checkpoint", type=str, help="The checkpoint to load the model")
parser.add_argument("--finetuned", type=bool, default=True, help="Whether to load finetuned models",
                    action=argparse.BooleanOptionalAction)
args = parser.parse_args()

embedding = args.embedding
input = args.input
dataset = args.dataset
antibody = args.antibody
checkpoint = args.checkpoint
use_finetuned = args.finetuned
#--------------------------------------------
def load_finetuned_model(model_dir):
    tokenizer = AutoTokenizer.from_pretrained(model_dir)
    model = AutoModelForSequenceClassification.from_pretrained(model_dir, num_labels=2)
    model = model.cuda()
    model_size = sum(p.numel() for p in model.parameters())
    print(f"Model size: {model_size/1e6:.2f}M")
    return model, tokenizer

def return_model_dir(model):
    if "BALM_paired" in model:
        base_dir = '../models/'
        model_dir = base_dir + 'BALM-paired_LC-coherence_90-5-5-split_122222'
        token_dir = model_dir

    elif "ESM2" in model:
        model_dir = "facebook/esm2_t33_650M_UR50D"
        token_dir = model_dir
        if "finetuned" in model:
            base_dir = '../models/'
            model_dir = base_dir + 'ESM2-650M_paired-fine-tuning/'

    elif "antiBERTa2" in model:
        model_dir = token_dir = "alchemab/antiberta2"

    elif "antiBERTy" in model:
        project_path = os.path.dirname(os.path.realpath(antiberty.__file__))
        trained_models_dir = os.path.join(project_path, 'trained_models')
        model_dir = os.path.join(trained_models_dir, 'AntiBERTy_md_smooth')
        token_dir = os.path.join(trained_models_dir, 'vocab.txt')
    return model_dir, token_dir

def load_pretrained_model(model):
    model_dir, token_dir = return_model_dir(model)
    if 'antiBERTy' in model:
        tokenizer = transformers.BertTokenizer(vocab_file=token_dir, do_lower_case=False)
    else:
        tokenizer = AutoTokenizer.from_pretrained(token_dir)
    model = AutoModelForSequenceClassification.from_pretrained(model_dir, num_labels=2)
    model = model.cuda()
    model_size = sum(p.numel() for p in model.parameters())
    print(f"Model size: {model_size/1e6:.2f}M")
    return model, tokenizer

def preprocess(batch):
    t_inputs = tokenizer(batch['sequence'],
                         padding="max_length",
        truncation=True,
        max_length=MAX_LENGTH,
        return_special_tokens_mask=True)
    batch['input_ids'] = t_inputs.input_ids
    batch['attention_mask'] = t_inputs.attention_mask
    return batch

def process_data(train):
    ab_dataset = DatasetDict({
        "train": Dataset.from_pandas(train)
    })
    ab_dataset_featurised = ab_dataset.map(
        lambda seq: {
            "sequence": seq,
        },
        input_columns=["HL"], batched=True
    )
    ab_dataset_tokenized = ab_dataset_featurised.map(
        preprocess,
        batched=True,
        remove_columns=['H', 'L', 'HL']
    )
    return ab_dataset_tokenized

def insert_space_every_other_except_cls(input_string):
    parts = input_string.split('[CLS]')
    modified_parts = [''.join([char + ' ' for char in part]).strip() for part in parts]
    result = ' [CLS] '.join(modified_parts)
    return result

# Compare before and after fine-tuning
# Forward pass through the model
def run_attention(model, model_name, name, seq, tokens, layers, split = '[CLS] [CLS]'):
    with torch.no_grad():
        outputs = model(
            **tokens,
            output_attentions=True,
            output_hidden_states=True,
        )

        # parse the sequence
        h, l = seq.split(split)

        if "antiBERT" in model_name:
            factor = 1/2
        else: factor = 1
        h_positions = list(range(1, int(len(h)*factor  + 1)))
        l_positions = list(range(int(len(h)*factor + 2), int(len(h)*factor + 2 + len(l)*factor)))
        all_positions = h_positions + l_positions

        # Get the attention values for each layer and attention head
        attentions = outputs.attentions
        num_layers = len(attentions)
        num_heads = attentions[0].size(1)

        # Extract attention values for each attention head in every layer
        all_attentions = []
        for layer in layers: #relevant layers
            layer_attentions = attentions[layer]

            for head in range(num_heads): #for each head in that layer
                head_attentions = layer_attentions[0, head]
                for p1 in all_positions:
                    for p2 in all_positions:
                        p1_region = "heavy" if p1 in h_positions else "light"
                        p2_region = "heavy" if p2 in h_positions else "light"
                        comp_type = f"intra-{p1_region}" if p1_region == p2_region else "cross-chain"
                        all_attentions.append(
                            {
                                "position1": p1,
                                "position2": p2,
                                "comparison": comp_type,
                                "attention": head_attentions[p1, p2].item(),
                                "layer": layer,
                                "head": head
                            }
                        )

        # Convert to dataframe
        attention_df = pd.DataFrame(all_attentions)
    return attention_df

# 1. Load model
print(f"### Loading model... ")
if "HA" in dataset:
    antigen = "HA"
elif "S" in dataset:
    antigen = "S"
if use_finetuned:
    base_dir = "../models/"
    model_dir = base_dir + f"{antigen}_{embedding}_FULL_{input}-fine_tuning_FULL/" + checkpoint
    model, tokenizer = load_finetuned_model(model_dir)
    out_suffix = "finetuned"
else:
    print(f"Loading the pre-trained models...")
    model_dir, token_dir = return_model_dir(embedding)
    model, tokenizer = load_pretrained_model(embedding)
    out_suffix = "pretrained"
print(out_suffix)

# 2. Format the sequences
print(f"### Loading {antibody} sequences... ")
data_dir = "../data/"
data = pd.read_parquet(data_dir + dataset)

sequence = data.loc[int(antibody), input]

split = '<cls><cls>'
# just output attention from the last three layers
if 'antiBERTy' in embedding:
    MAX_LENGTH = 512 - 2
    layers = [5,6,7]
elif 'antiBERTa2' in embedding:
    MAX_LENGTH = 256
    layers = [13,14,15]
elif 'BALM_paired' in embedding:
    MAX_LENGTH = 512 - 2
    layers = [21,22,23]
elif ('ESM2' in embedding):
    MAX_LENGTH = 512
    layers = [30,31,32]

def fix_token_format(sequence):
    sequence = sequence[:MAX_LENGTH]
    sequence = sequence.replace('<cls><cls>', '[CLS][CLS]')
    sequence = insert_space_every_other_except_cls(sequence)
    sequence = sequence.replace('  ', ' ')
    return sequence

if ('antiBERTa2' in embedding) | ('antiBERTy' in embedding):
    if antibody == 'none':
        sequence = [fix_token_format(x) for x in sequence]
    else:
        sequence = fix_token_format(sequence)
    split = '[CLS] [CLS]'

print(f"Loaded {model_dir} and {antibody} sequence.")

# 3. Obtain the attention matrix
print(f"Computing the attention matrix...")

tokens = tokenizer(sequence, return_tensors='pt').to('cuda')
base_dir = "../data/"
data_path = dataset.split('.', 1)[0]
attention_df = run_attention(model, embedding, antibody, sequence, tokens, layers, split)
out_path = f'{base_dir}{data_path}_{antibody}_{embedding}_{out_suffix}.csv'
attention_df.to_csv(out_path, index=False)
print(f"Wrote the attention matrix to {out_path}")
