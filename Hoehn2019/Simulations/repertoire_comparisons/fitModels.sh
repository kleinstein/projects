#!/usr/bin/bash
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=48:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com
#SBATCH --exclude=c24n04,c22n04

base=$1
rep=$2
threads=$3

maskCDR3.pl $base\_lineages_$rep.tsv

igphyml --repfile $base\_ncdr_lineages_$rep.tsv -m GY \
	--outrep $base\_ncdr_lineages_GY_$rep.tsv --threads $threads \
	--run_id gy --repwidefreqs

igphyml --repfile $base\_ncdr_lineages_GY_$rep.tsv -m HLP \
	--outrep $base\_ncdr_lineages_HLP19_$rep.tsv --threads $threads \
	--oformat tab -o lr --run_id hlp19

perl runML.pl $threads $base\_ncdr_lineages_GY_$rep.tsv N N,N N N,N,N,N,N,N free 1
