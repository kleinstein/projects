#!/bin/bash

for f in `ls -d MS-M*/`
do
	ParseDb.py select -d ${f}*.tab -f JUNCTION_LENGTH -u 24 27 51 --outdir entropy --outname ${f%/*}
	CreateGermlines.py -d entropy/${f%/*}_*.tab -r /path/to/germline/repertoire
done
