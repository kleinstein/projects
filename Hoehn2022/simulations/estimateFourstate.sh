#!/usr/bin/env bash
#SBATCH --job-name=sims
#SBATCH --partition=pi_kleinstein
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=6gb 
#SBATCH --time=24:00:00
#SBATCH --mail-type=FAIL,END
#SBATCH --mail-user=kenneth.hoehn@gmail.com

module load R

rate=$1
mode=$2
rep=$3
nproc=$4

Rscript estimateFourstate.R $rate $mode $rep $nproc


