# Scripts for BCR analysis in Fichtner et al (2022)
Kenneth B. Hoehn
kenneth.hoehn@yale.edu
7/1/2022

## Processing bulk NEBNext data

First, run:

`bash presto.sh`

Will run `presto-abseq.sh` pipeline for all NEBNext samples

Then run:

`bash runIgblast.sh`

To assign VDJ sequences to these processed sequences, as well as the mAb sequences.

## Processing and analyzing 10X scRNAseq + BCR data

To process VDJ sequences, run:

`bash processVDJ10X.sh`

This will run the `changeo-10x.sh` pipeline on both 10X samples

To process scRNAseq data, run:

`bash runGEX.sh`

This will first run `filter_pca_umap.R` which will create UMAPs and DotPlots.
It will then run `cloneGermline10X.R` which will combine VDJ data with GEX annotations and make IgG4 plots.
Then, it will run `analyze10X.R` which will create supplemental figures and 10X sequence tables.

## Combined clonal variant analysis

To run the full, combined clonal analysis, run:

`bash runClones.sh`

This will first run `findClonalVariants.R` which will find clones across all data sourcess.
Next, it will run `analyzeClones.R` which will build lineage trees and create summary tables.

