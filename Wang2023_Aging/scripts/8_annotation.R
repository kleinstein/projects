## ---------------------------
##
## Script name: 8_annotation.R
##
## Purpose of script: B cell clustering
##
## Author: Mamie Wang
##
## Date Created: 2022-07-06
##
## Email: mamie.wang@yale.edu
## ---------------------------

## load the packages and functions
suppressPackageStartupMessages({
    library(Seurat)
    library(tidyverse)
    library(cowplot)
    library(readxl)
})

outdir = "/gpfs/gibbs/pi/kleinstein/shaw_flu/outs/6_clustering/"

B_clusters = c("B intermediate", "B memory", "B naive", "Plasmablast")

## ---------------------------
## Load IG, TR, cell cycle genes

load("/gpfs/gibbs/pi/kleinstein/mw957/data/QC_features_meta.RData")
biotypes_excl = unique(features_meta[["gene_biotype"]])[grepl(pattern="^IG_|^TR_", x=unique(features_meta[["gene_biotype"]]))]
remove.genes = features_meta[["external_gene_name"]][features_meta[["gene_biotype"]] %in% biotypes_excl]
remove.genes <- c(remove.genes, 'AC233755.1', 'AC233755.2')

## ---------------------------
## Load the data
flu_data = readRDS(file.path(outdir, "GEX_mapped.rds"))

print(ncol(flu_data))
# 117278

## ---------------------------
## Annotate the cluster based on majority vote of Azimuth annotations
anno = as.matrix(table(flu_data$RNA_snn_res.0.7, flu_data@meta.data$predicted.celltype.l2))
cluster_anno = colnames(anno)[apply(anno, 1, which.max)]

Idents(flu_data) = "RNA_snn_res.0.7"
names(cluster_anno) <- levels(flu_data$RNA_snn_res.0.7)
flu_data <- RenameIdents(flu_data, cluster_anno)
flu_data$annotated_cluster = Idents(flu_data)

## ---------------------------
## Select for B cells with both BCR and gene expression data
is_B = (flu_data$predicted.celltype.l2 %in% B_clusters) & (flu_data$annotated_cluster %in% B_clusters)
has_bcr = flu_data$has_bcr

B_gex = sum(is_B)
B_gex_w_bcr = sum(is_B & has_bcr)

B_gex # 100745
B_gex_w_bcr # 90133
B_gex_w_bcr/B_gex # 0.8946647

B = flu_data[, is_B & has_bcr]

## ---------------------------
## Diet Seurat object
DefaultAssay(B) = "RNA"
B[["refAssay"]] = NULL
B[["prediction.score.celltype.l2"]] = NULL
B = DietSeurat(B)
B@meta.data[,grepl("RNA_snn.res", colnames(B@meta.data))] = NULL

## ---------------------------
## Rerun UMAP

DefaultAssay(B) = "RNA"

B <- NormalizeData(B, normalization.method = "LogNormalize", scale.factor = 10^4)
B <- FindVariableFeatures(B, selection.method = "vst", nfeatures = 2000)

bool.remove.genes <- B@assays$RNA@var.features %in% remove.genes
B@assays$RNA@var.features = B@assays$RNA@var.features[!bool.remove.genes]

B <- ScaleData(B, verbose = F)
B <- RunPCA(B, npcs = 50, verbose = F)
B <- RunUMAP(B, reduction = "pca", n.neighbors = 20, dims = 1:50)

resolutions = c(0.5, 1, 1.5, 2)
B <- FindNeighbors(B, reduction = "pca", dims = 1:50, k.param = 20)

for (resolution in resolutions) {
    B <- FindClusters(B, resolution = resolution)
}

## ---------------------------
## Re-annotate the clusters
anno = as.matrix(table(B$RNA_snn_res.2, B@meta.data$predicted.celltype.l2))
cluster_anno = colnames(anno)[apply(anno, 1, which.max)] # by majority vote

Idents(B) = "RNA_snn_res.2"
names(cluster_anno) <- levels(B$RNA_snn_res.2)
B <- RenameIdents(B, cluster_anno)
B$annotated_cluster = Idents(B) 

azimuth_anno = table(B$`RNA_snn_res.2`, B$annotated_cluster) %>% 
  data.frame %>% 
  filter(Freq > 0) %>%
  arrange(Var2) %>%
  mutate(Var1 = as.character(Var1), Var2 = as.character(Var2))

modified_anno = azimuth_anno

## ---------------------------
## Rename the clusters
modified_anno$Var2[modified_anno$Var1 %in% c(6, 8, 18, 24)] = "B memory"
modified_anno$Var2[modified_anno$Var1 == 27] = "PB proliferating"
modified_anno$Var2 = plyr::mapvalues(modified_anno$Var2, 
                                    from = c("B intermediate", "B memory", "B naive", "Plasmablast", "PB proliferating"), 
                                    to = c("ABC", "RMB", "Naive B", "PB", "PB proliferating"))


B$annotated_cluster <- plyr::mapvalues(as.character(B$RNA_snn_res.2),
                                       from = as.character(modified_anno$Var1),
                                       to = as.character(modified_anno$Var2))
## ---------------------------
## counts

table(B$annotated_cluster)
#              ABC          Naive B               PB PB proliferating 
#             6336            62197             1944              337 
#              RMB 
#            19319 

## ---------------------------
## Save the B cell data

saveRDS(B, file.path(outdir, "B.rds"))

## ----------------------------
## Exporting the data frames of counts and metadata
B = readRDS(file.path(outdir, "B.rds"))

write.csv(B@assays$RNA@scale.data, 
          file = file.path(outdir, "B_counts_scaled.csv"))
write.csv(B@meta.data, 
          file = file.path(outdir, "B_meta_data.csv"))
