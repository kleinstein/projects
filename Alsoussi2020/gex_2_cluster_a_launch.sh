#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 14-
#SBATCH -c 1
#SBATCH --mem=120G

# --constraint E5-2660_v3
# nx360h (E5-2660_v3) has a total max memory of 121GB per node


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

#*
RUN_ID="2020-04-06"

SCRIPT_PATH="/home/qz93/projects/scripts/ellebedy_murineCovid/"
SCRIPT_NAME="gex_2_cluster_a.R"

#*
PATH_WORK="/home/qz93/project/ellebedy_murineCovid/gex/${RUN_ID}/cluster/"


LOG="${PATH_WORK}log_gex_cluster_$(date '+%m%d%Y_%H%M%S').txt"

Rscript "${SCRIPT_PATH}${SCRIPT_NAME}" &> "${LOG}"

