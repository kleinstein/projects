#!/bin/bash
#SBATCH --partition day

sbatch --mem=380g get_sim_bootstraps.sh 50 "125_sel" "pratchet" 
sbatch --mem=90g get_sim_bootstraps.sh 50 "125_sel" "pml" 
sbatch --mem=90g get_sim_bootstraps.sh 50 "125_sel" "dnaml"
sbatch --mem=90g get_sim_bootstraps.sh 50 "125_sel" "dnapars" 
sbatch --mem=200g get_sim_bootstraps.sh 50 "125_sel" "igphyml" 
sbatch --mem=200g get_sim_bootstraps.sh 50 "125_sel" "igphyml_p" 
sbatch --mem=90g get_sim_bootstraps.sh 50 "125_sel" "raxml" 
sbatch --mem=90g get_sim_bootstraps.sh 50 "125_sel" "raxml_p" 