#!/usr/bin/env python3
"""
Separate by subject, and
Add fields containing meta information to fasta header
"""

import re, sys, os

# input header: 
# >167960_SPENSER:65:000000000-A6N3C:A57:2:u1458:5_B-cell_naive_TW04B
# >437118_SPENSER:83:000000000-A7V23:A21:3:u64426:0_B-cell_memory_TW02A

# output header:
# >437118_SPENSER:83:000000000-A7V23:A21:3:u64426:0|SUBJECT=TW02A|SUBSET=memory

def addMetaToHeader(inputFasta, outputFasta, targetSubject):

    if os.path.exists(inputFasta):

        with open(inputFasta, "r") as in_handle:

            lines = [line.rstrip() for line in in_handle] # list
            # find index of lines containing ">"
            idxHeaders = [idx for idx in range(len(lines)) if ">" in lines[idx]]

            with open(outputFasta, 'w') as out_handle:

                for i in range(len(idxHeaders)):
                    idx = idxHeaders[i]

                    # current header
                    curHeader = lines[idx]

                    # extract subject
                    # expect "_TW04B" and alike
                    curSubj = re.findall("_TW[\w]+", curHeader)
                    if len(curSubj)==1:
                        curSubj = curSubj[0]
                        curSubj = curSubj[1:]
                    else:
                        sys.exit("Something unexpected happened while extracting subject")

                    # only proceed if curSubj is targetSubject
                    if curSubj==targetSubject:

                        # extract ID
                        # expect ">167962_SPENSER:65:000000000-A6N3C:A57:2:u1458:1_B-cell" and alike
                        curID = re.findall("[\S]+_B-cell", curHeader)
                        if len(curID)==1:
                            curID = curID[0]
                            curID = curID[0:(len(curID)-len("_B-cell"))]
                        else:
                            sys.exit("Something unexpected happened while extracting read ID")

                        # extract subset
                        # expect "B-cell_naive_TW" or "B-cell_memory_TW"
                        curSubset = re.findall("_B-cell_[\w]+_TW", curHeader)
                        if len(curSubset)==1:
                            curSubset = curSubset[0]
                            curSubset = curSubset[len("_B-cell_"):(len(curSubset)-len("_TW"))]
                        else:
                            sys.exit("Something unexpected happened while extracting subset")

                        # new header
                        newHeader = curID + "|SUBJECT=" + curSubj + "|SUBSET=" + curSubset

                        out_handle.writelines(newHeader+"\n")

                        # if not the last header
                        if i!=(len(idxHeaders)-1):
                            nextIdx = idxHeaders[i+1]
                            # copy over lines containing current sequence
                            # idx+1 : nextIdx-1
                            for seqIdx in range(idx+1, nextIdx):
                                out_handle.writelines(lines[seqIdx]+"\n")
                        else:
                        # if the last header
                            # copy over remaining lines containing sequence
                            for seqIdx in range(idx+1, len(lines)):
                                out_handle.writelines(lines[seqIdx]+"\n")


# run
SUBJS = ("TW01A", "TW01B", "TW02A", "TW02B", "TW03A", "TW03B", "TW04A", "TW04B", "TW05A", "TW05B")
#SUBJS = ("TW01A", "TW02B")

PATH_MEMORY = "/ysm-gpfs/home/qz93/scratch60/davis_twins/processed/B-cell_memory/B-cell_memory.fasta"
PATH_NAIVE = "/ysm-gpfs/home/qz93/scratch60/davis_twins/processed/B-cell_naive/B-cell_naive.fasta"

PATH_OUT = "/ysm-gpfs/home/qz93/scratch60/davis_twins/start_julian/"

for subj in SUBJS:

    # memory
    print(subj + " memory")
    outMemory = PATH_OUT + subj + "_memory.fasta"
    addMetaToHeader(PATH_MEMORY, outMemory, subj)

    # naive
    print(subj + " naive")
    outNaive = PATH_OUT + subj + "_naive.fasta"
    addMetaToHeader(PATH_NAIVE, outNaive, subj)
