from antiberty import AntiBERTyRunner
import argparse
import torch
import numpy as np
import time
from Bio import SeqIO
import pandas as pd
import math

def batch_loader(data, batch_size):
    num_samples = len(data)
    for i in range(0, num_samples, batch_size):
        end_idx = min(i + batch_size, num_samples)
        yield i, end_idx, data[i:end_idx]

parser = argparse.ArgumentParser(description="Input path")
parser.add_argument("fasta_file", type=str, help="Path to the fasta file")
parser.add_argument("output_file", type=str, help="Output file path")
args = parser.parse_args()

antiberty = AntiBERTyRunner()
sequences = []
for seq_record in SeqIO.parse(args.fasta_file, "fasta"):
    sequences.append(''.join(seq_record.seq))
print(f"Read {args.fasta_file} with {len(sequences)} sequences")

start_time = time.time()
batch_size = 1000
n_seqs = len(sequences)
dim = 512
n_batches = math.ceil(n_seqs / batch_size)
embeddings = torch.empty((n_seqs, dim))

# Use the batch_loader function to iterate through batches
i = 1
for start, end, batch in batch_loader(sequences, batch_size):
    print(f'Batch {i}/{n_batches}\n')
    x = antiberty.embed(batch)
    x = [a.mean(axis = 0) for a in x]
    embeddings[start:end] = torch.stack(x)
    i += 1

end_time = time.time()
print(end_time - start_time)

torch.save(embeddings, args.output_file)