#--------------------------------------------
# Generate ft-ESM2 embedding
# 
# ft-ESM2 is available on zenodo:
# https://zenodo.org/record/8253367
#
# Please download the model weights to model/ folder
#
# Example usage: 
#
# python ft-ESM2_embed.py S_FULL.parquet HL S_FULL_ft-ESM2.pt
#--------------------------------------------

import torch
from transformers import (
    AutoTokenizer, 
    AutoModelForMaskedLM,
    DataCollatorForLanguageModeling,
    TrainingArguments,
    Trainer,
)
import numpy as np
import pandas as pd
import time
import math
import argparse
#--------------------------------------------
parser = argparse.ArgumentParser(description="Input path")
parser.add_argument("parquet_file", type=str, help="Path to the parquet file")
parser.add_argument("sequence", type=str, help="Column name of sequence to embed (H, L, HL)")
parser.add_argument("output_file", type=str, help="Output file path")
args = parser.parse_args()
#--------------------------------------------
def batch_loader(data, batch_size):
    num_samples = len(data)
    for i in range(0, num_samples, batch_size):
        end_idx = min(i + batch_size, num_samples)
        yield i, end_idx, data[i:end_idx]
#--------------------------------------------
BASEDIR = '../models/'
MODEL_DIR = BASEDIR + 'ESM2-650M_paired-fine-tuning/'
model = AutoModelForMaskedLM.from_pretrained(MODEL_DIR)
model = model.cuda()
model_size = sum(p.numel() for p in model.parameters())
print(f"Model size: {model_size/1e6:.2f}M")

tokenizer = AutoTokenizer.from_pretrained("facebook/esm2_t33_650M_UR50D")
#--------------------------------------------
dat = pd.read_parquet(args.parquet_file)
trainer = Trainer(
    model=model
)

sequences = dat.loc[:,args.sequence].values
#--------------------------------------------
start_time = time.time()
batch_size = 50
n_seqs = len(sequences)
dim = 1280
n_batches = math.ceil(n_seqs / batch_size)
embeddings = torch.empty((n_seqs, dim))
max_length = 512

# Use the batch_loader function to iterate through batches
i = 1
for start, end, batch in batch_loader(sequences, batch_size):
    print(f'Batch {i}/{n_batches}\n')
    x = torch.tensor([
        tokenizer.encode(seq[:max_length], # Trim long sequences
                     padding="max_length",
                     truncation=True,
                     max_length=max_length,
                     return_special_tokens_mask=True) for seq in batch]).to('cuda')
    attention_mask = (x != tokenizer.pad_token_id).float().to('cuda')
    with torch.no_grad():
        outputs = model(x, attention_mask = attention_mask,
                       output_hidden_states = True)
        outputs = outputs.hidden_states[-1]
        outputs = list(outputs.detach())
    # aggregate across the residuals, ignore the padded bases
    # mean representation
    for j, a in enumerate(attention_mask):
        outputs[j] = outputs[j][a == 1,:].mean(0)
    # BOS representation
    # for j, a in enumerate(attention_mask):
    #     outputs[j] = outputs[j][0,:]
    embeddings[start:end] = torch.stack(outputs)
    del x
    del attention_mask
    del outputs
    i += 1
    
end_time = time.time()
print(end_time - start_time)

torch.save(embeddings, args.output_file)
