## Scripts

| File | Description |
|--|--|
| 1_train_immune2vec.py | Train immune2vec models |
| 2_immune2vec_embed.sh | Run immune2vec model to embed sequences |
| 3_esm_embed.py | Run ESM2 models to embed sequences |
| 4_antiBERTy.py | Run antiBERTy models to embed sequences |
| 5_ProtT5_embed.py | Run ProtT5 models to embed sequences |
| 6_baseline_embed.py | Embed sequences by amino acid frequency, physicochemical properties |
| 7_umap_visualization.py | Run UMAP on embeddings |
| 8_run_classification_task.py | Classification tasks |
| 9_run_regression_task.py | Regression tasks |
| 10_COVID_specificity.py | Specificity tasks |

## Data

### FASTA/

fasta files containing BCR amino acid sequences

| File | Description |
|--|--|
| combined_distinct_light.fa | fasta file for full-length light chain BCR sequences |
| combined_distinct_heavy.fa | fasta file for full-length heavy chain BCR sequences |
| combined_cdr3_light.fa | fasta file for light chain BCR CDR3 sequences |
| combined_cdr3_heavy.fa | fasta file for heavy chain BCR CDR3 sequences |

### Annotations/

tsv files containing the metadata for each receptor or chain, including source and prediction labels

|File| Description | Columns |
|--|--|--|
| specificity.anno | specificity to SARS-CoV-2 spike protein. Each row is a receptor.  | heavy_id: fasta header in combined_distinct_heavy.fa, light_id: fasta header in combined_distinct_light.fa, subject: donor, source: database, label: whether the receptor binds to spike protein or not) |
| cdr3_specificity.anno | specificity to SARS-CoV-2 spike protein. Each row is a receptor. | heavy_id: fasta header in combined_cdr3_heavy.fa, light_id: fasta header in combined_cdr3_light.fa, subject: donor, source: database, label: whether the receptor binds to spike protein or not |
| combined_distinct_light.anno | annotations for light chain sequences in combined_distinct_light.fa | v_call_gene_light: v gene, v_call_family_light: v family, j_call_family_light: j family, mu_freq_light: mutation frequency, junction_aa_length_light: junction length, isotype_light: type of light chain, source: data source, subject: donor, id: fasta header in combined_distinct_light.fa |
| combined_distinct_heavy.anno | annotations for heavy chain sequences in combined_distinct_heavy.fa | v_call_gene_heavy: v gene, v_call_family_heavy: v family, j_call_family_heavy, j family, mu_freq_heavy: mutation frequency, junction_aa_length_heavy: junction length, isotype_heavy: isotype, source: data source, subject: donor, id: fasta header in combined_distinct_heavy.fa |

* Note: All original data are from public sources (See data sources from Table 1 of the [preprint](https://biorxiv.org/cgi/content/short/2023.06.21.545145v1))
