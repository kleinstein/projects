#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --job-name=ind3
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=32
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=24:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com

module load CBLAS/20110120-foss-2016b
module load ATLAS/3.10.2-GCC-5.4.0-2.26-LAPACK-3.6.1
moduel load EMBOSS
perl runAllInd.pl 32 0 5