#!/usr/bin/perl

use strict;
use warnings;
use hotSpotUtils;
use Parallel::ForkManager;

my $pm=new Parallel::ForkManager($ARGV[0]);
my $type=$ARGV[1];

#my @files=glob("Patients/Healthy_7/test/*");

open(IN,$ARGV[2]) or die();
my @lines;
my $h=<IN>;
while(<IN>){
	my $line=$_;
	chomp($line);
	#$line =~ s/seqs/simulations/g;
	my @in=split("\\s+",$line);
	push(@lines,\@in);
}

my $p="ml";
foreach my $f (@lines){
	$pm->start and next;
	my @f=@$f;
	my $id="";
	if($f[0]=~/\/(\S+?)\.fa/){
		$id=$1;
	}else{die("@f")}
	#my $c="simulatehlp17.pl sim.config -seqfile $f[0] -rootid $f[2] -tree $f[1] -outdir simulations -stem $id";
	my $seqs=getfasta($f[0]);
	my @keys=keys %$seqs;
	if(scalar(@keys) >= 3){
		my $c;
		if($type eq "GY"){
			$c="igphyml -i $f[0] -m GY --run_id GYi --threads 1";
			system($c)==0 or die($c);
		}elsif($type eq "DNAML" || $type eq "DNAPARS"){
			$f[0] =~ s/clones/simulations/g;
			if($type eq "DNAPARS"){
				$p="pars"
			}
			my @keys = keys %$seqs;
			open(TO,">$f[0].tfa") or die();
			for(my$i=0;$i<scalar(@keys);$i++){
				print TO ">-$i-\n".$seqs->{$keys[$i]}."\n";
			}
			close(TO);
			system("seqret $f[0].tfa --osformat phylip --outseq $f[0].phy");
			system("rm outfile");
            system("rm outtree");
            system("rm outtree2");
            system("cat $f[0].phy > infile");
            system("echo Y | dna".$p);
            open(OT,"outtree") or die("outtree");
            my $tree="";
            while(<OT>){
            	my $line=$_;
            	chomp($line);
            	if($line =~ /\[\S+\];/){
            		$tree .= $`.";";
            		last;
            	}
            	$tree .= $line;
            }
            while($tree =~ /-(\d+)-/){
            	#print "$tree\n";
            	$tree = $`.($keys[$1]).$';
            }
            open(OT2,">outtree2") or die();
            print OT2 $tree;
            system("igphyml -i $f[0] -m HLP17 --motifs WRC_2:0 -u outtree2 -o n --run_id dna$p --root $f[2]") ==0 or die("igphyml -i $f[0] -m HLP17 --motifs WRC_2:0 -u outtree2 -o n --run_id dna$p --root $f[2]");
            #die(" $f[0]\_igphyml_tree_dna$p.txt");
		}elsif($type eq "HLPprior"){
			$c="igphyml -i $f[0] -m HLP17 --root $f[2] --motifs FCH --run_id HLPspi -s SPR --threads 1 --prior";
			system($c)==0 or die($c);
		}elsif($type eq "HLP19indr"){
			$c="igphyml -i $f[0] -m GY --run_id GYind --threads 1";
			system($c)==0 or die($c);
			$c="igphyml -i $f[0] -m HLP19 --root $f[2] -u $f[0]\_igphyml_tree_GYind.txt --motifs FCH --run_id HLP19indr -o r --threads 1 --omegaOpt e,e --partfile $f[3]";
			system($c)==0 or die($c);
		}elsif($type eq "HLP19indlr"){
			$c="igphyml -i $f[0] -m GY --run_id GYind --threads 1";
			system($c)==0 or die($c);
			$c="igphyml -i $f[0] -m HLP19 --root $f[2] -u $f[0]\_igphyml_tree_GYind.txt --motifs FCH --run_id HLP19indlr -o lr --threads 1 --omegaOpt e,e --cbmat --partfile $f[3]";
			system($c)==0 or die($c);
		}elsif($type eq "HLP19indtlr"){
            $c="igphyml -i $f[0] -m GY --run_id GYind --threads 1";
            system($c)==0 or die($c);
            $c="igphyml -i $f[0] -m HLP19 --root $f[2] -u $f[0]\_igphyml_tree_GYind.txt --motifs FCH --run_id HLP19indtlr -o tlr --threads 1 --omegaOpt e,e --cbmat --partfile $f[3]";
            system($c)==0 or die($c);
        }elsif($type eq "HLP17indlr"){
            $c="igphyml -i $f[0] -m GY --run_id GYind --threads 1";
            system($c)==0 or die($c);
            $c="igphyml -i $f[0] -m HLP17 --root $f[2] -u $f[0]\_igphyml_tree_GYind.txt --motifs FCH --run_id HLP17indlr -o lr --threads 1 --omegaOpt e,e --cbmat --partfile $f[3]";
            system($c)==0 or die($c);
        }elsif($type eq "HLP17indlrncb"){
            $c="igphyml -i $f[0] -m GY --run_id GYind --threads 1";
            system($c)==0 or die($c);
            $c="igphyml -i $f[0] -m HLP17 --root $f[2] -u $f[0]\_igphyml_tree_GYind.txt --motifs FCH --run_id HLP17indlrncb -o lr --threads 1 --omegaOpt e,e --partfile $f[3]";
            system($c)==0 or die($c);
        }elsif($type eq "HLP17indtlr"){
            $c="igphyml -i $f[0] -m GY --run_id GYind --threads 1";
            system($c)==0 or die($c);
            $c="igphyml -i $f[0] -m HLP17 --root $f[2] -u $f[0]\_igphyml_tree_GYind.txt --motifs FCH --run_id HLP17indtlr -o tlr --threads 1 --omegaOpt e,e --cbmat --partfile $f[3]";
            system($c)==0 or die($c);
        }else{
		die("MODEL NOT FOUND\n");
		$c="igphyml -i $f[0] -m HLP17 --root $f[2] --motifs FCH --run_id HLPsi -s SPR --threads 1 --omegaOpt e,e --partfile $f[3]";
		system($c)==0 or die($c);
		}
	}
	$pm->finish;
}
$pm->wait_all_children;
