

use strict;
use warnings;


my @folders=glob("*/*/seqs_lineages.tsv");
my $depth=$ARGV[0];

foreach my $f (@folders){
	if($f=~/(\S+)\/(\S+)\/seqs_lineages.tsv/){
		print "$f\n";
		my $sub=$1;
		my $visit=$2;
		my $base="seqs";
		system("mkdir $sub/$visit/$base$depth/");
		system("rm $sub/$visit/$base$depth/*");
		my $c="subSampleRep.pl $f $sub/$visit/$base$depth $depth $sub/$visit/$base\_$depth\_lineages.tsv";
		system($c) == 0 or die($c);
	}
}
