#!/usr/bin/bash
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=36
#SBATCH --mem-per-cpu=5gb
#SBATCH --partition=pi_kleinstein
#SBATCH --time=48:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com

nproc=36

DefineClones.py -d ../changeo/combined.tab --act set --model ham \
    --norm len --dist 0.1 --nproc $nproc

CreateGermlines.py -d ../changeo/combined_clone-pass.tab -g dmask --cloned \
    -r ~/share/germlines/imgt/human/vdj/imgt_human_IGHV.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGHD.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGHJ.fasta 

