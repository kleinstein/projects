# Pipeline for analyses in Hoehn et al. 2021, J. Immuno
File locations are hardcoded. May need to change them manually.

## Run Changeo-10x Pipeline on raw VDJ data
bash processVDJ.sh

## Do filtering and annotation on healthy/mild GEX data
Rscript filter_pca_umap.R

## Integrate all patient cohort GEX data
Rscript integrate.R 

## Combine VDJ and GEX data
Rscript combineVDJData.R

## Identify clonal clusters in VDJ data
Rscript cloneGermlineIntegrated.R

## Perform analyses in paper and make base figures
Rscript analysis.R