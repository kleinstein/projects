# Phylogenetic analysis scripts

Kenneth B. Hoehn (kenneth.hoehn@yale.edu)
These are scripts used for phylogenetic analysis of BCR sequence data from mice.

The scripts are run in the following order:

```
cd scripts

# Run preprocessing for each sequence
bash runSamples.sh

cd balbc

# Set up balbc germline
bash setupGermlineDb.sh

# Run IgBlast on all samples using IMGT and BALB/c germline
bash runAllIgBlast.sh

# Compare SHM using each germline database
Rscript compareSHM.R 

# Run cloning pipeline and build trees
bash runMice.sh

# Generate figures and results
Rscript results_dowser.R
```
