#!/bin/bash
#SBATCH --partition=general
#SBATCH --job-name=makedb
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=10G
#SBATCH --time=1-00:00:00
#SBATCH --mail-type=ALL
#SBATCH --error=log/makedb.%A_%a.err
#SBATCH --output=log/makedb.%A_%a.out

taskID=${SLURM_ARRAY_TASK_ID}

infolder="/gpfs/gibbs/pi/kleinstein/shaw_flu/10x/defaults/"
out="/gpfs/gibbs/pi/kleinstein/shaw_flu/processed/BCR/"
outfolder=$out"1_igblast/"
outfolder2=$out"2_makedb/"

germlines="/gpfs/gibbs/pi/kleinstein/mw957/igblast/germlines/imgt/human/vdj/imgt_human_*.fasta"

samples=(`ls -d $outfolder*_igblast.fmt7`)
sample=${samples[$taskID-1]}
sampleid=$(basename $sample "_igblast.fmt7")

echo $sampleid

echo "[$0 $(date +%Y%m%d-%H%M%S)] [start] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"
    
singularity exec /gpfs/ysm/project/kleinstein/mw957/software/immcantation/lab_devel.sif MakeDb.py igblast \
    -i $sample \
    -s $infolder/$sampleid"_BCRVDJ/bcrvdj/outs/filtered_contig.fasta" \
    -r $germlines \
    --extended \
    --failed \
    --partial \
    --10x $infolder/$sampleid"_BCRVDJ/bcrvdj/outs/filtered_contig_annotations.csv" \
    --format airr \
    --outname $sampleid \
    --outdir $outfolder2

echo "[$0 $(date +%Y%m%d-%H%M%S)] [end] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"