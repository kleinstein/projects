#!/usr/bin/bash
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=24:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn\@gmail.com
#SBATCH --exclude=c24n04

module load R

base=$1
threads=$2

echo $q
igphyml --repfile $base\_lineages.tsv -m GY --outrep $base\_gy_lineages.tsv --threads $threads --minseq 3 --run_id GY --repwidefreqs
igphyml --repfile $base\_gy_lineages.tsv -m HLP19 --motifs FCH --cbmat --threads $threads -o lr --omegaOpt e,e

rm $3/simsrg/*\_$4\_0.fa*
rm $3/simsrg/*\_$4\_0.part*
rm $3/simsrg/*\_$4\_0_ncdr.fa*
rm $3/simsrg/*\_$4\_0_ncdr.part*

echo $3/simsrg/*\_$4\_0_ncdr.fa*
echo $3/simsrg/*\_$4\_0_ncdr.part*