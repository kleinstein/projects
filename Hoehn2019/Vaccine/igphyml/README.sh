### Vaccine dataset analysis
# Kenneth Hoehn
# 2/1/2019
# Do not run this as a shell script! Execute each command individually
# and wait for it to complete. This is just an explanation of the pipeline

## Dataset processing
# runs split_to_patients.pl to split combined datasets from ChangeO 
# folder into separate sample dates. Creates subfolder system.
sbatch splitPatients.sh

# Summarize the number of lineages with at least 3 sequences
perl summarizeLins.pl seqs_lineages.tsv 3

#subsample to 3000 sequences
#uses subSampleRep.pl from help scripts
perl subSampleReps.pl 3000 > logs/subSampleLog.txt


## Parameter estimation
#for each sample remove CDR3s, create GY94 trees, then estimate HLP19 parameters 
#with confidence intervals for omegaCDR. Creates and submits a separate batch
#file for each sample.
#uses maskCDR3.pl
perl makeBatchFiles.pl

#clean up/hoard intermediate files 
mkdir old_slurm
mkdir old_run
mv slurm* old_slurm
mv run* old_run

#Summarize model parameter estimation results from the previous command
#creates RepResults.tsv, which is then used for further analysis
perl parseStats.pl

## Simulation analysis
#for each sample:
# -reroots GY94 trees to the germline and sets up directory structure
# -submits simulateRepertoire.sh which is a wrapper script for 
# simulateRepertoire.pl:
#	-simulates sequence dataset based on GY94 input and parameters
#	-uses helper function simulateFull.pl, sim_FCH.config, FCH_mut_obs.tsv
#	-submits runSim.sh:
#		-removes CDR3, estimates GY94 topology and HLP19 parameters, removes 
#		intermediate files
sbatch simulateFCH.bash

#Summarize simulation results from the previous command
#creates RepResults_sim_rg.tsv, which is then used for further analysis
perl parseStats_sim.pl

## Split repertoire analysis
# split IB/+7 by size, and then do parameter estimation on each quartile
# uses runq.sh, splitD7.R
bash run_split_analysis.sh

## Summarize size distribution of clones - used for stream plots
perl summarizeCloneSizes.pl