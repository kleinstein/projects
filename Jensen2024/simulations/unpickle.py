# -*- coding: utf-8 -*-
# required packages
# pip install ete3
# pip install PyQt5
from ete3 import TreeNode, TreeStyle, NodeStyle, SVG_COLORS
import pickle

for x in range(0,50):
  with open('.../.../lineage_trees/' + str(x) + '_lineage_tree.p', 'rb') as fh:
    tree = pickle.load(fh, encoding="latin1")
  tree.write(format=1, outfile=".../.../lineage_trees/starting_tree_" + str(x) + ".nw")