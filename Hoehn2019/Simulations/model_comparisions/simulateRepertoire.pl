

use strict;
use warnings;
use hotSpotUtils;
use Parallel::ForkManager;

my $reps = $ARGV[0];
my $pm = new Parallel::ForkManager($ARGV[1]);
my $treef = $ARGV[2];
my $outdir = $ARGV[3];
my $outstem = $ARGV[4];
my $options = "";
for(my $i=5;$i<scalar(@ARGV);$i++){
	$options .= " ".$ARGV[$i];
}

print "OPTIONS:\n$options\n";

my $nclones = 200;
open(IN,$treef) or die($treef);
my $h=<IN>;

for(my $rep=0;$rep<$reps;$rep++){
	for(my $i=0;$i<$nclones;$i++){
		$pm->start and next;
		my $tree = $h;
		my $id = $i;
		$tree =~ s/GERM/$id\_GERM/;
		open(OUT,">sim_trees/$outstem\_$id.tree") or die();
		print OUT $tree;
		print "\n$id\n";
		my $srand = int(($i+$rep+time()));
		print "sim_trees/$outstem\_$id.tree\t$srand\t$i\n";
		my $command = "simulateFull.pl blank.txt -tree sim_trees/$outstem\_$id.tree -nsim 1 -stem $id\_$rep -outdir $outdir -srand $srand $options";
		system($command) == 0 or die($command);
		close(OUT);
		$pm->finish;
	}
	$pm->wait_all_children;
	
	open(OUT,">sims/$outstem\_lineages_$rep.tsv") or die();
	print OUT "$nclones\n";
	for(my $i=0;$i<$nclones;$i++){
		my $id = $i;
		print OUT "$outdir/$id\_$rep\_0.fa\tN\t$id\_GERM\t$outdir/$id\_$rep\_0.part.txt\n";
	}
	if($rep < 4){
		system("sbatch --partition=pi_kleinstein --cpus-per-task=8  --job-name=$outstem runq.sh sims/$outstem\_lineages_$rep sims/$outstem\_ncdr_lineages_$rep 8")
	}else{
		system("sbatch --partition=general       --cpus-per-task=8  --job-name=$outstem runq.sh sims/$outstem\_lineages_$rep sims/$outstem\_ncdr_lineages_$rep 8")
	}
	#system("bash runq.sh sims/$outstem\_lineages_$rep $ARGV[1]") == 0 or die("FAILED!");
}
