#!/bin/bash
#SBATCH --partition=general
#SBATCH --job-name=azimuth
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=60G
#SBATCH --time=00:30:00
#SBATCH --error=log/azimuth.%A.err
#SBATCH --output=log/azimuth.%A.out

echo "[$0 $(date +%Y%m%d-%H%M%S)] [start] $SLURM_JOBID"

singularity exec /gpfs/gibbs/pi/kleinstein/mw957/dockers/azimuth_0.4.5.sif Rscript 6_azimuth.R

echo "[$0 $(date +%Y%m%d-%H%M%S)] [end] $SLURM_JOBID"
