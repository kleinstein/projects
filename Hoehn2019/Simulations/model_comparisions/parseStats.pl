


use strict;
use warnings;
use hotSpotUtils;

my @folders=glob("sims/*");
open(OUT,">RepResults_startingseq.tsv") or die();
print OUT "Type\tCDR\tP\tRID\tTime\tParam\tMLE\tLCI\tUCI\tTRUTH\n";

my %tparam=(
"WRC_2"=>2,
"GYW_0"=>3,
"WA_1"=>2,
"TW_0"=>1,
"SYC_2"=>-0.6,
"GRS_0"=>-0.6
	);


my $c=0;
foreach my $f (@folders){
	print "$f\n";
	if($f=~/sims\/(\S+)_(\d+)_(\S*)_?lineages_(\d+)\S*.tsv_igphyml_stats_(\S+).txt/){
	my $n=$1;
	my $ncdr=$3;
	my $time=$2;
	my $t=$4;
	my $ri=$5;
	if($ncdr eq ""){$ncdr="cdr"}
	if($time =~ /([-|+])(\d+)d/){
		if($1 eq "+"){
			$time=$2;
		}else{
			$time=$1.$2;
		}
	}elsif($time =~ /([-|+])(\d+)h/){
		if($1 eq "+"){
			$time=($2/24);
		}else{
			$time=$1.($2/24);
		}
	}
	if($ri eq ""){$ri="NA"}
	#if($f !~ /FCH/ && $f !~ /H/){next;}
	my %nseqs;
	my %real;
	my %lengths;
	my $nsubs=0;
	my $tl=0;
	my $tlncdr3=0;
	open(R,"sims/$n\_$time\_lineages_$t.tsv") or die("sims/$n\_$time\_lineages_$t.tsv");	
	my $h=<R>;
	while(<R>){
		my $line=$_;
		chomp($line);
		my @in=split("\t",$line);
		my $f = $in[0];
		#print "$f\n";
		my $id="";
		my $dir="";
		#print "$f\n";
		if($f =~ /(\S*)\/(\d+)_$t\_0.*\.fa/){
			$id = $2;
			$dir = $1;
		}else{die("\n$f\n$t")}
		#print "$dir\/$id\_$t\_0.fa\n";
		my $seq=getfasta("$dir\/$id\_$t\_0.fa");
		my @ks=keys %$seq;
		my $nseq=scalar(@ks);
		$nseqs{$id}=$nseq;
		#true number of substitutions
		open(T,"$dir/$id\_$t\_subs.txt") or die("$dir/$id\_$t\_subs.txt");
		my $h = <T>;
		my $curlength=0;
		while(<T>){
			my $line=$_;
			chomp($line);
			my @in=split("\t",$line);
			if(!exists($lengths{$in[0]})){
				$lengths{$in[0]}=0;
			}
			$real{"$in[0]:$in[5]"}=$in[2];
			$curlength+=$in[2];
			$nsubs+=$in[1];
			$tl += $in[2];
			$tlncdr3 += $in[4];
			if($in[5] =~ /(\d+)_GERM/){
				$lengths{$1}=$curlength;
				$curlength=0;
			}
		}
	}
	#foreach my $k (keys %lengths){
	#	print "$k\t$lengths{$k}\n";
	#}
	#die("$n $time $nsubs $tl\n");

	my $maxdiv = 0;
	my $sumdiv = 0;
	my $lincount = 0;
	open(IN,$f) or die($f);
	while(<IN>){
		my $line=$_;
		chomp($line);
			if($line=~/\. Estimated substitutions in repertoire\:	(\S+)/){print OUT "$t\t$ncdr\t$n\t$ri\t$time\tsubst\t$1\t$1\t$1\t$nsubs\n"}
			if($line=~/\. Transition\/transversion ratio\: 	(\S+)\s+\((\S+)\, (\S+)\)/){print OUT "$t\t$ncdr\t$n\t$ri\t$time\tkappa\t$1\t$2\t$3\t2\n"}
			elsif($line=~/\. Transition\/transversion ratio\: 	(\S+)/){print OUT "$t\t$ncdr\t$n\t$ri\t$time\tkappa\t$1\t$1\t$1\t2\n"}
			if($line=~/\. Omega 0:	(\S+)\s+\((\S+)\, (\S+)\)/){print OUT "$t\t$ncdr\t$n\t$ri\t$time\tomega0\t$1\t$2\t$3\t0.4\n"}
			elsif($line=~/\. Omega 0:	(\S+)/){print OUT "$t\t$ncdr\t$n\t$ri\t$time\tomega0\t$1\t$1\t$1\t0.4\n"}
			if($line=~/\. Omega 1:	(\S+)\s+\((\S+)\, (\S+)\)/){print OUT "$t\t$ncdr\t$n\t$ri\t$time\tomega1\t$1\t$2\t$3\t0.7\n"}
			elsif($line=~/\. Omega 1:	(\S+)/){print OUT "$t\t$ncdr\t$n\t$ri\t$time\tomega1\t$1\t$1\t$1\t0.7\n"}
			if($line=~/Motif\:\s+(\S+)\s+\d+\s+\d+\s+(\S+)\s+\((\S+)\,\s+(\S+)\)/){print  OUT "$t\t$n\t$ri\t$time\t$1\t$2\t$3\t$4\t$tparam{$1}\n"}
			elsif($line=~/Motif\:\s+(\S+)\s+\d+\s+\d+\s+(\S+)/){print OUT "$t\t$ncdr\t$n\t$ri\t$time\t$1\t$2\t$2\t$2\t$tparam{$1}\n"}
			if($line=~/Position \d\:\s+f\((\S\d)\)\=(\S+)\s+f\((\S\d)\)\=(\S+)\s+f\((\S\d)\)\=(\S+)\s+f\((\S\d)\)\=(\S+)/){
				print "$line";
			}
			if($line =~ /Ind\s+Seq\s+TreeL/){
				while(<IN>){
					$line = $_;
					last if $line =~ /oooooooooooooooo/;
					my @in = split("\\s+",$line);
					$lincount++;
					$sumdiv += $in[2];
					if($in[2] > $maxdiv){
						$maxdiv = $in[2];
					}
				}
				my $meandiv = $sumdiv/$lincount;
				my $tmeandiv = $tl/$lincount;
				if($ncdr eq "ncdr_"){
					$tmeandiv = $tlncdr3/$lincount;
				}
				print OUT "$t\t$ncdr\t$n\t$ri\t$time\tMaxDiv\t$maxdiv\t$maxdiv\t$maxdiv\tNA\n";
				print OUT "$t\t$ncdr\t$n\t$ri\t$time\tMeanDiv\t$meandiv\t$meandiv\t$meandiv\t$tmeandiv\n";
				print OUT "$t\t$ncdr\t$n\t$ri\t$time\tSumDiv\t$sumdiv\t$sumdiv\t$sumdiv\t$tl\n";
			}
		}
	}else{
		print("Skipping $f\n");
	}
}

#system("Rscript analyzeRepertoireResults.R") == 0 or die();
