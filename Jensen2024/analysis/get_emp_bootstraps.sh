#!/bin/bash

#SBATCH --time=1-00:00:00
#SBATCH --nodes=1
#SBATCH --mem=300g
#SBATCH --cpus-per-task=50
#SBATCH --partition day

module load R/4.1.0

nprocs=$1
proportion_light=$2
bootstrap_amount=$3
data=$4
quiet=$5
build=$6


Rscript empirical_bootstrapping.R $nprocs $proportion_light $bootstrap_amount $data $quiet $build
