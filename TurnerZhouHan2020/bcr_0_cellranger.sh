#!/bin/bash

##### 10x software
PATH_10X="/ysm-gpfs/pi/kleinstein/cellrager_3.1.0/"
PATH_CR_EXE="${PATH_10X}cellranger-3.1.0/cellranger"

# different from the reference for GEX
# https://support.10xgenomics.com/single-cell-vdj/software/pipelines/latest/installation
# https://support.10xgenomics.com/single-cell-vdj/software/downloads/latest
PATH_CR_REF="${PATH_10X}refdata-cellranger-vdj-GRCh38-alts-ensembl-3.1.0/"

#export PATH=${PATH_CR_MAIN}:$PATH


##### meta

PATH_ROOT="/home/qz93/scratch60/"

PATH_OUTPUT="${PATH_ROOT}cr_bcr/"

PATH_FASTQ="${PATH_ROOT}ellebedy_10x_P05_globus/"

PATH_SAMPLE_LIST="${PATH_ROOT}cr_bcr/cellRanger_bcr_run_sampleList.txt" #*

##### log

PATH_LOG="${PATH_OUTPUT}bcr_$(date '+%m%d%Y_%H%M%S').log"

dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
#parallel --version | head -n 1 &>> "${PATH_LOG}"
"${PATH_CR_EXE}" vdj --version &>> "${PATH_LOG}"

echo "sample/library list: ${PATH_SAMPLE_LIST}" &>> "${PATH_LOG}"

N_LINES=$(wc -l < "${PATH_SAMPLE_LIST}")
echo "N_LINES="$N_LINES &>> "${PATH_LOG}"

cd "${PATH_OUTPUT}"

for ((IDX=1;IDX<=${N_LINES};IDX++)); do
	
	echo "IDX=${IDX}" &>> "${PATH_LOG}"

	# read Library Name from file
	CUR_LIB=$(sed "${IDX}q;d" "${PATH_SAMPLE_LIST}")
	echo "CUR_LIB=${CUR_LIB}" &>> "${PATH_LOG}"

	# A unique run id, used to name output folder [a-zA-Z0-9_-]+.
	# The pipeline will create a new folder at current directory
	# named with the ID you specified for its output. 
	# If this folder already exists, cellranger will assume it is an existing 
	# pipestance and attempt to resume running it.
	
	# 14 b/c "H_ZW-WU321-05-"
	#* only works if lib names unique 

	ID="${CUR_LIB:14}"
	echo "ID=${ID}" &>> "${PATH_LOG}"

	# lib-specific log
	PATH_LOG_LIB="${PATH_OUTPUT}bcr_${ID}_$(date '+%m%d%Y_%H%M%S').log"

	# depending on library prep and sequencing setup, may or may not need to specify --sample
	# https://support.10xgenomics.com/single-cell-vdj/software/pipelines/latest/using/fastq-input#mkfastq

	# https://support.10xgenomics.com/single-cell-vdj/software/pipelines/latest/using/vdj

	# output files: ${PATH_OUTPUT}/${ID}/outs/*

	"${PATH_CR_EXE}" vdj \
		--id="${ID}" \
		--fastqs="${PATH_FASTQ}" \
		--reference="${PATH_CR_REF}" \
		--sample="${CUR_LIB}" \
		&>> "${PATH_LOG_LIB}"

done

echo "DONE" &>> "${PATH_LOG}"
