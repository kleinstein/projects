#--------------------------------------------
# Predict class probability of S protein binding
#
# Example usage: 
#
# python COVID19_specificity_repertoire_prediction_finetuning.py ft-ESM2 HL S_repertoires.parquet checkpoint-7290
#--------------------------------------------

import pickle
import argparse
import re
import time
from datetime import date
import random
import os
from collections import Counter

import torch
import pandas as pd
import numpy as np
from Bio import SeqIO
import antiberty
import transformers
from transformers import (
    AutoTokenizer, 
    AutoModelForSequenceClassification,
    Trainer,
    TrainingArguments
)
from torch.utils.data import DataLoader
from datasets import (
    Dataset,
    DatasetDict,
    Sequence,
    ClassLabel
)
from sklearn.metrics import (
    precision_score,
    recall_score,
    f1_score,
    matthews_corrcoef,
    roc_auc_score,
    average_precision_score, 
    balanced_accuracy_score
)
from sklearn.model_selection import (
    StratifiedGroupKFold
)
import json
import scipy

parser = argparse.ArgumentParser(description="Gene usage tasks")
parser.add_argument("embedding", type=str, help="Type of model (e.g. BALM_paired)")
parser.add_argument("input", type=str, help="Type of model (HL, H)")
parser.add_argument("data", type=str, help="Dataset")
parser.add_argument("checkpoint", type=str, help="The checkpoint to load the model")
args = parser.parse_args()

embedding = args.embedding
input = args.input
dataset = args.data
checkpoint = args.checkpoint

def load_model(model_dir):
    tokenizer = AutoTokenizer.from_pretrained(model_dir)
    model = AutoModelForSequenceClassification.from_pretrained(model_dir, num_labels=2)
    model = model.cuda()
    model_size = sum(p.numel() for p in model.parameters())
    print(f"Model size: {model_size/1e6:.2f}M")
    return model, tokenizer

MAX_LENGTH = 512
if 'antiBERTa2' in embedding:
    MAX_LENGTH = 256
elif ('BALM_paired' in embedding) or ('antiBERTy' in embedding):
    MAX_LENGTH = 512 - 2

def preprocess(batch):    
    t_inputs = tokenizer(batch['sequence'], 
                         padding="max_length",
        truncation=True,
        max_length=MAX_LENGTH,
        return_special_tokens_mask=True)
    batch['input_ids'] = t_inputs.input_ids
    batch['attention_mask'] = t_inputs.attention_mask
    return batch

def process_data(train):
    ab_dataset = DatasetDict({
        "train": Dataset.from_pandas(train)
    })
    ab_dataset_featurised = ab_dataset.map(
        lambda seq: {
            "sequence": seq,
        }, 
        input_columns=[input], batched=True
    )
    ab_dataset_tokenized = ab_dataset_featurised.map(
        preprocess, 
        batched=True,
        remove_columns=[input]
    )
    return ab_dataset_tokenized

def insert_space_every_other_except_cls(input_string):
    parts = input_string.split('[CLS]')
    modified_parts = [''.join([char + ' ' for char in part]).strip() for part in parts]
    result = ' [CLS] '.join(modified_parts)
    return result

# 1. Load model
print(f"### Loading model... ")
base_dir = "../models/"
model_dir = base_dir + f"COVID19_specificity_{embedding}_FULL_{input}-fine_tuning_FULL/" + checkpoint
model, tokenizer = load_model(model_dir)

# 2. Format the sequences
print(f"### Loading sequences... ")
data_dir = "../data/"
data = pd.read_parquet(data_dir + dataset)
MAX_LENGTH = 512
if 'antiBERTa2' in embedding:
    MAX_LENGTH = 256
elif ('BALM_paired' in embedding) or ('antiBERTy' in embedding):
    MAX_LENGTH = 512 - 2

if ('antiBERTa2' in embedding) | ('antiBERTy' in embedding):
    data.loc[:,args.input] = data.loc[:,args.input].apply(lambda a: a[:MAX_LENGTH])
    data.loc[:,args.input] = data.loc[:,args.input].str.replace('<cls><cls>', '[CLS][CLS]')
    data.loc[:,args.input] = data.loc[:,args.input].apply(insert_space_every_other_except_cls)
    data.loc[:,args.input] = data.loc[:,args.input].str.replace('  ', ' ')

ab_dataset_tokenized = process_data(data)
print(f"Loaded {model_dir} and data")

# 3. Predict 
print(f"### Predicting...")
model.eval()

# hyperparameters
batch_size = 64
RUN_ID = f"S_{embedding}_FULL_{input}-fine_tuning"
LR = 1e-5
n_epoch = 30
SEED = 0
folder = f"{RUN_ID}_FULL"
print(f"Saving model to {folder}")
training_args = TrainingArguments(
    folder,
    evaluation_strategy="epoch",
    save_strategy = "epoch",
    logging_strategy='epoch',
    learning_rate=LR, 
    per_device_train_batch_size=batch_size,
    per_device_eval_batch_size=batch_size,
    num_train_epochs=n_epoch,
    warmup_ratio=0,
    load_best_model_at_end=False,
    lr_scheduler_type='linear',
    seed=SEED
)

trainer = Trainer(
    model,
    args=training_args,
    tokenizer=tokenizer
)

# evaluate on the test dataset
outputs = trainer.predict(ab_dataset_tokenized["train"])
probs = torch.softmax(torch.from_numpy(outputs.predictions), dim=1).detach().numpy()[:,-1]

print(f"Predicted the binding probability of {probs.shape[0]} sequences.")

# 3. Load labels
data.loc[:,"pred_prob"] = probs

# 4. save the outputs as tsv
print(f"### Saving outputs...")
base_dir = "../data/"
data_path = dataset.split('.', 1)[0]
out_path = base_dir + f"{data_path}_{input}_{embedding}_COVID19_finetuned_prediction.csv"
data.to_csv(out_path)
print(f"Saved the output to {out_path}.")