#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --job-name=split
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=27
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=24:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com

#perl split_to_patients.pl
perl split_to_patients.V0.pl

