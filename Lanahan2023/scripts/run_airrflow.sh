nextflow pull nf-core/airrflow -r 3.1.0
nextflow run nf-core/airrflow -r 3.1.0 \
-profile docker \
--input metadata_BCR_umi.tsv \
--mode assembled \
--outdir results \
--collapseby sample_id \
--cloneby subject_id \
--crossby subject_id \
--max_memory 60.GB \
--max_cpus 16 \
--clonal_threshold 0.1 \
--igblast_base ../references/igblast \
--imgtdb_base  ../references/imgt \
-resume
