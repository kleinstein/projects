#!/bin/bash
#SBATCH --partition=general
#SBATCH --job-name=annotation
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=10G
#SBATCH --time=1-00:00:00
#SBATCH --error=log/annotations.%A_%a.err
#SBATCH --output=log/annotations.%A_%a.out
#SBATCH --array=1-12

taskID=${SLURM_ARRAY_TASK_ID}

script="/gpfs/ysm/project/kleinstein/mw957/repos/shaw_flu/scripts/5_bcr_annotations.R"

indir="/gpfs/gibbs/pi/kleinstein/shaw_flu/outs"
outdir=$indir"/5_annotations/"
infiles_heavy=(`ls $indir/4_clonotyping/*_light-cluster-pass.tsv`)
infiles_light=(`ls $indir/3_filter/*_light.tab`)
infiles=( "${infiles_heavy[@]}" "${infiles_light[@]}" )

inpath=${infiles[$taskID-1]}

echo "[$0 $(date +%Y%m%d-%H%M%S)] [start] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"

sample=$(basename $inpath)
sampleid=$(echo $sample | cut -f1 -d".")
outpath=$outdir/$sampleid"_translated.tsv"

apptainer exec /gpfs/ysm/project/kleinstein/mw957/software/immcantation/lab_devel.sif Rscript $script $inpath $outpath

echo "[$0 $(date +%Y%m%d-%H%M%S)] [end] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"
