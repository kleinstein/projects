#!/usr/bin/env python3
"""
Parse and extract essential seq info from CSV
"""

import re, sys, os
from os import path
from sys import argv
import pandas as pd

# inputCSV name format:
# 326651/16_consensus.txt

# output header format: 
# >34f631d6-6dfb-4146-a52f-93dfc5aa3786_1|BP=yes|BI=IgM

# goals:
# - parse CSV
# - extract these fields: seq_id, productive, isotype, raw_input
# - output as FASTA

def parseCSV(inputCSV, outputFasta):

	if os.path.exists(inputCSV):
		print("Processing " + inputCSV)

		df = pd.read_csv(inputCSV, dtype=object)

		# number of rows
		nrow = df.shape[0]

		with open(outputFasta, 'w') as out_handle:

			for i in range(nrow):
				#print(i)
				curSeqId = df["seq_id"][i]
				curProd = df["productive"][i]
				curIsotype = df["isotype"][i]
				curRaw = df["raw_input"][i]

				curHeader = ">" + curSeqId + "|BP=" + curProd + "|BI=" + curIsotype

				out_handle.writelines(curHeader+"\n")
				out_handle.writelines(curRaw+"\n")

			print("Outputted as: ", outputFasta)
	else:
		print(inputCSV + " does not exist.")

# run
parseCSV(sys.argv[1], sys.argv[2])
