## ---------------------------
##
## Script name: 9_MELD_Age_d0.py
##
## Purpose of script: Run MELD on Age on D0 data
##
## Author: Mamie Wang
##
## Date Created: 2022-07-25
##
## Email: mamie.wang@yale.edu
##
## ---------------------------
## load the packages and functions

import pandas as pd
import numpy as np
import meld
import graphtools
import sklearn
import argparse

##---------------------------
## load the arguments

parser = argparse.ArgumentParser(description="Run MELD on cell subsets.")
parser.add_argument("cell_type", help="Cell subset (full, Naive B, RMB, ABC, PB, PB proliferating")
args = parser.parse_args()
cell_type = args.cell_type

## ---------------------------
## load the data

indir = "/gpfs/ysm/project/mw957/repos/shaw_flu/data/outs/8_B_clustering/"
outdir = "/gpfs/ysm/project/mw957/repos/shaw_flu/data/outs/9_d0_MELD/"
B_cell_data = pd.read_csv(indir + "B_counts_scaled.csv", index_col=0)

metadata = pd.read_csv(indir + "B_meta_data.csv", index_col=0)
metadata.loc[:,'is_older'] = np.where(metadata['Age Group'] == "Older", 1, 0)

## ---------------------------
## perform MELD on age group labels for day 0 data

is_d0 = list(metadata.day == "D0")
d0_dat = B_cell_data.loc[:, is_d0]
d0_meta = metadata.loc[is_d0,:]

## ---------------------------
## MELD or Vertex frequency clustering on each clusters

if cell_type == "full":
    d0_G = graphtools.Graph(d0_dat.values.T, knn=20, n_pca=50, use_pygsp=True, 
                     n_jobs=-2, verbose=True)

    meld_op = meld.MELD(beta = 67, knn = 7)

    sample_density = meld_op.fit_transform(d0_G, sample_labels = d0_meta.is_older)

    d0_older_RL = sklearn.preprocessing.normalize(sample_density, norm='l1')

    d0_older_RL = pd.DataFrame(d0_older_RL, columns=["Young", "Older"], index = d0_meta.index)

else:
    n_clusters = [2, 3, 4, 5]

    is_target = d0_meta.annotated_cluster == cell_type
    cell_type_meta = d0_meta.loc[is_target,:]
    cell_type_data = pd.DataFrame(d0_dat).loc[:, is_target]
    curr_sample_labels = cell_type_meta.is_older
    d0_older_RL = np.empty(shape=(cell_type_meta.shape[0], len(n_clusters) + 1), dtype='float')

    curr_G = graphtools.Graph(cell_type_data.values.T, n_pca=50, use_pygsp=True, knn = 20)

    meld_op = meld.MELD(beta = 67, knn = 7)
    sample_density = meld_op.fit_transform(curr_G, sample_labels = curr_sample_labels)
    curr_likelihood = sklearn.preprocessing.normalize(sample_density, norm='l1')[:,1]
    d0_older_RL[:, 0] = curr_likelihood

    for i, n_cluster in enumerate(n_clusters):
        curr_vfc = meld.VertexFrequencyCluster(n_clusters = n_cluster)
        curr_vfc.fit_transform(curr_G, curr_sample_labels, curr_likelihood)
        d0_older_RL[:, i + 1] = curr_vfc.predict(n_cluster)
    d0_older_RL = pd.DataFrame(d0_older_RL, 
                               index = cell_type_meta.index, 
                               columns = ["Older", "k = 2", "k = 3", "k = 4", "k = 5"])

## ---------------------------
## save the VFC score
d0_older_RL.to_csv(outdir + "age_RL_" + cell_type + ".csv")
