#!/usr/bin/bash
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=48:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com
#SBATCH --exclude=c24n04

sims=$1
i=$2
part=$3
start=$4
threads=$5
shape=$6

perl simulateRepertoire.pl $sims $threads $i/seqs_lineages.HLP.tsv 0.7 $i/sims extra/HD1310.tsv sim_$shape pi_kleinstein $start $shape >> $i/$shape\_simlog.txt
