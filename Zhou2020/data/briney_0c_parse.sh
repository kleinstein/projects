#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 4-
#SBATCH --constraint E5-2660_v3
#SBATCH -c 1
#SBATCH --mem-per-cpu=118G

# JSON subjects
# Job Wall-clock time: 12:09:11
# Memory Utilized: 91.15 GB

# CSV subjects
# Job Wall-clock time: 09:09:33
# Memory Utilized: 8.02 GB

PATH_ROOT="/home/qz93/scratch60/briney2019/"
PATH_BRINEY="${PATH_ROOT}consensus_csv/"
PATH_PARSED="${PATH_ROOT}raw_parsed/"

PATH_LOG="${PATH_PARSED}parse_$(date '+%m%d%Y_%H%M%S').log"

PATH_SCRIPT="/home/qz93/projects/scripts/spatial/auxiliary/"
PATH_SCRIPT_JSON="${PATH_SCRIPT}briney_parse_json.py"
PATH_SCRIPT_CSV="${PATH_SCRIPT}briney_parse_csv.py"

chmod +x "${PATH_SCRIPT_JSON}"
chmod +x "${PATH_SCRIPT_CSV}"

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
python3 -V &>> "${PATH_LOG}"

SUBJ_ARRAY=(326651 326713 316188 326650 326737 326780 326797 326907 327059 D103)
#SUBJ_ARRAY=(326651 326713) # JSON only
#SUBJ_ARRAY=(316188 326650 326737 326780 326797 326907 327059 D103) # CSV only
#SUBJ_ARRAY=(326651) # testing

N_SAMPLES="18"
SAMPLE_ARRAY=($(seq 1 ${N_SAMPLES}))
#SAMPLE_ARRAY=(17) # testing

# every subject has its own folder, with 18 samples/files inside

for SUBJ in ${SUBJ_ARRAY[@]}; do

	DIR_BRINEY="${PATH_BRINEY}${SUBJ}/"
	DIR_PARSED="${PATH_PARSED}${SUBJ}/"

	# if DIR_PARSED does not exist, mkdir
	if [ ! -d "${DIR_PARSED}" ]; then
		mkdir "${DIR_PARSED}"
	fi

	if [[ ${SUBJ} == "326651" || ${SUBJ} == "326713" ]]; then
		FILENAME_SUFFIX=".json"
	else
		FILENAME_SUFFIX=".txt"
	fi

	# loop thru samples
	for SAMPLE in ${SAMPLE_ARRAY[@]}; do

		SAMPLE_BRINEY="${DIR_BRINEY}${SAMPLE}_consensus${FILENAME_SUFFIX}"
		SAMPLE_PARSED="${DIR_PARSED}${SUBJ}_${SAMPLE}.fasta"

		if [[ (-s "${SAMPLE_BRINEY}" ) ]]; then
			
			if [[ ${SUBJ} == "326651" || ${SUBJ} == "326713" ]]; then
				"${PATH_SCRIPT_JSON}" "${SAMPLE_BRINEY}" "${SAMPLE_PARSED}" &>> "${PATH_LOG}"
			else
				"${PATH_SCRIPT_CSV}" "${SAMPLE_BRINEY}" "${SAMPLE_PARSED}" &>> "${PATH_LOG}"
			fi

		else
			echo "${SAMPLE_BRINEY} does not exist" &>> "${PATH_LOG}"
		fi

	done

done

