#!/usr/bin/bash
#SBATCH --mem-per-cpu=5gb
#SBATCH --partition=pi_kleinstein
#SBATCH --cpus-per-task=36
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=100:00:00
#SBATCH --mail-type=FAIL,END
#SBATCH --mail-user=kenneth.hoehn@gmail.com

module load R/4.1.0-foss-2020b
module load Python/3.8.6-GCCcore-10.2.0

Rscript findClonalVariants.R
Rscript analyzeClones.R
