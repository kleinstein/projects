#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 2-
#SBATCH --constraint E5-2660_v3
#SBATCH -c 20
#SBATCH --mem-per-cpu=2G


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

PATH_ROOT_1="/home/qz93/project/ellebedy_pairedBulk/" #*
PATH_ROOT_2="/home/qz93/project/ellebedy_bulk/"
PATH_PHIX="${PATH_ROOT_1}phix/"
PATH_PRESTO="${PATH_ROOT_1}presto/"
PATH_SUPPORT="${PATH_ROOT_2}preprocess_support_files/"
PATH_ABSEQ="${PATH_SUPPORT}AbSeq/"

FILENAME_P1="AbSeq_R1_Human_IG_Primers.fasta"
FILENAME_P2="AbSeq_R2_TS.fasta"
FILENAME_IC="AbSeq_Human_IG_InternalCRegion.fasta"

GERM_TAG="ref_201931-4_1Aug2019" #*
# same as "~/germlines/ref_201931-4_1Aug2019/human_IG/concat_imgt_human_IG_V.fasta" #*
FILENAME_VREF="human_imgt_V_08012019.fasta" #*

FILENAME_YAML="prestor.yaml"

PATH_SCRIPT_ABSEQ="/home/qz93/projects/scripts/turnerZhouHan_2020/immcantation_presto-abseq_10242018_JZ.sh" 
PATH_SCRIPT_Q2A="/home/qz93/projects/scripts/turnerZhouHan_2020/immcantation_fastq2fasta.py"
PATH_SCRIPT_RMV_INCONSISTENT="/home/qz93/projects/scripts/turnerZhouHan_2020/JZ_remove_inconsistent_internalC.py" 

chmod +x "${PATH_SCRIPT_ABSEQ}"
chmod +x "${PATH_SCRIPT_Q2A}"
chmod +x "${PATH_SCRIPT_RMV_INCONSISTENT}"
#chmod +x "${PATH_SCRIPT_META}" #*

# log (for lopping through samples)
PATH_LOG="${PATH_PRESTO}presto_allSamples_$(date '+%m%d%Y_%H%M%S').log"

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "germline" $GERM_TAG &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
parallel --version | head -n 1 &>> "${PATH_LOG}"
MaskPrimers.py --version &>> "${PATH_LOG}" # presto version
muscle -version &>> "${PATH_LOG}" # muscle version
usearch --version &>> "${PATH_LOG}" # usearch version
python3 -V &>> "${PATH_LOG}"

# phix directory structure
# phix/
# 	SampleName_R1_noPhix_selected.fastq
# 	SampleName_R2_noPhix_selected.fastq

#SUBJ_ARRAY=(S7) # testing 
SUBJ_ARRAY=(S7 S8 S9 S10 S11 S12) 

for SUBJ in ${SUBJ_ARRAY[@]}; do

	echo "*********** STARTING ON ${SUBJ} ***********" &>> "${PATH_LOG}"

	# presto-abseq.sh creates a logs/ and report/ folder for each run
	# the log and report files inside have the same filename across runs
	# -> set up a separate folder within /presto for each sample

	cd "${PATH_PRESTO}"

	# create sample folder: SampleName
	DIR_SAMPLE="${SUBJ}/"

	# if existing, remove first
	if [ -d "${DIR_SAMPLE}" ]; then
		echo "Removed pre-exisitng folder for sample ${SUBJ}" &>> "${PATH_LOG}"
		rm -r "${DIR_SAMPLE}"
	fi

	echo "Created new folder for sample ${SUBJ}" &>> "${PATH_LOG}"
	mkdir "${DIR_SAMPLE}"

	# start sample-specific log file
	PATH_LOG_SAMPLE="${PATH_PRESTO}${DIR_SAMPLE}presto_sample-${SUBJ}_$(date '+%m%d%Y_%H%M%S').log"
	dt=$(date '+%d/%m/%Y %H:%M:%S')
	echo "$dt" &> "${PATH_LOG_SAMPLE}"


	#   -1  Read 1 FASTQ sequence file (sequence beginning with the C-region or J-segment).
	#   -2  Read 2 FASTQ sequence file (sequence beginning with the leader or V-segment).
	#   -j  Read 1 FASTA primer sequences (C-region or J-segment).
	#       Defaults to /usr/local/share/protocols/AbSeq/AbSeq_R1_Human_IG_Primers.fasta
	#   -v  Read 2 FASTA primer sequences (template switch or V-segment).
	#       Defaults to /usr/local/share/protocols/AbSeq/AbSeq_R2_TS.fasta.
	#   -c  C-region FASTA sequences for the C-region internal to the primer.
	#       If unspecified internal C-region alignment is not performed.
	#   -r  V-segment reference file.
	#       Defaults to /usr/local/share/germlines/igblast/fasta/imgt_human_ig_v.fasta
	#   -y  YAML file providing description fields for report generation.
	#   -n  Sample name or run identifier which will be used as the output file prefix.
	#       Defaults to a truncated version of the read 1 filename.
	#   -o  Output directory.
	#       Defaults to the sample name.
	#   -x  The mate-pair coordinate format of the raw data.
	#       Defaults to illumina.
	#   -p  Number of subprocesses for multiprocessing tools.
	#       Defaults to the available processing units.

	# choose -x illumina or -x presto based on header

	"${PATH_SCRIPT_ABSEQ}" \
		-1 "${PATH_PHIX}${SUBJ}_R1_noPhix_selected.fastq" \
		-2 "${PATH_PHIX}${SUBJ}_R2_noPhix_selected.fastq" \
		-j "${PATH_ABSEQ}${FILENAME_P1}" \
		-v "${PATH_ABSEQ}${FILENAME_P2}" \
		-c "${PATH_ABSEQ}${FILENAME_IC}" \
		-r "${PATH_SUPPORT}${FILENAME_VREF}" \
		-y "${PATH_SUPPORT}${FILENAME_YAML}" \
		-n "${SUBJ}" \
		-o "${PATH_PRESTO}${DIR_SAMPLE}" \
		-x "illumina" \
		-p "${NPROC}" \
		-t "${PATH_SCRIPT_RMV_INCONSISTENT}" \
		&>> "${PATH_LOG_SAMPLE}"

	# convert fastq to fasta
	# creates ${SUBJ}-final_collapse-unique_atleast-2.fasta
	cd "${PATH_PRESTO}${DIR_SAMPLE}"
	"${PATH_SCRIPT_Q2A}" "${SUBJ}-final_collapse-unique_atleast-2.fastq" &>> "${PATH_LOG_SAMPLE}"

	# add meta info
	#"${PATH_SCRIPT_META}" \
	#	"${PATH_PRESTO}${DIR_SAMPLE}${SUBJ}-final_collapse-unique_atleast-2.fasta" \
	#	"${SUBJ}" \
	#	"${PATH_PRESTO}${DIR_SAMPLE}${SUBJ}-final_collapse-unique_atleast-2_meta.fasta" \
	#	&>> "${PATH_LOG_SAMPLE}"

done

