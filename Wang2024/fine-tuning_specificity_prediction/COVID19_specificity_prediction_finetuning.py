#--------------------------------------------
# Fine-tune language models to predict
# S protein binding
#
# Example usage:
#
# python COVID19_specificity_prediction_finetuning.py ft-ESM2 HL FULL
#--------------------------------------------

import pickle
import argparse
import re
import time
from datetime import date
import random
import os
from collections import Counter

import torch
import pandas as pd
import numpy as np
from Bio import SeqIO
import antiberty
import transformers
from transformers import (
    AutoTokenizer,
    AutoModelForSequenceClassification,
    Trainer,
    TrainingArguments
)
from torch.utils.data import DataLoader
from datasets import (
    Dataset,
    DatasetDict,
    Sequence,
    ClassLabel
)
from sklearn.metrics import (
    precision_score,
    recall_score,
    f1_score,
    matthews_corrcoef,
    roc_auc_score,
    average_precision_score,
    balanced_accuracy_score
)
from sklearn.model_selection import (
    StratifiedGroupKFold
)
import json
#--------------------------------------------

parser = argparse.ArgumentParser(description="Fine-tune language models to predict S protein binding")
parser.add_argument("embedding", type=str, help="Type of model (antiBERTy, antiBERTa2, BALM_paired, ft-ESM2)")
parser.add_argument("model", type=str, help="Type of model (HL, H)")
parser.add_argument("fold", type=str, help="The cross-validation fold to run (1, 2, 3, 4, FULL")
parser.add_argument("--resume", type=str, default=None, help="Which folder to resume")
args = parser.parse_args()

def return_model_dir(model):
    if "BALM_paired" in model:
        base_dir = '../models/'
        model_dir = base_dir + 'BALM-paired_LC-coherence_90-5-5-split_122222'
        token_dir = model_dir

    elif "ESM2" in model:
        model_dir = "facebook/esm2_t33_650M_UR50D"
        token_dir = model_dir
        if "ft" in model:
            base_dir = '../models/'
            model_dir = base_dir + 'ESM2-650M_paired-fine-tuning/'

    elif "antiBERTa2" in model:
        model_dir = token_dir = "alchemab/antiberta2"

    elif "antiBERTy" in model:
        project_path = os.path.dirname(os.path.realpath(antiberty.__file__))
        trained_models_dir = os.path.join(project_path, 'trained_models')
        model_dir = os.path.join(trained_models_dir, 'AntiBERTy_md_smooth')
        token_dir = os.path.join(trained_models_dir, 'vocab.txt')

    if args.resume:
        model_dir = args.resume

    return model_dir, token_dir

def load_model(model):
    model_dir, token_dir = return_model_dir(model)
    if 'antiBERTy' in model:
        tokenizer = transformers.BertTokenizer(vocab_file=token_dir, do_lower_case=False)
    else:
        tokenizer = AutoTokenizer.from_pretrained(token_dir)
    model = AutoModelForSequenceClassification.from_pretrained(model_dir, num_labels=2)
    model = model.cuda()
    model_size = sum(p.numel() for p in model.parameters())
    print(f"Model size: {model_size/1e6:.2f}M")

    return model, tokenizer

def freeze_weights(model, embedding, layers = 3):
    if "BALM" in embedding:
        for param in model.roberta.embeddings.parameters():
            param.requires_grad = False
        for layer in model.roberta.encoder.layer[:(24-layers)]:
            for param in layer.parameters():
                param.requires_grad = False
    if "ESM2" in embedding:
        for param in model.esm.embeddings.parameters():
            param.requires_grad = False
        for layer in model.esm.encoder.layer[:(33-layers)]:
            for param in layer.parameters():
                param.requires_grad = False
    if "antiBERTy" in embedding:
        for param in model.bert.embeddings.parameters():
            param.requires_grad = False
        for layer in model.bert.encoder.layer[:(8-layers)]:
            for param in layer.parameters():
                param.requires_grad = False
    if "antiBERTa2" in embedding:
        for param in model.roformer.embeddings.parameters():
            param.requires_grad = False
        for layer in model.roformer.encoder.layer[:(16-layers)]:
            for param in layer.parameters():
                param.requires_grad = False
    return model

def insert_space_every_other_except_cls(input_string):
    parts = input_string.split('[CLS]')
    modified_parts = [''.join([char + ' ' for char in part]).strip() for part in parts]
    result = ' [CLS] '.join(modified_parts)
    return result

MAX_LENGTH = 512
if 'antiBERTa2' in args.embedding:
    MAX_LENGTH = 256
elif ('BALM_paired' in args.embedding) or ('antiBERTy' in args.embedding):
    MAX_LENGTH = 512 - 2

def load_data(embedding, model = "HL"):
    # load the labels
    if "FULL" in embedding:
        print(f"Loading full length data...")
        path = "S_FULL.parquet"
    else:
        print(f"Loading CDR3 data...")
        path = "S_CDR3.parquet"
    BASEDIR = "../data/"
    dat = pd.read_parquet(BASEDIR + path)

    # load the matching sequences
    X = dat.loc[:,args.model]

    if ('antiBERTa2' in embedding) | ('antiBERTy' in embedding):
        X = X.apply(lambda a: a[:MAX_LENGTH])
        X = X.str.replace('<cls><cls>', '[CLS][CLS]')
        X = X.apply(insert_space_every_other_except_cls)
        X = X.str.replace('  ', ' ')

    y_groups = dat.subject.values
    y = np.isin(dat.label.values, ["S+", "S1+", "S2+"]).astype(int)
    assert X.shape[0] == len(y)
    return X, y, y_groups

def preprocess(batch):
    t_inputs = tokenizer(batch['sequence'],
                         padding="max_length",
        truncation=True,
        max_length=MAX_LENGTH,
        return_special_tokens_mask=True)
    batch['input_ids'] = t_inputs.input_ids
    batch['attention_mask'] = t_inputs.attention_mask
    return batch

def process_data(train, val = None, test = None):
    if (val is None) and (test is None):
        ab_dataset = DatasetDict({
        "train": Dataset.from_pandas(train)
    })
    elif (val is None):
        ab_dataset = DatasetDict({
            "train": Dataset.from_pandas(train),
            "test": Dataset.from_pandas(test)
        })
    else:
        ab_dataset = DatasetDict({
            "train": Dataset.from_pandas(train),
            "validation": Dataset.from_pandas(val),
            "test": Dataset.from_pandas(test)
        })
    class_label = ClassLabel(2, names=[0, 1])
    ab_dataset_featurised = ab_dataset.map(
        lambda seq, labels: {
            "sequence": seq,
            "labels": class_label.str2int(labels)
        },
        input_columns=["sequence", "labels"], batched=True
    )
    ab_dataset_tokenized = ab_dataset_featurised.map(
        preprocess,
        batched=True,
        remove_columns=['sequence']
    )
    return ab_dataset_tokenized

def compute_metrics(p):
    """
    A callback added to the trainer so that we calculate various metrics via sklearn
    """
    predictions, labs = p

    probs = torch.softmax(torch.from_numpy(predictions), dim=1).detach().numpy()[:,-1]
    # We run an argmax to get the label
    preds = np.argmax(predictions, axis=1)

    return {
        "precision": precision_score(labs, preds, pos_label=1),
        "recall": recall_score(labs, preds, pos_label=1),
        "f1": f1_score(labs, preds, pos_label=1, average = "weighted"),
        "apr": average_precision_score(labs, probs, pos_label=1),
        "balanced_accuracy": balanced_accuracy_score(labs, preds),
        "auc": roc_auc_score(labs, probs),
        "mcc": matthews_corrcoef(labs, preds),
    }

def set_seed(seed: int = 42):
    """
    Set all seeds to make results reproducible (deterministic mode).
    When seed is None, disables deterministic mode.
    """
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    np.random.seed(seed)
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)

# 1. Load embeddings and labels
X, y, y_groups = load_data(args.embedding, args.model)
y_group_counts = Counter(y_groups)
print(f"Class size: {Counter(np.sort(y)).most_common()}")

print(f"In total, {len(y)} sequences from {len(np.unique(y_groups))} donors/studies.")
print(f"Class size: {Counter(np.sort(y)).most_common()}")

# load the model
model_dir, token_dir = return_model_dir(args.embedding)
model, tokenizer = load_model(args.embedding)
model = freeze_weights(model, args.embedding)

# hyperparameters
batch_size = 64
RUN_ID = f"S_{args.embedding}_{args.model}-fine_tuning"
LR = 1e-5
n_epoch = 30

if args.fold != "FULL":
    # 2. Cross validation + fine-tuning (combine all data)
    n_splits_outer = 4
    n_splits_inner = 3
    if "FULL" in args.embedding:
        random_state = 9
    else:
        random_state = 7
    outer_cv = StratifiedGroupKFold(n_splits=n_splits_outer, shuffle=True, random_state=random_state)
    inner_cv = StratifiedGroupKFold(n_splits=n_splits_inner, shuffle=True, random_state=1)
    outer_cv_groups = outer_cv.split(X, y, y_groups)
    i = 1

    while i <= int(args.fold):
        k, (train_index, test_index) = next(enumerate(outer_cv_groups))
        i += 1

    #for train_index, test_index in outer_cv_groups:
    print(f"##### Outer fold {i - 1} #####")
    # get the cross validation score on the test
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]
    y_groups_train = y_groups[train_index]
    print(f"Train size: {len(train_index)}, test size: {len(test_index)}")
    print(f"% positive train: {np.mean(y_train)}, % positive test: {np.mean(y_test)}")

    # split validation data from the training data
    inner_cv_groups = inner_cv.split(X_train, y_train, y_groups_train)
    j, (inner_train_index, val_index) = next(enumerate(inner_cv_groups))
    X_inner_train, X_val = X.iloc[inner_train_index], X.iloc[val_index]
    y_inner_train, y_val = y[inner_train_index], y[val_index]
    train = pd.DataFrame({'sequence': X_inner_train.values, 'labels': y_inner_train})
    val = pd.DataFrame({'sequence': X_val.values, 'labels': y_val})
    #train = pd.DataFrame({'sequence': X_train.values, 'labels': y_train})
    test = pd.DataFrame({'sequence': X_test.values, 'labels': y_test})
    print(f'Train data size: {train.shape[0]}')
    print(f'Validation data size: {val.shape[0]}')

    # process the data
    ab_dataset_tokenized = process_data(train, val, test)

    # parameters
    SEED = i-1
    set_seed(SEED)
    folder_base_dir = "../models/"
    folder = f"{RUN_ID}_Fold_{SEED}"
    folder_path = folder_base_dir + "/" + folder

    print(f"Saving model to {folder}")
    training_args = TrainingArguments(
        folder_path, # this is the name of the checkpoint folder
        evaluation_strategy="epoch",
        save_strategy = "epoch",
        logging_strategy='epoch',
        learning_rate=LR,
        per_device_train_batch_size=batch_size,
        per_device_eval_batch_size=batch_size,
        num_train_epochs=n_epoch,
        warmup_ratio=0,
        load_best_model_at_end=True,
        metric_for_best_model="auc",
        lr_scheduler_type='linear',
        seed=SEED
    )

    trainer = Trainer(
        model,
        args=training_args,
        tokenizer=tokenizer,
        train_dataset=ab_dataset_tokenized['train'],
        eval_dataset=ab_dataset_tokenized['validation'],
        compute_metrics=compute_metrics
    )

    # watch stuff fly
    if args.resume:
        trainer.train(resume_from_checkpoint=model_dir)
    else:
        trainer.train()

    # evaluate on the test dataset
    model.eval()
    outputs = trainer.predict(ab_dataset_tokenized["test"])
    print(outputs.metrics)

    out = trainer.state.log_history
    out.append(outputs.metrics)
    with open(folder_base_dir + '/' + folder + '/' + folder + '.json', 'w') as f:
        json.dump(out, f, indent=4)
else:
    train = pd.DataFrame({'sequence': X.values, 'labels': y})
    ab_dataset_tokenized = process_data(train)
    # parameters
    SEED = 0
    set_seed(SEED)
    folder_base_dir = "../models/"
    folder = f"{RUN_ID}_FULL"
    folder_path = folder_base_dir + "/" + folder
    print(f"Saving model to {folder}")
    training_args = TrainingArguments(
        folder_path,
        save_strategy = "epoch",
        logging_strategy='epoch',
        learning_rate=LR,
        per_device_train_batch_size=batch_size,
        num_train_epochs=n_epoch,
        warmup_ratio=0,
        load_best_model_at_end=False,
        lr_scheduler_type='linear',
        seed=SEED
    )

    trainer = Trainer(
        model,
        args=training_args,
        tokenizer=tokenizer,
        train_dataset=ab_dataset_tokenized['train'],
        compute_metrics=compute_metrics
    )

    if args.resume:
        trainer.train(resume_from_checkpoint=model_dir)
    else:
        trainer.train()

    out = trainer.state.log_history
    with open(folder_path + '/' + folder + '.json', 'w') as f:
        json.dump(out, f, indent=4)
