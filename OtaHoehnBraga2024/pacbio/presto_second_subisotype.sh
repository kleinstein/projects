# Due to compatability issues on Farnam, need to 
# download and run this command locally..
# Kenneth B. Hoehn
# 3/18/22
# kenneth.hoehn@yale.edu
# Build consensus sequences


bases=("Miyo-scBCR-07142021.ccs" "MO-06232021-scBCR.ccs")
for base in ${bases[@]}
do
    echo $base
    NPROC=7
    PIPELINE_LOG="presto/pipeline_$base.txt"
    OUTNAME=presto/$base\_quality-pass_primers-pass_combined_primers-pass_reheader_reheader_length-pass.fastq

    # cluster within each barcode, seems like insufficient barcode diversity
    ClusterSets.py set \
    -s $OUTNAME  \
    -f BR \
    -k CLUSTER \
    --ident 0.8 \
    --cluster vsearch \
    --nproc $NPROC \
    --log presto/clustersets_2.log > $PIPELINE_LOG

    OUTNAME=presto/$base\_quality-pass_primers-pass_combined_primers-pass_reheader_reheader_length-pass_cluster-pass.fastq
    ParseHeaders.py merge -s $OUTNAME -f BR CLUSTER -k BRCLUSTER --act cat >> $PIPELINE_LOG
    OUTNAME=presto/$base\_quality-pass_primers-pass_combined_primers-pass_reheader_reheader_length-pass_cluster-pass_reheader.fastq

    # align sequences within each barcode set
    AlignSets.py muscle -s $OUTNAME --log "presto/align_2.log" --nproc $NPROC \
        --bf BRCLUSTER --exec "./muscle3.8.31_i86linux64" >> $PIPELINE_LOG

    # build consensus sequences
    OUTNAME=presto/$base\_quality-pass_primers-pass_combined_primers-pass_reheader_reheader_length-pass_cluster-pass_reheader_align-pass.fastq
    BuildConsensus.py -s $OUTNAME --nproc $NPROC -n 10 -q 20 --freq 0.9 \
        --bf BRCLUSTER --maxerror 0.1 --maxgap 0.5 --log presto/build_2.log \
        --cf SUBPRIMER --act set --fasta >> $PIPELINE_LOG
done
