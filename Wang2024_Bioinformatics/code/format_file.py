import argparse
import pandas as pd

parser = argparse.ArgumentParser(description='Format input to AIRR format')
parser.add_argument('path', help="Input file path")
parser.add_argument('outpath', help="Output file path")
args = parser.parse_args()

def reshape_data(df, heavy_cols, light_cols):
    # Split into heavy and light chain DataFrames
    heavy_df = df[heavy_cols].copy()
    light_df = df[light_cols].copy()
    
    # Rename the columns to match the desired output
    sequence_cols = ['cell_id', 'v_call', 'j_call', 'sequence_vdj_aa']
    heavy_df.columns = sequence_cols
    light_df.columns = sequence_cols

    # Append the chain type to the antibody id
    heavy_df['cell_id'] = heavy_df['cell_id'] + '_H'
    light_df['cell_id'] = light_df['cell_id'] + '_L'
    
    # Combine the DataFrames
    reshaped_df = pd.concat([heavy_df, light_df], ignore_index=True)
    
    additional_columns_df = df.drop(columns=heavy_cols[1:] + light_cols[1:])
    additional_columns_df_heavy = additional_columns_df.copy()
    additional_columns_df_light = additional_columns_df.copy()
    additional_columns_df_heavy['cell_id'] = additional_columns_df_heavy[heavy_cols[0]] + '_H'
    additional_columns_df_light['cell_id'] = additional_columns_df_light[light_cols[0]] + '_L'

    # Combine the additional columns DataFrames
    expanded_additional_columns_df = pd.concat([additional_columns_df_heavy, additional_columns_df_light], ignore_index=True)

    # Merge the reshaped DataFrame with the additional columns DataFrame
    result_df = pd.merge(reshaped_df, expanded_additional_columns_df, on='cell_id', how='left')

    return result_df

# need to install openpyxl
data = pd.read_excel(args.path, skiprows = 1)

# create the essential fields
heavy_cols = ['Antibody  Name', 'Heavy chain V gene', 'Heavy chain J gene', 'Heavy chain AA']
light_cols = ['Antibody  Name', 'Light chain V gene', 'Light chain J gene', 'Light chain AA']
data = reshape_data(data, heavy_cols, light_cols)

data.to_csv(args.outpath, sep = '\t', index = False)


