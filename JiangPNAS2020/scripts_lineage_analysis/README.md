Kenneth Hoehn
26/6/2020
Scripts to run lineage analyses

To install packages, within an R session use:
```
devtools::install_bitbucket("kleinstein/alakazam@8e7fe36")
devtools::install_bitbucket("kleinstein/dowser@141853a")
```

Set up output directories
```
mkdir results
mkdir intermediates
```

Obtain filtered clones from combined data frame db_full_prepare_v2.rda.
Point the script to that file by changing line 7 to its location on 
your system.
```
Rscript processDataFrame.R
```

Run switch analysis for each patient using 1 computing core
```
Rscript getSwitches.R THY1 1
Rscript getSwitches.R THY2 1
Rscript getSwitches.R THY5 1
Rscript getSwitches.R THY6 1
Rscript getSwitches.R THY7 1
```

Summarize switch test results
```
Rscript summarizeResults.R
```

Plot trees for Figure S8
```
Rscript getTrees.R
```
