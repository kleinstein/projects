#!/bin/bash 

DATA_DIR="/media/nimanouri/Seagate_Backup/office/Work/Yale/DataSets/covid19_data"
#DATA_DIR="/media/nima/DataHD/Work/Yale/DataSets/covid19_data"
pattern="${DATA_DIR}/*_BCR_VHT_cellranger"
FOLDERS=( $pattern )
numSamples=${#FOLDERS[@]}
echo "FOLDERS: $numSamples"

fastaFILEIN="filtered_contig.fasta"
csvFILEIN="filtered_contig_annotations.csv"

for((i=0; i<$numSamples;i++));do
	FOLDER=$(basename ${FOLDERS[$i]})
	IDENTIFIER="$(cut -d'_' -f1 <<<"$FOLDER")"
	echo "Processing: ${FOLDER} ${IDENTIFIER}"
	# remove existing director
	if [ -d "$DATA_DIR/$FOLDER/$IDENTIFIER" ]; then sudo rm -Rf $DATA_DIR/$FOLDER/$IDENTIFIER; fi
	# run 10X processing script
	sudo docker run -v $DATA_DIR:/data:z kleinstein/immcantation:devel \
		changeo-10x \
		-s /data/$FOLDER/$fastaFILEIN \
		-a /data/$FOLDER/$csvFILEIN \
		-n $IDENTIFIER \
		-o /data/$FOLDER/$IDENTIFIER \
		-g human -t ig -p 1
done
