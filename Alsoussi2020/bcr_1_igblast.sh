#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu

# -c 16
# --mem-per-cpu=6G
# --constraint E5-2660_v3

# 10c, 10G total
# Job Wall-clock time: 00:01:07
# Memory Utilized: 104.78 MB

# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

# change-o v0.4.6

PATH_ROOT="/home/qz93/project/ellebedy_murineCovid/" #*
PATH_10x="${PATH_ROOT}cr_bcr/MCAH-B6_02_03-d5_RBD_ASC-lib2/outs/"
PATH_NESTED_MAB="${PATH_ROOT}bcr/nested_mAb/"
PATH_IGBLAST="${PATH_ROOT}bcr/igblast/"

#TODO
PATH_LOG="${PATH_IGBLAST}igblast_allSamps_$(date '+%m%d%Y_%H%M%S').log"
PATH_COUNT="${PATH_IGBLAST}igblast_bySamp_count_$(date '+%m%d%Y_%H%M%S').txt"

GERM_TAG="ref_202011-3_11Mar2020" #*
FN_IMGTVDJ="/home/qz93/germlines/ref_202011-3_11Mar2020/C57BL6_IG/C57BL6_IG*.fasta" #*

DIR_IGDATA="/home/qz93/apps/ncbi-igblast-1.15.0_ref_202011-3_11Mar2020" #*
PATH_EXEC="${DIR_IGDATA}/bin/igblastn" #*


#* run control
# TRUE/FALSE
RUN_IGBLAST=TRUE
RUN_MAKEDB=TRUE

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "germline" $GERM_TAG &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
#parallel --version | head -n 1 &>> "${PATH_LOG}"
"${PATH_EXEC}" -version &>> "${PATH_LOG}"
MakeDb.py --version &>> "${PATH_LOG}"
python3 -V &>> "${PATH_LOG}"


SAMP_ARRAY=(nested_mAb 10x)

for SAMP in ${SAMP_ARRAY[@]}; do

	if [[ ${SAMP} == "nested_mAb" ]]; then

		PATH_FASTA="${PATH_NESTED_MAB}forIgblast_nested_mAb.fasta"

	elif [[ ${SAMP} == "10x" ]]; then

		PATH_FASTA="${PATH_10x}filtered_contig.fasta"
		PATH_CSV="${PATH_10x}filtered_contig_annotations.csv"

	fi

	# samp-specific log
	PATH_LOG_SAMP="${PATH_IGBLAST}igblast_${SAMP}_$(date '+%m%d%Y_%H%M%S').log"


	#######################
	##### run igblast #####
	#######################

	if [[ ${RUN_IGBLAST} == "TRUE" ]]; then

		if [[ (-s "${PATH_FASTA}" ) ]]; then

			# count reads in cell ranger filtered contig fasta
			COUNT_CR=`grep -c '^>' ${PATH_FASTA}`
			echo -e "${SAMP}_input\t${COUNT_CR}" >> ${PATH_COUNT}

			echo "##### run IgBLAST #####" &>> "${PATH_LOG_SAMP}"

			# output: [outname]_igblast.fmt7

			# vdb, ddb, jdb: name of the custom V/D/J reference in IgBLAST database folder
			# if unspecified, default to imgt_<organism>_<loci>_v/d/j
			AssignGenes.py igblast \
				-s "${PATH_FASTA}" \
				-b "${DIR_IGDATA}" \
				--exec "${PATH_EXEC}" \
				--organism "mouse" \
				--loci "ig" \
				--vdb "imgt_B6_ig_v" \
				--ddb "imgt_B6_ig_d" \
				--jdb "imgt_B6_ig_j" \
				--format "blast" \
				--outname "${SAMP}" \
				--outdir "${PATH_IGBLAST}" \
				--nproc "${NPROC}" \
				&>> "${PATH_LOG_SAMP}"

		else
			echo "${PATH_FASTA} does not exist" &>> "${PATH_LOG_SAMP}"
		fi

	fi


	#######################################
	##### process output from IgBLAST #####
	#######################################

	if [[ ${RUN_MAKEDB} == "TRUE" ]]; then

		PATH_IGBLAST_SAMP="${PATH_IGBLAST}${SAMP}_igblast.fmt7"

		if [[ (-s "${PATH_IGBLAST_SAMP}" ) ]]; then
			
			echo "##### process output from IgBLAST #####" &>> "${PATH_LOG_SAMP}"

			# default: partial=False; asis-id=False
			# --partial: if specified, include incomplete V(D)J alignments in the pass file instead of the fail file

			# v0.4.6: Combined the extended field arguments of all subcommands 
			#   (--scores, --regions, --cdr3, and --junction) into a single --extended argument

			# IMPORTANT: do not put "" around FN_IMGTVDJ (otherwise * will be interpreted as is)
			# output: [outname]_db-pass.tab

			if [[ ${SAMP} == "nested_mAb" ]]; then

				MakeDb.py igblast \
					-i "${PATH_IGBLAST_SAMP}" \
					-s "${PATH_FASTA}" \
					-r ${FN_IMGTVDJ} \
					--extended \
					--failed \
					--partial \
					--format changeo \
					--outname "${SAMP}" \
					--outdir "${PATH_IGBLAST}" \
					&>> "${PATH_LOG_SAMP}"	

			elif [[ ${SAMP} == "10x" ]]; then

				MakeDb.py igblast \
					-i "${PATH_IGBLAST_SAMP}" \
					-s "${PATH_FASTA}" \
					-r ${FN_IMGTVDJ} \
					--10x "${PATH_CSV}" \
					--extended \
					--failed \
					--partial \
					--format changeo \
					--outname "${SAMP}" \
					--outdir "${PATH_IGBLAST}" \
					&>> "${PATH_LOG_SAMP}"	

			fi

			# count
			COUNT_LINES=$(wc -l ${PATH_IGBLAST}${SAMP}_db-pass.tab | awk '{print $1}')
			COUNT_SEQS=$((COUNT_LINES - 1))
			echo -e "${SAMP}_MakeDb\t${COUNT_SEQS}" >> ${PATH_COUNT}

		else
			echo "${PATH_IGBLAST_SAMP} does not exist" &>> "${PATH_LOG_SAMP}"
		fi

	fi

done

echo "DONE" &>> "${PATH_LOG}"

# 10x db will not have: 
# "SEQ_TYPE" "PLATE" "WELL" "CHAIN"

# nested_mAb db will not have: 
# [1] "CELL"            "C_CALL"          "CONSCOUNT"       "UMICOUNT"       
# [5] "V_CALL_10X"      "D_CALL_10X"      "J_CALL_10X"      "JUNCTION_10X"   
# [9] "JUNCTION_10X_AA"
