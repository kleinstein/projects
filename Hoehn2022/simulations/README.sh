#!/usr/bin/env bash
#SBATCH --job-name=sims
#SBATCH --partition=pi_kleinstein
#SBATCH --cpus-per-task=1
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=20gb 
#SBATCH --time=24:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com

module load R

# get inital tree topologies and branch lengths used for simulation
Rscript getInitialTrees.R

# Simulate two state datasets 
Rscript simulateTreesTwoState.R

# Simulation four state datasets
Rscript simulateTreesFourState.R

# Do SP test estimations on all datasets (will queue a lot of jobs!)
bash runEstimates.sh

# Run these after above jobs finish to summarize results
#Rscript summarizeEstimates.R
#Rscript summarizeEstimatesFour.R
