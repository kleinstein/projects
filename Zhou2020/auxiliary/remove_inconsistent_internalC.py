#!/usr/bin/env python3
"""
Remove sequences with inconsistent isotypes and internal C regions
"""
import re, sys, os
from os import path
from sys import argv

# header: 
# >GATACTGGGATATAAAC|PRCONS=Human-IGK|SEQORIENT=F|CREGION=Human-IGKC-IGKJ-InternalC|CONSCOUNT=4|DUPCOUNT=3

HEADER_ISOTYPE = "PRCONS"
HEADER_INTERNALC = "CREGION"

PATTERN_ISOTYPE = re.compile(HEADER_ISOTYPE+"=[\w_-]+")
PATTERN_INTERNALC = re.compile(HEADER_INTERNALC+"=[\w_-]+")

# function to separate, if any, inconsistent pairs from consistent ones
# outputs 3 lists:
# - consistentLines: fasta lines corresponding to sequences with consistent pairs
# - inconsistentLines: fasta lines corresponding to sequences with inconsistent pairs
# - inconsistentRecords: "isotype, internal_c" corresponding to sequences with inconsistent pairs
def spotInconsistency(inputFasta):
    
    with open(inputFasta, "r") as in_handle:

        lines = [line.rstrip() for line in in_handle]
        consistentLines = []
        inconsistentLines = []
        inconsistentRecords = []
        
        for i in range(len(lines)):
            if (i%2)==0:

                line = lines[i]
                
                # parse for isotype
                isotypeMatch = PATTERN_ISOTYPE.findall(line)

                if len(isotypeMatch)==1:
                    isotype = re.sub(HEADER_ISOTYPE+"=", "", isotypeMatch[0])
                elif len(isotypeMatch)==0:
                    sys.exit("No match to PATTERN_ISOTYPE found in header")
                else:
                    sys.exit("More than exactly one matches to PATTERN_ISOTYPE found in header")
                
                # parse for internal C
                internalcMatch = PATTERN_INTERNALC.findall(line)

                if len(internalcMatch)==1:
                    internalc = re.sub(HEADER_INTERNALC+"=", "", internalcMatch[0])
                elif len(internalcpeMatch)==0:
                    sys.exit("No match to PATTERN_INTERNALC found in header")
                else:
                    sys.exit("More than exactly one matches to PATTERN_INTERNALC found in header")
                
                # if inconsistent
                if (isotype in internalc):
                    consistentLines.append(line)
                    consistentLines.append(lines[i+1])
                else:
                    inconsistentLines.append(line)
                    inconsistentLines.append(lines[i+1])
                    inconsistentRecords.append(isotype+", "+internalc)
                    
    return consistentLines, inconsistentLines, inconsistentRecords

# function to output files
# inputs: 3 lists from spotInconsistency(), and outputNameStem (to be used as stem for output filenames)
# outputs: files with names 
# - outputNameStem_consistent.fasta
# - outputNameStem_inconsistent.fasta
# - outputNameStem_inconsistent_count.txt
# a file will only be outputted if sequences that meet criteria of that file were found
def outputLines(consistentLines, inconsistentLines, inconsistentRecords, outputNameStem):

    numConsistentLines = len(consistentLines)
    numInconsistentLines = len(inconsistentLines)
    
    # >=2 because a seq in FASTA format has 2 lines
    if numConsistentLines>=2:
        outputNameConsistent = outputNameStem+"_consistent.fasta"
        
        with open(outputNameConsistent, 'w') as out_handle:
            out_handle.writelines(line+"\n" for line in consistentLines)

        print("> Output for sequences with consistent isotype and internal C: "+outputNameConsistent+"\n")
    else:
        print("> No sequence with consistent isotype and internal C found.\n")

    if numInconsistentLines>=2:
        outputNameInconsistent = outputNameStem+"_inconsistent.fasta"
        
        with open(outputNameInconsistent, 'w') as out_handle:
            out_handle.writelines(line+"\n" for line in inconsistentLines)

        print("> Output for sequences with inconsistent isotype and internal C: "+outputNameInconsistent+"\n")
    else:
        print("> No sequence with inconsistent isotype and internal C found.\n")
        
    if len(inconsistentRecords)>1:
        outputNameRecord = outputNameStem+"_inconsistent_count.txt"
        
        # tabulate counts for isotype-internalC pairs
        recordCount = [item+", {0}".format(inconsistentRecords.count(item)) for item in set(inconsistentRecords)]
        
        with open(outputNameRecord, 'w') as out_handle:
            out_handle.writelines("ISOTYPE, INTERNAL_C, COUNT\n")
            out_handle.writelines(line+"\n" for line in recordCount)

        print("> Output for counts of pairs of inconsistent isotype and internal C: "+outputNameRecord+"\n")

    print("# consistent seqs: {0} ; # inconsistent seqs: {1}.\n".format(numConsistentLines/2, numInconsistentLines/2))


# run
for f in sys.argv[1:]:

	print("\n* Starting to process {0} ...\n".format(f))

	consistentLines, inconsistentLines, inconsistentRecords = spotInconsistency(f)

	outputLines(consistentLines, inconsistentLines, inconsistentRecords, path.splitext(f)[0])

	print("* Finished processing {0}.\n".format(f))
