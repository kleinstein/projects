#!/usr/bin/bash
#SBATCH --mem-per-cpu=5gb
#SBATCH --partition=pi_kleinstein
#SBATCH --cpus-per-task=36
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=100:00:00
#SBATCH --mail-type=FAIL,END
#SBATCH --mail-user=kenneth.hoehn@gmail.com

module load Python/3.7.0-foss-2018b
#module load Biopython

studies=("MYG256" "MYG265" "MYG274" "MYG276" "MYG320" "MYG325" "MYG326" "MYG329")
threads=36

for study in ${studies[@]}
do
    ofile="presto/${study}/${study}-final_collapse-unique_atleast-2.fastq"
    ofile2="presto/${study}/${study}-final_collapse-unique_atleast-2_reheader.fasta"
    ofile3="presto/${study}/${study}-final_collapse-unique_atleast-2_reheader_igblast.fmt7"
    echo $ofile
    echo $ofile2
    echo $ofile3

    ParseHeaders.py add -s $ofile -f sample -u $study --fasta

    # run IgBlast to align to V and J genes
    AssignGenes.py igblast -s $ofile2 \
        -b ~/share/igblast --organism human --loci ig --format blast \
        --nproc $threads
    
    MakeDb.py igblast -i $ofile3 -s $ofile2 \
        -r ~/share/germlines/imgt/human/vdj/imgt_human_IGHV.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGHD.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGHJ.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGKV.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGKJ.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGLV.fasta \
         ~/share/germlines/imgt/human/vdj/imgt_human_IGLJ.fasta 
done     


# run IgBlast to align to V and J genes
AssignGenes.py igblast -s mabs/mabs.fa \
    -b ~/share/igblast --organism human --loci ig --format blast \
    --nproc $threads

MakeDb.py igblast -i mabs/mabs_igblast.fmt7 -s mabs/mabs.fa \
    -r ~/share/germlines/imgt/human/vdj/imgt_human_IGHV.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGHD.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGHJ.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGKV.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGKJ.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGLV.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGLJ.fasta 
     

# run IgBlast to align to V and J genes
AssignGenes.py igblast -s mabs/kazu_sequences.fasta \
    -b ~/share/igblast --organism human --loci ig --format blast \
    --nproc $threads

MakeDb.py igblast -i mabs/kazu_sequences_igblast.fmt7 -s mabs/kazu_sequences.fasta \
    -r ~/share/germlines/imgt/human/vdj/imgt_human_IGHV.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGHD.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGHJ.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGKV.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGKJ.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGLV.fasta \
     ~/share/germlines/imgt/human/vdj/imgt_human_IGLJ.fasta 
     