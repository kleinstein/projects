## ---------------------------
##
## Script name: 8_annotation.R
##
## Purpose of script: B cell clustering
##
## Author: Mamie Wang
##
## Date Created: 2022-07-06
##
## Email: mamie.wang@yale.edu
## ---------------------------
## load the packages and functions
suppressPackageStartupMessages({
    library(Seurat)
    library(tidyverse)
    library(cowplot)
    library(readxl)
})

## ---------------------------
## Load arguments

args = commandArgs(trailingOnly=TRUE)
# test if there is at least one argument: if not, return an error
if (length(args)==1) {
  cell_type = args[1]
} else {
  stop("Exact one argument must be supplied (input file).n", call.=FALSE)
}

## ---------------------------
## Load data

indir = "/gpfs/gibbs/pi/kleinstein/shaw_flu/outs/"
B = readRDS(file.path(indir, "8_B_clustering/B.rds"))

## ---------------------------
## Load IG, TR, cell cycle genes

load("/gpfs/gibbs/pi/kleinstein/mw957/data/QC_features_meta.RData")
biotypes_excl = unique(features_meta[["gene_biotype"]])[grepl(pattern="^IG_|^TR_", x=unique(features_meta[["gene_biotype"]]))]
remove.genes = features_meta[["external_gene_name"]][features_meta[["gene_biotype"]] %in% biotypes_excl]
remove.genes <- c(remove.genes, 'AC233755.1', 'AC233755.2')
genes <- rownames(B@assays$RNA@data)
features.test <- genes[!genes %in% remove.genes]
cell_types = c("Naive B", "RMB", "ABC", "PB", "PB proliferating")
patients = c("Y1", "Y2", "Y3", "O1", "O2", "O3")

## ---------------------------
## Load VFC cluster assignments and load it onto the data frame

meld_d7_files = paste0(indir, "10_d7_MELD/d7_RL_", cell_types, ".csv")

VFC_separate = map(meld_d7_files, ~readr::read_csv(.x, col_types = cols()) %>%
  rename(cell_id_unique = X1)) %>%
  bind_rows

cell_id_order = B@meta.data %>% select(cell_id_unique) %>%
  left_join(VFC_separate, by = "cell_id_unique")

## ---------------------------
## Differential gene expression

top_cells = which((cell_id_order["k = 3"] == 2) & (B@meta.data$day == "D7"))
bottom_cells = which((cell_id_order["k = 3"] == 0) & (B@meta.data$day == "D0"))

B$status = 'other'
B$status[top_cells] = "D7 RL high"
B$status[bottom_cells] = "D0 RL high"

table(B$status, B$ID, B$annotated_cluster)

# , ,  = ABC


#                O1   O2   O3   Y1   Y2   Y3
#   D0 RL high  648   90  272  151   75  232
#   D7 RL high  374  113  203  132   38   48
#   other      1304  453  970  513  427  293

# , ,  = Naive B


#                O1   O2   O3   Y1   Y2   Y3
#   D0 RL high 1641 4089 1829 1018  362 5884
#   D7 RL high 4709 3665 1917  137 3779 3648
#   other      7089 1713  765 8553 5039 6360

# , ,  = PB


#                O1   O2   O3   Y1   Y2   Y3
#   D0 RL high   17   30   52  128   38   15
#   D7 RL high   57   23   42  440  168   73
#   other        69   85  162  258  183  104

# , ,  = PB proliferating


#                O1   O2   O3   Y1   Y2   Y3
#   D0 RL high   21    1   15    9   10    0
#   D7 RL high    6    5    6   26   17    6
#   other        33   14   41   58   51   18

# , ,  = RMB


#                O1   O2   O3   Y1   Y2   Y3
#   D0 RL high  519 1129  165  513   84 2639
#   D7 RL high   55  352  667  157 1238   40
#   other       663 2237 1683 3780 1456 1942
subsample = TRUE
subsample_counts = list(ABC = 38, `Naive B` = 137, PB = 15, RMB = 40)

DE_VFC = data.frame()

for (patient in patients) {
    print(patient)
    is_patient = B$ID == patient
    is_cell_type = B$annotated_cluster == cell_type
    B_cell_patient = B[,is_patient & is_cell_type]
    if (subsample) {
            min_n = subsample_counts[[cell_type]]
            D7_cells = colnames(B_cell_patient)[B_cell_patient$status == "D7 RL high"]
            D0_cells = colnames(B_cell_patient)[B_cell_patient$status == "D0 RL high"]
            cell_id = c(sample(D7_cells, min_n, replace = F),
                        sample(D0_cells, min_n, replace = F))
            markers = FindMarkers(B_cell_patient[, cell_id], assay = "RNA",
                                       group.by = "status", 
                                      ident.1 = "D7 RL high",
                                      ident.2 = "D0 RL high",
                                       features = features.test, 
                                       min.cells.group = 1,
                                       logfc.threshold = 0, min.pct = 0) %>%
    tibble::rownames_to_column(var = "gene") %>%
       mutate(pat = patient)
        }
    else {
        markers = FindMarkers(B_cell_patient, assay = "RNA",
                                       group.by = "status", 
                                       ident.1 = "D7 RL high", 
                                       ident.2 = "D0 RL high",
                                       features = features.test, 
                                       min.cells.group = 1,
                                       logfc.threshold = 0, min.pct = 0) %>%
    tibble::rownames_to_column(var = "gene") %>%
       mutate(pat = patient)
    }
    DE_VFC = rbind(DE_VFC, markers)
}

## ---------------------------
## Save the DE genes data
outdir = file.path(indir, "11_DE")
saveRDS(DE_VFC, file.path(outdir, paste0(cell_type, "_DE_2_subsample.rds")))
