##### build updated S5F model
# build substitution model & count mutations for tuning

#**************
#### setup ####
#**************

MAC = F
if (MAC) {
    pathRoot = "~/Dropbox (recherche)/yale/" # mac
} else {
    pathRoot = "/ysm-gpfs/home/qz93/" # hpc
}

pathScript = paste0(pathRoot, "projects/scripts/")
pathData = paste0(pathRoot, "projects/spatial/data/")
pathWork = paste0(pathRoot, "projects/spatial/newS5F/")

library(shazam) # v0.1.11 (alakazam v0.2.11)

sinkName = paste0("sessionInfo_1_", Sys.Date(), ".txt")
setwd(pathWork)
sink(sinkName)
print(Sys.time())
sessionInfo()
sink()

#### load data ####

load(paste0(pathData, "productive/pr_aggregate.RData")) # db

cat("total number of PR seqs:", nrow(db), "\n")

# sanity check
# first 50 IMGT positions in briney data excluded
# expect all dots 
table(substr(db[sample(x=which(db[["DATASET"]]=="briney"), size=10000), "SEQUENCE_IMGT"], 1, 50))

#### setup ####

V_CALL_COL = "GERMLINE_V_CALL"
GERMLINE_COL = "GERMLINE_IMGT_D_MASK"
OBSV_COL = "SEQUENCE_IMGT"

MODEL = "S"
MULTIPLE_MUTATION = "independent"
#*
MIN_NUM_MUTATIONS = 50

COUNT_SUB = F
BUILD_SUB_1 = F
BUILD_SUB_5 = F
COUNT_MUT = F

saveName_sub_count = "sub_count.RData"
saveName_sub_1 = "sub_1mer.RData"
saveName_sub_5 = "sub_5mer.RData"
saveName_mut_count = "mut_count.RData"

#### sub - prelim ####

if (COUNT_SUB) {
    cat("\ncounting number of mutations for a given 5-mer\n")
    
    sub_count = createSubstitutionMatrix(db = db, model = MODEL, 
                                         multipleMutation = MULTIPLE_MUTATION,
                                         returnModel = "5mer",
                                         sequenceColumn = OBSV_COL,
                                         germlineColumn = GERMLINE_COL,
                                         vCallColumn = V_CALL_COL,
                                         numMutationsOnly = T)
    # save
    setwd(pathWork)
    save(sub_count, file=saveName_sub_count)
}

#### sub - 1mer ####

if (BUILD_SUB_1) {
    cat("\nconstructing 1-mer subsitution model\n")
    
    # col = from, row = to (opposite of result from returnModel='5mer')
    sub_model_1mer_raw = createSubstitutionMatrix(db = db, model = MODEL, 
                                                  multipleMutation = MULTIPLE_MUTATION,
                                                  returnModel = "1mer_raw",
                                                  sequenceColumn = OBSV_COL,
                                                  germlineColumn = GERMLINE_COL,
                                                  vCallColumn = V_CALL_COL,
                                                  minNumMutations = MIN_NUM_MUTATIONS,
                                                  numMutationsOnly = F)
    
    # normalize by row (such that after normalization each row sums up to 1)
    # result is equivalent to setting returnModel to '1mer'
    
    # for value in each row, divide value by rowSum
    # then transpose back resultant matrix, since apply returns matrix after transposing it
    sub_model_1mer_norm = t(apply(sub_model_1mer_raw, 1, function(x) {x/sum(x)} ))
    
    # save
    setwd(pathWork)
    save(sub_model_1mer_raw, sub_model_1mer_norm, file=saveName_sub_1)
}

#### sub - 5mer ####

if (BUILD_SUB_5) {
    cat("\nconstructing 5-mer subsitution model\n")
    
    sub_model_5mer = createSubstitutionMatrix(db = db, model = MODEL, 
                                              multipleMutation = MULTIPLE_MUTATION,
                                              returnModel = "5mer",
                                              sequenceColumn = OBSV_COL,
                                              germlineColumn = GERMLINE_COL,
                                              vCallColumn = V_CALL_COL,
                                              minNumMutations = MIN_NUM_MUTATIONS,
                                              numMutationsOnly = F)
    
    # extend 5-mer substitution model 
    cat("\nextending subsitution model\n")
    sub_model_5mer_ex = extendSubstitutionMatrix(sub_model_5mer)
    
    # save
    setwd(pathWork)
    save(sub_model_5mer, sub_model_5mer_ex, file=saveName_sub_5)
}

#### mut - prelim ####

if (COUNT_MUT) {
    cat("\ncounting number of mutations in sequences containing a given 5-mer\n")
    
    # need sub_model_5mer
    if (!"sub_model_5mer" %in% ls()) {
        setwd(pathWork)
        if (file.exists(saveName_sub_5)) {
            cat("\nloading", saveName_sub_5, "\n")
            load(saveName_sub_5) # sub_model_5mer, sub_model_5mer_ex
        } else {
            stop(saveName_sub_5, " not found")
        }
    }
    
    mut_count = createMutabilityMatrix(db = db, 
                                       substitutionModel = sub_model_5mer,
                                       model = MODEL,
                                       multipleMutation = MULTIPLE_MUTATION,
                                       sequenceColumn = OBSV_COL,
                                       germlineColumn = GERMLINE_COL,
                                       vCallColumn = V_CALL_COL,
                                       numSeqMutationsOnly = T,
                                       returnSource = F)
    # save
    setwd(pathWork)
    save(mut_count, file=saveName_mut_count)
}

