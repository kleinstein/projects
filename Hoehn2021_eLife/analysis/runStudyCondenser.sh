

studies=("Davis_2019" "Doria-Rose_2015" "Ellebedy_2016" "Huang_2016" "Johnson_2018" "Landais_2017" "Liao_2013" "Setliff_2018a" "Wu_2015")

nproc=10
mab=1

for study in ${studies[@]}
do
 	echo $study
    sbatch --cpus-per-task=$nproc --mem-per-cpu=5gb --job-name=$study runCondenser.sh $study $nproc $mab
done


studies=("Galson_2015" "Galson_2015a" "Galson_2016" "Gupta_2017" "Levin_2015" "Schanz_2014" "Setliff_2018a")

mab=0
for study in ${studies[@]}
do
    echo $study
    sbatch --cpus-per-task=$nproc --mem-per-cpu=5gb --job-name=$study runCondenser.sh $study $nproc $mab
done