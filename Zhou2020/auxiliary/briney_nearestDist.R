require(stringi)
require(alakazam) # 0.2.11
require(shazam) # 0.1.11
#require(doParallel)

indpt_nonsquareDist = function(seq_uniq, indx, dist_mat) {
    # set up matrix
    mat = matrix(NA, nrow=length(indx), ncol=length(seq_uniq))
    # fill in
    for (i in 1:length(indx)) {
        for (j in 1:length(seq_uniq)) {
            mat[i, j] = alakazam::seqDist(seq1=seq_uniq[indx[i]],
                                          seq2=seq_uniq[j],
                                          dist_mat=dist_mat)
        }
    }
    return(mat)
}

# compared to shazam::nearestDist v0.1.11
# 1) replaced alakazam::nonsquareDist with indpt_nonsquareDist
# 2) fixed bug in .dcross

indpt_nearestDist <- function(sequences, model=c("ham", "aa", "hh_s1f", "hh_s5f", "mk_rs1nf", "mk_rs5nf", 
                                                 "hs1f_compat", "m1n_compat"),
                              normalize=c("none", "len", "mut"),
                              symmetry=c("avg", "min"),
                              crossGroups=NULL, mst=FALSE,
                              subsample=NULL) {
    ## DEBUG
    # sequences <- c("ACGTACGTACGT", "ACGAACGTACGT", "AAAAAAAAAAAA", "A-AAAA---AAA")
    # model="aa"; normalize="len"; crossGroups=NULL; mst=FALSE
    
    # Initial checks
    model <- match.arg(model)
    normalize <- match.arg(normalize)
    
    ## If crossGroup requested, but only one group found, return NA
    if (!is.null(crossGroups) & length(unique(crossGroups)) < 2) {
        seq_dist <- rep(NA, length(sequences))
        return (seq_dist)
    }
    
    # Find unique sequences
    seq_uniq <- shazam:::findUniqSeq(sequences)
    n_uniq <- length(seq_uniq)
    
    # corresponding crossGroups values for seq_uniq
    if (!is.null(crossGroups)) {
        stopifnot( all.equal(sequences[match(seq_uniq, sequences)], 
                             seq_uniq, check.attributes=FALSE) )
        crossGroups_uniq <- crossGroups[match(seq_uniq, sequences)]
    }
    
    # Initialize return vector and computation vector
    seq_dist <- setNames(rep(NA, length(sequences)), sequences)
    seq_uniq_dist <- rep(NA, n_uniq)
    
    # Compute distances between sequences
    if (n_uniq > 1) {
        # Check for length mismatches
        seq_length <-  unique(stri_length(seq_uniq))
        if (length(seq_length) > 1) {
            stop("Unexpected. Different sequence lengths found.")
        }
        # check subSampling
        subSampling <- all(!is.null(subsample), subsample < n_uniq)
        if (subSampling) indx <- sample(x=1:n_uniq, size=subsample, replace=FALSE, prob=NULL)
        # corresponding subsampling of crossGroups_uniq
        if (subSampling & !is.null(crossGroups)) {
            crossGroups_uniq_sub <- crossGroups_uniq[indx]
        }
        # Get distance matrix
        if (model == "ham") {
            if (subSampling) {
                dist_mat <- indpt_nonsquareDist(seq_uniq, indx, dist_mat=getDNAMatrix(gap=0))
            } else {
                dist_mat <- pairwiseDist(seq_uniq, dist_mat=getDNAMatrix(gap=0))
            }
        } else if (model == "aa") {
            seq_uniq <- setNames(alakazam::translateDNA(seq_uniq), seq_uniq)
            if (subSampling) {
                dist_mat <- indpt_nonsquareDist(seq_uniq, indx, dist_mat=getAAMatrix())
            } else {
                dist_mat <- pairwiseDist(seq_uniq, dist_mat=getAAMatrix())
            }
        } else if (model == "hh_s1f") {
            if (subSampling) {
                dist_mat <- indpt_nonsquareDist(seq_uniq, indx, dist_mat=HH_S1F_Distance)
            } else {
                dist_mat <- pairwiseDist(seq_uniq, dist_mat=HH_S1F_Distance)
            }
        } else if (model == "mk_rs1nf") {
            if (subSampling) {
                dist_mat <- indpt_nonsquareDist(seq_uniq, indx, dist_mat=MK_RS1NF_Distance)
            } else {
                dist_mat <- pairwiseDist(seq_uniq, dist_mat=MK_RS1NF_Distance)
            }
        } else if (model == "hh_s5f") {
            if (subSampling) {
                dist_mat <- nonsquare5MerDist(seq_uniq, indx, HH_S5F_Distance, symmetry=symmetry)
            } else {
                dist_mat <- pairwise5MerDist(seq_uniq, HH_S5F_Distance, symmetry=symmetry)
            }
        } else if (model == "mk_rs5nf") {
            if (subSampling) {
                dist_mat <- nonsquare5MerDist(seq_uniq, indx, MK_RS5NF_Distance, symmetry=symmetry)
            } else {
                dist_mat <- pairwise5MerDist(seq_uniq, MK_RS5NF_Distance, symmetry=symmetry)
            }
        } else if (model == "hs1f_compat") {
            if (subSampling) {
                dist_mat <- indpt_nonsquareDist(seq_uniq, indx, dist_mat=HS1F_Compat)
            } else {
                dist_mat <- pairwiseDist(seq_uniq, dist_mat=HS1F_Compat)
            }
        } else if (model == "m1n_compat") {
            if (subSampling) {
                dist_mat <- indpt_nonsquareDist(seq_uniq, indx, dist_mat=M1N_Compat)
            } else {
                dist_mat <- pairwiseDist(seq_uniq, dist_mat=M1N_Compat)
            }
        }                
        ## DEBUG
        # cat("\n-> seq_uniq:\n")
        # print(seq_uniq)
        # cat("\n-> dist_mat (raw):\n")
        # print(dist_mat)
        
        # Normalize distances
        if (normalize == "len") { 
            dist_mat <- dist_mat / seq_length
        } else if (normalize == "mut") {
            #dist <- dist/sum(strsplit(seq1,"")[[1]] != strsplit(seq2,"")[[1]])
            stop('Sorry! nomalize="mut" is not available.')
        }
        
        ## DEBUG
        # cat("\n-> seq_length:\n")
        # print(seq_length)
        # cat("\n-> dist_mat (normalized):\n")
        # print(dist_mat)
        
    } else {
        return(seq_dist)
    }
    
    # Find minimum distance for each sequence
    if (is.null(crossGroups)) {
        if(!mst) {
            # Return smaller value greater than 0
            # If all 0, return NA
            .dmin <- function(i) { 
                x <- dist_mat[, i]
                gt0 <- which(x > 0)
                if (length(gt0) != 0) { min(x[gt0]) } else { NA }
            }
            
            ## TODO: Could be an apply over columns
            seq_uniq_dist <- setNames(sapply(1:n_uniq, .dmin), names(seq_uniq))
        } else {
            # Get adjacency matrix of minimum spanning tree
            adj <- ape::mst(dist_mat)
            
            # TODO: This could be cleaner
            # Get value(s) from mst branches
            # If none (broken mst!), return NA
            # If multiple values, comma-join
            .dmst <- function(i) { 
                gt0 <- which(adj[, i] == 1)
                if (length(gt0) != 0) { 
                    stri_join(round(dist_mat[, i][gt0], 4), collapse=",") 
                } else {
                    NA
                }
            }
            
            ## TODO: Could be an apply over columns
            seq_uniq_dist <- setNames(sapply(1:n_uniq, .dmst), names(seq_uniq))
        }
        
        # Define return distance vector
        seq_dist <- seq_uniq_dist[match(names(seq_dist), names(seq_uniq_dist))]
        
        ## DEBUG
        # cat("\n-> seq_uniq_dist:\n")
        # print(seq_uniq_dist)
        # cat("\n-> seq_dist:\n")
        # print(seq_dist)
    } else {
        # Identify sequences to be considered when finding minimum
        # cross distance
        .dcross <- function(i) {
            #cat(i,"\n")
            this_group <- crossGroups[i]
            other_groups <-  which(crossGroups != this_group)
            other_seq <- unique(sequences[other_groups])
            other_idx <- match(other_seq, seq_uniq)
            this_idx <- match(sequences[i], seq_uniq)
            
            stopifnot( all.equal( other_seq, seq_uniq[other_idx] , check.attributes=FALSE ) )
            stopifnot( all.equal( sequences[i], seq_uniq[this_idx] , check.attributes=FALSE ) )
            
            # the next two checks may not always be true
            # this happens when all the out-group sequences are identical to the in-group sequences
            #stopifnot( all( crossGroups_uniq[other_idx] != this_group ) )
            #stopifnot( crossGroups_uniq[this_idx] == this_group )
            
            if (subSampling) {
                # When there is subsampling, nonsquareDist returns a non-n-by-n matrix 
                # This matrix has fewers than n rows, and exactly n cols
                # For each unique sequence, look for its cross-group distances in its column, 
                #     NOT in its row (because there will be fewer than n rows)
                
                # dist_mat rows correspond to seq_uniq[indx]
                # (indx itself is wrt seq_uniq)
                # (other_idx is also wrt seq_uni)
                
                # which other_seq are included in the subsampled seqs represented by
                #       the available rows in dist_mat?
                # wrt dist_mat
                other_avail_wrt_dist_mat <- which(indx %in% other_idx)
                
                if (length(other_avail_wrt_dist_mat)>0) {
                    # the next two checks may not always be true
                    # this happens when all the out-group sequences are identical to the in-group sequences
                    #stopifnot(all( crossGroups_uniq_sub[other_avail_wrt_dist_mat] != this_group ))
                    #stopifnot(all( crossGroups_uniq_sub[-other_avail_wrt_dist_mat] == this_group ))
                    
                    r <- dist_mat[other_avail_wrt_dist_mat, this_idx]
                } else {
                    stopifnot(all( crossGroups_uniq_sub == this_group ))
                    return(NA)
                }
            } else {
                # without subsampling
                # dist_mat is a n-by-n matrix
                stopifnot( all(other_idx <= nrow(dist_mat) ) ) 
                r <- dist_mat[other_idx, this_idx]
            }
            
            gt0 <- which(r > 0)
            
            if (length(gt0) != 0) { return(min(r[gt0])) } else { return(NA) }
        }
        
        # Define return distance vector
        seq_dist <- setNames(sapply(1:length(sequences), .dcross), sequences)
    }
    
    return(round(seq_dist, 4))
}
