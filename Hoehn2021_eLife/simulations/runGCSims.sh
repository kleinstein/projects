#!/usr/bin/bash

ftime=10
times=(12 15 20 25 30 40)
sels=(0 1)
carry_caps=(1000)

reps=100
nproc=2

for time in ${times[@]}
do
    for sel in ${sels[@]}
    do
        for carry in ${carry_caps[@]}
        do
            echo $time $sel $carry $reps
            sbatch --cpus-per-task=$nproc --mem-per-cpu=5gb gcSim.sh $time $sel $carry $reps $nproc
        done
    done
done

