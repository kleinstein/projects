#!/usr/bin/env python3
"""
Split .tab based on chains in V_CALL and J_CALL
"""
import re, sys, os
from os import path
from sys import argv
import pandas as pd

# command line input: inputTab, outname

# outputs: 
# - outname_IG-H.tab
# - outname_IG-KL.tab
# - outname_IG-HKL.tab
# - outname_IG-NA.tab

def getCallChain(call):

    # if NaN
    if pd.isnull(call):
        return "NA"
    else:
        matchIGH = re.findall("IGH", call)
        matchIGKL = re.findall("IG[KL]", call)

        if len(matchIGH)>0 and len(matchIGKL)>0:
            return "H_KL"
        elif len(matchIGH)==0 and len(matchIGKL)>0:
            return "KL"
        elif len(matchIGH)>0 and len(matchIGKL)==0:
            return "H"
        else:
            return "NA"

# V_CALL J_CALL  CASE
# H_KL   H_KL    2
# H_KL   KL      2
# H_KL   H       2
# H_KL   NA      1

# KL     H_KL    2
# KL     KL      
# KL     H       3
# KL     NA      1 

# H      H_KL    2
# H      KL      3
# H      H
# H      NA      1 

# NA     H_KL    1
# NA     KL      1
# NA     H       1
# NA     NA      1

# sequence order of the following logic matters
# if either one of V_CALL or J_CALL is NA   => case 1 - NA
# if either one of V_CALL or J_CALL is H_KL => case 2 - H_KL
# if one is KL and the other is H           => case 3 - H_KL
# if both KL => KL
# if both H  => H

def determineChain(vCallChain, jCallChain):

    if vCallChain=="NA" or jCallChain=="NA":
        return "NA"

    if vCallChain=="H_KL" or jCallChain=="H_KL":
        return "H_KL"

    if (vCallChain=="KL" and jCallChain=="H") or (vCallChain=="H" and jCallChain=="KL"):
        return "H_KL"

    if vCallChain=="KL" and jCallChain=="KL":
        return "KL"

    if vCallChain=="H" and jCallChain=="H":
        return "H"

def splitByChain(inputTab, outname):

    df = pd.read_table(inputTab, dtype=object)

    chains_V = df["V_CALL"].apply(getCallChain)
    chains_J = df["J_CALL"].apply(getCallChain)
    chains_VJ = pd.Series([determineChain(chains_V[i], chains_J[i]) for i in range(df.shape[0])])

    if (sum(chains_VJ=="H")>0):
        df_H = df[chains_VJ=="H"]
        df_H.to_csv(outname+"_IG-H.tab", sep="\t", index=False)

    if (sum(chains_VJ=="KL")>0):
        df_KL = df[chains_VJ=="KL"]
        df_KL.to_csv(outname+"_IG-KL.tab", sep="\t", index=False)

    if (sum(chains_VJ=="H_KL")>0):
        df_H_KL = df[chains_VJ=="H_KL"]
        df_H_KL.to_csv(outname+"_IG-HKL.tab", sep="\t", index=False)

    if (sum(chains_VJ=="NA")>0):
        df_NA = df[chains_VJ=="NA"]
        df_NA.to_csv(outname+"_IG-NA.tab", sep="\t", index=False)

# run
splitByChain(sys.argv[1], sys.argv[2])
