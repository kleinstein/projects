#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 14-

##### 10x software
PATH_10X="/ysm-gpfs/pi/kleinstein/cellrager_3.1.0/"
PATH_CR_EXE="${PATH_10X}cellranger-3.1.0/cellranger"

# different from the reference for GEX
# https://support.10xgenomics.com/single-cell-vdj/software/pipelines/latest/installation
# https://support.10xgenomics.com/single-cell-vdj/software/downloads/latest
#PATH_CR_REF="${PATH_10X}refdata-cellranger-GRCh38-3.0.0/"

#export PATH=${PATH_CR_MAIN}:$PATH


##### meta

PATH_ROOT="/home/qz93/scratch60/"

#*
PATH_OUTPUT="${PATH_ROOT}cr_gex_aggr/"

#* this excludes d28_FNA, d0/5/12/28_PBMC
PATH_CSV="${PATH_ROOT}cr_gex_aggr/gex_aggr_P05_01072020.csv"


ID="aggr_P05_01072020"

##### log

PATH_LOG="${PATH_OUTPUT}gex_aggr_$(date '+%m%d%Y_%H%M%S').log"

dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
#parallel --version | head -n 1 &>> "${PATH_LOG}"
"${PATH_CR_EXE}" aggr --version &>> "${PATH_LOG}"


echo "aggregation csv: ${PATH_CSV}" &>> "${PATH_LOG}"


# https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/using/aggregate

# output files: ${PATH_OUTPUT}/${ID}/outs/*
# --normalize: library depth normalization mode (mapped [default], raw, none)
# --nosecondary: disable secondary analysis (e.g. clustering)

# normalization
# mapped (default): For each library type, subsample reads from higher-depth 
#    GEM wells until they all have an equal number of reads that are 
#    confidently mapped to the transcriptome or assigned to the feature IDs 
#    per cell.
# none: Do not normalize at all.

cd "${PATH_OUTPUT}"

"${PATH_CR_EXE}" aggr \
	--id="${ID}" \
	--csv="${PATH_CSV}" \
	--normalize="mapped" \
	&>> "${PATH_LOG}"

echo "DONE" &>> "${PATH_LOG}"
