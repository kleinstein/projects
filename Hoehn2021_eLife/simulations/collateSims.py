# Combine files from simulations into one big fasta file

from Bio import SeqIO 
import os
import re 
import sys

simtype = sys.argv[1]

print(simtype)

if simtype != "gc" and simtype != "emp":
    exit(simtype + " not recognized!")

filename = "../../data/processed/" + simtype + "_simulations/" + \
    simtype + "_simulations_Processed.fasta"
if os.path.exists(filename):
    os.remove(filename)

outfile = open(filename,"a+")
dirs = os.listdir("../../data/processed/" + simtype + "_simulations/")

for sdir in dirs:
    if os.path.isdir("../../data/processed/" + simtype + "_simulations/" + sdir):
        print(sdir)
        files = os.listdir("../../data/processed/" + simtype + "_simulations/" + sdir)
        fasta_regex = re.compile("fasta")
       
        for file in files:
            if fasta_regex.search(file):
                clone = file.split(".")[0]
                fasta = SeqIO.parse("../../data/processed/" + simtype + "_simulations/" + \
                    sdir + "/" + file, "fasta")
                for record in fasta:
                    sub = re.compile("subject=(\\S+)")
                    subject = str(sub.search(record.description).group(1))
                    outfile.write(">" + subject + "-" + clone + "-" + record.id + "\n")
                    outfile.write(str(record.seq) + "\n")
