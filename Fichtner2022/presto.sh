#!/usr/bin/bash
#SBATCH --mem-per-cpu=5gb
#SBATCH --partition=pi_kleinstein
#SBATCH --cpus-per-task=36
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=100:00:00
#SBATCH --mail-type=FAIL,END
#SBATCH --mail-user=kenneth.hoehn@gmail.com

module load Python/3.7.0-foss-2018b

samples=("MYG256" "MYG265" "MYG274" "MYG276" "MYG320" "MYG325" "MYG326" "MYG329")

threads=36

for sample in ${samples[@]}
do
    r1=$(ls sample_dir_000006082/Sample_$sample/$sample*R1*fastq)
    r2=$(ls sample_dir_000006082/Sample_$sample/$sample*R2*fastq)
    echo "$r1 $r2"

    bash presto-abseq.sh \
    -1 $r1 \
    -2 $r2 \
    -j immcantation/protocols/AbSeq/AbSeq_R1_Human_IG_Primers.fasta \
    -v immcantation/protocols/AbSeq/AbSeq_R2_TS.fasta \
    -c immcantation/protocols/AbSeq/AbSeq_Human_IG_InternalCRegion.fasta \
    -r ~/share/igblast/fasta/imgt_human_ig_v.fasta \
    -n $sample -o presto/$sample -p $threads -y yaml.yaml
done    
