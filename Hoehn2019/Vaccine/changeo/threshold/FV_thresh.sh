#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --job-name=FV
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=8
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=96:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com
#SBATCH --exclude=c24n04


Rscript DistNearest.R church_flu_SUBJECT-FV.tsv 8