# on immcantation@ig.med.yale.edu

pathRoot="/mnt/data/projects/oconnor_mg_memory/presto/"

NAME_ARRAY=(A79HP_HD07M A79HP_HD07N AB0RF_HD07U AAYHL_HD09M AB0RF_HD09U AB8KB_HD09N AAYHL_HD10M AB0RF_HD10U AB8KB_HD10N AAYHL_HD13M AB0RF_HD13U AB8KB_HD13N)

cd "${pathRoot}"

for NAME in ${NAME_ARRAY[@]}; do

	echo "*************** ${NAME} *****************"

	echo "# decompressing..."
	nameTar="${NAME}.tar.gz"
	tar -zxvf "${nameTar}" "${NAME}/${NAME}-FIN_collapse-unique_atleast-2.fastq"
	mv "${NAME}/${NAME}-FIN_collapse-unique_atleast-2.fastq" .

done
