#!/bin/bash
#SBATCH --partition=general
#SBATCH --job-name=igblast
#SBATCH --ntasks=1 --nodes=1
#SBATCH --mem-per-cpu=10G
#SBATCH --time=1-00:00:00
#SBATCH --error=log/igblast.%A_%a.err
#SBATCH --output=log/igblast.%A_%a.out

taskID=${SLURM_ARRAY_TASK_ID}

infolder="/gpfs/gibbs/pi/kleinstein/shaw_flu/10x/defaults/"
outfolder="/gpfs/gibbs/pi/kleinstein/shaw_flu/processed/BCR/1_igblast/"

samples=(`ls -d $infolder/*_BCRVDJ/`)
sample=${samples[$taskID-1]}
sampleid=$(basename $sample "_BCRVDJ")

echo $sampleid

echo "[$0 $(date +%Y%m%d-%H%M%S)] [start] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"

singularity exec /gpfs/ysm/project/kleinstein/mw957/software/immcantation/lab_devel.sif AssignGenes.py igblast -s $sample"/bcrvdj/outs/filtered_contig.fasta" -b ~/share/igblast \
   --organism human --loci ig --format blast --outdir $outfolder \
   --outname $sampleid --nproc 1

echo "[$0 $(date +%Y%m%d-%H%M%S)] [end] $SLURM_JOBID $SLURM_ARRAY_TASK_ID"