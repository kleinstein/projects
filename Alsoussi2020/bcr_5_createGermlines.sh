#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 20:00
#SBATCH --constraint E5-2660_v3
#SBATCH -c 1
#SBATCH --mem-per-cpu=2G


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
# NPROC=${SLURM_CPUS_PER_TASK:-1}

# IN:
# - bar_clust_heavy/light.tsv
# OUT:
# - [outname]_germ-pass.tab
# - [outname]_CG.log


#*
BCR_VERSION="2020-04-06"

PATH_ROOT="/home/qz93/project/ellebedy_murineCovid/bcr/${BCR_VERSION}/"
PATH_CLUST="${PATH_ROOT}VJL/"
PATH_CG="${PATH_ROOT}CG/"

FILE_HC="bcr_clust_heavy.tsv"
FILE_LC="bcr_clust_light.tsv"

GERM_TAG="ref_202011-3_11Mar2020" #*
DIR_REF="/home/qz93/germlines/${GERM_TAG}/C57BL6_IG/" #*

PATH_LOG=${PATH_CG}createGermlines_$(date '+%m%d%Y_%H%M%S').log

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
echo "germline" ${GERM_TAG} &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
CreateGermlines.py --version &>> "${PATH_LOG}"
python3 -V &>> "${PATH_LOG}"


echo "############## CreateGermlines for HC" &>> "${PATH_LOG}"

#! important: if genotyping by tigger was performed, 
#  add novel alleles to germline .fasta before running CreateGermlines

# if you have run the clonal assignment task prior to invoking CreateGermlines, 
# then adding the --cloned argument is recommended, as this will generate a 
# single germline of consensus length for each clone

# no nproc

# appends _germ-pass.tab, _germ-fail.tab (even if input -d is .tsv)
CreateGermlines.py \
	-d "${PATH_CLUST}${FILE_HC}" \
	--failed \
	-r "${DIR_REF}" \
	-g full dmask vonly regions \
	--cloned \
	--vf V_CALL \
	--df D_CALL \
	--jf J_CALL \
	--sf SEQUENCE_IMGT \
	--log "heavy_CG.log" \
	--outdir "${PATH_CG}" \
	--outname "bcr_heavy" \
	&>> "${PATH_LOG}"

echo "############## CreateGermlines for LC" &>> "${PATH_LOG}"

# --cloned flag off for LC (could be >1 LCs per cell, e.g. 1 IGK and 1 IGL)

CreateGermlines.py \
	-d "${PATH_CLUST}${FILE_LC}" \
	--failed \
	-r "${DIR_REF}" \
	-g full dmask vonly regions \
	--vf V_CALL \
	--df D_CALL \
	--jf J_CALL \
	--sf SEQUENCE_IMGT \
	--log "light_CG.log" \
	--outdir "${PATH_CG}" \
	--outname "bcr_light" \
	&>> "${PATH_LOG}"

echo "############## DONE" &>> "${PATH_LOG}"

