Code for Turner\*, Zhou\* & Han\* et al., Nature, 2020.

BioProject PRJNA610059

--------------------------------------------------------

Order     | Script                                  | Figure
--------- | --------------------------------------- | ----------
1         | bcr_0_remove_phix.sh                    |
2         | bcr_0_presto-abseq.sh                   |
3         | bcr_0_presto_collapseSeq.sh             |
4         | bcr_0_cellranger.sh                     |
5         | bcr_1_igblast_bulk.sh                   |
6         | bcr_1_igblast_mAb.sh                    |
7         | bcr_1_igblast_nested.sh                 |
8         | bcr_1_igblast_10x.sh                    |
9         | bcr_1_QC_10x.R                          |
10        | bcr_2_a_tigger_04-11.R                  |
11        | bcr_2_a_tigger_05.R                     |
12        | bcr_2_b_tigger_correction_04-11.R       |
13        | bcr_2_b_tigger_correction_05.R          |
14        | bcr_2_c_tigger_getNovelFasta_04-11.R    |
15        | bcr_2_c_tigger_getNovelFasta_05.R       |
16        | bcr_3_a_calcDist_04-11.R                |
17        | bcr_3_a_calcDist_05.R                   |
18        | bcr_3_b_overlap_04-11.R                 |
19        | bcr_3_b_overlap_05.R                    |
20        | bcr_3_c_findThreshold_04-11.R           |
21        | bcr_3_c_findThreshold_05.R              |
22        | bcr_3_d_visualize.R                     | ExD Fig 3d
23        | bcr_3_e_excl_d0IdentBar_05.R            | 
24        | bcr_3_f_cluster_04-11.sh                |
25        | bcr_3_f_cluster_05.sh                   |
26        | bcr_4_createGermline_04-11.sh           |
27        | bcr_4_createGermline_05.sh              |
28        | bcr_5_collapseDups_04-11.R              |
29        | bcr_5_collapseDups_05.R                 |
30        | bcr_6_muFreq_calc_04-11.R               |
31        | bcr_6_muFreq_calc_05.R                  |
32        | gex_0_cellranger.sh                     | 
33        | gex_0_cellranger_aggr.sh                |
34        | gex_1_QC.R                              | 
35        | gex_2_cluster_a.R                       |
36        | gex_2_cluster_b.R                       | Fig 2d; ExD Fig 2defgh
37        | gex_3_recluster_a.R                     |
38        | gex_3_recluster_b.R                     | Fig 3de; ExD Fig 3g; ExD Fig4
39        | bcr_7_a_GEXmeta_05.R                    |
40        | bcr_7_b_cloneSummary_04-11.R            |
41        | bcr_7_b_cloneSummary_05.R               |
42        | bcr_7_c_cloneSummary_export.R           |
43        | bcr_7_d_overlap_circos.R                | Fig 3b; ExD Fig 3e
44        | bcr_8_muFreq_export_bulk.R              |
45        | bcr_8_muFreq_export_10x.R               | 
46        | bcr_8_muFreq_plot_bulk.R                | Fig 3c
47        | bcr_8_muFreq_plot_10x.R                 | ExD Fig 3h
48        | bcr_9_a_tree_prep_05.R                  |
49        | bcr_9_b_MLtree_05.sh                    | 
50        | bcr_9_c_MLtree_visual_05.R              | Fig 3fg
51        | bcr_10_diversity.R                      | ExD Fig 3f

Auxiliary scripts

* immcantation_preprocess-phix_10242018_JZ.sh
* immcantation_presto-abseq_10242018_JZ.sh
* immcantation_fastq2fasta.py
* JZ_additional_qc.py
* JZ_calcAAmut_generalized.R
* JZ_handleDots.R
* JZ_isNovel.R
* JZ_plotCrossHam.R
* JZ_remove_inconsistent_internalC.py
* JZ_split_chains.py
* susanna_overlapPlot.R

