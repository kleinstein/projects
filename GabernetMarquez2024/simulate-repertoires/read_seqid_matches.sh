#!/bin/bash
set -euo pipefail

REP=( "rep000" "rep002" "rep003" "rep004" "rep005" "rep001" "rep006" "rep007" "rep008" "rep009" "rep101" "rep102" "rep103" "rep104" "rep105" )

for i in "${!REP[@]}"
do
    python list_sequences_from_fastq.py -i sim_repertoires/${REP[i]}_all_primers_reads_R1_renamed.fastq.gz -o sim_repertoires/read_sequence_id_matches/${REP[i]}_read_sequence_id_matches.tsv
done

REP=( "rep010" "rep012" "rep013" "rep014" "rep015" "rep011" "rep016" "rep017" "rep018" "rep019" "rep111" "rep112" "rep113" "rep114" "rep115" )

for i in "${!REP[@]}"
do
    python list_sequences_from_fastq.py --umi -i sim_repertoires/${REP[i]}_all_primers_reads_R1_renamed.fastq.gz -o sim_repertoires/read_sequence_id_matches/${REP[i]}_read_sequence_id_matches.tsv
done