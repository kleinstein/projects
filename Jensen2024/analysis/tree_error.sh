#!/bin/bash

#SBATCH --nodes=1
#SBATCH --cpus-per-task=20
#SBATCH --mem=20g
#SBATCH --partition day
#SBATCH --time=1-0:00:00

module load R/4.1.0

iterations=$1
Rscript get_tree_error.R $iterations