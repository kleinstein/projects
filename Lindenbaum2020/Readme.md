Code for Lindenbaum, O., Nouri, N., Kluger, Y., & Kleinstein, S. (2020).
Alignment free identification of clones in B cell receptor repertoires. BioRxiv.‏
https://www.biorxiv.org/content/10.1101/2020.03.30.017384v1.abstract

To use the code, copy the core.py file, and use 
from core import *

then use the function
table_out=alignement_free_clone(repertoire,table_neg,W_l=150,per=0.1)

repertoire- is the input table of sequences for clonal assignments.
table neg- is the table of sequences for estimating the threshold.
W_l- number of base pairs used for analysis
per- estimated specificity error in %

For a complete example see Example.ipynb
