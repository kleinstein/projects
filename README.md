Repository of publication scripts
------------------------------------------------------------------------

Folder            | Publication
----------------- | -------------
Gupta2017         | Gupta et al, 2017. J Immunol.
Hoehn2019         | Hoehn et al, 2019. PNAS. https://doi.org/10.1073/pnas.1906020116
Oh_Iijima2019     | Oh et al, 2019. Nature.
Zhou2019          | Zhou & Kleinstein, 2019. J Immunol.
Alsoussi2020      | Alsoussi\*, Turner\*, Case\*, Zhao\*, Schmitz\* & Zhou\* et al., 2020. J Immunol.
Jiang2020		  | Jiang et al, 2020. JCI Insight. oconnor-musk. https://insight.jci.org/articles/view/136471
JiangPNAS2020	  | Jiang et al, 2020. PNAS. oconnor-thymus. https://doi.org/10.1073/pnas.2007206117
Lu2020_JEM	      | Lu et al, 2020. JEM. craft-tfr. https://doi.org/10.1084/jem.20200547
TurnerZhouHan2020 | Turner\*, Zhou\* & Han\* et al., 2020. Nature.
Zhou2020          | Zhou & Kleinstein, 2020. J Immunol.
Csepregi2021      | Csepregi et al, 2021. bioRxiv. https://doi.org/10.1101/2021.09.15.460420
Hoehn2021_JI      | Hoehn et al, 2021. J Immunol. https://doi.org/10.4049/jimmunol.2100135
Hoehn2021_eLife   | Hoehn et al, 2021. eLife. https://elifesciences.org/articles/70873
Jiang2021		  | Jiang et al, 2021. JCI Insight. bockenstedt-lyme. https://doi.org/10.1172/jci.insight.148035
Jiang2021_JI	  | Mandel-Brehm\*, Fichtner\*, Jiang\*, et al, 2021. JI. oconnor-glycosylation. https://doi.org/10.4049/jimmunol.2100225
Oh2022_SI		  | Oh et al, 2022. Science Immunology. iwasaki-flu. https://doi.org/10.1126/sciimmunol.abj5129
Song2021_CellReports	| Song et al, 2021. Cell Reports. farhadian-csf. https://doi.org/10.1016/j.xcrm.2021.100288
Fichtner2022      | Fichtner et al, 2022. Acta Neuropathologica Communications. https://doi.org/10.1186/s40478-022-01454-0
Hoehn2022         | Hoehn et al, 2022. PLoS CB. https://doi.org/10.1371/journal.pcbi.1009885
Jensen2023        | Jensen et al, submitted
Nickerson2023     | Nickerson et al, in revision. MRL mouse germline setup.
OtaHoehnBraga2024      | Ota\*, Hoehn\* et al, 2024 Science Translational Medicine https://doi.org/10.1126/scitranslmed.adi0673
Wang2023          | Wang et al, 2023 NAR https://doi.org/10.1093/nar/gkad1128
Wang2023_Aging    | Wang et al, 2023 Aging https://doi.org/10.18632/aging.204778
GabernetMarquez2024    | Gabernet\*, Marquez\* et al, submitted.