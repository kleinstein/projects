#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 20:00
#SBATCH --mem=10G

# reference:  https://www.biostars.org/p/85929/ Irsan's answer

PATH_FASTQ="/ysm-gpfs/home/qz93/scratch60/church_flu/presto_fastq/"
PATH_FASTA="/ysm-gpfs/home/qz93/scratch60/church_flu/presto_fasta/"

for sample in {1..30}; do

	FASTQ_NAME="${sample}-FIN_collapse-unique_atleast-2.fastq"
	FASTA_NAME="${sample}-FIN_collapse-unique_atleast-2.fasta"

	# if sample file exists
	
	if [[ (-s "${PATH_FASTQ}${FASTQ_NAME}") ]]; then

		echo "starting on sample "${sample} # print to standard output
	
		cat ${PATH_FASTQ}${FASTQ_NAME} | paste - - - - | sed 's/^@/>/g'| cut -f1-2 | tr '\t' '\n' > ${PATH_FASTA}${FASTA_NAME}
	
		echo "finished sample "${sample} # print to standard output

	fi

done
