
pathRoot = "~/Dropbox (yale)/projects/"
versionTag = "ref_202011-3_11Mar2020" #*
pathGermIMGT = paste0(pathRoot, "germ/", versionTag)

BCR_VERSION = "2020-04-06"

pathBCR = "~/Dropbox (yale)/projects/ellebedy_murineCovid/bcr/"

pathData = paste0(pathBCR, "QC/") 
pathWork = paste0(pathBCR, BCR_VERSION, "/cleanGerm/")


#### identical ref alleles ####

# identified in 0_germRef_a_subset.R

filename = "ident_ref_allele_C57BL6.RData"
setwd(pathGermIMGT)
load(filename) # megaIdentLst

megaIdentLst

#* [2020-04-06]
# $Ighv
# $Ighv[[1]]
# [1] "IGHV1-62-2*01" "IGHV1-71*01"  

# $Ighd
# $Ighd[[1]]
# [1] "IGHD5-2*01" "IGHD5-3*01" "IGHD5-4*01" "IGHD5-6*01"
# 
# $Ighd[[2]]
# [1] "IGHD5-3*01" "IGHD5-4*01" "IGHD5-6*01"
# 
# $Ighd[[3]]
# [1] "IGHD5-4*01" "IGHD5-6*01"
# 
# $Ighd[[4]]
# [1] "IGHD2-5*01" "IGHD2-6*01"


#***********************************************************
#### keep only one of the identical alleles in genotype ####
#***********************************************************

### helper function

cleanVCall = function(VCall, keep, dups) {
    
    # 1 boolean value for each item in dups
    # fixed=T is a must
    boolDupsPresent = sapply(dups, function(d){ grepl(pattern=d, x=VCall, fixed=T) })
    
    if (any(boolDupsPresent)) {
        
        # illustration
        # suppose A1, A2, A3, A4 are duplicates of each other; want to keep A1
        # "A1, A3, A4, B, C" 
        # strsplit: c("A1", "A3", "A4", "B", "C")
        # %in%:        F     T     T     F    F
        #           c("A1", "A1", "A1", "B", "C")
        # unique:   c("A1", "B", "C")
        # paste: "A1, B, C"
        
        VCallSplit = strsplit(VCall, ",")[[1]]
        boolReplace = VCallSplit %in% dups
        stopifnot(any(boolReplace))
        VCallSplit[boolReplace] = keep
        
        #VCallSplitUniq = sort( unique(VCallSplit) )
        # preserve order
        VCallSplitUniq = unique(VCallSplit)
        
        VCallStringUniq = paste0(VCallSplitUniq, collapse=",")
        
        return(VCallStringUniq)
        
    } else {
        # no dup present: return as is
        # sanity check: then allele to keep must be present (could be other non-identical alleles as well)
        # fixed=T is a must
        stopifnot( grepl(pattern=keep, x=VCall, fixed=T) )
        return(VCall)
    }
    
}


### load QC'ed BCR data

setwd(pathData)
filename = "bcr_paired.RData"
load(filename) # db_paired
db = db_paired; rm(db_paired)

uniqD_bf = sort(unique(db[["D_CALL"]]))

for (seg in names(megaIdentLst)) {
    
    lstIdent = megaIdentLst[[seg]]
    
    # skip if there is no pair of identical ref alleles
    if (length(lstIdent)>0) {
        
        cat("\n******", seg,"*******\n")
        
        # column to fix
        if (seg %in% c("Ighv", "Igkv", "Iglv")) {
            CALL_COL = "V_CALL"
        } else if (seg %in% c("Ighj", "Igkj", "Iglj")) {
            CALL_COL = "J_CALL"
        } else {
            CALL_COL = "D_CALL"
        }
        
        #cat("\n BEFORE \n")
        tab_bf = table(db[[CALL_COL]])
        #print(tab_bf)
        
        ### strategy
        # Go thru list
        # For each list entry, if more than 1 item is in genotype,
        #     keep only 1 item in CALL_COL
        # Compare tabulation of CALL_COL before and after
        
        bool_detected = rep(F, length(lstIdent))
        
        for (i in 1:length(lstIdent)) {
            # print(i)
            
            # this should be within for loop as this gets updated 
            CALL_COL_SPLIT = sapply(db[[CALL_COL]], function(s){ strsplit(s, ",")[[1]] })
            
            alleleIdents = lstIdent[[i]]
            # designate the first one to be the one to keep
            alleleKeep = alleleIdents[1]
            # to replace with alleleKeep
            alleleDups = alleleIdents[-1]
            
            # For each sequence's CALL_COL, is each of the allele in genotype?
            # one boolean vector for each item in alleleIdents
            # fixed=T is a must
            boolIdents = sapply(alleleIdents, function(d){ 
                # differentiate 
                # "IGHV3-30*02"   "IGHV3-30-5*02" vs.
                # "IGHV3-30*02_T201C"   "IGHV3-30-5*02_T201C"
                
                # This could also happen
                # IGHV3-7*01,IGHV3-30*02_T201C,IGHV3-30*02,IGHV3-30-5*02_T201C,IGHV3-30-5*02
                
                return(sapply(CALL_COL_SPLIT, function(curAs){ d %in% curAs }))
                
                #if (!grepl(pattern="_", x=d)) {
                #    grepl(pattern=d, x=db[[CALL_COL]], fixed=T) & !grepl(pattern="_", x=db[[CALL_COL]])
                #} else {
                #    grepl(pattern=d, x=db[[CALL_COL]], fixed=T)
                #}
                
            }, simplify=F)
            
            boolIdents = do.call(cbind, boolIdents)
            
            # For this person's genotype, more than 1 allele present?
            # if there is TRUE in a column in boolIdents, that indicates that allele is present
            # if more than 1 columns have TRUE, that indicates presence of alleles
            #    with identical sequence but different names
            boolByCol = apply(boolIdents, 2, any)
            boolBoth = sum(boolByCol)>1
            
            # print(boolBoth)
            
            if (boolBoth) {
                
                cat("\n ********************** \n")
                cat("Detected", alleleIdents[boolByCol], "\n")
                cat("Keep:", alleleKeep, "\n")
                cat("Replace:", alleleDups, "\n")
                
                bool_detected[i] = T
                
                uniqRecordsBf = unique(db[[CALL_COL]][apply(boolIdents, 1, any)])
                
                cat("\n")
                for (curRecBf in uniqRecordsBf) {
                    #cat(curRecBf,"\n")
                    curRecAf = cleanVCall(VCall=curRecBf, keep=alleleKeep, dups=alleleDups)
                    
                    if (curRecBf!=curRecAf) { 
                        cat(curRecBf, "===>", curRecAf, "\n") 
                        
                        # edit CALL_COL
                        idx_j = which(db[[CALL_COL]]==curRecBf)
                        stopifnot( length(idx_j)>0 )
                        db[[CALL_COL]][idx_j] = curRecAf
                    }
                }
                
                uniqRecordsAf = unique(db[[CALL_COL]][apply(boolIdents, 1, any)])
                
                cat("\nBefore\n")
                print(uniqRecordsBf)
                cat("\nAfter\n")
                print(uniqRecordsAf)
                cat("\n")
            }
            
        }
        
        if (any(bool_detected)) {
            
            #cat("\nAFTER\n")
            tab_af = table(db[[CALL_COL]])
            #print(tab_af)
            
            detected_vec = unlist(lstIdent[bool_detected])
            
            # only show relevant parts of the table
            # one col for each item in detected_vec
            bool_show_tab_bf = sapply(detected_vec, 
                                      function(d) { grepl(pattern=d, x=names(tab_bf), fixed=T) }, 
                                      simplify=F)
            bool_show_tab_bf = do.call(cbind, bool_show_tab_bf)
            bool_show_tab_bf = apply(bool_show_tab_bf, 1, any)
            
            bool_show_tab_af = sapply(detected_vec, 
                                      function(d) { grepl(pattern=d, x=names(tab_af), fixed=T) }, 
                                      simplify=F)
            bool_show_tab_af = do.call(cbind, bool_show_tab_af)
            bool_show_tab_af = apply(bool_show_tab_af, 1, any)
            
            # compare tabulation of CALL_COL
            cat("*****************\n")
            cat("\nBEFORE\n")
            print( sort( tab_bf[bool_show_tab_bf] ) )
            cat("\nAFTER\n")
            print( sort( tab_af[bool_show_tab_af] ) )
            
            # rest of the table should stay the same
            tab_bf_rest = sort( tab_bf[!bool_show_tab_bf] )
            tab_af_rest = sort( tab_af[!bool_show_tab_af] )
            
            stopifnot( all.equal( tab_bf_rest, tab_af_rest ) )
            
            ##### sanity check
            # check for absence (expected) of alleles that should have been replaced
            for (d in 1:length(lstIdent)) {
                if (bool_detected[d]) {
                    dupsReplaced = lstIdent[[d]][-1]
                    for (dd in dupsReplaced) {
                        #print(c(d, dd))
                        stopifnot( !any( grepl(pattern=dd, x=db[[CALL_COL]], fixed=T) ) )
                    }
                }
            }
            cat("\n Sanity checked.\n")
            
        } else {
            cat("\nNo correction needed for", seg, "in", CALL_COL, ".\n")
        }
        
    }
}

uniqD_af = sort(unique(db[["D_CALL"]]))

# compare D_CALL before and after

# [2020-04-06]
# before
# [1] "IGHD1-1*01,IGHD2-5*01,IGHD2-6*01" "IGHD2-4*01,IGHD2-5*01,IGHD2-6*01" "IGHD2-4*01,IGHD5-2*01,IGHD5-3*01"
# [4] "IGHD2-5*01,IGHD2-6*01"            "IGHD2-5*01,IGHD2-6*01,IGHD2-8*01" "IGHD2-5*01,IGHD2-6*01,IGHD4-1*01"
# [7] "IGHD2-5*01,IGHD2-6*01,IGHD5-2*01" "IGHD5-2*01,IGHD5-3*01,IGHD5-4*01"
# after
# [1] "IGHD1-1*01,IGHD2-5*01" "IGHD2-4*01,IGHD2-5*01" "IGHD2-4*01,IGHD5-2*01" 
# [4] "IGHD2-5*01"            "IGHD2-5*01,IGHD2-8*01" "IGHD2-5*01,IGHD4-1*01" 
# [7] "IGHD2-5*01,IGHD5-2*01" "IGHD5-2*01"  

uniqD_bf[uniqD_bf != uniqD_af]
uniqD_af[uniqD_bf != uniqD_af]

#### export ####

# paired
db_paired = db; rm(db)
saveName = "bcr_cleanGerm_paired.RData"
setwd(pathWork)
save(db_paired, file=saveName)

# H only
#db_heavy = db_paired[db_paired[["LOCUS"]]=="IGH", ]
#saveName = "bcr_cleanGerm_heavy.RData"
#setwd(pathWork)
#save(db_heavy, file=saveName)

# L only
#db_light = db_paired[db_paired[["LOCUS"]]!="IGH", ]
#saveName = "bcr_cleanGerm_light.RData"
#setwd(pathWork)
#save(db_light, file=saveName)

# tsv
#alakazam::writeChangeoDb(data=db_heavy, file="bcr_cleanGerm_heavy.tsv")
#alakazam::writeChangeoDb(data=db_light, file="bcr_cleanGerm_light.tsv")

