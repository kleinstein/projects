#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 1-
#SBATCH -c 16
#SBATCH --mem=4G

# --constraint=E5-2660_v3
# --constraint=6240


# pass -c configuration to NPROC
# -1 is a quirk to default to 1 if no -c was passed to slurm
NPROC=${SLURM_CPUS_PER_TASK:-1}

# IN:
# 
# OUT:
# 

# This script is designed to work with change-o 0.4.6.999 (Oct 3, 2019)
# This version has the -nmask option added to BuildTrees.py

# IgPhyML v1.1.0 (Spet 16, 2019); pulled Oct 2, 2019

#*
RUN_ID="04272020"
VERSION="01232020"

PATH_ROOT="/home/qz93/scratch60/${VERSION}_${RUN_ID}/"
PATH_DATA="${PATH_ROOT}MLtrees/db2/"
PATH_WORK="${PATH_ROOT}MLtrees/"

METHOD="DTN" # log name uses this
MODE_ARRAY=(mAb_bulk_10x mAb_10x)
SUBJ_ARRAY=(P05)
#HACK_ARRAY=(TRUE FALSE)
HACK_ARRAY=(FALSE)
#EXCL_FIRST_ARRAY=(TRUE FALSE)
EXCL_FIRST_ARRAY=(TRUE)
EXCL_FIRST_NUM="18"

TSV_INPUT_PREFIX="db_mAbClone_${METHOD}_"

RUN_BT=TRUE
RUN_ML=TRUE

PATH_LOG=${PATH_WORK}MLtree_${METHOD}_$(date '+%m%d%Y_%H%M%S').log

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${PATH_LOG}"
#echo "germline" ${GERM_TAG} &>> "${PATH_LOG}"
echo "bash" $BASH_VERSION &>> "${PATH_LOG}"
BuildTrees.py --version &>> "${PATH_LOG}"
igphyml -version &>> "${PATH_LOG}"
python3 -V &>> "${PATH_LOG}"


for SUBJ in ${SUBJ_ARRAY[@]}; do
	for HACK in ${HACK_ARRAY[@]}; do
		for EXCL_FIRST in ${EXCL_FIRST_ARRAY[@]}; do
			for MODE in ${MODE_ARRAY[@]}; do

				echo "   " &>> "${PATH_LOG}"
				echo "*************************************************************************" &>> "${PATH_LOG}"
				echo "**** subj = ${SUBJ} mode = ${MODE} hack = ${HACK} excl_first_${EXCL_FIRST_NUM} = ${EXCL_FIRST} *****" &>> "${PATH_LOG}"
				echo "*************************************************************************" &>> "${PATH_LOG}"

				# this must be outside if/else loops
				
				TSV_INPUT_SUFFIX="mode-${MODE}_excl-${EXCL_FIRST_NUM}-${EXCL_FIRST}_hack-${HACK}_${SUBJ}"

				######################
				##### BuildTrees #####
				######################

				# creates directory [outdir]/[outname], containing
				# <clone ID>.fasta: IMGT MSA for a clone
				# <clone ID>.part.txt: info abt V, J, IMGT numbering

				# creates file [outdir]/[outname_lineages.tsv]
				# direct input to IgPhyML
				# each line represents a clone and shows the MSA

				# [outdir]/[log]: whether or not each sequence was included in analysis

				# --collapse highly recommended to collapse identical seqs

				# --md: List of fields containing metadata to include in output fasta file sequence headers

				# --append: List of columns to append to sequence ID to ensure uniqueness

				# required fields in -d (incomplete list -- do not trust):
				# SEQUENCE_ID, SEQUENCE_INPUT, SEQUENCE_IMGT, GERMLINE_IMGT_D_MASK
				# V_CALL, J_CALL, CLONE

				# counts in "FAIL" can be due to:
				# - nonfunctional
				# - frame-shifting deletion
				# - single frame-shifting insertion
				# - duplicate 

				if [[ ${RUN_BT} == "TRUE" ]]; then
					
					echo "   " &>> "${PATH_LOG}"
					echo "\n##### BuildTrees #####\n" &>> "${PATH_LOG}"

					# no need to specify --md CELL_TYPE TIMEPOINT b/c these are coded in SEQUENCE_ID

					# e.g.: db_mAbClone_DTN_mode-mAb_bulk_hack-FALSE_P11.tsv
					
					TSV_INPUT_FULL="${TSV_INPUT_PREFIX}${TSV_INPUT_SUFFIX}.tsv"

					BuildTrees.py \
						-d "${PATH_DATA}${TSV_INPUT_FULL}" \
						--format changeo \
						--md SEQ_TYPE anno_10x_B1 anno_10x_B2 PRCONS TIMEPOINT CELL_TYPE SAMPLE_2 SAMPLE \
						--minseq 2 \
						--collapse \
						--outdir "${PATH_WORK}" \
						--outname "BT_${TSV_INPUT_SUFFIX}" \
						--log "BT_${TSV_INPUT_SUFFIX}.log" \
						--failed \
						--nmask \
						&>> "${PATH_LOG}"

					echo "   " &>> "${PATH_LOG}"
					echo "If any failed:" &>> "${PATH_LOG}"
					echo "   " &>> "${PATH_LOG}"

					grep -B 2 -A 1 "PASS> False" BT_${TSV_INPUT_SUFFIX}.log &>> "${PATH_LOG}"

				fi


				###################
				##### igphyml #####
				###################

				if [[ ${RUN_ML} == "TRUE" ]]; then

					### GY94

					echo "   " &>> "${PATH_LOG}"
					echo "\n##### igphyml GY94 #####\n" &>> "${PATH_LOG}"

					# recommended to estimate initial tree topologies using GY94
					# for runtime improvement

					# by default, topologies searched using NNI moves
					#    use -s SPR for a more thorough search

					# can view tree topologies in <clone id>.fasta_igphyml_tree_GY.txt
					# using, e.g. FigTree

					igphyml \
						--repfile "${PATH_WORK}BT_${TSV_INPUT_SUFFIX}_lineages.tsv" \
						-m GY \
						--outrep "ML_GY_${TSV_INPUT_SUFFIX}.tsv" \
						--run_id GY \
						--threads "${NPROC}" \
						&>> "${PATH_LOG}"

					### HLP19

					echo "   " &>> "${PATH_LOG}"
					echo "\n##### igphyml HLP19 #####\n" &>> "${PATH_LOG}"

					# estimate ML tree topologies using HLP19 with a GY94 starting topology

					# can view tree topologies in <clone id>.fasta_igphyml_tree_HLP.txt
					# using, e.g. FigTree

					# parallel calculations (via --threads) are by tree
					# so no point in using more threads than there are lineages

					igphyml \
						--repfile "${PATH_WORK}ML_GY_${TSV_INPUT_SUFFIX}.tsv" \
						-m HLP \
						--outrep "ML_HLP_${TSV_INPUT_SUFFIX}.tsv" \
						--run_id HLP \
						--threads "${NPROC}" \
						&>> "${PATH_LOG}"

				fi

				echo "   " &>> "${PATH_LOG}"

			done
		done
	done
done
