#! /bin/bash

#SBATCH --job-name=airrflow-cv-all
#SBATCH --out="slurm-%j.out"
#SBATCH --time=23:00:00
#SBATCH --cpus-per-task=64
#SBATCH --mem=100G
#SBATCH --reservation=id
#SBATCH --qos=nothrottle
#SBATCH --mail-type=ALL

module load Java/17.0.4
export NXF_WRAPPER_STAGE_FILE_THRESHOLD='40000'

nextflow pull nf-core/airrflow -r 4.0
nextflow run nf-core/airrflow -r 4.0 \
-profile singularity \
--input 2023-10-11-allrepertoires_merged_filtered_lesscols_scsamplesmerged.tsv \
--mode assembled \
--outdir airrflow_results \
--clonal_threshold 0.1 \
--save_databases \
--skip_report_threshold \
--skip_all_clones_report \
-c custom.config \
-w work-covid-new-benchmark \
-resume