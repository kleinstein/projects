


use strict;
use warnings;
use hotSpotUtils;

my @folders=("097","095","093","083","074","073","065","064","063","054","049","034","033","026","025","023","021","018","016","015","014","013","011","008","004","003","001");
my @visits=("V1");

my $filestem = $ARGV[0];
open(OUT,">ClonesSizes.tsv") or die();
my $y=0;
my $min = 9**9**9;
my $max = 0;
my $mean = 0;
my $nfile = 0;
foreach my $f (@folders){
	#foreach my $y (@year){
		foreach my $v (@visits){
			my $c=0;
			my $cr=0;
			open(IN,"$f/mrna/$v/$filestem") or do{print "$f/mrna/$v/$filestem not found!";next;};
			my $seqcount=0;
			my $h=<IN>;
			while(<IN>){
				my $line=$_;
				my @in=split("\t",$line);
				my $seqs = getfasta($in[0]);
				my @k=keys %$seqs;
				my $cr = (scalar(@k)-1);
				if($cr <= 2){ #catch fasta files where a single sequence has been duplicated
					foreach my $k2(@k){
						if($k2=~/_1_0$/){
							die("oops!\n");
							$cr=1;
						}
					}	
				}
				$c += $cr;
				#print "$f\t$y\t$v\t$cr\t$in[0]\n";
				print OUT "$f\t$y\t$v\t$cr\t$in[0]\n";
			}
			print "$f\t$v\t$c\n";
			if($c < $min){$min = $c;}
			if($c > $max){$max = $c;}
			$mean += $c;
			$nfile++;
		}
	#}
}
$mean /= $nfile;
print "$min\t$mean\t$max\n";
