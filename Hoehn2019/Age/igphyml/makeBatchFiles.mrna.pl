


use strict;
use warnings;


my @folders=("034","097","095","093","083","074","073","065","064","063","054","049","033","026","025","023","021","018","016","015","014","013","011","008","004","003","001");
my @visits=("V1");
my $minseq=3;
my $hlpdiff=3;
my $threads=8;
my $c=0;
foreach my $f (@folders){
	open(OUT,">run$f.mrna.sh") or die($f);

	my $part = "general";
	if($c < 3){
		$part = "pi_kleinstein";
	}
	$c++;
	print OUT 
	"#!/usr/bin/bash
#SBATCH --partition=$part
#SBATCH --job-name=$f
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=$threads
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=24:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=kenneth.hoehn\@gmail.com
#SBATCH --exclude=c24n04

module load CBLAS/20110120-foss-2016b
module load ATLAS/3.10.2-GCC-5.4.0-2.26-LAPACK-3.6.1\n";

foreach my $v (@visits){
	
	print OUT "maskCDR3.pl $f/mrna/$v/seqs_lineages.tsv\n";
	print OUT "igphyml --repfile $f/mrna/$v/seqs_ncdr_lineages.tsv -m GY --threads $threads --run_id GY:$minseq --minseq $minseq --outrep $f/mrna/$v/seqs_ncdr_lineagesn.GY$minseq.tsv --repqidefreqs\n";
	print OUT "igphyml --repfile $f/mrna/$v/seqs_ncdr_lineagesn.GY$minseq.tsv -m HLP19 --motifs FCH --omegaOpt e,ce --threads $threads --run_id HLP19ci -o lr --minseq $hlpdiff --cbmat\n";
	
	#Models used for AIC comparisons
	print OUT "igphyml --repfile $f/mrna/$v/seqs_ncdr_lineagesn.GY$minseq.tsv -m HLP17 --motifs WRC_2:0,GYW_0:0 --hotness 0 -f empirical --omega e,e --rootpi --threads $threads --run_id H0pi -o lr --minseq $hlpdiff --cbmat\n";
	print OUT "igphyml --repfile $f/mrna/$v/seqs_ncdr_lineagesn.GY$minseq.tsv -m HLP17 --motifs FCH --omega e,e --threads $threads --run_id HLP17ncbmatpi --rootpi -o lr --minseq $hlpdiff\n";
	print OUT "igphyml --repfile $f/mrna/$v/seqs_ncdr_lineagesn.GY$minseq.tsv -m HLP18 --motifs FCH --omega e,e --threads $threads --run_id HLP18pi -o lr --rootpi --minseq $hlpdiff --cbmat\n";
}
system("sbatch run$f.mrna.sh");
}

