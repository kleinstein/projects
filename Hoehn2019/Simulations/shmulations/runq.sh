#!/usr/bin/bash
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=24:00:00
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=kenneth.hoehn\@gmail.com
#SBATCH --exclude=c24n04,c22n05

base=$1
threads=$2

echo $base

BuildTrees.py -d $base.tab --collapse --minseq 0
maskCDR3.pl $base\_lineages.tsv
igphyml --repfile $base\_ncdr_lineages.tsv -m GY --outrep $base\_ncdr_gy_lineages.tsv --threads $threads --minseq 3 --run_id GY
igphyml --repfile $base\_ncdr_gy_lineages.tsv -m HLP --motifs FCH --cbmat --threads $threads -o lr --run_id hlp19
igphyml --repfile $base\_ncdr_gy_lineages.tsv -m HLP17 --motifs FCH --cbmat --threads $threads -o lr --run_id hlp17
igphyml --repfile $base\_ncdr_gy_lineages.tsv -m HLP17 --hotness 0 --motifs WRC_2:0,GYW_0:0 --cbmat --threads $threads -o lr --rootpi -f empirical --run_id h0
rm -R $base/
