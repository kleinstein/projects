#!/usr/bin/bash
#SBATCH --partition=pi_kleinstein
#SBATCH --job-name=definclonesfv
#SBATCH --ntasks=1 --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --mem-per-cpu=4gb 
#SBATCH --time=96:00:00
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=kenneth.hoehn@gmail.com
#SBATCH --exclude=c24n04



DefineClones.py -d church_flu_SUBJECT-FV.tsv --act set --model ham --sym min --norm len --dist 0.10 --nproc 16 --log FV_clone.log 

