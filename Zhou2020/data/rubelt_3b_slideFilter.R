##### compute mutation count, frequency, and
##### apply a particular set of sliding window parameters
pathRoot = "/ysm-gpfs/home/qz93/scratch60/spatial_data/davis_twins/"
pathData = paste0(pathRoot, "slide/")
pathWork = paste0(pathRoot, "slide/")

WINDOW_SIZE = 10 #*
MUT_THRESH = 6 #*

cat("window size: ", WINDOW_SIZE, "; mut_thresh: ", MUT_THRESH, "\n")

library(alakazam) # writeChangeoDb
library(shazam)
library(doMC)
library(foreach)

#num.cores.foreach = detectCores() #*
num.cores.foreach = 10 #*

registerDoMC(num.cores.foreach) #* if using foreach loop

subjects = paste0(rep("TW", 10), rep(paste0("0", 1:5), each=2), rep(c("A","B"), 5))

idxMtx = expand.grid(subjects, c("T","F"), stringsAsFactors = F)

foreach(i=1:nrow(idxMtx)) %dopar% { #* if using foreach loop
    
    subj = idxMtx[i, 1]
    FUNCTIONAL = idxMtx[i, 2]
    
    cat("##### subject: ", subj, "; FUNCTIONAL: ", FUNCTIONAL, "\n")
    filename = paste0(subj, "_IGHV_FUNCTIONAL-", FUNCTIONAL, "_genotyped_preClonal_germ-pass_parse-rename.tab")
    
    setwd(pathData)
    
    if (file.exists(filename)) {
        
        db = read.table(filename, sep="\t", header=T, stringsAsFactors=F)
        
        ### count number of observed mutations in IMGT V 1..312
        mutMtx = sapply(1:nrow(db), 
                        function(i) {
                            calcObservedMutations(inputSeq = db[i, "SEQUENCE_IMGT"],
                                                  germlineSeq = db[i, "PRECLONAL_GERMLINE_IMGT_D_MASK"], #*
                                                  regionDefinition = IMGT_V_BY_SEGMENTS, # only avail in v0.1.9+
                                                  returnRaw = T)
                        })
        
        ### list of data frames
        mutPosBefore = mutMtx[1, ]
        
        ### tuning for sliding window 
        dbTune = slideWindowTune(db=db, dbMutList = mutPosBefore, 
                                 mutThreshRange = MUT_THRESH, windowSizeRange = WINDOW_SIZE)
        
        ### index of sequences to discard
        
        # dbTune: a list of logical matrices
        # - Each matrix corresponds to a windowSize in windowSizeRange
        # - Each column in a matrix corresponds to a mutThresh in mutThreshRange
        dbFilter = dbTune[[as.character(WINDOW_SIZE)]][, as.character(MUT_THRESH)] #*
        cat("subject: ", subj, "; FUNCTIONAL: ", FUNCTIONAL, "; #filtered: ", sum(dbFilter), "\n")
        
        setwd(pathWork)
        
        db = db[!dbFilter, ]
        mutPosAfter = mutPosBefore[!dbFilter]
        
        save(db, file=paste0(subj, "_IGHV_FUNCTIONAL-", FUNCTIONAL, "_genotyped_slideFiltered.RData"))
        writeChangeoDb(data=db, file=paste0(subj, "_IGHV_FUNCTIONAL-", FUNCTIONAL, "_genotyped_slideFiltered.tsv"))
        
        ### save other objects
        save(mutPosBefore, mutPosAfter, dbTune, 
             file=paste0(subj, "_IGHV_FUNCTIONAL-", FUNCTIONAL, 
                         "_slideTune_mutThresh_", MUT_THRESH,
                         "_windowSize_", WINDOW_SIZE, ".RData"))
        
        rm(mutMtx, mutPosBefore, mutPosAfter, db, dbTune)
        
    } else {
        cat(filename, "does not exist; skipped. \n")
    } 
    
}
