# Kenneth B. Hoehn
# 2/1/2021
# Make 10X cluster figures

library(alakazam)
library(dplyr)
library(tidyr)
library(ggpubr)
library(dowser)
library(ggpubr)
library(ggtree)
library(gridExtra)
library(scoper)

cloned = readChangeoDb("intermediates/all_cloned_data.tsv")
cloned = filter(cloned, !is.na(c_call))
cloned = filter(cloned, !is.na(seurat_clusters))
cloned$c_call[is.na(cloned$c_call)] = "Unkn"
cloned$seurat_clusters[is.na(cloned$seurat_clusters)] = "Unkn"
cloned$major_c = substr(cloned$c_call,1,4)

cloned = filter(cloned, !is.na(germline_alignment_d_mask))

cloned$c_call = factor(cloned$c_call,
	levels = c("IGHM","IGHD","IGHG3","IGHG1","IGHA1",
		"IGHG2","IGHG4","IGHE","IGHA2","Unkn"))

cloned$major_c = factor(cloned$major_c,
  levels = c("IGHM","IGHD","IGHG","IGHA","IGHE","Unkn"))

cloned$seurat_clusters = factor(cloned$seurat_clusters,
  levels = c(0,4,1,3,5,6,7,9,2,8,10))

# Cluster calibration plots
pdf("results/cluster_plots.pdf",height = 5, width =7, useDingbats=FALSE)
g1 = ggplot(filter(cloned,locus=="IGH"),aes(x=seurat_clusters,y=mu_freq))+
    geom_boxplot(outlier.shape=NA)+
    geom_jitter(width=0.1,height=0,size=0.3,aes(color=c_call))+theme_bw()+
    xlab("Cluster")+ylab("SHM frequency")+
    scale_color_brewer(palette="Set1",name="Isotype")+
    theme(axis.text.x = element_text(angle = 0, vjust = 0.5, 
      color = "black"), axis.text.y = element_text(color = "black"))+
    theme(legend.key.size = unit(0.5,"line"))
  
cluster_isotypes = cloned %>%
      filter(!is.na(c_call) & locus=="IGH") %>%
    group_by(seurat_clusters,c_call, .drop=FALSE) %>%
    summarize(n = n()) %>%
    mutate(Frequency = n/sum(n))
  
g2 = ggplot(filter(cluster_isotypes),
    aes(x=factor(seurat_clusters),y=Frequency,fill=c_call))+
    geom_bar(stat='identity',color="black",size=0.3,width=1)+
    scale_fill_brewer(palette="Set1",name="Isotype")+theme_bw()+
    ylab("Proportion of cells")+
    xlab("Cluster")+
    theme(axis.text.x = element_text(angle = 0, vjust = 0.5, 
      color = "black"), axis.text.y = element_text(color = "black"))+
    theme(legend.key.size = unit(0.5,"line"))

cluster_donor = cloned %>%
    filter(locus=="IGH") %>%
    group_by(seurat_clusters,sample) %>%
    summarize(n = n()) %>%
    mutate(Frequency = n/sum(n))  

g4 = ggplot(filter(cluster_donor),
    aes(x=factor(seurat_clusters),y=Frequency,fill=sample))+
    geom_bar(stat='identity',color="black",size=0.3,width=1)+theme_bw()+
    ylab("Proportion of cells")+
    xlab("Cluster")+
    theme(axis.text.x = element_text(angle = 0, vjust = 0.5, 
      color = "black"), axis.text.y = element_text(color = "black"))+
    theme(legend.key.size = unit(0.5,"line"))

print(grid.arrange(g1,g2,g4,ncol=2))

dev.off()

# cells in each cluster per patient
counts = cloned %>%  
  group_by(seurat_clusters, sample) %>%
  summarize(n=n()) %>%
  group_by(seurat_clusters) %>%
  spread(sample, n)

write.csv(counts, file="results/cluster_counts.csv")

annotations = c(
"0"="naive",
"1"="memory",
"2"="PB",
"3"="memory",
"4"="naive",
"5"="memory",
"6"="memory",
"7"="memory",
"8"="PB",
"9"="memory",
"10"="pro-PB"
  )

cloned$annotation = annotations[as.character(cloned$seurat_clusters)]
writeChangeoDb(cloned, file="intermediates/cloned_annotated.tsv")

# Make file of all sequences with heavy and light chains
all = readChangeoDb("intermediates/all_cloned_data.tsv")
all = filter(all, !is.na(c_call))
all = filter(all, !is.na(seurat_clusters))
all$c_call[is.na(all$c_call)] = "Unkn"
all$seurat_clusters[is.na(all$seurat_clusters)] = "Unkn"
all$major_c = substr(all$c_call,1,4)
all = filter(all, !is.na(germline_alignment_d_mask))
all$annotation = annotations[as.character(all$seurat_clusters)]

# make ordered table of clone sizes
clonesize = sort(table(filter(all,locus=="IGH")$clone_id), decreasing=TRUE)
clonesize = clonesize[!grepl("NA",names(clonesize))]

clonerank = 1:length(clonesize)
names(clonerank) = names(clonesize)

all$clonerank = clonerank[all$clone_id]
all$clonesize = clonesize[all$clone_id]

# make table of all clones organized by size
allcells = all %>%
  filter(!grepl("NA", clone_id)) %>%
  arrange(clonerank, cell_id, annotation, c_call) %>%
  select(clone_id, clonesize, sample_cell_id, locus, sequence_id, sample, annotation, 
    seurat_clusters, c_call, sequence_alignment)

write.csv(allcells, file="results/allcells.csv", row.names=FALSE)
