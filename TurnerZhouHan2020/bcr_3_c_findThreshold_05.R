#### argument(s) from command line ####
# for this script, by design, expect $NPROC
ARGS = commandArgs(trailingOnly=T)

if (length(ARGS)==0) {
    warning("No nproc supplied from command line. Setting nproc to 1.")
    num.cores.foreach = 1
} else {
    num.cores.foreach = ARGS[1]
}
cat("nproc:", num.cores.foreach, "\n")

#*
VERSION = "01092020"

pathRoot = paste0("/home/qz93/project/ellebedy_10x_P05/bcr/", VERSION, "/")
pathData = paste0(pathRoot, "changeo/distToNearest/")
pathWork = paste0(pathRoot, "changeo/distToNearest/")

library(alakazam) # v0.2.11
library(shazam)   # v0.1.10/11
library(doMC)
library(foreach)

registerDoMC(num.cores.foreach)

#### meta setup ####

SUBJECTS = c("P05") #N

#### run config ####

#* subsampling? (default: NULL)
#SUBSAMPLE = 15000 
SUBSAMPLE = NULL

#* method to use
METHOD = "density"
#METHOD = "gmm"

# findThreshold density parameters
EDGE = 0.9 # default: 0.9

# findThreshold gmm parameter
GMM.MODEL = "gamma-gamma"

# record config
setwd(pathWork)
# eg: 20181028_194125
timeStamp = gsub(pattern="[:-]", replacement="",
                 x=paste0(strsplit(as.character(Sys.time()), " ")[[1]], collapse="_"))
sink(paste0("calcThresh_config_", timeStamp, ".txt"))
date()

cat("**************", "\n")
if (is.null(SUBSAMPLE)) {
    cat("SUBSAMPLE: NULL", "\n")
} else {
    cat("SUBSAMPLE:", SUBSAMPLE, "\n")
}

cat("**************", "\n")
cat("METHOD:", METHOD, "\n")

if (METHOD=="density") {
    cat("- EDGE:", EDGE, "\n")
} else if (METHOD=="gmm") {
    cat("- GMM MODLE:", GMM.MODEL)
}

cat("**************", "\n")
sessionInfo()
sink()

#### find threshold ####

saveNamePart = ifelse(is.null(SUBSAMPLE), "full", paste0("subsample_", SUBSAMPLE))

thresh.list = foreach(subj=SUBJECTS) %dopar% {
    
    cat("\nsubject = ", subj, "\n")
    load(paste0("distToNearest_", subj, ".RData")) # db
    
    ### subsample
    # if applicable, do this outside call to `findThreshold`
    # so as to be able to record indices of sequences subsampled (for reproducbility)
    if (!is.null(SUBSAMPLE)) {
        if (SUBSAMPLE < nrow(db)) {
            cat("subsampling... size=", SUBSAMPLE, "\n")
            subsampleIdx = base::sample(x=nrow(db), size=SUBSAMPLE, replace=F)
            db = db[subsampleIdx, ]
        } else {
            cat("no need to subsample \n")
            subsampleIdx = NA
        }
    } else {
        subsampleIdx = NA
    }
    
    cat("finding thresh \n")
    
    # equivalent subsampling, if applicable, has already been performed earlier
    # no need to have findThreshold perform it again (hence `NULL` to `subsample`)
    
    if (METHOD=="density") {
    
        threshObj = findThreshold(distances=db$DIST_NEAREST, method=METHOD, 
                                  edge=EDGE, 
                                  subsample=NULL, progress=T)
        
    } else if (METHOD=="gmm") {
        
        #! from Nima:
        # important to visually inspect for bi-modality first before running findTreshold
        # low seq count (say, <1000) and lack of bi-modality => won't work (stuck)
        
        # withTimeout does not appear to work with findThreshold
        threshObj = findThreshold(distances=db$DIST_NEAREST, method=METHOD, 
                                  model=GMM.MODEL, cutoff="optimal",
                                  subsample=NULL, progress=T)
        
    }
    
    cat("finished", subj, "\n")
    
    # save for individual first in case any other individual failed
    # (since it can take a long time to run)
    setwd(pathWork)
    save(threshObj, subsampleIdx, 
         file=paste0("thresh_", subj, "_", METHOD, "_", saveNamePart, ".RData"))
    
    return(list(threshObj=threshObj, subsampleIdx=subsampleIdx))
    
}

names(thresh.list) = SUBJECTS

# save as a single object for all individuals
setwd(pathWork)
save(thresh.list, 
     file=paste0("thresh_all_", METHOD, "_", saveNamePart, ".RData"))

#### output thresholds ####

# save as a plain text file
# to be used by shell script for changeo threshold (if using a different threshold
# for each subject)
# the format is fixed -- DO NOT CHANGE

thresh.list = thresh.list[!unlist(lapply(thresh.list, is.null))]
thresh.vec = unlist(lapply(thresh.list, function(obj){ obj[["threshObj"]]@threshold }))

sink(paste0("thresh_all_", METHOD, "_", saveNamePart, ".txt"))
for (i in 1:length(thresh.vec)) {
    # sep="" essential to get rid of space that follows
    cat(names(thresh.vec)[i], sep="", "\n")
    cat(thresh.vec[i], sep="", "\n")
}
sink()

