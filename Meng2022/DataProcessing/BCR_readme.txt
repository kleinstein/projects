## Processing Year 1 and Year 2 BCR data
0. Both bulk and 10x BCR data processing followed standard pipeline.
1. Deal with mAb sequences: break "mAb_fromAliLab.xlsx" (P04 and P05) and "Subject11_P11.xlsx" (P11) to three .txt file in folder txt for each subject
2. run toFasta.R to convert each txt file to two fasta files, one for mAb and hte other for non-mAb
3. using the same version singularity image to run igblast and makeDB for each subject:/gpfs/gibbs/pi/kleinstein/Ali/y2/mAb
    sbatch igblastSingle.sh P04
    results file saved to changeO dir
4. using combinemAb.ipynb to combine heavy chain results with previous year 1 changeO file: 
    a. y1 Bulk
    b. y1 Single cell
    c. y1 mAb
    d. y1 Nested
    e. y2 Single cell
    f. y2 mAb and non-mAb
    g. y2 bulk PBMC_ASC (need to process bulk data first)
    the combined results will be saved to combinedChangeO fodler
    
    remove seq with JUNCTION_Length==0
    
    And then go to farnam /gpfs/gibbs/pi/kleinstein/Ali/y2/BCR_y1y2

5.  generate Germline 
6.  call genotype  
7.  assign  clone using previous cutoff 0.1 and process results to dir data
8. Run assignClone/passData to add mutation Freq and AA seq.
9. Run assignClone/AnnotateAg.ipynb to add following antigen specific information to the data file:
    seperate for P11 since P11 don't have FNAs - AnnotateAg_P11.ipynb
# a) y1 mAb 
# b) y1 mAb from non-day0 GCs (from Bassem)
# c) y2 mAb from PBMC_ASC (contains both Ag-binding and non-Ag-binding)
# d) y2 mAb from non-day0 GCs (contains both Ag-binding and non-Ag-binding)
    (find antigen specific clones cloned with 1) y1 and y2 mAb 2) y1 late GC acs clones verified by Bassem
    
# New columns in the data file: 
# 1. is.AgSeq: 
#    "Yes" for verified Ag-binding; 
#    "No" for verified non-Ag-binding; 
#     NA for all others
# 2. is.AgClone: Antigen Specific Clones; 
#    "Yes" for cloned with verified Ag-binding seq; 
#     "No" for cloned wiht verified non-Ag-binding seq; 
#     NA for all others

10. For P11, run data/RemoveSingleCellDup.R to remove duplicate cells that have same barcode and seqeunces between two samples
    For P04 and P05, run data_v2/BCR_Process_P0x.ipynb to deal wiht duplicated cells.

11. run output_BCR_P0x.ipynb to 
    a) correct two sample names
    b) remvoe cells have multiple heavy chains
    c) put cell annotation to BCR data.
    
  