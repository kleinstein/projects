#!/bin/bash
#SBATCH --mail-type=ALL
#SBATCH --mail-user=julian.zhou@yale.edu
#SBATCH -t 5:00:00
#SBATCH --constraint E5-2660_v3
#SBATCH -c 10
#SBATCH --mem-per-cpu=12G

# note: adjust number of cores in .R according to -c 
# detect.cores() doeesn't seem to work on farnam
# (for this script, make this match the number of subjects)

# 10c x 12G
# Job Wall-clock time: 00:17:16
# Memory Utilized: 2.71 GB

# nx360h (E5-2660_v3) has a total max memory of 121GB per node

# IN:
# - tigger/[subj]_IGHV_FUNCTIONAL-T_/F_genotyped.tsv
# OUT:
# - if CREATE_GERM==true, slide/[subj]_IGHV_FUNCTIONAL-T_genotyped_preClonal_germ-pass.tsv
# - if CREATE_GERM==true, slide/[subj]_IGHV_FUNCTIONAL-T_genotyped_preClonal_germ-pass_parse-rename.tab
# - slide/[subj]_IGHV_FUNCTIONAL-T/F_genotyped_slideTune_mutThresh_[]_windowSize_[].RData
# - slide/[subj]_IGHV_FUNCTIONAL-T/F_genotyped_slideFiltered.RData/.tsv

# the change-o commans in this file are intended to work with 0.4.5

SCRIPT_PATH="/ysm-gpfs/home/qz93/projects/scripts/spatial/"
SCRIPT_NAME="rubelt_3b_slideFilter.R"
LOG="/ysm-gpfs/home/qz93/scratch60/spatial_data/davis_twins/slide/slideNoTuneDirectFilter.log"

PATH_ROOT="/ysm-gpfs/home/qz93/scratch60/spatial_data/davis_twins/"
PATH_TIGGER="${PATH_ROOT}tigger/"
PATH_SLIDE="${PATH_ROOT}slide/"

FN_IMGT_DJ="/ysm-gpfs/home/qz93/germlines/tag_201918-4_9May2019/human/vdj/imgt_human_IGH[DJ].fasta"
#FN_IMGT_V_NOVEL="/ysm-gpfs/home/qz93/germlines/tag_2018_03_05/imgt_human_IGHV_novel_twins_20180517.fasta" #*
FN_IMGT_V_NOVEL="/ysm-gpfs/home/qz93/germlines/tag_201918-4_9May2019/human/vdj/imgt_human_IGHV.fasta" #*
GERM_TAG="tag_201918-4_9May2019"

# start log file
dt=$(date '+%d/%m/%Y %H:%M:%S')
echo "$dt" &> "${LOG}"
echo "germline" $GERM_TAG &>> "${LOG}"
echo "bash" $BASH_VERSION &>> "${LOG}"
ParseDb.py --version &>> "${LOG}"
python3 -V &>> "${LOG}"

# whether to create germline 
# this can only be skipped if germline has been created by calling slideTune.sh
# (when using noTuneDirectFilter to skip tuning if the latter is taking too long)
CREATE_GERM=true #*

if ${CREATE_GERM}; then

	SUBJS=(TW01A TW01B TW02A TW02B TW03A TW03B TW04A TW04B TW05A TW05B)

	for SUBJ in ${SUBJS[@]}; do

		echo "############## CreateGermlines for " ${SUBJ} &>> "${LOG}"

		# create germline so that slideTune.R can use it to count mutation
		# output filename: [outname]_germ-pass.tab, _germ-fail.tab
		# do not use --clone flag (this is pre-clonal clustering)
		# create column GERMLINE_IMGT_D_MASK

		# change-o 0.4.5 takes --df and --jf 
		# default values are:
		# --df D_CALL \
		# --jf J_CALL \

		#*
		if [[ ${SUBJ} == "TW01A" || ${SUBJ} == "TW01B" || ${SUBJ} == "TW02A" || ${SUBJ} == "TW02B" || ${SUBJ} == "TW03A" || ${SUBJ} == "TW03B" || ${SUBJ} == "TW04A" || ${SUBJ} == "TW04B" || ${SUBJ} == "TW05A" || ${SUBJ} == "TW05B" ]]; then
			PATH_TSV_T="${PATH_TIGGER}corrected_${SUBJ}_IGHV_FUNCTIONAL-T_genotyped.tsv"
			PATH_TSV_F="${PATH_TIGGER}corrected_${SUBJ}_IGHV_FUNCTIONAL-F_genotyped.tsv"
		else
			PATH_TSV_T="${PATH_TIGGER}${SUBJ}_IGHV_FUNCTIONAL-T_genotyped.tsv"
			PATH_TSV_F="${PATH_TIGGER}${SUBJ}_IGHV_FUNCTIONAL-F_genotyped.tsv"
		fi

		#echo "${PATH_TSV_T}" &>> "${LOG}"
		#echo "${PATH_TSV_F}" &>> "${LOG}"

		if [[ (-s "${PATH_TSV_T}" ) ]]; then
			CreateGermlines.py \
				-d "${PATH_TSV_T}" \
				-r ${FN_IMGT_V_NOVEL} ${FN_IMGT_DJ} \
				-g dmask \
				--vf V_CALL_GENOTYPED \
				--df D_CALL \
				--jf J_CALL \
				--sf SEQUENCE_IMGT \
				--failed \
				--log "${SUBJ}_IGHV_FUNCTIONAL-T_genotyped_preClonal.log" \
				--outname "${SUBJ}_IGHV_FUNCTIONAL-T_genotyped_preClonal" \
				--outdir "${PATH_SLIDE}" \
				&>> "${LOG}"

			# output filename: [outname]_parse-rename.tab
			ParseDb.py rename \
				-d "${PATH_SLIDE}${SUBJ}_IGHV_FUNCTIONAL-T_genotyped_preClonal_germ-pass.tab" \
				-f GERMLINE_IMGT_D_MASK \
				-k PRECLONAL_GERMLINE_IMGT_D_MASK \
				--outname "${SUBJ}_IGHV_FUNCTIONAL-T_genotyped_preClonal_germ-pass" \
				--outdir "${PATH_SLIDE}" \
				&>> "${LOG}"
		fi


		if [[ (-s "${PATH_TSV_F}" ) ]]; then
			CreateGermlines.py \
				-d "${PATH_TSV_F}" \
				-r ${FN_IMGT_V_NOVEL} ${FN_IMGT_DJ} \
				-g dmask \
				--vf V_CALL_GENOTYPED \
				--df D_CALL \
				--jf J_CALL \
				--sf SEQUENCE_IMGT \
				--failed \
				--log "${SUBJ}_IGHV_FUNCTIONAL-F_genotyped_preClonal.log" \
				--outname "${SUBJ}_IGHV_FUNCTIONAL-F_genotyped_preClonal" \
				--outdir "${PATH_SLIDE}" \
				&>> "${LOG}"

			ParseDb.py rename \
				-d "${PATH_SLIDE}${SUBJ}_IGHV_FUNCTIONAL-F_genotyped_preClonal_germ-pass.tab" \
				-f GERMLINE_IMGT_D_MASK \
				-k PRECLONAL_GERMLINE_IMGT_D_MASK \
				--outname "${SUBJ}_IGHV_FUNCTIONAL-F_genotyped_preClonal_germ-pass" \
				--outdir "${PATH_SLIDE}" \
				&>> "${LOG}"
		fi

	done
	
fi

echo "############## rubelt_3b_slideFilter.R" &>> "${LOG}"

Rscript "${SCRIPT_PATH}${SCRIPT_NAME}" &>> "${LOG}"

