# Set up custom BALB/c reference

module load R/4.1.0-foss-2020b

# Download and extract IgBLAST
VERSION="1.18.0"
wget ftp://ftp.ncbi.nih.gov/blast/executables/igblast/release/${VERSION}/ncbi-igblast-${VERSION}-x64-linux.tar.gz
tar -zxf ncbi-igblast-${VERSION}-x64-linux.tar.gz

# Download reference databases and setup IGDATA directory
fetch_igblastdb.sh -o share/igblast
cp -r ncbi-igblast-${VERSION}/internal_data share/igblast
cp -r ncbi-igblast-${VERSION}/optional_file share/igblast

# Build IgBLAST database from IMGT reference sequences
fetch_imgtdb.sh -o share/germlines/imgt
imgt2igblast.sh -i share/germlines/imgt -o share/igblast

# copy over mouse D and J segments
mkdir share/germlines/imgt/balbc
mkdir share/germlines/imgt/balbc/vdj

cp share/germlines/imgt/mouse/vdj/imgt_mouse_IGHD.fasta share/germlines/imgt/balbc/vdj/imgt_balbc_IGHD.fasta
cp share/germlines/imgt/mouse/vdj/imgt_mouse_IGHJ.fasta share/germlines/imgt/balbc/vdj/imgt_balbc_IGHJ.fasta

# copy over new BALBc gene segments
cp ogredb/Mouse_BALB_c_IGH_rev_1_gapped_noimgt.fasta share/germlines/imgt/balbc/vdj/imgt_balbc_IGHV.fasta

# copy over mouse aux file (corresponds to J segments, which are mouse)
cp share/igblast/optional_file/mouse_gl.aux share/igblast/optional_file/balbc_gl.aux

#set up internal data dir
mkdir share/igblast/internal_data/balbc

# set up igblast databases
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in ogredb/Mouse_BALB_c_IGH_rev_1_ungapped_noimgt.fasta -out share/igblast/database/imgt_balbc_ig_v
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in share/igblast/fasta/imgt_mouse_ig_j.fasta -out share/igblast/database/imgt_balbc_ig_j
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in share/igblast/fasta/imgt_mouse_ig_d.fasta -out share/igblast/database/imgt_balbc_ig_d
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in ogredb/Mouse_BALB_c_IGH_rev_1_ungapped_noimgt.fasta -out share/igblast/internal_data/balbc/balbc_V

# create internal_data file
Rscript makeBlastFileBALBc.R ogredb/Mouse_BALB_c_IGH_rev_1_gapped_noimgt.fasta share/igblast/internal_data/balbc/balbc.ndm.imgt

