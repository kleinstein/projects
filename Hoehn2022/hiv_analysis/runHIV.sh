#!/usr/bin/bash
#SBATCH --mem-per-cpu=5gb
#SBATCH --partition=pi_kleinstein
#SBATCH --ntasks=1 --nodes=1
#SBATCH --time=100:00:00
#SBATCH --mail-type=FAIL,END
#SBATCH --mail-user=kenneth.hoehn@gmail.com

module load R

id=$1
minseq=$2
bootstraps=$3
nproc=$4

Rscript getSwitches_dowser_all.R $id $minseq $bootstraps $nproc
Rscript getSwitches_dowser_gcmemhi.R $id $minseq $bootstraps $nproc

