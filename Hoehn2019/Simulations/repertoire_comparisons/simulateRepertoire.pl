

use strict;
use warnings;
use Math::Random qw(random_gamma);
use hotSpotUtils;
use Parallel::ForkManager;

my $reps = $ARGV[0];
my $pm = new Parallel::ForkManager($ARGV[1]);
my $rep = $ARGV[2];
my $ocdr = $ARGV[3];
my $outdir = $ARGV[4];
my $naive = $ARGV[5];
my $outstem = $ARGV[6];
my $partition = $ARGV[7];
my $startrep=$ARGV[8];
my $shape = $ARGV[9];

my $rate = 0;
my $ratefwr = 0;

if($shape ne "N"){
	$rate = $shape/$ocdr;
	$ratefwr = $shape/0.5;
}

system("mkdir $outdir/sims_$shape");

print "STARTREP $startrep\n";
open(IN,$rep) or die();
my $h = <IN>;
my @IN = <IN>;

for(my $rep=$startrep;$rep<$reps;$rep++){
	print "$rep\n";
	my @ocdrs;
	my @ofwrs;
	if($shape ne "N"){
		@ocdrs = random_gamma(scalar(@IN),$rate,$shape);
		@ofwrs = random_gamma(scalar(@IN),$ratefwr,$shape);
	}else{
		for(my $i=0;$i<scalar(@IN);$i++){
			push(@ocdrs,$ocdr);
			push(@ofwrs,0.5);
		}
	}
	for(my $i=0;$i<scalar(@IN);$i++){
		$pm->start and next;
		my $line = $IN[$i];
		my $ocdr = $ocdrs[$i];
		my $ofwr = $ofwrs[$i];
		my @in = split("\t",$line);
		my @ids = split("\/",$in[0]);
		my $id = $ids[-1];
		print "\n$id\t$ocdr\n";
		my $srand = int(time()*$i/10);
		#my $command = "simulateFull.pl sim_s5f.config -tree $in[1] -nsim 1 -stem $id\_$rep -outdir $outdir/sims_s5f -naivefile $naive -omegas 0.4,$ocdr -srand $srand";
		my $command = "simulateFull.pl extra/sim_FCH.config -tree $in[1] -nsim 1 -stem $id\_$rep -outdir $outdir/sims_$shape -omegas $ofwr,$ocdr -naivefile $naive -srand $srand -rootid NAIVE -seqfile $in[0] -partfile $in[3]";
		print "$command\n";
		#die("$command");
		system($command) == 0 or die($command);
		$pm->finish;
	}
	$pm->wait_all_children;
	
	open(OUT,">$outdir/$outstem\_lineages_$rep.tsv") or die();
	open(OUT2,">$outdir/$outstem\_omegas_$rep.tsv") or die();
	print OUT $h;
	my $counter = 0;
	foreach(@IN){
		my $line=$_;
		my @in=split("\t",$line);
		my @ids=split("\/",$in[0]);
		my $id = $ids[-1];
		print OUT "$outdir/sims_$shape/$id\_$rep\_0.fa\tN\t$in[2]\t$outdir/sims_$shape/$id\_$rep\_0.part.txt\n";
		print OUT2 "$outdir/sims_$shape/$id\_$rep\_0.fa\t$ofwrs[$counter]\t$ocdrs[$counter]\n";
		$counter++;
	}
	#system("sbatch --partition=$partition --cpus-per-task=4 --job-name=$outdir runSim.sh $outdir/$outstem\_lineages_$rep $outdir/$outstem\_ncdr_lineages_$rep 4 $outdir $rep") == 0 or die();
}


