#!/bin/bash 

DATA_DIR="/media/nimanouri/Seagate_Backup/office/Work/Yale/Projects/covid19/data_analyzed"
pattern="${DATA_DIR}/*_BCR_VHT_cellranger_combined"
FOLDERS=( $pattern )
numSamples=${#FOLDERS[@]}
echo "FOLDERS: $numSamples"

# Changeo version report
CHANGEO_VERSION=$(python3 -c "import changeo; print('%s-%s' % (changeo.__version__, changeo.__date__))")

# DefineClones run parameters
DC_MODE="gene"
DC_ACT="set"
MODEL="ham"
DIST=0.15
NPROC=1

# Set format options
FORMAT="airr"

# Create germlines parameters
CG_GERM="full dmask"

# Set germline ref directory
REFDIR="/home/nimanouri/Work/Yale/B_Cell_Repertoires/germlines/IMGT/human/vdj/imgt_human_IG[HKL][VDJ].fasta"

# Check for errors
check_error() {
    if [ -s $ERROR_LOG ]; then
        echo -e "ERROR:"
        cat $ERROR_LOG | sed 's/^/    /'
        exit 1
    fi
}

for((i=0; i<$numSamples;i++));do
	FOLDER=$(basename ${FOLDERS[$i]})
	OUTNAME="$(cut -d'_' -f1 <<<"$FOLDER")"
	echo -e "\nSTART"
	echo "Processing: ${FOLDER} ${OUTNAME}"
	INDIR=$DATA_DIR/$FOLDER

	# Define log files
	LOGDIR=${INDIR}/"logs"
        # remove existing director
	if [ -d "$LOGDIR" ]; then rm -Rf $LOGDIR; fi
	PIPELINE_LOG="${LOGDIR}/pipeline-10x.log"
	ERROR_LOG="${LOGDIR}/pipeline-10x.err"
	mkdir -p ${LOGDIR}
	echo '' > $PIPELINE_LOG
	echo '' > $ERROR_LOG

	# Start
	echo -e "IDENTIFIER: ${OUTNAME}"
	echo -e "CHANGEO VERSION: ${CHANGEO_VERSION}"
	STEP=0

	HEAVY_PROD="${INDIR}/${OUTNAME}_heavy_productive-T.tsv"
	LIGHT_PROD="${INDIR}/${OUTNAME}_light_productive-T.tsv"

	printf "  %2d: %-*s $(date +'%H:%M %D')\n" $((++STEP)) 30 "DefineClones"
	DefineClones.py -d ${HEAVY_PROD} --model ${MODEL} \
		--dist ${DIST} --mode ${DC_MODE} --act ${DC_ACT} --nproc ${NPROC} \
		--outname "${OUTNAME}_heavy" \
		--log "${LOGDIR}/clone.log" --format ${FORMAT} \
		>> $PIPELINE_LOG 2> $ERROR_LOG
	CLONE_FILE="${INDIR}/${OUTNAME}_heavy_clone-pass.tsv"
	check_error

	printf "  %2d: %-*s $(date +'%H:%M %D')\n" $((++STEP)) 30 "VL clone correction"
	light_cluster.py -d ${CLONE_FILE} -e ${LIGHT_PROD} \
		-o "${INDIR}/${OUTNAME}_heavy_clone-light.tsv" --format ${FORMAT} --doublets count \
		> /dev/null 2> $ERROR_LOG
        CLONE_FILE="${INDIR}/${OUTNAME}_heavy_clone-light.tsv"

	printf "  %2d: %-*s $(date +'%H:%M %D')\n" $((++STEP)) 30 "CreateGermlines"
	CreateGermlines.py -d ${CLONE_FILE} --cloned -r ${REFDIR} -g ${CG_GERM} \
		--outname "${OUTNAME}_H" --log "${LOGDIR}/germline.log" --format ${FORMAT} \
	        >> $PIPELINE_LOG 2> $ERROR_LOG
	check_error

	printf "  %2d: %-*s $(date +'%H:%M %D')\n" $((++STEP)) 30 "CreateGermlines"
	CreateGermlines.py -d ${LIGHT_PROD} -r ${REFDIR} -g ${CG_GERM} \
		--outname "${OUTNAME}_L" --log "${LOGDIR}/germline.log" --format ${FORMAT} \
	        >> $PIPELINE_LOG 2> $ERROR_LOG
	check_error
done
