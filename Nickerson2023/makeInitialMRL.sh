# Kenneth B. Hoehn
# 7/22/2022
# Set up custom MRL reference
# Find unaccounted-for novel V gene alleles

in="MRL_IGHV"
export IGDATA=share/igblast 
threads=7
VERSION="1.18.0"

echo "Downloading IgBlast and germlines"

# Download and extract IgBLAST
wget ftp://ftp.ncbi.nih.gov/blast/executables/igblast/release/${VERSION}/ncbi-igblast-${VERSION}-x64-linux.tar.gz
tar -zxf ncbi-igblast-${VERSION}-x64-linux.tar.gz

# Download reference databases and setup IGDATA directory
fetch_igblastdb.sh -o share/igblast
cp -r ncbi-igblast-${VERSION}/internal_data share/igblast
cp -r ncbi-igblast-${VERSION}/optional_file share/igblast

# Build IgBLAST database from IMGT reference sequences
fetch_imgtdb.sh -o share/germlines/imgt
imgt2igblast.sh -i share/germlines/imgt -o share/igblast

# Align MRL V genes to IMGT to get gapped sequences\
echo "Aligning MRL to IMGT"

# align mrl segments to IMGT database
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in share/igblast/fasta/imgt_mouse_ig_v.fasta -out database/imgt_mouse_ig_V
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in share/igblast/fasta/imgt_mouse_ig_d.fasta -out database/imgt_mouse_ig_D
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in share/igblast/fasta/imgt_mouse_ig_j.fasta -out database/imgt_mouse_ig_J

ncbi-igblast-1.18.0/bin/igblastn -query raw_naive/${in}.fasta -out raw_naive/${in}_igblast.fmt7 \
	-organism mouse -ig_seqtype Ig \
	-auxiliary_data share/igblast/optional_file/mouse_gl.aux \
	-germline_db_V share/igblast/database/imgt_mouse_ig_v \
	-germline_db_D share/igblast/database/imgt_mouse_ig_d \
	-germline_db_J share/igblast/database/imgt_mouse_ig_j \
	-domain_system imgt -outfmt '7 std qseq sseq btop'

MakeDb.py igblast -i raw_naive/${in}_igblast.fmt7 -s raw_naive/${in}.fasta \
  -r share/germlines/imgt/mouse/vdj/imgt_mouse_IGHV.fasta \
  share/germlines/imgt/mouse/vdj/imgt_mouse_IGHD.fasta \
  share/germlines/imgt/mouse/vdj/imgt_mouse_IGHJ.fasta \
  --extended --partial

# create fasta file of gapped MRL sequences
Rscript formatGermline.R ${in} > other/formatGermline.log

# check MRL vs IMGT gapped alignments
echo "Check IMGT alignments"
seaview raw_naive/${in}_gapped_gl.fa

# copy over mouse D and J segments
mkdir share/germlines/imgt/mrl-initial
mkdir share/germlines/imgt/mrl-initial/vdj

cp share/germlines/imgt/mouse/vdj/imgt_mouse_IGHD.fasta share/germlines/imgt/mrl-initial/vdj/imgt_mrl-initial_IGHD.fasta
cp share/germlines/imgt/mouse/vdj/imgt_mouse_IGHJ.fasta share/germlines/imgt/mrl-initial/vdj/imgt_mrl-initial_IGHJ.fasta

# copy over gapped MRL V gene segments
cp raw_naive/${in}_gapped.fasta share/germlines/imgt/mrl-initial/vdj/imgt_mrl-initial_IGHV.fasta

# copy over mouse aux file (corresponds to J segments, which are mouse)
cp share/igblast/optional_file/mouse_gl.aux share/igblast/optional_file/mrl-initial_gl.aux

#set up internal data dir
mkdir share/igblast/internal_data/mrl-initial

echo "Setting up IgBlast files"
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in raw_naive/${in}.fasta -out share/igblast/database/imgt_mrl-initial_ig_v
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in share/igblast/fasta/imgt_mouse_ig_j.fasta -out share/igblast/database/imgt_mrl-initial_ig_j
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in share/igblast/fasta/imgt_mouse_ig_d.fasta -out share/igblast/database/imgt_mrl-initial_ig_d
ncbi-igblast-1.18.0/bin/makeblastdb -parse_seqids -dbtype nucl -in raw_naive/${in}.fasta -out share/igblast/internal_data/mrl-initial/mrl-initial_V

# create internal_data file
Rscript makeInternalBlastFile.R raw_naive/${in}_gapped.fasta share/igblast/internal_data/mrl-initial/mrl-initial.ndm.imgt > other/makeInternalBlastFile.txt

echo "Aligning naive MRL sequences to MRL reference, finding novel alleles"
# mrl-initial germline against naive mouse data
ncbi-igblast-1.18.0/bin/igblastn \
    -germline_db_V share/igblast/database/imgt_mrl-initial_ig_v\
    -germline_db_D share/igblast/database/imgt_mrl-initial_ig_d \
    -germline_db_J share/igblast/database/imgt_mrl-initial_ig_j \
    -domain_system imgt -ig_seqtype Ig -organism mrl-initial \
    -auxiliary_data share/igblast/optional_file/mouse_gl.aux \
    -outfmt '7 std qseq sseq btop' \
    -query raw_naive/raw/filtered_contig.fasta \
    -out raw_naive/processed/filtered_contig_mrl-initial\.fmt7 -num_threads $threads

MakeDb.py igblast -i raw_naive/processed/filtered_contig_mrl-initial\.fmt7 \
    -s raw_naive/raw/filtered_contig.fasta \
    -r \
    share/germlines/imgt/mrl-initial/vdj/imgt_mrl-initial_IGHV.fasta \
    share/germlines/imgt/mrl-initial/vdj/imgt_mrl-initial_IGHD.fasta \
    share/germlines/imgt/mrl-initial/vdj/imgt_mrl-initial_IGHJ.fasta \
    --extended --fail --asis-calls --outdir raw_naive/processed

# merge annotations to data
python3 merge10x.py raw_naive/processed/filtered_contig_mrl-initial_db-pass.tsv \
    raw_naive/raw/filtered_contig_annotations_BCR_forKen.csv raw_naive/processed/mrl_db-pass.tsv

# find novel alleles
Rscript findVariants.R > other/findVariants.log
